# Felyx processor

## Documentation

<https://felyx.gitlab-pages.ifremer.fr/felyx_docs/>

## Installation

### installing with pip

If installing with *pip*, some C++ software and libraries must be installed first. They are required by `pyinterp` dependency, as documented here: https://pangeo-pyinterp.readthedocs.io/en/latest/setup/build.html#requirements

On Linux, simply type the following command:

```bash
sudo apt-get install g++ cmake libeigen3-dev libboost-dev
```bash


### Conda environment

```bash
conda create -n felyx_processor --file https://gitlab.ifremer.fr/felyx/felyx_processor/-/raw/master/assets/conda/felyx-dev-linux-64.lock
conda run -n felyx_processor pip install --extra-index-url https://gitlab.ifremer.fr/api/v4/projects/1829/packages/pypi/simple felyx_processor
```

### Docker

```bash
docker pull gitlab-registry.ifremer.fr/felyx/felyx_processor
```


## Development

### Install environment

- Install conda env

```bash
conda create -n felyx_processor --file https://gitlab.ifremer.fr/felyx/felyx_processor/-/raw/master/assets/conda/felyx-dev-linux-64.lock
```

- Install all dependencies(runtime and dev)

```bash
poetry install -vv
```

### Pre-commit

- Register pre-commit

```bash
pre-commit install
```

- Run the hooks manually

```bash
pre-commit run --all-files
```

### Check code quality

```bash
flake8 .
```

### Run unit tests

```bash
# start elasticsearch server
sudo docker run -d \
  -p 127.0.0.1:9200:9200 \
  -p 127.0.0.1:9300:9300 \
  -e "discovery.type=single-node" \
  --name elasticsearch \
  docker.elastic.co/elasticsearch/elasticsearch:7.16.2

# run unittests
pytest -o log_cli=true --log-cli-level=ERROR -ra -q tests/test_configuration.py
```

## Launch application

TODO
