# encoding: utf-8
"""
felyx_processor.query.handler
------------------------

This module contains code required to enable queries to be performed on the
felyx system. It handles requests passed from felyx_server, and shares the
workload as required.

.. :copyright: Copyright 2013 Pelamis Scientific Software Ltd.
.. :license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: David Poulter <david.poulter@pelamis.co.uk>
.. codeauthor:: David Poulter <david.poulter@pelamis.co.uk>
"""
import logging
import os
from urllib.parse import urljoin

LOGGER = logging.getLogger('felyx.processor')
# TODO TO DELETE
# EXTERNAL_INSTANCES = get_felyx_config().get('external_instances', None)
# CONFIG = get_felyx_config()
MINIPROD_ACCESS = 'extraction/miniprod/'


class MultipleDataTypeError(ValueError):
    """An exception class for when more than one datatype is present in a
    selection query."""


def add_urls(values):
    """
    Add URLs to a query result, allowing direct access to the files
    produced. This will only effect the 'miniprod' entry of any
    plugin result, and should be ignored by other plugins that are
    called later in a processing sequence.

    :param values: The result from a query.
    :return: The result with URLs added
    """
    if isinstance(values, dict):
        if 'miniprods' not in values:
            for element in values.values():
                add_urls(element)
        else:
            values['urls'] = [
                get_url(_) for _ in values['miniprods']
            ]
    elif isinstance(values, list):
        for element in values:
            add_urls(element)
    else:
        pass
    return values


def get_url(miniprod):
    if 'instance_url' not in CONFIG:
        LOGGER.warning(
            str(
                'Your configuration file ({}) does not include the local '
                'felyx instance. Without this formation, stored under the key '
                "'instance_url', it is not possible to construct URLs for "
                'miniprods or derived minprods. '
            ).format(os.environ['FELYX_CFG'])
        )
        return None

    server = CONFIG['instance_url']

    try:
        if not server.endswith('/'):
            server += '/'
        if ':' in miniprod:
            server_name, miniprod = miniprod.split(':')
            if server_name in EXTERNAL_INSTANCES:
                server = EXTERNAL_INSTANCES[server_name]
        root = urljoin(server, MINIPROD_ACCESS)

        if '/' in miniprod:
            if 'output_derived_miniprod_url' in CONFIG.keys():
                root = CONFIG['output_derived_miniprod_url']
            else:
                LOGGER.warning(
                    str(
                        'Your configuration file ({}) does not include the '
                        'local felyx instance. Without this formation, '
                        "stored under the key 'output_derived_miniprod_url', "
                        'it is not possible to construct URLs for miniprods '
                        'or derived minprods. '
                    ).format(os.environ['FELYX_CFG'])
                )
        result = urljoin(root, miniprod)
    except Exception:
        LOGGER.warning(
            'Failed to generate a URL for minprod {}'.format(miniprod)
        )
        return

    return result
