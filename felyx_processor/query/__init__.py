# encoding: utf-8
"""
felyx_processor.query
----------------

This module contains code required to enable queries to be performed on the
felyx system. It handles requests passed from felyx_server, and shares the
workload as required.

.. :copyright: Copyright 2013 Pelamis Scientific Software Ltd.
.. :license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: David Poulter <david.poulter@pelamis.co.uk>
.. codeauthor:: David Poulter <david.poulter@pelamis.co.uk>
"""
import json
import logging

LOGGER = logging.getLogger('felyx.processor')


# Define the possible felyx query steps.
EXTRACTION_STEPS = [
    # "extraction",
    'selection',
    'colocation',
    'remapping',
    'processing',
    'compositing',
    'transformation',
    'formatting',
    'packaging'
]

OPERATORS = {
    '<': 'lt',
    'lt': 'lt',
    '<=': 'lte',
    'lte': 'lte',
    '>': 'gt',
    'gt': 'gt',
    '>=': 'gte',
    'gte': 'gte',
    '==': '==',
    '!=': '!=',
    '<>': '!=',
    '=': '=='
}
# todo replace with config reader
#  CONFIG = get_felyx_config()
#  INSTANCE = CONFIG.get('external_instances', {})


def format_return(
        error, outfile=None, source='selection', level=None):
    """
    Return a JSON formatted error notification to be sent to the user.
    This function will both return the original message, and attempt to
    write the message to an outfile. It will also log the message to the
    appropriate level.

    Level will default to 'exception'.

    :param str source: The processing step reporting the error.
    :param str outfile: The file to write the user return to.
    :param instancemethod level: The logging level function to be used.
    :param str error: The string to be sent to the user.
    :return: The error message in human readable format.
    :rtype: str
    """
    # Set a default logging level.
    if level is None:
        level = LOGGER.exception

    message = json.dumps({'error': error, 'step_name': source})
    level(
        str(
            'A user made a request, which raised an error. This is the '
            'message that was sent to the user: \n{}.\n\n'
            'The traceback for the raised exception follows:\n'
        ).format(message)
    )
    if outfile:
        with open(outfile, 'w') as json_file:
            json_file.write(message)
        return message
    else:
        return message
