# encoding: utf-8
"""
felyx_processor.query.finders
------------------------

This module contains code required to find miniprods on both local and remote
instances of felyx.

.. :copyright: Copyright 2014 Pelamis Scientific Software Ltd.
.. :license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: David Poulter <david.poulter@pelamis.co.uk>
.. codeauthor:: David Poulter <david.poulter@pelamis.co.uk>
"""

import datetime
import logging
import os
import urllib.parse as urlparse

import requests

import felyx_processor.storage

LOGGER = logging.getLogger('felyx.processor')

# TODO TO DELETE
# CONFIG = get_felyx_config()
# INSTANCE = CONFIG.get('external_instances', {})


def _collect_remote_miniprod(miniprod_name, instance):
    """TODO TO DELETE
    Download a copy of a remote miniprod.

    :param str miniprod_name: The name of the miniprod for which to supply a
      local path.
    :param str instance: The name of a remote instance from which to download
     that miniprod
    :returns: The local path to that miniprod.
    :rtype: str
    """
    # Establish and / or create a workspace
    workspace = CONFIG['shared_workspace_root']
    path = os.path.join(workspace, 'remote_miniprods', instance)
    felyx_processor.storage.makedirs(path)

    # Establish name and return it if the miniprod already exists locally.
    full_path = os.path.join(path, miniprod_name)
    if os.path.exists(full_path):
        return full_path

    # Download the remote miniprod
    remote_url = urlparse.urljoin(INSTANCE[instance], 'extraction/miniprod/')
    miniprod_url = urlparse.urljoin(remote_url, miniprod_name)
    data = requests.get(miniprod_url, verify=False).content
    with open(full_path, 'wb') as outfile:
        outfile.write(data)

    return full_path


def _find_local_miniprod(root, miniprod_name, miniprod_date):
    """Establish if a miniprod exists, return its path if it does.

    :param str root: The root directory in which to start the search.
    :param str miniprod_name: The name of the miniprod file.
    :param str miniprod_date: The datetime.datetime object representing the
      start time of the miniprod.
    :return: The path to the miniprod.
    :rtype: str or NoneType
    """
    # Ensure that the testing location exists.
    if not os.path.exists(root):
        LOGGER.debug(
            str(
                'The search path {} does not exist, but it has been selected '
                'as a candidate in the search for miniprod {}. This might '
                'not be an error.'
            ).format(root, miniprod_name)
        )
        return None

    # Get list of datasets for this site, in the public repository.
    datasets = [_ for _ in os.listdir(root) if _ in miniprod_name]

    # If multiple datasets match, take the one with the shortest name.
    # This is so that 'data1' will match 'data1' over 'data1.1'.
    # Return None if no datasets match.
    if len(datasets):
        dataset = max(datasets, key=len)
    else:
        return None

    # Evaluate the path of the miniprod, from it's name.
    miniprod_date_stamp = datetime.datetime.strptime(
        miniprod_date, '%Y%m%d%H%M%S'
    ).strftime('%Y/%j')
    path = os.path.join(root, dataset, miniprod_date_stamp, miniprod_name)

    # If that path doesn't exist in the public section, search for it in the
    # private user sections.
    if os.path.exists(path):
        return path

    # If the path doesn't exist, return None.
    return None


def get_miniprod_root():
    """
    TODO TO MOVE TO To felyx_contrib BaseQueryPlugin or configuration
    Return the file system root for derived, miniprod based, files.

    :return: The root path.
    """
    root = CONFIG['shared_workspace_root']
    if 'output_derived_miniprod_dir' in CONFIG:
        root = CONFIG['output_derived_miniprod_dir']
    return root


def get_miniprod_path(miniprod_name, instance=None):
    """
    TODO TO MOVE To felyx_contrib BaseQueryPlugin
    Return the path on this instance of a miniprod. If instance is supplied,
    collect that miniprod from the remote instance and store it locally,
    returning the path to that new local copy.

    :param str miniprod_name: The name of the miniprod for which to supply a
      local path.
    :param str instance: The name of a remote instance from which to download
       that miniprod.
    :returns: The local path to that miniprod, or None.
    :rtype: str or NoneType
    """

    # Collect if miniprod is from a remote instance, and return path.
    # TODO TO DELETE
    # if instance:
    #     try:
    #         miniprod_path = _collect_remote_miniprod(miniprod_name, instance)
    #         LOGGER.debug(
    #             'Collected remote miniprod {} from {}'.format(
    #                 miniprod_name, instance
    #             )
    #         )
    #         return miniprod_path
    #     except requests.ConnectionError:
    #         LOGGER.exception(
    #             str(
    #                 'Failed to download remote miniprod '
    #                 '{} from remote instance {}'
    #             ).format(miniprod_name, instance)
    #         )
    #     LOGGER.warning(
    #         'Failed to collect remote miniprod {} from {}.'.format(
    #             miniprod_name, instance
    #         )
    #     )
    #     return None

    # If miniprod_name includes a path component, it has been produced by a
    # plugin, and should be in the shared workspace
    provided_path, provided_name = os.path.split(miniprod_name)
    if provided_path:
        root = get_miniprod_root()

        miniprod_path = os.path.join(
            root,
            provided_path,
            provided_name
        )
        if os.path.exists(miniprod_path):
            return miniprod_path
        else:
            LOGGER.warning(
                str(
                    'The manipulated miniprod name {} is not valid. This path '
                    'is one given by a plugin, for example a mapper, formatter '
                    'or a renderer.'
                ).format(
                    miniprod_path
                )
            )
        return None

    # Attempt to find miniprod locally.
    try:
        miniprod_date, miniprod_site = miniprod_name.split('_')[:2]
    except ValueError:
        LOGGER.error(
            'The miniprod name {} is not valid.'.format(miniprod_name)
        )
        return None

    store = CONFIG['default_felyx_miniprod_store']
    public = CONFIG['felyx_public_folder_name']
    root = os.path.join(store, public, miniprod_site)

    # Search for the miniprod
    path = _find_local_miniprod(root, miniprod_name, miniprod_date)
    if path:
        LOGGER.debug(
            'Found local public miniprod {}.'.format(miniprod_name)
        )
        return path
    else:
        # Get a list of the possible private owners.
        for owner in [_ for _ in os.listdir(store) if _ != public]:
            owner_path = os.path.join(store, owner)

            # Check path is valid
            if not os.path.exists(owner_path):
                continue
            elif not os.path.isdir(owner_path):
                continue

            for collection in os.listdir(owner_path):
                # Search in all the private locations, until the miniprod is
                # found.
                root = os.path.join(store, owner, collection, miniprod_site)

                # Do not search the expected path if it is not valid.
                if not os.path.exists(root):
                    continue

                path = _find_local_miniprod(root, miniprod_name, miniprod_date)
                if path:
                    LOGGER.debug(
                        'Found local private miniprod {}.'.format(miniprod_name)
                    )
                    return path

        LOGGER.warning(
            'Failed to find local miniprod {}.'.format(
                miniprod_name
            )
        )
        return None
