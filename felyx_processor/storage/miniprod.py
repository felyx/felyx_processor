# -*- coding: utf-8 -*-

import datetime
import logging
from pathlib import Path

import felyx_processor.storage

logger = logging.getLogger('felyx.processor')


class MissingMetadata(Exception):
    """"""
    def __init__(self, fields):
        """"""
        super(MissingMetadata, self).__init__()
        self.fields = fields


def write_netcdf():
    """"""


def write_manifest():
    """"""


def get_name(meta):
    """"""
    # Note: miniprod names generated with this method should be unique but in
    # some rare cases (two subsequent granules overlapping and having the same
    # timestamp due to time resolution) there can be a name conflict.

    # time_coverage_start
    dt_str = meta['time_coverage'][0].strftime('%Y%m%d%H%M%S')

    return f'{dt_str}_{meta["site"]}_{meta["dataset"]}'


def get_relative_path(meta) -> Path:
    """"""
    dt = Path(meta['time_coverage'][0].strftime('%Y/%j'))

    return Path(meta['site']) / Path(meta['dataset']) / dt


def get_full_path(
        root_dir: Path, meta, collection_id: str) -> Path:
    """"""
    filename = f'{get_name(meta)}.nc'
    relative_path = get_relative_path(meta)

    return root_dir / Path(collection_id) / relative_path / filename
