# -*- encoding: utf-8 -*-
import datetime
import errno
import fcntl
import json
import logging
import os
import sys

import shapely
import shapely.wkt
import xarray as xr

logger = logging.getLogger('felyx.processor')


class MetricsEncoder(json.JSONEncoder):
    """JSON encoder for metrics objects"""
    def default(self, obj):
        if isinstance(obj, (datetime.datetime, datetime.date, datetime.time)):
            return obj.isoformat()
        if isinstance(obj, xr.DataArray):
            return obj.item()
        if isinstance(obj, shapely.Geometry):
            return shapely.wkt.dumps(obj)

        return super(MetricsEncoder, self).default(obj)


def makedirs(dir_path: str) -> bool:
    """@TODO uselesss ? replace with Pathlib methods?"""
    full_path = os.path.abspath(dir_path)
    if os.path.exists(full_path):
        _msg = '{} already exists. Skipping directory creation'
        logger.debug(_msg.format(full_path))
        return True

    try:
        os.makedirs(full_path)
    except OSError:
        _, e, _ = sys.exc_info()
        if e.errno != errno.EEXIST:
            # Error was not due to a simple race with another process which
            # already created the directory => raise exception
            _msg = 'Exception raised while creating {}'
            logger.error(_msg.format(full_path), exc_info=e)
            return False
    return True


def save_json(data: dict, output_path: str, overwrite: bool) -> bool:
    """"""
    ok = True
    with open(output_path, 'a+') as data_file:
        # Lock the file to get an exclusive access
        try:
            fcntl.flock(data_file, fcntl.LOCK_EX)
        except IOError:
            _msg = ('Could not save JSON file: impossible to get exclusive '
                    'access to {}')
            logger.error(_msg.format(output_path))
            logger.debug('flock error', exc_info=sys.exc_info())
            return False

        try:
            data_file.seek(0, os.SEEK_END)  # move cursor at the end of file
            is_file_empty = (0 >= data_file.tell())

            if (overwrite is False) and (is_file_empty is False):
                data_file.seek(0)  # move back to beginning of file
                try:
                    json_data = json.load(data_file)
                    json_data.update(data)
                except ValueError:
                    _msg = 'Could not load existing JSON content from {}'
                    logger.error(_msg.format(output_path))
                    raise  # will be caught by the except ValueError below
            else:
                json_data = data

            data_file.seek(0, os.SEEK_SET)  # put cursor at the beginning
            data_file.truncate(0)  # remove existing content from file
            try:
                json.dump(json_data, data_file, cls=MetricsEncoder)
            except ValueError:
                logger.error('Could not serialize data as JSON')
                raise  # will be caught by except ValueError below

        except ValueError:
            logger.error('Something went wrong with JSON data (see previous '
                         'log messages for more info)')
            logger.debug('json error', exc_info=sys.exc_info())
            ok = False
        finally:
            # Whatever happens, try to unlock the file
            # Beware: this might raise an exception too!
            fcntl.flock(data_file, fcntl.LOCK_UN)
    return ok
