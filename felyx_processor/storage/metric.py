# -*- coding: utf-8 -*-

import datetime
import logging
import os

import felyx_processor.storage

logger = logging.getLogger('felyx.processor')

REQUIRED_FIELDS = ('collection', 'dataset', 'site', 'time_coverage',)


class MissingMetadata(Exception):
    """"""
    def __init__(self, fields):
        """"""
        super(MissingMetadata, self).__init__()
        self.fields = fields


def get_relative_path(meta: dict) -> str:
    """"""
    # @TODO use Pathlib
    collection = meta['collection']
    dataset = meta['dataset']
    site = meta['site']
    start = meta['time_coverage'][0]
    year = start.strftime('%Y')
    day = start.strftime('%j')
    result = os.path.join(collection, dataset, site, year, day)

    return result


def write_json(meta: dict, output_path: str, overwrite: bool = False) -> bool:
    """"""
    missing_meta = [_ for _ in REQUIRED_FIELDS if _ not in meta.keys()]
    if 0 < len(missing_meta):
        raise MissingMetadata(missing_meta)

    output_dir = os.path.dirname(output_path)

    ok = felyx_processor.storage.makedirs(output_dir)
    if not ok:
        return False

    ok = felyx_processor.storage.save_json(meta, output_path, overwrite)
    return ok
