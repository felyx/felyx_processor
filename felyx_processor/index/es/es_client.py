# encoding: utf-8
"""
es_client
--------------------------------------

Contains class managing Elastic Search client

"""

import logging
from datetime import datetime
from pathlib import Path
from typing import Any, Dict, Generator, List, Optional, Tuple

from elasticsearch import Elasticsearch, ElasticsearchException, helpers
import elasticsearch.exceptions

try:
    import simplejson as json
except ImportError:
    import json

from felyx_processor.utils.configuration import ElasticSearchConfig
from felyx_processor.utils.exceptions import (
    ElasticsearchError,
    FelyxProcessorError,
    QueryToJsonError,
    UnknownOperatorError,
)

LOG = logging.getLogger('felyx.processor')
TIME_FMT = '%Y-%m-%d %H:%M:%S'

OPERATORS = {
    '<': 'lt',
    'lt': 'lt',
    '<=': 'lte',
    'lte': 'lte',
    '>': 'gt',
    'gt': 'gt',
    '>=': 'gte',
    'gte': 'gte',
    '==': 'eq',
    '=': 'eq'
}


class ESOperation:
    """Elastic Search operation base class """


class ESSearchQuery(ESOperation):
    """Class for building Elastic Search search query"""

    def __init__(self,
                 source_fields: List[str],
                 terms_filters: Dict[str, List[str]],
                 range_filters: Dict[str, Tuple[datetime, datetime]] = None,
                 wkt_coords: List[List[float]] = None,
                 constraints_list: List[Dict[str, str]] = None,
                 sort_docs: bool = False,
                 sort_fields: Dict[str, str] = None
                 ):
        """
        Elasticsearch search query constructor
        Args:
            source_fields: the list of the document data to retrieve
            terms_filters: the terms filters (dictionary with the fields in keys
             and the values list in values)
            range_filters: the range time filters (dictionary with the fields in
             keys and the tuple(start, stop) in values)
            wkt_coords: list of (Longitude,Latitude) coordinates defining a
            polygon for geo shape filtering
            constraints_list (optional) : additional data filters
            sort_docs: boolean for sorting by docs
            sort_fields: list of fields to sort (dictionary with fields in keys
            dans sorter order in values)
        """
        self._source_fields = source_fields
        self._terms_filters = terms_filters
        self._range_filters = range_filters
        self._sort_docs = sort_docs
        self._sort_fields = sort_fields
        self._wkt_coords = wkt_coords
        self._constraints_list = constraints_list
        self._query = None

    def create_query(self
                     ) -> Dict[str, Any]:
        """
        Create the ES query within the query attributes
        Returns:
            The ES Query.
        """

        # Create filter clause
        filters = list()
        # term filters
        for key, value in self._terms_filters.items():
            term_filter = self.create_term_filter(key, value)
            if term_filter is not None:
                filters.append(term_filter)
        # range filters
        if self._range_filters is not None and len(self._range_filters) > 0:
            for key, value in self._range_filters.items():
                range_filter = ESSearchQuery.create_time_range_filter(
                    key,
                    value[0],
                    value[1])
                if range_filter is not None:
                    filters.append(range_filter)

        # geo shape filter
        if self._wkt_coords is not None and len(self._wkt_coords) > 0:
            geo_filter = self.create_geo_shape_filter(self._wkt_coords)
            filters.append(geo_filter)

        # constraints filters
        if self._constraints_list is not None:
            for constraint in self._constraints_list:
                es_constraint = self._constraint2filter(constraint)
                data_filter = {'nested': {'path': 'data',
                                          'query': {'bool': {'must': [es_constraint]}}}}
                filters.append(data_filter)

        # Create the query
        query = dict()
        query['query'] = {'bool': {'filter': filters}}
        query['_source'] = self._source_fields

        # Add sorting clauses
        sort_fields = []
        if self._sort_fields is not None:
            for key, value in self._sort_fields.items():
                sort_field = self.create_sort_field(key, value)
                sort_fields.append(sort_field)

        if self._sort_docs is True:
            sort_fields.append('_doc')

        if(len(sort_fields) > 0):
            query['sort'] = sort_fields

        self._query = query
        return query

    def create_term_filter(self,
                           field: str,
                           values: List[str]
                           ) -> dict:
        """
        Create a term filter
        Args:
            field: field to filter
            values: values allowed for the field
        Returns:
            The term filter in ES syntax.
        """
        term_filter = None
        if len(values) > 0:
            term_filter = {'terms': {field: values}}
        return term_filter

    def create_geo_shape_filter(self,
                                wkt_coords: List[List[float]]
                                ) -> dict:
        """
        Create a geo shape filter with a list of geo coordinates defining a
        polygon.
        Args:
            wkt_coords: list of (Longitude,Latitude) coordinates
        Returns:
            The geo shape filter in ES syntax.
        """
        geo_filter = None
        if len(wkt_coords) > 0:
            geo_filter = {'geo_shape': {
                'shape': {'shape': {'type': 'polygon',
                                    'coordinates': [wkt_coords]},
                          'relation': 'intersects'}
            }
            }
        return geo_filter

    @staticmethod
    def create_time_range_filter(field: str,
                                 start: datetime,
                                 stop: datetime
                                 ) -> dict:
        """
        Create a time range filter
        Args:
            field: field to filter on a time range
            start: the start time value
            stop: the end time value
        Returns:
            The time range filter in ES syntax.
        """
        range_filter = None

        if (start is not None) or (stop is not None):
            dt_range = {'relation': 'intersects'}
            if start is not None:
                dt_range['gte'] = start.strftime(TIME_FMT)
            if stop is not None:
                dt_range['lte'] = stop.strftime(TIME_FMT)

            range_filter = {'range': {field: dt_range}}

        return range_filter

    def create_sort_field(self,
                          field: str,
                          order: str) -> dict:
        """
        Create a sort field
        Args:
            field: field to sort
            order: sort order (None or asc: ascending, desc: descending)
        Returns:
            the sort field in ES syntax
        Raises:
            FelyxProcessorError: If invalid sorting order.
        """
        sort_field = None
        if order is None:
            order = 'asc'
        elif order in ('asc','desc'):
            sort_field = {field: {'order': order}}
        else:
            msg = 'Unknown sorting order for Elasticsearch search query : "{}"'\
                .format(order)
            LOG.error(msg)
            raise FelyxProcessorError(msg)

        return sort_field

    def _constraint2filter(self,
                           constraint: Dict[str, str],
                           allow_no_value: bool = False
                           ) -> Dict:
        """
        Build filter from constraints list.
        Args:
            constraint: a dictonnary defining the contraint
            allow_no_value: not implemented
        Returns:
            The filter in ES syntax.
        Raises:
            UnknownOperatorError: If unknown operator.
            NotImplemented: If allow_no_value is set to True.
        """
        result = None
        if 'operator' in constraint:
            # Numerical constraint
            operator = OPERATORS.get(constraint['operator'], None)
            if operator is None:
                msg = 'Unknown operator : "{}"'.format(constraint['operator'])
                LOG.error(msg)
                raise UnknownOperatorError(msg)

            name = {'term': {'data.name': constraint['metric_name']}}
            if 'eq' == operator:
                if isinstance(constraint['value'], int):
                    value = {'term': {'data.int_value': constraint['value']}}
                else:
                    value = {'term': {'data.num_value': constraint['value']}}
            elif operator in ('lt', 'lte', 'gt', 'gte',):
                # Range
                if isinstance(constraint['value'], int):
                    value = \
                        {'range': {'data.int_value': {operator: constraint['value']}}}
                else:
                    value = \
                        {'range': {'data.num_value': {operator: constraint['value']}}}
            else:
                raise NotImplemented

            result = {'bool': {'filter': [name, value]}}
            if 'field' in constraint:
                _field_filter = {'term': {'data.field': constraint['field']}}
                result['bool']['filter'].append(_field_filter)

        elif ('value' in constraint) and isinstance(constraint['value'], bool):
            # Boolean constraint
            name = {'term': {'data.name': constraint['metric_name']}}
            value = {'term': {'data.bool_value': constraint['value']}}
            result = {'bool': {'filter': [name, value]}}
        else:
            # String constraint
            name = {'term': {'data.name': constraint['metric_name']}}
            value = {'term': {'data.txt_value': constraint['value']}}
            result = {'bool': {'filter': [name, value]}}

        if allow_no_value is True:
            raise NotImplemented  # probably doable with a "should" statement

        return result


class ESClient:
    """Wrapper class for Elasticsearch client"""

    def __init__(self,
                 es_config: ElasticSearchConfig = None):
        """
        Elasticsearch Data Source constructor

        Parameters
        ----------
        es_config
            the Elasticsearch configuration
        """
        super().__init__()

        self.__config = es_config
        if self.__config is None:
            self.__config = ElasticSearchConfig()

        self.__client = self.init_client()

    def init_client(self
                    ) -> Elasticsearch:
        """
        Initialize an ES client with the node url defined in configuration

        Returns
        -------
        Elasticsearch
            An ES client.

        Raises
        ______
        ElasticsearchException
            If fails to instantiate ES client.
        """
        node_url = self.__config.url
        try:
            verify_certs = not (node_url.username is None)
            es = Elasticsearch(hosts=[str(node_url)], verify_certs=verify_certs)
            return es
        except ElasticsearchException as e:
            msg = f'Cannot connect to ES server with url : {node_url}'
            LOG.error(msg)
            LOG.error(f'{e}')
            raise ElasticsearchError(msg)

    def search(self,
               index_path: str,
               query: ESSearchQuery,
               operation_key: str,
               clear_scroll: bool = False
               ) -> List[Dict]:
        """
        Retrieve data from ES from a query and index
        Args:
                index_path: the ES index
                query: the ES query
                operation_key: the type of ES operation
                clear_scroll: Clear scrolls after search
        Returns:
                a list of dicts containing the data
        Raises:
            QueryToJsonError: If query to json conversion failed.
            ElasticsearchError: If search, scroll or clear_scroll in ES
            failed.
        """

        # Convert query to JSON
        try:
            json_query = json.dumps(query.create_query())
        except ValueError:
            msg = 'The provided query cannot be converted to JSON.'
            LOG.error(msg)
            LOG.debug('{}'.format(query))
            raise QueryToJsonError(msg)

        # Retrieve ES config operation
        es_operation_config = self.__config.get_operation_config(operation_key)

        try:
            res = self.__client.search(
                index=index_path,
                body=json_query,
                scroll=es_operation_config.scroll_retention_time,
                timeout=es_operation_config.timeout,
                size=es_operation_config.chunksize,
            )
        except ElasticsearchException as e:
            msg = 'Elasticsearch search failure'
            LOG.error(msg)
            LOG.debug('{}'.format(e))
            raise ElasticsearchError(msg)

        # Get the scroll ID
        sid = res['_scroll_id']
        scroll_size = len(res['hits']['hits'])
        results = list()
        scroll_ids = list()

        # Scrolling
        while scroll_size > 0:
            # Before scroll, process current batch of hits
            hits = res['hits']['hits']
            results.extend(hits)

            try:
                res = self.__client.scroll(
                            scroll_id=sid,
                            scroll=es_operation_config.scroll_retention_time)
            except ElasticsearchException as e:
                msg = 'Elasticsearch scroll failure'
                LOG.error(msg)
                LOG.debug('{}'.format(e))
                raise ElasticsearchError(msg)

            # Update the scroll ID
            sid = res['_scroll_id']
            scroll_ids.append(sid)

            # Get the number of results that returned in the last scroll
            scroll_size = len(res['hits']['hits'])

        # Clear the scrolls
        if clear_scroll is True and len(scroll_ids) > 0:
            try:
                self.__client.clear_scroll(scroll_id=','.join(scroll_ids))
            except ElasticsearchException as e:
                msg = 'Elasticsearch clear scroll failure'
                LOG.error(msg)
                LOG.debug('{}'.format(e))
                raise ElasticsearchError(msg)

        return results

    def iterate_distinct_field(self,
                               fieldname: str,
                               index_path: str,
                               operation_key: str
                               ) -> Generator[dict, None, None]:
        """
        Helper to get all distinct values from ElasticSearch query
        Args:
            fieldname : the field to retrieve
            index_path : the ES index
            operation_key: the key specifying ES operation type
        Yields:
            dict: The bucket resulting of aggregation query.
        """
        # Retrieve ES config operation
        es_operation_config = self.__config.get_operation_config(operation_key)

        compositeQuery = {
            'size': es_operation_config.chunksize,
            'sources': [{
                fieldname: {
                    'terms': {
                        'field': fieldname
                    }
                }
            }
            ]
        }
        # Iterate over pages
        while True:
            # Build the query : retrieve all distinct values for site field
            # using aggregation
            result = self.__client.search(
                index=index_path,
                body={
                    # size is set to zero to get only aggregation results and
                    # not all site data
                    'size': 0,
                    'aggs': {
                        'values': {
                            'composite': compositeQuery
                        }
                    }
                },
                timeout=es_operation_config.timeout)

            # Yield each bucket
            for aggregation in result['aggregations']['values']['buckets']:
                yield aggregation
            # Set "after" field
            if 'after_key' in result['aggregations']['values']:
                compositeQuery['after'] = \
                    result['aggregations']['values']['after_key']
            # Finished!
            else:
                break

    def index(self,
              index_path: str,
              docs: List[Tuple[str, Dict]],
              operation_key: str,
              raise_error_on_failure: bool = True
              ) -> Tuple[int, int]:
        """
        Index a document in Elastic Search
        Args:
            raise_error_on_failure: raise an exception if failed to index one
            document.
            index_path: the ES index
            docs : the documents to index as a list of tuples of str and
            dictionary
            operation_key: the type of ES operation
        Returns:
            Number of successfully indexed documents and number of documents
            failed to index.
        Raises:
            FelyxProcessorError: If failed to index document.
        """

        actions = list()
        for doc in docs:
            action = {'_op_type': 'index',
                      '_index': index_path,
                      '_type': '_doc',
                      '_id': doc[0],
                      **doc[1]}
            actions.append(action)

        # Retrieve ES config operation
        es_operation_config = self.__config.get_operation_config(operation_key)
        try:
            result = helpers.bulk(self.__client, actions,
                                  stats_only=True,
                                  timeout=es_operation_config.timeout,
                                  chunk_size=es_operation_config.chunksize,
                                  raise_on_exception=raise_error_on_failure,
                                  raise_on_error=raise_error_on_failure
                                  )
        except ElasticsearchException as e:
            if raise_error_on_failure:
                msg = 'Elasticsearch index failure'
                LOG.error(msg)
                LOG.debug('{}'.format(e))
                raise ElasticsearchError(msg)

        # Error processing
        if raise_error_on_failure and result[1] > 0:
            msg = 'Elasticsearch index failure'.format()
            LOG.error(msg)
            raise FelyxProcessorError(msg)

        return result

    def delete_by_ids(self,
                      index_path: str,
                      doc_ids: List[str],
                      operation_key: str) -> int:
        """
        Delete documents in Elastic Search by their ids
        Args:
            index_path: the ES index
            doc_ids : the list of ids of the documents to delete
            operation_key: the type of ES operation
        Returns:
            the number of deleted documents
        Raises:
            ElasticsearchError: If failed to delete documents.
            FelyxProcessorError: If failed to delete docuements.
        """

        actions = list()
        for id in doc_ids:
            action = {'_op_type': 'delete',
                      '_index': index_path,
                      '_type': '_doc',
                      '_id': id}
            actions.append(action)

        # Retrieve ES config operation
        es_operation_config = self.__config.get_operation_config(operation_key)
        try:
            result = helpers.bulk(self.__client, actions,
                                  stats_only=True,
                                  timeout=es_operation_config.timeout,
                                  chunk_size=es_operation_config.chunksize
                                  )
        except ElasticsearchException as e:
            msg = 'Elasticsearch delete failure'
            LOG.error(msg)
            LOG.debug('{}'.format(e))
            raise ElasticsearchError(msg)

        nb_docs = result[0]

        # Error processing
        if result[1] > 0:
            msg = 'Elasticsearch delete failure'.format()
            LOG.error(msg)
            LOG.debug('{}'.format(result[1]))
            raise FelyxProcessorError(msg)

        return nb_docs

    def delete_by_query(self,
                        index_path: str,
                        query: ESSearchQuery,
                        operation_key: str) -> int:
        """
        Delete documents in Elastic Search by searching by query
        Args:
            index_path: the ES index
            query : the query used for retrieving documents
            operation_key: the type of ES operation
        Returns:
            the number of deleted documents
        Raises:
            QueryToJsonError: If query conversion to JSON failed.
            ElasticsearchError: If failed to delete document.
            FelyxProcessorError: If failed to delete document.
        """

        # Retrieve ES config operation
        es_operation_config = self.__config.get_operation_config(operation_key)

        # Convert query to JSON
        try:
            json_query = json.dumps(query.create_query())
        except ValueError:
            msg = 'The provided query cannot be converted to JSON.'
            LOG.error(msg)
            LOG.debug('{}'.format(query))
            raise QueryToJsonError(msg)

        try:
            result = self.__client.delete_by_query(
                            index=index_path, body=json_query,
                            scroll=es_operation_config.scroll_retention_time,
                            timeout=es_operation_config.timeout)

        except ElasticsearchException as e:
            msg = 'Elasticsearch delete failure'
            LOG.error(msg)
            LOG.debug('{}'.format(e))
            raise ElasticsearchError(msg)

        # Error processing
        if len(result['failures']) > 0:
            msg = 'Elasticsearch delete failure'.format()
            LOG.error(msg)
            LOG.debug('{}'.format(result['failures']))
            raise FelyxProcessorError(msg)

        return result['deleted']

    def _load_mappings(self,
                       mappings_path: Path
                       ) -> Dict:
        """
        Load the mappings file
        Args:
            mappings_path: the path to the mappings file
        Returns:
            the dictionary with the mappings
        """

        if not mappings_path.exists():
            msg = 'Mapping file {} not found'.format(mappings_path)
            LOG.error(msg)
            raise FelyxProcessorError(msg)

        with open(mappings_path, 'r') as f:
            mappings = json.load(f)

        return mappings

    def create_index(self,
                     index_path: str,
                     mappings_path: Path
                     ):
        """ Create an ES index
        Args:
            index_path: the ES index
            mappings_path: the path to the file containing the mappings
        Raises:
            ElasticsearchError: If failed to create index.
        """

        # Retrieve the mappings
        index_mappings = self._load_mappings(mappings_path)
        index_cfg = {'settings': {},
                     'mappings': index_mappings['mappings']}

        # Create the ES index
        try:
            response = self.__client.indices.create(
                index=index_path, body=index_cfg, ignore=400)

        except ElasticsearchException as e:
            msg = 'Elasticsearch create index failure'
            LOG.error(msg)
            LOG.debug('{}'.format(e))
            raise ElasticsearchError(msg)

        if 'error' in response:
            # Catch already index exists response
            if response['error']['type'] == 'resource_already_exists_exception':
                msg = 'Elasticsearch index already exists : {}. Maybe '\
                      'another process created it between the '\
                      'index existence check and the creation request.'\
                    .format(index_path)
                LOG.warning(msg)
            else:
                msg = 'Elasticsearch create index failure : {}'\
                    .format(response['error']['type'])
                LOG.error(msg)
                LOG.debug('{}'.format(response['error']))
                raise ElasticsearchError(msg)

    def delete_index(self,
                     index_path: str
                     ):
        """ Delete an ES index
        Args:
            index_path: the ES index
        Raises:
            ElasticsearchError: If failed to delete index.
        """

        # Delete the ES index
        try:
            response = self.__client.indices.delete(index=index_path,
                                                    ignore=400)
        except ElasticsearchException as e:
            msg = 'Elasticsearch failure on delete index'
            LOG.error(msg)
            LOG.debug('{}'.format(e))
            raise ElasticsearchError(msg)

        if 'error' in response:
            msg = 'Elasticsearch failure on delete index : {}'.\
                format(response['error']['type'])
            LOG.error(msg)
            LOG.debug('{}'.format(response['error']))
            raise ElasticsearchError(msg)

    def has_index(self,
                  index_path: str) -> bool:
        """ Check the existence of an ES index
        Args:
            index_path: the ES index
        Returns True if exists False otherwise
        """
        try:
            return self.__client.indices.exists(index=index_path)
        except Exception as e:
            if isinstance(e, elasticsearch.exceptions.ConnectionError):
                msg = 'Elasticsearch connection failure. Check the host/port of the ES server. {0}'.format(e)
            elif isinstance(e, elasticsearch.exceptions.AuthenticationException):
                msg = 'Elasticsearch authentification failure. Check the ES credentials. {0}'.format(e)
            elif isinstance(e, elasticsearch.exceptions.AuthorizationException):
                msg = 'Elasticsearch authorization failure. Check the ES index prefix. {0}'.format(e)
            else:
                msg = 'Elasticsearch check index existence failure : {0}'.format(e)
            LOG.error(msg)
            LOG.debug('{}'.format(e))
            raise ElasticsearchError(msg)

    def search_coverages(self,
                         index_path: str,
                         operation_key: str,
                         fieldname: str,
                         start_fieldname: str,
                         end_fieldname: str) -> Dict:
        """
        Retrieve coverages of a field from ES
        Args:
            index_path: the ES index
            operation_key: the type of ES operation
            fieldname: name of the field to search
            start_fieldname: name of the coverage start field
            end_fieldname: name of the coverage end field
        Returns:
            a dict containing the ES response
        Raises:
            QueryToJsonError: If query conversion to JSON failed.
            ElasticsearchError: If retrieving coverages failed.
        """
        query = {
            'size': 0,
            'aggs': {
                fieldname: {
                    'terms': {
                        'field': fieldname
                    },
                    'aggs': {
                        start_fieldname: {
                            'min': {
                                'field': start_fieldname
                            }
                        },
                        end_fieldname: {
                            'max': {
                                'field': end_fieldname
                            }
                        }
                    }
                }
            }
        }

        # Convert query to JSON
        try:
            json_query = json.dumps(query)
        except ValueError:
            msg = 'The provided query cannot be converted to JSON.'
            LOG.error(msg)
            LOG.debug('{}'.format(query))
            raise QueryToJsonError(msg)

        # Retrieve ES config operation
        es_operation_config = self.__config.get_operation_config(operation_key)

        try:
            results = self.__client.search(
                index=index_path, body=json_query,
                scroll=es_operation_config.scroll_retention_time,
                timeout=es_operation_config.timeout,
                size=es_operation_config.chunksize)
        except ElasticsearchException as e:
            msg = 'Elasticsearch search coverages failure'
            LOG.error(msg)
            LOG.debug('{}'.format(e))
            raise ElasticsearchError(msg)

        return results

    def get_docs_count(self,
                       index_path: str,
                       time_field: Optional[str] = None,
                       start: Optional[datetime] = None,
                       stop: Optional[datetime] = None) -> int:
        """
            Retrieve document counts on a time range.
            Args:
                index_path: the ES index
                time_field: the field to apply time filter
                start: the time range start
                stop: the time range end
            Returns:
                The number of documents.
            Raises:
                QueryToJsonError: If query conversion to JSON failed.
                ElasticsearchError: If retrieving data failed.
        """

        # Create the query
        query = {'query': {'match_all': {}}}
        if time_field is not None and start is not None and stop is not None:
            time_filter = ESSearchQuery.create_time_range_filter(
                                                            field=time_field,
                                                            start=start,
                                                            stop=stop)

            query = {'query': {'bool': {'filter': time_filter}}}

        # Convert query to JSON
        try:
            json_query = json.dumps(query)
        except ValueError:
            msg = 'The provided query cannot be converted to JSON.'
            LOG.error(msg)
            LOG.debug('{}'.format(query))
            raise QueryToJsonError(msg)

        try:
            results = self.__client.count(index=index_path, body=json_query)
        except ElasticsearchException as e:
            msg = 'Elasticsearch get documents count failure'
            LOG.error(msg)
            LOG.debug('{}'.format(e))
            raise ElasticsearchError(msg)

        return results['count']
