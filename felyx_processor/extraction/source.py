# encoding: utf-8
"""
Contains the code for the identification of pixels to be extracted to form
child products.
"""
import contextlib
import logging
import os
from pathlib import Path

import cerbere

from felyx_processor.felyx_data import get_felyx_data
from felyx_processor.miniprod.tools import lazy_property
from felyx_processor.utils.configuration import FelyxProcessorConfig


LOGGER = logging.getLogger('felyx.processor')


class SourceFile(object):
    """"""

    def __init__(
            self,
            felyx_processor_config: FelyxProcessorConfig,
            url: Path,
            dataset_id: str
        ):
        """"""
        # get felyx data (collections, sites, ...)
        felyx_data = get_felyx_data(
            felyx_processor_config=felyx_processor_config,
            reset=True)

        self.config = felyx_processor_config
        self.input_url = Path(url)
        self.dataset_id = dataset_id
        self.feature = None

        # Load dataset metadata in configuration
        self.dataset_cfg = felyx_data.dataset(
            dataset_id=self.dataset_id)

        self._feature_class = self.dataset_cfg.feature_class
        if self._feature_class is None:
            raise ValueError(
                f'Missing Feature class definition for {self.dataset_id} in '
                f'configuration')

        self._reader_class = self.dataset_cfg.dataset_class
        if self._reader_class is None:
            raise ValueError(
                f'Missing Dataset class definition for {self.dataset_id} in '
                f'configuration')

    @contextlib.contextmanager
    def load(self):
        try:
            # Load the data in cerbere.
            self.feature = cerbere.open_feature(
                filename_or_obj=self.input_url,
                reader=self._reader_class,
                feature=self._feature_class)

            yield self

            # Store metadata about file for later use in dynamic site
            # generation.
            # self.record_metadata()
        except Exception as e:
            raise
        finally:
            # Closing EO file
            if (hasattr(self.feature, 'ds') and
                    hasattr(self.feature.ds, 'close')):
                self.feature.ds.close()

    @lazy_property
    def data_shape(self):
        """Return a shape object describing the data in the input file."""
        from felyx_processor.extraction.helpers import get_footprint_shape
        try:
            return get_footprint_shape(self.feature)
        except ValueError:
            _msg = ('The input file {} does not contain enough geospatial '
                    'information for felyx to process. This is not considered '
                    'an error.')
            LOGGER.warning(_msg.format(self.input_url))
        return None

    @lazy_property
    def full_lon(self):
        """Return the full longitude field for the data source."""
        # # @TODO order of coordinates must match data space (X,Y) or (Y,X)
        # if (isinstance(self.feature, cerbere.Grid) and
        #         self.feature.is_cylindrical()):
        #     return self.feature.ds.cb.longitude.cb.broadcast(
        #         [self.feature.ds.cb.Y, self.feature.ds.cb.X])

        return self.feature.ds.cb.longitude

    @lazy_property
    def full_lat(self):
        """Return the full latitude field for the data source."""
        # if (isinstance(self.feature, cerbere.Grid) and
        #         self.feature.is_cylindrical()):
        #     return self.feature.ds.cb.latitude.cb.broadcast(
        #         [self.feature.ds.cb.Y, self.feature.ds.cb.X])

        return self.feature.ds.cb.latitude

    @lazy_property
    def dimensions_hint(self):
        """Return the geolocation dimsizes of the input datamodel."""
        # TODO CR A voir : return len(self.datamodel.geodimsizes)
        #return len(self.datamodel.dataset.cb.dimsizes)
        return len(self.feature.ds.cb.cf_dims)

    @lazy_property
    def colocation_limits(self):
        """Return a dict of all colocation period and radii for all
        dynamic sites.
        """
        server_data = get_felyx_data(self.config)
        return server_data.constraints(self.dataset_id)

    @lazy_property
    def time_coverage_start(self):
        """Return the time coverage start of the input EO feature."""
        return self.feature.ds.cb.time_coverage_start

    @lazy_property
    def time_coverage_end(self):
        """Return the time coverage end of the input  EO feature."""
        return self.feature.ds.cb.time_coverage_end
