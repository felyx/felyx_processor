# encoding: utf-8
"""
felyx.backend.extract.slicers.swath
-----------------------------------

Contains the code for the slicing and extraction of miniprods
using the :class:`cerbere.datamodel.cswath.Swath` feature.

.. :copyright: Copyright 2013 Pelamis Scientific Software Ltd.
.. :license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: David Poulter <david.poulter@pelamis.co.uk>
.. codeauthor:: David Poulter <david.poulter@pelamis.co.uk>

# @TODO rename as curvilinear
"""
import logging

import numexpr
import numpy
import scipy
import scipy.spatial
import scipy.spatial.qhull
from scipy.ndimage import (
    generate_binary_structure, find_objects, label, binary_dilation)
from shapely.geometry import MultiPolygon, Polygon
from shapely.geometry.multipoint import MultiPoint
import xarray as xr

from felyx_processor.extraction import EOSlice
from felyx_processor.extraction.slicers import (
    mask_as_da,
    get_centre_index,
    get_disjoint_mask,
    is_shape_complicated,
)


LOGGER = logging.getLogger('felyx.processor')


def get_coverage(
        lats: xr.DataArray,
        lons: xr.DataArray,
        site_shape) -> float:
    """
    Returns the percentage of a site covered by the extracted pixels.

    Args:
        lats: An array of latitudes which have been marked as for
            extraction
        lons: An array of longitudes that have been marked as for
            extraction.
        site_shape: A shapely shape describing the site.

    Returns:
        percentage of coverage.
    """
    coords = numpy.array((
        lons.cb.to_masked_array().compressed(),
        lats.cb.to_masked_array().compressed())).T
    if not coords.size:
        return

    if coords.size < 7:
        # There are not enough points for qhull to calculate a hull.
        coverage_shape = MultiPoint(coords).convex_hull

    else:
        try:
            hull = scipy.spatial.ConvexHull(coords)
            coverage_shape = MultiPoint(
                numpy.array(
                    (
                        hull.points[hull.simplices[:, 0], 0],
                        hull.points[hull.simplices[:, 1], 1]
                    )
                ).T
            ).convex_hull
        except scipy.spatial.qhull.QhullError:
            # noinspection PyBroadException
            try:
                coverage_shape = MultiPoint(coords).convex_hull
            except Exception:
                LOGGER.warning(
                    'It was not possible to establish the convex hull of a '
                    'shape. This is a numerical issue and not an error, as '
                    'it can only occur in situations where no data is within '
                    'a site. However, a  large number of these warnings '
                    'implies a programming instability. If you see many copies '
                    'of this message in your logs, please report it to the '
                    'felyx team.'
                )
                return None

    # Provide a small extension, to ensure the shape is not a single point.
    # Only as an effect if a single pixel is selected.
    coverage_extension = 0.001
    try:
        coverage_extension = max(
            coverage_extension,
            numpy.nanmin(numpy.abs(numpy.diff(lats))) / 2.0)
    except ValueError:
        pass

    coverage_shape = coverage_shape.buffer(coverage_extension)

    if site_shape.within(coverage_shape):
        return 100.0
    else:
        value = site_shape.intersection(coverage_shape).area / site_shape.area
        return numpy.round(value * 100., decimals=1)


# noinspection PyUnusedLocal
def slicer(
        full_lon: xr.DataArray,
        full_lat: xr.DataArray,
        shape, code, fixed_shape=None,
        padded_lon=None,
        padded_lat=None, **kwargs):
    """Return a list of all contiguous regions of lat and full_lon that fall
    within a shape. Expected to work only for features extracted with
    the cerbere.datamodel.cswath.Swath class.

    :param numpy.ndarray full_lon: A numpy array of longitudes, it is
      expected that this is
      a vector, as this is intended for processing geographic cylindrical grids.
    :param numpy.ndarray full_lat: A numpy array of longitudes, it is expected
      that this
      is a vector, as this is intended for processing geographic cylindrical
      grids.
    :param shape: A shapely or geometry shape representing the exact
      shape of the region of interest.
    :type shape: :class:`felyx_processor.miniprod.extract.helpers.FastShape`
    :param str code: The name of the region of interest
    :param int fixed_shape: If set, the miniprod slices will set to this fixed
      width focused on the centre point of shape.
    :param numpy.ndarray padded_lat: Pre-padded version of full_lat
    :param numpy.ndarray padded_lon: Pre-padded version of full_lon
    :param kwargs: Arguments ignored but allowable.
    """
    # Get the expansion size of the fixed shape miniprod if applicable
    if fixed_shape:
        expansion = int((fixed_shape - 1) / 2)
    else:
        expansion = None

    # Setup return value holders
    returned_slices = []
    returned_masks = []
    returned_coverages = []
    returned_centres = []

    if not isinstance(shape, (Polygon, MultiPolygon)):
        raise TypeError('The shape is not valid')
    lon_min, lat_min, lon_max, lat_max = shape.bounds

    #Use numexpr to evaluate the maximum possible region.
    mask = numexpr.evaluate(
        str(
            '(full_lon >= {}) & '
            '(full_lon <= {}) & '
            '(full_lat >= {}) & '
            '(full_lat <= {})'
        ).format(lon_min, lon_max, lat_min, lat_max)
    )

    if not mask.any():
        return

    if not is_shape_complicated(shape):
        #Is the site a simple rectangle, parallel with meridians and latitudes.
        y_labels, y_count = label(mask.any(axis=0))
        x_labels, x_count = label(mask.any(axis=1))
        if not (x_count * y_count):
            #Bail out if no pixels are in the site.
            return None

        def append(x_slice, y_slice, returned_slice, returned_mask,
                   returned_coverage):
            returned_slices.append(returned_slice)
            returned_masks.append(returned_mask)
            # @TODO Fix static sites extraction
            returned_coverages.append(returned_coverage)

        # replaced JFP code
        # def append(x_slice, y_slice, returned_slice, returned_mask):
        #     returned_slices.append(returned_slice)
        #     returned_masks.append(returned_mask)
        #     returned_coverages.append(None)

            returned_centres.append(
                ((x_slice.start + x_slice.stop) // 2,
                 (y_slice.start + y_slice.stop) // 2))

            if expansion is not None:
                # Get smaller arrays to work with
                centre_lon = shape.centroid.coords.xy[0][0]
                centre_lat = shape.centroid.coords.xy[1][0]
                area_lat = full_lat[y_slice, x_slice]
                area_lon = full_lon[y_slice, x_slice]
                returned_coverage = get_coverage(area_lat, area_lon, shape)

                x_centre_index, y_centre_index = get_centre_index(
                    centre_lon, centre_lat, area_lon, area_lat
                )

                final_x_slice = slice(
                    x_centre_index + x_slice.start - expansion,
                    x_centre_index + x_slice.start + expansion + 1
                )

                final_y_slice = slice(
                    y_centre_index + y_slice.start - expansion,
                    y_centre_index + y_slice.start + expansion + 1
                )

                returned_slices[-1] = (final_x_slice, final_y_slice)
                returned_masks[-1] = (mask[(final_x_slice, final_y_slice)])
                returned_coverages[-1] = returned_coverage
                returned_centres[-1] = (
                    x_centre_index + x_slice.start,
                    y_centre_index + y_slice.start)

        for x in range(x_count):
            for y in range(y_count):
                # Get the x and y slices
                y_slice = find_objects(y_labels)[y][0]
                x_slice = find_objects(x_labels)[x][0]

                # Get the return values
                returned_slice = (x_slice, y_slice)
                returned_mask = mask[returned_slice]
                if returned_mask.all():
                    returned_coverage = 100.0
                else:
                    returned_coverage = get_coverage(
                        full_lat[returned_slice].where(returned_mask),
                        full_lon[returned_slice].where(returned_mask),
                        shape)
                if returned_coverage is not None:
                    # @TODO Fixed static_site extraction
                    append(x_slice, y_slice, returned_slice, returned_mask,
                           returned_coverage)
                    # aboves replace JFP code:
                    # append(x_slice, y_slice, returned_slice, returned_mask)

            # if returned_coverage is not None:
            #     break

        if len(returned_slices) == 0:
            return

    else:
        #
        feature_connections = generate_binary_structure(2,2)
        mask = binary_dilation(mask, iterations=3)
        labels = label(mask, structure=feature_connections)[0]

        slices = find_objects(labels)

        for slice_number, _slice in enumerate(slices):
            _slice = {_: _slice[i] for i, _ in enumerate(full_lat.dims)}
            if fixed_shape:
                # Get the centre point of the extracted data
                centre_lon = shape.centroid.coords.xy[0][0]
                centre_lat = shape.centroid.coords.xy[1][0]
                area_lat = full_lat[_slice]
                area_lon = full_lon[_slice]
                returned_coverage = get_coverage(area_lat, area_lon, shape)

                if not area_lat.any() or not area_lon.any():
                    continue

                x_centre_index, y_centre_index = get_centre_index(
                    centre_lon, centre_lat, area_lon, area_lat
                )

                # Get the final (that are used for sub-setting)
                x_slice, y_slice = _slice.values()

                final_x_slice = slice(
                    x_centre_index + x_slice.start - expansion,
                    x_centre_index + x_slice.start + expansion + 1
                )

                final_y_slice = slice(
                    y_centre_index + y_slice.start - expansion,
                    y_centre_index + y_slice.start + expansion + 1
                )

                reported_slice = (final_x_slice, final_y_slice)

                slices[slice_number] = reported_slice

                # no padding
                if padded_lon is None:
                    xbound, ybound = full_lon.shape
                    # Get the padded slices for calculating the content mask
                    padded_x_slice = slice(
                        max(0, x_centre_index + x_slice.start),
                        min(x_centre_index + x_slice.start + expansion * 2 + 1,
                            xbound)
                        )

                    padded_y_slice = slice(
                        max(0, y_centre_index + y_slice.start),
                        min(y_centre_index + y_slice.start + expansion * 2 + 1,
                            ybound)
                    )

                    padded_slice = (padded_x_slice, padded_y_slice)
                    reported_slice = padded_slice

                    # noinspection PyUnboundLocalVariable
                    sub_mask = get_disjoint_mask(
                        shape,
                        full_lon[padded_slice],
                        full_lat[padded_slice]
                    ).reshape(
                        [_.stop - _.start for _ in reported_slice]
                    )

                else:
                    # Get the padded slices for calculating the content mask
                    padded_x_slice = slice(
                        x_centre_index + x_slice.start,
                        x_centre_index + x_slice.start + expansion * 2 + 1
                    )

                    padded_y_slice = slice(
                        y_centre_index + y_slice.start,
                        y_centre_index + y_slice.start + expansion * 2 + 1
                    )

                    padded_slice = (padded_x_slice, padded_y_slice)


                    # noinspection PyUnboundLocalVariable
                    sub_mask = get_disjoint_mask(
                        shape,
                        padded_lon[padded_slice],
                        padded_lat[padded_slice]
                    ).reshape(
                        [_.stop - _.start for _ in reported_slice]
                    )

                returned_slices.append(reported_slice)
                returned_masks.append(sub_mask)
                returned_coverages.append(returned_coverage)
                returned_centres.append((x_centre_index + x_slice.start,
                                         y_centre_index + y_slice.start))
            else:

                returned_coverage = get_coverage(
                    full_lat[_slice].flatten(), full_lon[_slice].flatten(),
                    shape
                )

                # if not len(coords):
                #     # bail out if no coords are present
                #     return None
                # if numpy.ma.masked in zip(*coords)[0]:
                #     # bail out if any missing values in the input lat / lon
                #     return None

                sub_mask = get_disjoint_mask(
                    shape,
                    full_lon[_slice],
                    full_lat[_slice],
                ).reshape([_.stop - _.start for _ in _slice])

                returned_slices.append(_slice)
                returned_masks.append(sub_mask)
                returned_coverages.append(returned_coverage)
                returned_centres.append((-1, -1))

    result = []
    # @TODO useless mask information (set later in subset) ?
    for index, _slice in enumerate(returned_slices):
        _coverage = returned_coverages[index]
        result.append(EOSlice(
            shape=shape,
            slices={_: _slice[i] for i, _ in enumerate(full_lat.dims)},
            mask=mask_as_da(returned_masks[index], full_lat.dims),
            site=code,
            coverage=_coverage,
            source_center_index={
                _: returned_centres[index][i]
                for i, _ in enumerate(full_lat.dims)}))

    return result
