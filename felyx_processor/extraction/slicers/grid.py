# encoding: utf-8
"""
felyx.backend.extract.slicers.grid
----------------------------------

Contains the code for the slicing and extraction of miniprods
using the :class:`cerbere.datamodel.cgrid.Grid` feature.

.. :copyright: Copyright 2013 Pelamis Scientific Software Ltd.
.. :license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: David Poulter <david.poulter@pelamis.co.uk>
.. codeauthor:: David Poulter <david.poulter@pelamis.co.uk>
"""
import logging
import typing as T

import numpy
import scipy.ndimage
import xarray as xr

from felyx_processor.extraction.slicers import (
    get_centre_index, get_disjoint_mask, get_regular_envelope)
from felyx_processor.extraction import EOSlice
from . import  mask_as_da


LOGGER = logging.getLogger('felyx.processor')


def meshgrid(
        lons: xr.DataArray,
        lats: xr.DataArray) -> T.Tuple[xr.DataArray, xr.DataArray]:
    """return the lons, lats broadcast to the whole observation space"""
    return lons.cb.broadcast([lats, lons]), lats.cb.broadcast([lats, lons]),


# noinspection PyUnusedLocal
def slicer(
        full_lon, full_lat, shape, code, fixed_shape=None,
        dimensions=2, meshed_lon=None, meshed_lat=None, **kwargs):
    """Return a list of all contiguous regions of lat and full_lon that fall
    within a shape. Expected to work only for features extracted with
    the :class:`cerbere.datamodel.grid.CylindricalGrid` class.

    :param numpy.ndarray full_lon: A numpy array of longitudes, it is
      expected that this is
      a vector, as this is intended for processing geographic cylindrical grids.
    :param numpy.ndarray full_lat: A numpy array of longitudes,
      it is expected that this
      is a vector, as this is intended for processing geographic cylindrical
      grids.
    :param shape: A shapely or geometry shape representing the exact
      shape of the region of interest.
    :type shape: :class:`felyx_processor.miniprod.extract.helpers.FastShape`
    :param str code: The name of the region of interest
    :param int fixed_shape: If set, the miniprod slices will set to this fixed
      width focused on the centre point of shape.
    :param int dimensions: The number of dimensions the output mask should have.
    :param kwargs: Unused argument for compatibility with other Swath and
      Trajectory slicers.
    """
    # @TODO why calculate a mask here ?
    # Get the expansion size of the fixed shape miniprod if applicable
    if fixed_shape:
        expansion = int((fixed_shape - 1) / 2)
    else:
        expansion = None

    # @ TODO Improve with full xarray slice usage + non circular grids
    spatial_dims = [full_lat.dims[0], full_lon.dims[0]]

    # Expand out the search shape, by half the resolution of the grid, to
    # ensure that small sites do not fall through the gaps.
    # @TODO improve with better latitude dependent criteria
    search_shape = shape.buffer(
        abs(full_lat.values.flat[1] - full_lat.values.flat[2]))

    lon_min, lat_min, lon_max, lat_max = search_shape.bounds

    if not full_lat.ndim == 1:
        raise ValueError(
            'The provided latitude and longitudes are not '
            'of the correct dimensionality for the Grid slicer class. '
            'Are you sure that this dataset is really a Grid datamodel?'
        )

    lon_mask = full_lon >= lon_min
    lon_mask *= full_lon <= lon_max
    if not lon_mask.any():
        return None

    lat_mask = full_lat >= lat_min
    lat_mask *= full_lat <= lat_max
    if not lat_mask.any():
        return None

    y_labels, y_bodies = scipy.ndimage.label(lat_mask)
    x_labels, x_bodies = scipy.ndimage.label(lon_mask)

    # @TODO : probably does not work very well if x/y dimensions reversed or
    # if not global grid
    if y_bodies == x_bodies == 1:

        #Only one contiguous region identified, simple extraction.

        y_slice = scipy.ndimage.find_objects(y_labels)[0][0]
        x_slice = scipy.ndimage.find_objects(x_labels)[0][0]

        slices = (y_slice, x_slice)

        if expansion is not None:
            # Get smaller arrays to work with
            centre_lon = shape.centroid.coords.xy[0][0]
            centre_lat = shape.centroid.coords.xy[1][0]

            if meshed_lat is not None and meshed_lon is not None:
                area_lon = meshed_lon[y_slice, x_slice]
                area_lat = meshed_lat[y_slice, x_slice]
            else:
                area_lon, area_lat = meshgrid(
                    full_lon[x_slice], full_lat[y_slice])

            y_centre_index, x_centre_index = get_centre_index(
                centre_lon, centre_lat, area_lon, area_lat)

            final_x_slice = slice(
                x_centre_index + x_slice.start - expansion,
                x_centre_index + x_slice.start + expansion + 1
            )

            final_y_slice = slice(
                y_centre_index + y_slice.start - expansion,
                y_centre_index + y_slice.start + expansion + 1
            )

            slices = (final_y_slice, final_x_slice)

            if final_x_slice.start < 0:
                # @TODO not valid for non circular grids
                final_x_slice_left = slice(final_x_slice.start, None)
                final_x_slice_right = slice(None, final_x_slice.stop)

                if meshed_lat is not None and meshed_lon is not None:
                    grid_lon = xr.concat(
                        [meshed_lon[final_y_slice, final_x_slice_left],
                         meshed_lon[final_y_slice, final_x_slice_right]],
                        dim='lon'
                    )
                    grid_lat = xr.concat(
                        [meshed_lat[final_y_slice, final_x_slice_left],
                         meshed_lat[final_y_slice, final_x_slice_right]],
                        dim='lon')
                else:
                    grid_lon, grid_lat = meshgrid(
                        xr.concat(
                            [full_lon[final_x_slice_left],
                             full_lon[final_x_slice_right]],
                            dim='lon'),
                        full_lat[final_y_slice])
                    # grid_lon, grid_lat = numpy.meshgrid(
                    #     numpy.concatenate([
                    #         full_lon[final_x_slice_left],
                    #         full_lon[final_x_slice_right]]),
                    #     full_lat[final_y_slice]
                    # )

            else:
                if meshed_lat is not None and meshed_lon is not None:
                    grid_lon = meshed_lon[final_y_slice, final_x_slice]
                    grid_lat = meshed_lat[final_y_slice, final_x_slice]
                else:
                    grid_lon, grid_lat = meshgrid(
                        full_lon[final_x_slice], full_lat[final_y_slice]
                    )

                    #
                    # grid_lon, grid_lat = numpy.meshgrid(
                    #     full_lon[final_x_slice], full_lat[final_y_slice]
                    # )

            if not grid_lon.size:
                # bail out if no coords are present
                return None

            mask = get_disjoint_mask(search_shape, grid_lon, grid_lat)

            if mask.size not in (fixed_shape ** 2, (fixed_shape - 1) ** 2):
                # this error occurs, apparently randomly, about once every 3000
                # miniprods. The cause has not yet identified.
                LOGGER.warning(
                    str(
                        'Unknown mask size mismatch error occured.'
                    )
                )
                return None

            mask = mask.reshape((fixed_shape, fixed_shape))

            coverage_shape = get_regular_envelope(
                full_lon[x_slice], full_lat[y_slice],
                simple_grid=True
            )

        else:
            if lon_mask[x_slice].all() and lat_mask[y_slice].all():
                m_shape = (
                    y_slice.stop - y_slice.start, x_slice.stop - x_slice.start)
                mask = numpy.ones(m_shape, dtype=bool)

            else:
                mask = numpy.multiply(
                    *numpy.meshgrid(lon_mask[x_slice], lat_mask[y_slice])
                )

            coverage_shape = get_regular_envelope(
                full_lon[x_slice], full_lat[y_slice],
                simple_grid=True
            )
            x_centre_index = \
                int((x_slice.start + x_slice.stop) / 2) - x_slice.start
            y_centre_index = \
                int((y_slice.start + y_slice.stop) / 2) - y_slice.start

        if coverage_shape.contains(shape):
            coverage = 100.0
        else:
            value = shape.intersection(coverage_shape).area / shape.area
            coverage = numpy.round(value * 100., decimals=1)

        while len(mask.shape) < dimensions:
            mask = numpy.expand_dims(mask, axis=0)

        return [(EOSlice(
            shape=shape,
            slices={spatial_dims[0]: slices[0], spatial_dims[1]: slices[1]},
            mask=mask_as_da(mask, spatial_dims),
            site=code,
            coverage=coverage,
            source_center_index={
                spatial_dims[0]: y_centre_index + y_slice.start,
                spatial_dims[1]: x_centre_index + x_slice.start}))]

    else:

        mask = numpy.multiply(*numpy.meshgrid(lon_mask, lat_mask))

        labels = scipy.ndimage.measurements.label(mask)[0]
        slices = scipy.ndimage.measurements.find_objects(labels)

        result = []
        for index, _slice in enumerate(slices):

            coverage_shape = get_regular_envelope(
                full_lon[_slice[1]], full_lat[_slice[0]]
            )

            if coverage_shape.contains(shape):
                coverage = 100.0
            else:
                value = shape.intersection(coverage_shape).area / shape.area
                coverage = numpy.round(value * 100., decimals=1)

            tmp_mask = mask[_slice].astype(numpy.int8)

            while len(tmp_mask.geometry) < dimensions:
                tmp_mask = numpy.expand_dims(tmp_mask, axis=0)

            result.append(EOSlice(
                shape=shape,
                slices={spatial_dims[0]: _slice[0], spatial_dims[1]: _slice[1]},
                mask=mask_as_da(tmp_mask, spatial_dims),
                site=code,
                coverage=coverage,
                source_center_index={spatial_dims[0]: -1, spatial_dims[1]: -1}))

        return result
