# encoding: utf-8
# pylint: disable=C0301,W0142,F0401,W0703,R0801
"""
felyx.backend.extract.slicers.trajectory
========================================

Contains the code for the slicing and extraction of miniprods
using the cerbere.datamodel.ctrajectory.Trajectory feature.

:copyright: Copyright 2013 Pelamis Scientific Software Ltd.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: David Poulter <david.poulter@pelamis.co.uk>
.. codeauthor:: David Poulter <david.poulter@pelamis.co.uk>
"""
import logging
import typing as T

import numexpr
import numpy as np
from scipy.ndimage import find_objects, label
from shapely import LineString, MultiLineString, MultiPoint, disjoint
import xarray as xr

from felyx_processor.extraction.slicers import get_centre_index
from felyx_processor.extraction import EOSlice
from . import mask_as_da


LOGGER = logging.getLogger('felyx.processor')


def fill_in_mask(
        lons: xr.DataArray,
        lats: xr.DataArray,
        shape: T.Union[LineString, MultiLineString]):
    """
    Return a modified colocation mask, based on the coverage of the miniprod.

    Args:
        lons: The longitudes in the data file.
        lats: The latitudes in the data file.
        shape: The shape of the felyx site.

    Return:
        A filled mask or None.
    """
    coords = np.array([lons, lats]).T

    if coords.size == 0:
        # bail out if no coords are present
        return

    if lons.count() != lons.size:
        # bail out if any missing values in the input
        # full_lat / full_lon
        return

    return mask_as_da(
        (~disjoint(shape, MultiPoint(coords).geoms)).astype(np.int8),
        dims=tuple(lons.dims))


# noinspection PyUnusedLocal
def slicer(full_lon, full_lat, shape, code, fixed_shape=None,
           padded_lat=None, padded_lon=None,
           **kwargs):
    """
    Return a list of all contiguous regions of lat and full_lon that fall
    within a shape. Expected to work only for features extracted with
    the cerbere.datamodel.ctrajectory.Trajectory class.

    :param numpy.ndarray full_lon: A numpy array of longitudes, it is
      expected that this is
      a vector, as this is intended for processing geographic cylindrical grids.
    :param numpy.ndarray full_lat: A numpy array of longitudes, it is expected
      that this
      is a vector, as this is intended for processing geographic cylindrical
      grids.
    :param shape: A shapely or geometry shape representing the exact
      shape of the region of interest.
    :type shape: :class:`felyx_processor.miniprod.extract.helpers.FastShape`
    :param str code: The name of the region of interest
    :param int fixed_shape: If set, the miniprod slices will set to this fixed
      width focused on the centre point of shape.
    :param kwargs: Unused argument for compatibility with other Swath and
      Trajectory slicers.
    """

    # Get the expansion size of the fixed shape miniprod if applicable
    if fixed_shape:
        expansion = int((fixed_shape - 1) / 2)
    else:
        expansion = None

    lon_min, lat_min, lon_max, lat_max = shape.bounds

    # Use numexpr to evaluate the maximum possible region.
    mask = numexpr.evaluate(
        str(
            '(full_lon >= {}) & '
            '(full_lon <= {}) & '
            '(full_lat >= {}) & '
            '(full_lat <= {})'
        ).format(lon_min, lon_max, lat_min, lat_max)
    )

    if not mask.any():
        return None

    labels = label(mask)[0]
    slices = find_objects(labels)
    centers = []
    returned_masks = []

    # slice dimension for a trajectory
    s_dim = full_lon.dims[0]

    # Calculate the area of the shape bounding box.
    for slice_number, _slice in enumerate(slices):
        _slice = {s_dim: _slice[0]}

        if not expansion:
            centers.append({
                s_dim: int((_slice[s_dim].start + _slice[s_dim].stop) / 2)})
            try:
                _mask = fill_in_mask(full_lon[_slice], full_lat[_slice], shape)
                returned_masks.append(_mask)
            except AttributeError:
                LOGGER.exception(
                    str(
                        'An exception occurred whilst trying to process '
                        'trajectory extractions for site {}. The '
                        'calculated slice was: {}'
                    ).format(code, str(_slice))
                )
                raise

            slices[slice_number] = _slice
        else:
            bound = full_lat.size
            centre_lon = shape.centroid.coords.xy[0][0]
            centre_lat = shape.centroid.coords.xy[1][0]
            area_lat = full_lat[_slice]
            area_lon = full_lon[_slice]

            centre_index = get_centre_index(
                centre_lon, centre_lat, area_lon, area_lat
            )[0]

            if padded_lon is None:
                final_slice = {
                    s_dim: slice(
                     max(0, centre_index + _slice[s_dim].start - expansion),
                     min(centre_index + _slice[s_dim].start + expansion + 1,
                         bound))}
            else:
                final_slice = {
                    s_dim: slice(
                        centre_index + _slice[s_dim].start - expansion,
                        centre_index + _slice[s_dim].start + expansion + 1)}

            centers.append({s_dim: centre_index + _slice[s_dim].start})

            slices[slice_number] = final_slice

            if padded_lon is not None:
                # shifted slice to adjust to padded lat/lon
                padded_slice = {s_dim: slice(
                    centers[slice_number][s_dim],
                    centers[slice_number][s_dim] + expansion * 2 + 1)}

                _mask = fill_in_mask(
                    padded_lon[padded_slice],
                    padded_lat[padded_slice],
                    shape
                )

            else:
                _mask = fill_in_mask(
                    full_lon[_slice],
                    full_lat[_slice],
                    shape
                )
            returned_masks.append(_mask)

    return [
        EOSlice(
            shape=shape,
            slices=_,
            mask=returned_masks[slice_number],
            site=code,
            coverage=0.,
            source_center_index=centers[slice_number])
        for slice_number, _ in enumerate(slices)
        if returned_masks[slice_number].any()]


