# encoding: utf-8
"""
felyx.backend.extract.slicers
-----------------------------

This module contains the code for creating extractions from cerbere
features. Each feature type (swath, grid etc.) has a 'slice' function
which enables the identification and generation of slices over a site for a
set of input coordinates.

Each type of cerbere datamodel supported must have a function called 'slicer'
in a file of the same name as the file that contains the cerbere datamodel
class. For example:


    cerbere.datamodels.cswath.Swath => swath.slicer

.. :copyright: Copyright 2013 Pelamis Scientific Software Ltd.
.. :license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: David Poulter <david.poulter@pelamis.co.uk>
.. codeauthor:: David Poulter <david.poulter@pelamis.co.uk>
"""
import typing as T

import numexpr
import numpy
import shapely
from shapely.geometry import Polygon
import xarray as xr


def mask_as_da(mask: numpy.ndarray, dims=T.Tuple[str]) -> xr.DataArray:
    """Return a colocation mask as a fully formed DataArray"""
    return xr.DataArray(
        data=mask.astype(numpy.int8),
        dims=tuple(dims),
        attrs={
            'long_name': 'region of interest, defined as the data within '
                         'the colocation radius',
            'flag_meanings': 'outside inside',
            'flag_values': numpy.array((0, 1)).astype(numpy.int8).tolist()})


# noinspection PyUnusedLocal
def get_centre_index(
        centre_lon: float,
        centre_lat: float,
        area_lon: xr.DataArray,
        area_lat: xr.DataArray):
    """
    Return the index of the closest pixel in area_lon and area_lat, to
    centre_lon and centre_lat.

    :param float centre_lon: The longitude of a dynamic site observation.
    :param float centre_lat: The latitude of a dynamic site observation.
    :param numpy.ndarray area_lon: The reduced longitude field
      (a subset removing points that cannot be the closest to the point).
    :param numpy.ndarray area_lat: The reduced latitude field
      (a subset removing points that cannot be the closest to the point).
    """
    # fill in masked lat/lon with locations far far away in another galaxy
    alon = area_lon.fillna(10000)
    alat = area_lat.fillna(10000)

    return numpy.unravel_index(
        numexpr.evaluate(
            '((centre_lon - alon) ** 2) + ((centre_lat - alat) ** 2)'
        ).argmin(),
        area_lon.shape
    )


def is_shape_complicated(shape):
    """
    Return true if the shape is not a simple rectangle with edges parallel to
    the latitude and longitude lines.

    :param shape: The shape to test.
    :type shape: :class:`shapely.geometry.Polygon`
    :return: True if the shape is not on a meridian-parallel contiguous
      rectangle on a regular projection.
    :rtype: bool
    """
    area = shape.bounds[0] - shape.bounds[2]
    area *= (shape.bounds[1] - shape.bounds[3])

    # Get epsilon to test equality between two floating point numbers
    area_type = type(area)
    shape_area_type = type(shape.area)
    area_eps = numpy.finfo(area_type).eps
    shape_area_eps = numpy.finfo(shape_area_type).eps
    eps = area_eps + shape_area_eps

    # Return the truth value
    return abs(area - shape.area) > eps


def get_regular_envelope(lons, lats, simple_grid=False):
    """Return a shapely shape equal to the envelope of regularly shaped
    miniprod. Much faster than evaluating a MultiPoint shape's envelope.

    :param bool simple_grid: True if grid is classical regular projections.
    :param numpy.ndarray lons: The longitudes of the points.
    :param numpy.ndarray lats: The latitudes of the points.
    :returns: The boundary of lats and lons.
    :rtype: :class:`shapely.geometry.Polygon`
    """
    if simple_grid:
        lon_min, lon_max = lons[0], lons[-1]
        lat_min, lat_max = lats[0], lats[-1]
    else:
        lon_min, lon_max = lons.min(), lons.max()
        lat_min, lat_max = lats.min(), lats.max()

    envelope = Polygon(
        (
            (lon_min, lat_min),
            (lon_max, lat_min),
            (lon_max, lat_max),
            (lon_min, lat_max),
            (lon_min, lat_min)
        )
    ).envelope
    return envelope


def get_disjoint_mask(
        shape: shapely.Geometry,
        lons: xr.DataArray,
        lats: xr.DataArray) -> numpy.ndarray:
    """Return a numpy mask for points that exist within the shape.

    :param shape: A FastShape object. FastShape objects
      will perform on average 1000 times faster than shapely shapes.
      Automatic conversion to FastShape will be applied.
    :type shape: :class:`FastShape`
    :param numpy.ndarray lons: An array of lons.
    :param numpy.ndarray lats: An array of lats.
    """
    lons = lons.values
    lats = lats.values

    mask = numpy.zeros(lons.shape, dtype=bool)
    valid = numpy.isfinite(lons)

    mask[valid] = shapely.contains_xy(shape, lons[valid], lats[valid])

    return mask
