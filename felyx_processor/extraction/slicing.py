# -*- coding: utf-8 -*-
import datetime
import logging
import typing as T

import numpy
import pandas as pd
import shapely.geometry

import felyx_processor.extraction
import felyx_processor.extraction.helpers
import felyx_processor.extraction.slicers.grid
import felyx_processor.extraction.slicers.swath
import felyx_processor.extraction.slicers.trajectory
from felyx_processor.extraction import EOSlice
from felyx_processor.extraction.source import SourceFile
from felyx_processor.felyx_data import get_felyx_data
from felyx_processor.miniprod import ChildMetadata, build_child_id
from felyx_processor.utils.configuration import (FelyxProcessorConfig,
                                                 get_extraction_settings)

LOGGER = logging.getLogger('felyx.processor')
SLICERS = {'cgrid': felyx_processor.extraction.slicers.grid,
           'cswath': felyx_processor.extraction.slicers.swath,
           'ctrajectory': felyx_processor.extraction.slicers.trajectory}

# Used to estimate the distance in degrees for the colocation limits
DEGREE_LATITUDE = 111.699  # TODO: document magic numbers
DEGREE_LONGITUDE = 111.321  # TODO: document magic numbers


class MissingSlicer(Exception):
    """"""
    def __init__(self, datamodel_class):
        """"""
        self.datamodel_class = datamodel_class


def get_slicer(source_file):
    """"""
    global SLICERS

    datamodel_class = source_file.feature.__module__.split('.')[-1].lower()
    if datamodel_class not in SLICERS:
        raise MissingSlicer(datamodel_class)

    return SLICERS[datamodel_class].slicer


def get_slice_time_coverage(
        granule: SourceFile,
        slice: EOSlice
) -> T.Union[T.Tuple[datetime.datetime, datetime.datetime], None]:
    """Return the min and max time within a source granule's slice"""
    # fill in child product metadata
    times = granule.feature.isel(
        slice.slices, padding=True).ds.time.to_masked_array().compressed()
    if times.size == 0:
        return

    return pd.to_datetime(times.min()), pd.to_datetime(times.max())


def static_sites(source_file, site_collections):
    """
    For each site, establish the slices and indexes of valid data.
    """
    slicer = get_slicer(source_file)

    # For both types of collections (private and public)
    # @TODO rename to result
    revised_collections = {}

    # For each collection, establish which sites are valid.
    for collection_id, sites in site_collections.items():
        children = []

        # For each site, establish a list of data ranges
        for code, shape in sites.items():

            # Calculate the slices
            slices = slicer(source_file.full_lon, source_file.full_lat,
                            shape, code,
                            dimensions=source_file.dimensions_hint)

            # Only add data which is valid
            if slices is not None:
                for _slice in slices:

                    slice_temporal_coverage = get_slice_time_coverage(
                        source_file, _slice)
                    if slice_temporal_coverage is None:
                        logging.info(
                            f'no valid times in data source for match {code}. '
                            f'skipped.')
                        continue
                    slice_start_dt, slice_end_dt = slice_temporal_coverage

                    children.append(ChildMetadata(
                        id=build_child_id(
                            slice_start_dt, code, source_file.dataset_id),
                        source=source_file.input_url.name,
                        source_path=source_file.input_url,
                        source_time_coverage_start=(
                            source_file.feature.ds.cb.time_coverage_start),
                        source_time_coverage_stop=(
                            source_file.feature.ds.cb.time_coverage_end),
                        time_coverage_start=slice_start_dt,
                        time_coverage_stop=slice_end_dt,
                        dataset=source_file.dataset_id,

                        site=_slice.site,
                        site_collection=collection_id,

                        slices=_slice.slices,
                        source_center_index=_slice.source_center_index,
                        coverage=_slice.coverage,
                        shape=_slice.shape))

        revised_collections[collection_id] = children

    return revised_collections


# noinspection PyUnresolvedReferences
def dynamic_sites(
        felyx_context: FelyxProcessorConfig,
        source_file: SourceFile,
        colocation_matches,
        all_trajectories
):
    """For each possible dynamic site, recalculate the shape, and hence
    actual slice and mask."""
    # Get the width of the dynamic miniprods, with a default of 25, but
    # which can be set per collection/dataset pair.

    # Establish list of all possible sizes. Variables must be established
    # for each of these.
    expansion = {}
    meshed_lon = {}
    meshed_lat = {}
    padded_lon = {}
    padded_lat = {}

    full_lon = source_file.feature.ds.cb.longitude
    full_lat = source_file.feature.ds.cb.latitude
    datamodel = source_file.feature.__class__.__name__

    # @TODO uses only max value in the following
    miniprod_sizes = felyx_context.child_product_sizes(source_file.dataset_id)

    for miniprod_size in miniprod_sizes:
        fixed_shape = miniprod_size
        expansion[miniprod_size] = int((fixed_shape - 1) / 2)

        if datamodel == 'Trajectory' or full_lat.ndim > 1:
            meshed_lon[miniprod_size] = None
            meshed_lat[miniprod_size] = None
        else:
            # @TODO Improve (account for order of dims in observation space)
            meshed_lon[miniprod_size] = full_lon.cb.broadcast(
                [full_lat, full_lon])
            meshed_lat[miniprod_size] = full_lat.cb.broadcast(
                [full_lat, full_lon])
            # meshed_lon[miniprod_size], meshed_lat[miniprod_size] = \
            #     numpy.meshgrid(full_lon, full_lat)

        # Generate these outside of the loop
        if fixed_shape:
            # @TODO replace with calls to cerbere isel()
            padded_lon[miniprod_size] = source_file.full_lon.pad(
                pad_width={
                    _: expansion[miniprod_size]
                    for _ in source_file.full_lon.dims},
                constant_values=-1000,
                mode='constant')

            padded_lat[miniprod_size] = source_file.full_lat.pad(
                pad_width={
                    _: expansion[miniprod_size]
                    for _ in source_file.full_lat.dims},
                constant_values=-1000,
                mode='constant')
        else:
            padded_lat[miniprod_size], padded_lon[miniprod_size] = \
                None, None

    revised_collections = {}

    # For each collection, establish which sites are valid.
    for collection_id in colocation_matches:
        colocated_sites = colocation_matches[collection_id]

        settings = get_extraction_settings(
            felyx_processor_config=felyx_context,
            dataset_id=source_file.dataset_id,
            site_collection_id=collection_id)

        miniprod_size = settings.subset_size

        trajectories = all_trajectories[collection_id]
        children = evaluate_dynamic_collection(
            felyx_context,
            source_file,
            collection_id,
            colocated_sites,
            trajectories,
            miniprod_size,
            full_lon, full_lat,
            padded_lon, padded_lat,
            meshed_lon, meshed_lat)

        if children is None:
            continue

        revised_collections[collection_id] = children

    return revised_collections


def evaluate_dynamic_collection(felyx_processor_config,
                                source_file,
                                collection_id, sites_shape,
                                trajectories, miniprod_size,
                                full_lon, full_lat,
                                padded_lon, padded_lat,
                                meshed_lon, meshed_lat
                                ) -> T.List[EOSlice]:
    """"""
    global DEGREE_LONGITUDE

    result = {}

    # local aliases
    extract_subset = felyx_processor.extraction.extract_subset
    distance = felyx_processor.extraction.helpers.distance

    slicer = get_slicer(source_file)

    datamodel = source_file.feature.__class__.__name__

    option_miniprod = get_extraction_settings(
        felyx_processor_config,
        source_file.dataset_id,
        collection_id)
    non_empty_vars = option_miniprod.non_empty_field
    padding = option_miniprod.padding
    coloc_radius = option_miniprod.matching_criteria.search_radius
    max_time_difference = option_miniprod.matching_criteria.time_window * 60

    if padding:
        collection_padded_lon = padded_lon[miniprod_size]
        collection_padded_lat = padded_lat[miniprod_size]
    else:
        collection_padded_lon = None
        collection_padded_lat = None

    # For each site, establish a list of data ranges
    for code, shape in sites_shape.items():

        # coloc_radius = limits[code]['collocation_radius']
        # max_time_difference = limits[code]['collocation_period'] * 60

        LOGGER.debug(f'Evaluate dynamic site: {code}')
        # Calculate first guess / initial data slice
        tmp_slices = slicer(
            source_file.full_lon,
            source_file.full_lat,
            shape.geometry,
            code,
            dimensions=source_file.dimensions_hint)

        if tmp_slices is None:
            LOGGER.warning(
                f'Could not find a suitable slice for site {code} and file '
                f'{source_file.input_url}')
            continue

        for tmp_slice in tmp_slices:

            # For each dynamic site location, establish if there is
            # any input data that is coincident.
            tmp_subset = extract_subset(
                tmp_slice, source_file.feature,
                padding=False, non_empty_vars=non_empty_vars)

            if not tmp_subset:
                continue

            time_difference = numpy.timedelta64(max_time_difference, 's')

            # noinspection PyUnresolvedReferences
            # TODO CR Faut-il faire un flatten ?
            #sat_times = tmp_subset.get_datetimes() => return un MaskedArray
            sat_times = tmp_subset.dataset.cb.time.to_masked_array().flatten()
            points = trajectories[code]

            # Loop around each point to establish the closest on
            # in time.
            point_index = None
            point_lat = None
            point_lon = None

            # skip miniprods with no valid time
            if sat_times.count() == 0:
                logging.info(f'No valid times in data for match with site'
                             f' {code}')
                continue

            # TODO CR alternative à compressed (ou conversion en NdArray) ?
            sat_times = tmp_subset.dataset.cb.time.cb\
                .to_masked_array().compressed()

            for index, point in enumerate(points):
                point_time = numpy.datetime64(point[2])

                if index > 0:
                    # skip close measurements to save time
                    tdelta_to_prev = numpy.abs(prev_time - point_time)
                    if tdelta_to_prev < numpy.timedelta64(1, 's'):
                        continue
                else:
                    prev_time = point_time

                delta = numpy.abs(sat_times - point_time).min().astype(
                    'timedelta64[s]')

                # Establish the most temporally coincident point
                # for any pixel
                if delta < time_difference:
                    point_index = index

                    # Create a new shape for the dynamic site,
                    # centred on the data observation
                    point_lon, point_lat = point[0], point[1]
                    if point_lon > 180.:
                        point_lon -= 360.
                    time_difference = delta

            if time_difference >= max_time_difference:
                continue

            if point_index is None:
                continue

            point = points[point_index]
            point_time = point[2]

            _tmp = (coloc_radius / DEGREE_LONGITUDE)
            lon_correlation = _tmp / numpy.cos(numpy.radians(point_lat))

            # Use a circle for dynamic sites
            _shape = shapely.geometry.Point(point_lon, point_lat)
            shape = _shape.buffer(lon_correlation)

            slices = slicer(
                source_file.full_lon,
                source_file.full_lat,
                shape,
                code,
                fixed_shape=miniprod_size,
                padded_lon=collection_padded_lon,
                padded_lat=collection_padded_lat,
                dimensions=source_file.dimensions_hint,
                meshed_lon=meshed_lon[miniprod_size],
                meshed_lat=meshed_lat[miniprod_size]
            )

            if not slices:
                # This occurs when a shape crosses is malformed, for example
                # when data contain filled geolocation values.
                LOGGER.warning(
                    f'Site {code} is thought to have valid data,  but no '
                    'subset was generated.')
                continue

            # Update slices with information about the
            # dynamic target.
            filtered_slices = []
            for _slice in slices:

                if full_lat.ndim > 1 or datamodel == 'Trajectory':
                    coloc_dist = distance(
                        point_lat, point_lon,
                        source_file.full_lat[_slice.source_center_index],
                        source_file.full_lon[_slice.source_center_index]
                    )
                else:
                    coloc_dist = distance(
                        point_lat, point_lon,
                        meshed_lat[miniprod_size][_slice.source_center_index],
                        meshed_lon[miniprod_size][_slice.source_center_index]
                    )

                # @TODO wrong (for instance for L3C files) - replace with
                #  closest
                times = source_file.feature.ds.time.isel(
                    _slice.source_center_index, missing_dims='ignore')
                if times.count() == 0:
                    logging.info(
                        f'invalid time in data source for match {code}. '
                        f'skipped.')
                    continue

                delta_dt = numpy.abs(
                    (times - numpy.datetime64(point_time))
                ).values.astype('timedelta64[s]')
                if delta_dt >= max_time_difference:
                    continue

                slice_temporal_coverage = get_slice_time_coverage(
                    source_file, _slice)
                if slice_temporal_coverage is None:
                    logging.info(
                        f'no valid times in data source for match {code}. '
                        f'skipped.')
                    continue
                slice_start_dt, slice_end_dt = slice_temporal_coverage

                child = ChildMetadata(
                    # TODO replace by call to get_name in storage/miniprod
                    id=build_child_id(
                        slice_start_dt, code, source_file.dataset_id),
                    source=source_file.input_url.name,
                    source_path=source_file.input_url,
                    source_time_coverage_start=(
                        source_file.feature.ds.cb.time_coverage_start),
                    source_time_coverage_stop=(
                        source_file.feature.ds.cb.time_coverage_end),
                    time_coverage_start=slice_start_dt,
                    time_coverage_stop=slice_end_dt,
                    dataset=source_file.dataset_id,

                    # @TODO change to site_collection_id
                    site=_slice.site,
                    site_collection=collection_id,
                    search_radius=coloc_radius,
                    time_window=max_time_difference,

                    slices=_slice.slices,
                    source_center_index=_slice.source_center_index,
                    coverage=_slice.coverage,
                    shape=_slice.shape,

                    # Store the dynamic target information for
                    # insertion into miniprods and metrics.
                    dynamic_target_distance=int(coloc_dist),
                    dynamic_target_latitude=point_lat,
                    dynamic_target_longitude=point_lon,
                    dynamic_target_time=point_time,
                    dynamic_target_time_difference=int(
                        time_difference / numpy.timedelta64(1, 's'))
                )

                filtered_slices.append(child)

            slices = filtered_slices
            if slices:

                LOGGER.debug(
                    f'matchups for {code}: {[_.slices for _ in slices]}')

                if code in result:
                    _msg = 'overwriting previous slices for site {}'
                    LOGGER.warning(_msg.format(code))
                    LOGGER.debug('Is this really ok?')  # TODO: check this

                result[code] = slices

    # flatten result into a list of child metadata
    returned_slices = []
    for site_id, slices in result.items():
        if len(slices) > 1:
            # @TODO use felyx exception
            raise ValueError(f'multiple children for site {code} and '
                             f'dataset {source_file.dataset_id}')
        if len(slices) == 1:
            returned_slices.append(slices[0])

    return returned_slices
