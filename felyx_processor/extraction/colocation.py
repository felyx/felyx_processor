# -*- coding: utf-8 -*-

import datetime
import logging
import typing as T

import numpy
import shapely.geometry
import shapely.wkt

from felyx_processor.extraction.source import SourceFile
from felyx_processor.felyx_data import get_felyx_data
from felyx_processor.miniprod.shape import GeographicPolygon
from felyx_processor.sites.insitu_factory import InSituDataSourceFactory
from felyx_processor.utils.configuration import (
    FelyxProcessorConfig, FelyxSystemConfig,
    get_extraction_settings, SiteCollectionConfig)

logger = logging.getLogger('felyx.processor')

TIME_FMT = '%Y-%m-%d %H:%M:%S'

# Used to estimate the distance in degrees for the colocation limits
DEGREE_LATITUDE = 111.699  # TODO: document magic numbers
DEGREE_LONGITUDE = 111.321  # TODO: document magic numbers


def static_sites(source_file, site_collections, allowed_sites=None):
    """Identify all the possible static sites which coincide spatially with
    this input file."""
    boundary_shape = source_file.data_shape

    revised_collections = {}

    for collection in site_collections:
        result = {}
        # @TODO prefer as a list, add id to site class
        for code, site in collection.sites.items():
            if (allowed_sites is not None) and code not in allowed_sites:
                continue
            if boundary_shape.geometry.intersects(site.shape):
                result[code] = site.shape
        if result == {}:
            continue
        revised_collections[collection.id] = result

    return revised_collections


def dynamic_sites(
        felyx_sys_config: FelyxSystemConfig,
        felyx_processor_config: FelyxProcessorConfig,
        source_file: SourceFile,
        site_collections: T.List[SiteCollectionConfig],
        allowed_sites=None):
    """Identify all the possible dynamic sites which coincide spatially and
    temporally with this input file."""
    dynamic_sites = {}
    boundary_shape = None

    # Find the start and end time of the input file
    stop_dt = source_file.time_coverage_end.replace(tzinfo=None)
    start_dt = source_file.time_coverage_start.replace(tzinfo=None)

    revised_collections = {}
    for site_collection in site_collections:

        result = {}

        settings = get_extraction_settings(
            felyx_processor_config=felyx_processor_config,
            dataset_id=source_file.dataset_id,
            site_collection_id=site_collection.id
        )

        # Find the temporal colocation window for this collection
        max_temporal_diff = settings.matching_criteria.time_window
        stop_time = stop_dt + datetime.timedelta(
            seconds=int(max_temporal_diff) * 60)
        start_time = start_dt - datetime.timedelta(
            seconds=int(max_temporal_diff) * 60)

        logger.debug(
            f'find_dynamic_matches time window: [{start_time}, {stop_time}]')

        # For each site collection, get a list of the dynamic sites
        # with obs in the time window
        ds = InSituDataSourceFactory.get_insitu_datasource(
            felyx_sys_config=felyx_sys_config,
            felyx_processor_config=felyx_processor_config,
            collection_code=site_collection.id)

        # @TODO keep as DataFrame : as_dataframe=true
        _trajectories = ds.get_trajectories(
            site_collection.id, allowed_sites, start_time,
            stop_time, False)

        # Set the site list to dynamic collection
        # felyx_data.set_sites_list(site_collection.id,
        #                           _trajectories.keys())

        # Store for later
        dynamic_sites[site_collection.id] = _trajectories

        # @TODO process as geodataframe
        for site, points in _trajectories.items():
            # For each valid site, build a shape of the maximum extent
            # of the trajectory taking into account the changing
            # colocation length with latitude
            if len(points) == 0:
                continue

            coloc_radius = settings.matching_criteria.search_radius
            points = numpy.array(points)
            lats = points[:, 1]
            lat_correlation = coloc_radius / DEGREE_LATITUDE

            lons = points[:, 0]
            lons[lons > 180] -= 360

            # @TODO: optimize this
            for ind, lon in enumerate(lons):
                dynamic_sites[site_collection.id][site][ind][0] = lon

            _tmp = (coloc_radius / DEGREE_LONGITUDE)
            lon_correlation = numpy.abs(
                _tmp / numpy.cos(numpy.radians(lats.mean())))

            lon_min = lons.min() - lon_correlation
            lon_max = lons.max() + lon_correlation

            lat_min = lats.min() - lat_correlation
            lat_max = lats.max() + lat_correlation

            shape = GeographicPolygon(
                shapely.geometry.box(lon_min, lat_min, lon_max, lat_max))

            if boundary_shape is None:
                boundary_shape = source_file.data_shape

            if boundary_shape is None:
                logger.warning('Cannot compute boundary shape')
                logger.warning(
                    f'Source file data shape is '
                    f'{source_file.data_shape.geometry}')
                logger.warning(
                    f'Computed lon and lat boundaries: {lon_min} '
                    f'{lat_min} {lon_max} {lat_max}')
                continue

            if boundary_shape.geometry.intersects(shape.geometry):
                result[site] = shape

        if result == {}:
            continue
        revised_collections[site_collection.id] = result

    results = revised_collections

    return results, dynamic_sites
