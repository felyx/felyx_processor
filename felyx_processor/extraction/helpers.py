# encoding: utf-8
"""
felyx_processor.miniprod.extract.helpers
-----------------------------------

Contains functions removed from the SourceFile class to improve import
performance (where they used to be methods).

.. :copyright: Copyright 2014 Pelamis Scientific Software Ltd.
.. :license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: David Poulter <david.poulter@pelamis.co.uk>
.. codeauthor:: David Poulter <david.poulter@pelamis.co.uk>
"""
import typing as T

import numpy
import shapely.geometry
from cerbere.feature.cgrid import Grid
from cerbere.feature.cswath import Swath
from cerbere.feature.ctrajectory import Trajectory

from felyx_processor.miniprod.shape import GeographicLine, GeographicPolygon

EARTH_MEAN_RADIUS = 6.37123E6  # meters


def distance(lat1, lon1, lat2, lon2):
    """Returns the distance in meter between two points"""
    lat1 = numpy.radians(lat1)
    lon1 = numpy.radians(lon1)
    lat2 = numpy.radians(lat2)
    lon2 = numpy.radians(lon2)

    dlat = lat1 - lat2
    dlon = lon1 - lon2

    a = ((numpy.sin(dlat / 2)) ** 2 +
         numpy.cos(lat2) * numpy.cos(lat1) * (numpy.sin(dlon / 2)) ** 2)
    c = 2 * numpy.arcsin(numpy.minimum(1, numpy.sqrt(a)))
    return c * EARTH_MEAN_RADIUS


def get_footprint_shape(
        feature: T.Union[Trajectory, Swath, Grid]
) -> T.Union[GeographicLine, GeographicPolygon]:
    """
    Return the external shapes of the input data.

    Args:
        feature: A Cerbere feature instance containing EO data.
    """
    lon, lat = feature.dataset.cb.longitude, feature.dataset.cb.latitude

    if isinstance(feature, Trajectory):
        return GeographicLine([(lon, lat)])

    elif isinstance(feature, Swath):
        return GeographicPolygon([(lon, lat)])

    elif isinstance(feature, Grid):

        if not feature.is_cylindrical():
            return GeographicPolygon([(lon, lat)])

        # approximate resolution
        # @TODO : replace with cerbere method
        lon_res = (numpy.fabs(lon[1] - lon[0])) / 2.
        lat_res = (numpy.fabs(lat[1] - lat[0])) / 2.

        # bounding box
        poly = shapely.geometry.box(
            lon.min() - lon_res, lat.min() - lat_res,
            lon.max() + lon_res, lat.max() + lat_res, ccw=True)

        return GeographicPolygon(poly)

    else:
        raise NotImplementedError(f'feature {feature.__class__} not supported')
