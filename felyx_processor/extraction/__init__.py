# -*- coding: utf-8 -*-
import itertools
import logging
import re
import typing as T
from typing import Dict, Generator, List, Tuple, Optional, Union

import numpy as np
import shapely
from pydantic import BaseModel
import xarray as xr

from cerbere.feature.cbasefeature import BaseFeature

from felyx_processor.extraction.source import SourceFile
from felyx_processor.felyx_data import get_felyx_data
from felyx_processor.utils.configuration import (
    ContentSelectionConfig,
    FelyxProcessorConfig,
    FelyxSystemConfig,
    get_extraction_settings,
    SiteCollectionConfig
)
from felyx_processor.miniprod import ChildMetadata


LOGGER = logging.getLogger('felyx.processor')


class NoMiniprod(Exception):
    """"""


class NoExtraction(Exception):
    """"""
    def __init__(self, dataset, dynamic, allowed_collections):
        """"""
        self.dataset = dataset
        self.dynamic = dynamic
        self.allowed_collections = allowed_collections


class EOSlice(BaseModel):
    """Metadata defining a slice from a EO granule"""
    shape: shapely.Geometry
    slices: T.Dict[str, slice]
    mask: xr.DataArray
    site: str
    coverage: float
    source_center_index: T.Optional[T.Dict[str, int]] = None

    class Config:
        """Validate constraints on assignment"""
        validate_default = True
        validate_assignment = True
        arbitrary_types_allowed = True


def haversine(lon1, lat1, lon2, lat2):
    """Return the spherical distance in km between two points or a point and
    a list of points"""
    lon1, lat1, lon2, lat2 = map(np.radians, [lon1, lat1, lon2, lat2])
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = np.sin(dlat / 2) ** 2\
        + np.cos(lat1) * np.cos(lat2) * np.sin(dlon / 2) ** 2

    return 2 * 6371 * np.arcsin(np.sqrt(a))


def get_disjoint_mask(
        shape: shapely.Geometry,
        lons: xr.DataArray,
        lats: xr.DataArray) -> np.ndarray:
    """Return a numpy mask for points that exist within the shape.

    :param shape: A FastShape object. FastShape objects
      will perform on average 1000 times faster than shapely shapes.
      Automatic conversion to FastShape will be applied.
    :type shape: :class:`FastShape`
    :param numpy.ndarray lons: An array of lons.
    :param numpy.ndarray lats: An array of lats.
    """
    lons = lons.values
    lats = lats.values

    mask = np.zeros(lons.shape, dtype=bool)
    valid = np.isfinite(lons)

    mask[valid] = shapely.contains_xy(shape, lons[valid], lats[valid])

    return mask


def colocation_mask(
        shape: Union[shapely.Geometry, Tuple[float, float, float]],
        feature: BaseFeature
) -> xr.DataArray:
    """Return the colocation mask, indicating the feature data within the
    shape boundaries, given as a geometry or a center point and radius

    Parameters
    ----------
    shape: shapely.Geometry, tuple
        the geometry of the site colocation area or a tuple (center lon,
        center lat, radius)
    feature: BaseFeature
        the (cerbere) feature overlapping the colocation area

    Returns
    -------
    mask: xr.DataArray
        a fully formed DataArray where ones are the values within the
        shape boundaries
    """
    if feature.ds.cb.longitude.dims == feature.ds.cb.latitude.dims:
        b_dims = feature.ds.cb.longitude.dims
        full_lon = feature.ds.cb.longitude
        full_lat = feature.ds.cb.latitude
    else:
        b_dims = [feature.ds.cb.longitude.name, feature.ds.cb.latitude.name]
        full_lon = feature.ds.cb.longitude.cb.broadcast(b_dims, feature)
        full_lat = feature.ds.cb.latitude.cb.broadcast(b_dims, feature)

    if isinstance(shape, tuple):
        c_lon, c_lat, radius = shape
        mask = haversine(c_lon, c_lat, full_lon, full_lat) <= radius
    else:
        mask = get_disjoint_mask(shape, full_lon, full_lat)

    return xr.DataArray(
        data=mask.astype(np.int8),
        dims=tuple(b_dims),
        attrs={
            'long_name': 'region of interest, defined as the data within '
                         'the colocation radius',
            'flag_meanings': 'outside inside',
            'flag_values': np.array((0, 1)).astype(np.int8).tolist()})


def extract_subset(
        site_info: ChildMetadata,
        feature: BaseFeature,
        padding: bool = True,
        non_empty_vars: List[str] = None
) -> Optional[BaseFeature]:
    """Extract a subset from the input feature and return it.

    Provides a unified calling sequence for all feature types.

    Parameters
    ----------
    site_info: ChildMetadata
        The shape and name of a site.
    feature: BaseFeature
        The Cerbere feature object from which to extract data.
    padding: bool, optional
        apply padding in miniprod extraction?
    non_empty_vars: list(str), optional
        Cancel extraction if no valid data in the named variables.

    Returns
    -------
    feature: BaseFeature
        An extract of the input feature
    """
    index = site_info.slices

    subset = feature.isel(indexers=index, padding=padding)
    subset.ds.attrs.update({'slices': str(index)})

    # if not further check is required, just return the result
    if non_empty_vars is None:
        return subset

    # if check cannot be performed, just return the result
    _non_empty_vars = []
    for var in non_empty_vars:
        if var not in subset.ds:
            LOGGER.debug(
                f'The file has no variable {var}, so felyx cannot test for '
                f'empty child products with this variable')
            continue
        _non_empty_vars.append(var)

    if len(_non_empty_vars) == 0:
        return subset

    mask = colocation_mask(site_info.shape, subset)

    # Check that the non-emptiness constraint is respected
    for v in _non_empty_vars:

        site_has_valid_values = subset.ds[v].where(
            mask.broadcast_like(subset.ds[v])).count()
        subset_has_valid_values = subset.ds[v].count()

        if not site_has_valid_values:
            if subset_has_valid_values:
                LOGGER.info(
                    f'Skipped a child product at site {site_info.site} as '
                    f'variable "{v}" has no valid data within the felyx site '
                    f'boundaries, although it does contain valid data outside '
                    f'the felyx site).')
            else:
                LOGGER.info(
                    f'Skipped a child product at site {site_info.site} as '
                    f'entire variable "{v}" is empty in the subsetted data.')
            return

    # subset satisfies requirements, return result
    # TODO checl correct DataArray with attributes
    subset.ds['__colocation_mask'] = mask

    return subset


def extract_found_subsets_from_eo_file(
    felyx_context: FelyxProcessorConfig,
    source_file: SourceFile,
    extractions: Dict[str, dict]
) -> Generator[Tuple, None, None]:
    """Extract the pre-identified subsets from their source EO file

    Parameters
    ----------
    felyx_context:
        Felyx processor configuration and site data
    source_file:
        An instance of a SourceFile class.
    extractions:
        slices by sites and by collections

    Yields:
        BaseFeature: subset from EO file
        str: site id
        dict: info about site
        str: collection id
        collection: configuration of collection

    """
    for collection_id, sites in extractions.items():
        collection = felyx_context.SiteCollections[collection_id]

        settings = get_extraction_settings(
            felyx_processor_config=felyx_context,
            dataset_id=source_file.dataset_id,
            site_collection_id=collection_id
        )
        non_empty_field = settings.non_empty_field
        padding = settings.padding

        # Loop around each evaluated site in the collection
        # @TODO children instead of sites
        for site_info in sites:
            # Extract the subset
            subset = extract_subset(
                site_info, source_file.feature,
                padding=padding, non_empty_vars=non_empty_field)

            yield (subset, site_info.site, site_info, collection_id,
                   collection)


def get_slices(
        felyx_sys_config: FelyxSystemConfig,
        felyx_processor_config: FelyxProcessorConfig,
        source_file: SourceFile,
        site_collections: List[SiteCollectionConfig],
        allowed_sites: Optional[List] = None) -> Dict[str, ChildMetadata]:
    """Returns the properties of the subsets from the source EO file matching
    the sites of the given site collections.

    Args:
        felyx_sys_config: Felyx system configuration
        felyx_processor_config: Felyx processor configuration
        source_file: An instance of a SourceFile class.
        site_collections: Dictionary containing the collection to process
        allowed_sites ():

    Returns:
        tuple:
            BaseFeature: subset from EO file
            str: site id
            dict: info about site
            str: collection id
            collection: configuration of collection

    """

    # Import here to avoid circular dependencies issues
    # local aliases
    import felyx_processor.extraction.colocation as colocate
    import felyx_processor.extraction.slicing as find_slices

    dynamic_collections = [_ for _ in site_collections if not _.static]
    static_collections = [_ for _ in site_collections if _.static]

    slices = {}

    if len(dynamic_collections) > 0:
        colocations, trajectories = colocate.dynamic_sites(
            felyx_sys_config=felyx_sys_config,
            felyx_processor_config=felyx_processor_config,
            source_file=source_file,
            site_collections=dynamic_collections,
            allowed_sites=allowed_sites)
        colocated_slices = find_slices.dynamic_sites(
            felyx_context=felyx_processor_config,
            source_file=source_file,
            colocation_matches=colocations,
            all_trajectories=trajectories
        )
        slices.update(colocated_slices)

    if len(static_collections) > 0:
        colocations = colocate.static_sites(source_file,
                                            static_collections,
                                            allowed_sites)
        colocated_slices = find_slices.static_sites(source_file, colocations)
        slices.update(colocated_slices)

    return slices


def select_attributes(dst, attrs, excluded):
    """Return the dataset after adding and dropping the selected and excluded
    attributes respectively
    """
    # selected attributes
    cfg_attrs = attrs
    sel_attrs = list(dst.attrs.keys())
    if cfg_attrs is not None:
        sel_attrs = list(itertools.chain.from_iterable(
            [list(filter(re.compile(_).match, sel_attrs))
             for _ in cfg_attrs]))

    # dropped attributes
    cfg_exc_attrs = excluded
    if cfg_exc_attrs is not None:
        exc_attrs = itertools.chain.from_iterable(
            [list(filter(re.compile(_).match, sel_attrs))
             for _ in cfg_exc_attrs])
        sel_attrs = list(set(sel_attrs).difference(
            set(list(exc_attrs))))

    # ensure unique names in list
    sel_attrs = list(set(sel_attrs))

    dst.attrs = {attr: dst.attrs[attr] for attr in sel_attrs}

    return dst


def select_variables(dst, variables, excluded):
    """Return the list of selected variables to extract from a dataset"""
    # selected variables
    cfg_variables = variables
    sel_variables = list(dst.variables.keys())
    if cfg_variables is not None:
        sel_variables = list(itertools.chain.from_iterable(
            [list(filter(re.compile(_).match, sel_variables))
             for _ in cfg_variables]))

    # dropped variables
    cfg_exc_variables = excluded
    if cfg_exc_variables is not None:
        exc_variables = itertools.chain.from_iterable(
            [list(filter(re.compile(_).match, sel_variables))
             for _ in cfg_exc_variables])
        sel_variables = list(set(sel_variables).difference(
            set(list(exc_variables))))

    # ensure unique names in list
    sel_variables = list(set(sel_variables))

    if len(sel_variables) == 0:
        raise ValueError(
            'No variables were selected. You should revise your configuration '
            'file.')

    return dst[sel_variables]


def tailor(dst: xr.Dataset,
           content_cfg: ContentSelectionConfig,
           prefix: str = ''
           ) -> xr.Dataset:
    """Returns a tailored child product (or stack of products) from a given EO
    dataset.

    Only the requested variables and attributes are returned (from `variables`
    and `except_variables` configuration settings).

    Returned variables and attributes are possibly prefixed (`prefix` setting).
    """
    dst = dst.copy(deep=False)

    # selected variables
    dst = select_variables(
        dst,
        content_cfg.variables,
        content_cfg.except_variables
    )

    # attributes to transform into variables
    for att in content_cfg.attributes_as_variables:
        if att not in dst.attrs:
            raise KeyError(f'{att} was not found in dataset attributes: '
                           f'{dst.attrs}')
        dst[att] = xr.DataArray(
            data=str(dst.attrs.pop(att)),
            attrs={
                'comment': f'traced value of original dataset attribute `{att}`'
            }
        )

    # selected attributes
    dst = select_attributes(
        dst,
        content_cfg.attributes,
        content_cfg.except_attributes
    )

    # prefix variables, dimensions, and attributes, except those used for
    # jointure
    if content_cfg.prefix is not None:
        prefix = f'{content_cfg.prefix}_'

    if prefix != '':
        dst = dst.rename(
            {v: f'{prefix}{v}'
             for v in set(dst.variables) - {'obs', '__site_id', '__site_time'}}
        ).rename_dims(
            {d: f'{prefix}{d}' for d in set(dst.dims) - {'obs'}}
        )
        dst.attrs = {prefix + _: dst.attrs[_] for _ in dst.attrs}

    # custom attributes
    dst.attrs.update(content_cfg.custom_attributes)

    return dst
