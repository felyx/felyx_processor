# encoding: utf-8
"""
es_metrics_datasource
--------------------------------------

Contains class for retrieve metrics data from Elastic Search

"""
import logging
from datetime import datetime
from pathlib import Path
from typing import Dict, List, Tuple, Union

import pandas as pd
from pandas import DataFrame

import felyx_processor.metrics.metrics_datasource as metrics_datasource
from felyx_processor.index.es.es_client import ESClient, ESSearchQuery
from felyx_processor.metrics.metrics_datasource import MetricsDataSource
from felyx_processor.utils.configuration import ElasticSearchConfig
from felyx_processor.utils.exceptions import FelyxProcessorError

try:
    pass
except ImportError:
    pass

# ES_OPERATIONS  : keys of the operations discribed in the ES Configuration
# for metrics data """
ES_METRICS_SEARCH = 'metrics_search'
ES_METRICS_INDEX = 'metrics_index'
ES_METRICS_DELETE = 'metrics_delete'

# METRICS INDEX PATH
ES_INDEX_METRICS_PATH = 'metrics'

# RELATIVE PATH TO METRICS INDEX MAPPINGS
INDEX_METRICS_MAPPINGS_RELATIVE_PATH = 'share/es/metrics_mappings.json'

# VALID_METRIC_FIELDS : list of the authorized to register fields
VALID_METRIC_FIELDS = (
                        'site', 'collection', 'dataset', 'time_coverage',
                        'shape', 'data', 'miniprod'
                        )
VALID_DATA_FIELDS = ('name', 'value', 'bool_value', 'txt_value', 'num_value',
                     'field', 'dims',)
VALID_DIMS_FIELDS = ('name', 'value',)

TIME_FMT = '%Y-%m-%d %H:%M:%S'

LOG = logging.getLogger('felyx.processor')


class ESMetricsDataSource(MetricsDataSource):
    """Elastic Search Metrics Data Source"""

    def __init__(self,
                 yaml_config: ElasticSearchConfig = None):
        """Elasticsearch Data Source constructor.

         Args:
            yaml_config: The YAML ES configuration.
        """
        super().__init__()

        self.__config = yaml_config
        if self.__config is None:
            self.__config = ElasticSearchConfig()

        self.__index_path = '{}{}'.format(
        self.__config.prefix, ES_INDEX_METRICS_PATH)

        # Create metrics index
        self.create_metrics_index()

    def _from_metrics_response(self,
                               es_response: List[Dict],
                               metrics: List[str] = None
                               ) -> Dict:
        """
        Convert the ES response to dictionary.

        Args:
            es_response: The ES metrics results.
            metrics: (optional) The parameter list to keep.

        Returns:
            The converted dictionary.
        """

        # Metrics are tied to an extraction, i.e. a (dataset, collection) couple
        # , and several collections can share the same products.
        # There is one document per extraction in the ES metrics index, meaning
        # that the metrics associated with one miniprod are potentially spread
        # across several ES documents.
        # In order to regroup all the metrics associated with a miniprod, it
        # is necessary to loop over all results, and merge the results which
        # have the same dataset, site and start datetime (these fields are
        # enough to uniquely identify one miniprod in most cases).
        tmp = dict()
        _available_metrics = dict()
        for hit in es_response:
            dataset = hit['_source']['dataset']
            site = hit['_source']['site']
            start_str = hit['_source']['time_coverage']['gte']
            if dataset not in tmp:
                tmp[dataset] = dict()
            if site not in tmp[dataset]:
                tmp[dataset][site] = dict()
            if start_str not in tmp[dataset][site]:
                tmp[dataset][site][start_str] = dict()

            metric_results = hit['_source']['data']
            for metric_data in metric_results:
                metric_name = metric_data['name']

                _available_metrics[metric_name] = True

                # Only keep requested metrics
                if (metrics is not None) and (metric_name not in metrics):
                    continue

                # Extract metric value
                metric_value = None
                if 'txt_value' in metric_data:
                    metric_value = metric_data['txt_value']
                elif 'num_value' in metric_data:
                    metric_value = metric_data['num_value']
                elif 'bool_value' in metric_data:
                    metric_value = metric_data['bool_value']
                elif 'no_value' in metric_data:
                    metric_value = None  # same as default value

                # Store metric
                tmp[dataset][site][start_str][metric_name] = metric_value

        if metrics is None:
            metrics = list(_available_metrics.keys())

        # Reformat the results and use placeholders (None) when one of the
        #  requested metrics is not available.
        # For each site there is a dictionary of metrics. Each metric is
        # associated with a list of values. The size of these lists *must* match
        # the number of items in the "times" list, i.e. the *must* be one value
        # per metric and per miniprod extracted for each site.
        result = dict()
        for dataset in tmp:
            result[dataset] = dict()
            for site in tmp[dataset]:
                result[dataset][site] = {'times': [], 'metrics': {}}

                for metric_name in metrics:
                    result[dataset][site]['metrics'][metric_name] = []

                for start_str in tmp[dataset][site]:
                    _start = start_str.replace(' ', '').replace('-', '')
                    _start = _start.replace(':', '')

                    tmp_metrics = tmp[dataset][site][start_str]
                    for metric_name in metrics:
                        value = None
                        if metric_name in tmp_metrics:
                            value = tmp_metrics[metric_name]
                        result[dataset][site]['metrics'][metric_name].append(
                            value)

                    result[dataset][site]['times'].append(start_str)

        return result

    def get_metrics(self,
                    datasets: List[str],
                    sites_codes: List[str],
                    start: datetime,
                    stop: datetime,
                    wkt_coords: List[List[float]] = None,
                    constraints_list: List[Dict[str, str]] = None,
                    metrics: List[str] = None,
                    as_dataframe: bool = False
                    ) -> Union[Dict, DataFrame]:
        """
         Search Metrics data in a list of datasets and list of sites over
            a timespan and spatial filter.

        Args:
            datasets: The list of datasets to search.
            sites_codes: The list of sites codes to search.
            start: The begin time coverage.
            stop: the end time coverage.
            wkt_coords: (optional) The spatial filter defined by a list of
            coordinates of a polygon.
            constraints_list: (optional) Additional data filters.
            metrics: (optional) The list of metrics parameters to extract.
            as_dataframe: If True, returns a dataframe,
            if False, returns a dictionary.

        Returns:
            if as_dataframe is set to True :
                a pandas dataframe with following columns :
                dataset, site, time, param1, param2 ...
            else :
                a dictionary formatted as following :
                    {'dataset_name':
                        {'site_code':
                            {'times': ['YYYY-MM-DD HH:MM:SS',...],
                            'metrics': {'measurement_1': [float_value,...],
                            'measurement_2': [float_value,...], ...
                            }
                        }
                    }

        Raises:
            FelyxProcessorError: If retrieve failed.
        """

        # Create search query
        terms_filters = dict()
        if sites_codes is not None:
            terms_filters['site'] = sites_codes

        if datasets is not None:
            terms_filters['dataset'] = datasets

        range_filters = dict()
        range_filters['time_coverage'] = (start, stop)

        fields = ['site', 'dataset', 'time_coverage', 'data.name',
                  'data.no_value',
                  'data.num_value', 'data.bool_value', 'data.txt_value']

        query = ESSearchQuery(fields, terms_filters, range_filters, wkt_coords,
                              constraints_list)
        clientES = ESClient(self.__config)

        try:
            response = clientES.search(self.__index_path, query,
                                       ES_METRICS_SEARCH)
        except FelyxProcessorError as e:
            LOG.error('Failed to retrieve metrics data for datasets'.format(
                                                                    datasets))
            if sites_codes is not None:
                LOG.error('Request was limited to the following sites: {}'.
                          format(', '.join(sites_codes)))
            raise e

        # Extract data from response and keep only desired metrics
        output = self._from_metrics_response(response, metrics)

        if as_dataframe is True:
            # Convert the metrics dict to dataframe
            metrics_df = self.convert_metrics_to_dataframe(output)
            return metrics_df

        return output


    def convert_metrics_to_dataframe(
            self,
            metrics: Dict
    ) -> DataFrame:
        """
            Convert metrics dictionary to pandas dataframe.

            Args:
                metrics: the dictionary to convert.

            Returns:
                A pandas dataframe with columns :
                dataset, site, time, param1, param2 ...
        """

        metrics_df = DataFrame()

        # For each dataset
        for dataset in metrics:
            # For each site
            site_df = DataFrame()
            for site in metrics[dataset]:
                dataframe = DataFrame(metrics[dataset][site]['metrics'])
                dataframe[metrics_datasource.DATASET_COLUMN] = dataset
                dataframe[metrics_datasource.SITE_COLUMN] = site

                # Convert string dates to datetimes
                times = list(map(lambda x: datetime.strptime(x, TIME_FMT),
                                 metrics[dataset][site]['times']))
                dataframe[metrics_datasource.TIME_COLUMN] = times
                dataframe.sort_values(inplace=True,
                                      by=[metrics_datasource.TIME_COLUMN])
                site_df = pd.concat([site_df, dataframe], ignore_index=True)

            metrics_df = pd.concat([metrics_df, site_df], ignore_index=True)

        return metrics_df

    def get_metrics_id(self,
                       metric_dict : Dict
                       ) -> str:
        """
        Compute the metrics id.

        Args:
            metric_dict: A dictionary containing metrics data.

        Returns:
            The metric id.

        Raises:
            FelyxProcessorError: If computing metric id failed.
        """
        # There can be only one miniproduct extracted from a
        # (dataset, site, time) tuple (even if several files from the dataset
        # are colocated with a site, their time coverage will differ) but a
        # single miniproduct can be linked to several collections and thus
        # several metrics sets so it is mandatory to include the collection in
        # the identifier.
        try:
            dt_str = metric_dict['time_coverage'][0]
            dt_str = dt_str.replace(' ', '').replace('-', '').replace(':', '')
            _id = '{}_{}_{}_{}'.format(metric_dict['collection'],
                                       metric_dict['dataset'],
                                       metric_dict['site'],
                                       dt_str)
        except Exception as e:
            msg = 'Error when computing metric id : {}'.format(e)
            LOG.error(msg)
            raise FelyxProcessorError(msg)
        return _id


    def _dims2doc(self,
                  dims: Dict
                  ) -> Dict:
        """
        Check the valid dimension fields.

        Args:
            dims: A dictionary containing the dimension fields.

        Returns:
            A dictionary with the valid dimension fields.

        Raises:
            FelyxProcessorError: If invalid metric dimension field.
        """
        result = {}
        for key, value in iter(dims.items()):
            if key not in VALID_DIMS_FIELDS:
                msg = 'Invalid metric dimension field : {}. ' \
                      'Valid fields are : {}'.format(key, VALID_DIMS_FIELDS)
                LOG.error(msg)
                raise FelyxProcessorError(msg)
            result[key] = value
        return result

    def _data2doc(self,
                  data: Dict
                  ) -> Dict:
        """
           Convert the document data fields.

           Args:
                data: A dictionary containing data fields.

           Returns:
               A dictionary with converted data.

            Raises:
                FelyxProcessorError: If invalid metric data field.
        """
        result = {}
        for key, value in iter(data.items()):
            if key not in VALID_DATA_FIELDS:
                msg = 'Invalid metric data field : {}. Valid fields are : {}'.\
                    format(key, VALID_DATA_FIELDS)
                LOG.error(msg)
                raise FelyxProcessorError(msg)

            if 'dims' == key:
                value = [self._dims2doc(_) for _ in value]

            # Adapt field name to value type
            if 'value' == key:
                if value is None:
                    key = 'no_value'
                    value = True
                elif isinstance(value, bool):
                    key = 'bool_value'
                elif isinstance(value, str):
                    key = 'txt_value'
                else:
                    key = 'num_value'
            result[key] = value
        return result

    def _timecoverage2doc(self,
                          time_coverage: Tuple
                          ) -> Dict:
        """
           Convert and check time coverage.

           Args:
               time_coverage: A tuple with start and end date.

           Returns:
               A dictionary with time coverage range.

            Raises:
                FelyxProcessorError: If invalid time coverage field.
        """
        result = {}
        if 2 != len(time_coverage):
            msg = 'Invalid time coverage field'
            LOG.error(msg)
            raise FelyxProcessorError(msg)

        for i, key in enumerate(('gte', 'lte',)):
            value = time_coverage[i]
            if isinstance(value, datetime):
                result[key] = value.strftime(TIME_FMT)
            else:
                value = value.replace('T', ' ').replace('Z', '')
                result[key] = value

        return result

    def convert2doc(self,
                    metric_dict: Dict
                    ) -> Dict:
        """
           Convert the metrics document.

           Args:
                metric_dict: A dictionary containing the metrics data.
           Returns:
                A dictionary with converted data.

            Raises:
                FelyxProcessorError: If invalid metric field.
        """
        result = {}
        for key, value in iter(metric_dict.items()):
            if key not in VALID_METRIC_FIELDS:
                msg = 'Invalid metric field to index : {} ' \
                      'allowed fields are : {}'.format(key, VALID_METRIC_FIELDS)
                LOG.error(msg)
                raise FelyxProcessorError(msg)

            if 'data' == key:
                result[key] = [self._data2doc(_) for _ in value]
            elif 'time_coverage' == key:
                result[key] = self._timecoverage2doc(value)
            elif isinstance(value, datetime):
                result[key] = value.strftime(TIME_FMT)
            else:
                result[key] = value

        return result

    def register_metrics(self,
                         metrics: List[Dict]
                         ):
        """ Register metrics data.

        Args:
            metrics: The list of metrics to register as a list of dictionaries
            formatted as following :
                {
                    'shape': 'POLYGON((Longitude Latitude, ...))',
                    'time_coverage': ['YYYY-MM-DD HH:MM:SS',
                    'YYYY-MM-DD HH:MM:SS'],
                    'site': 'site_code',
                    'dataset': 'dataset_name',
                    'collection': 'collection_code',
                    'data': [
                            {
                            'name': 'parameter_name',
                            'value': double_value
                            },
                            ...
                            ],
                    'miniprod': 'miniprod_id'
                }

        Raises:
            FelyxProcessorError: If register failed.
        """
        if 0 >= len(metrics):
            LOG.warning('Cannot register metrics from an empty list...')
            return

        LOG.debug('Convert dicts to indexable documents')

        # Index documents in Elastic search
        clientES = ESClient(self.__config)

        try:
            docs = [(self.get_metrics_id(x), self.convert2doc(x))
                    for x in metrics]

            response = clientES.index(self.__index_path, docs, ES_METRICS_INDEX)
            LOG.debug(response)
        except FelyxProcessorError as e:
            LOG.error('Failed to register metrics')
            raise e

    def create_metrics_index(self):
        """
            Create the metrics index if it does not exist

            Raises:
                FelyxProcessorError: If create index failed.
        """

        # Create metrics index in Elastic search
        clientES = ESClient(self.__config)
        mappings_path = Path(__file__).parent.parent / \
                        INDEX_METRICS_MAPPINGS_RELATIVE_PATH

        try:
            # Check if the index already exists, if not, create it
            if not clientES.has_index(self.__index_path):
                response = clientES.create_index(self.__index_path,
                                                 mappings_path)
                LOG.debug(response)
        except FelyxProcessorError as e:
            LOG.error('Failed to create metrics index : {}'.
                      format(self.__index_path))
            raise e

    def delete_metrics_index(self):
        """
            Delete the metrics index

            Raises:
                FelyxProcessorError: If delete index failed.
        """
        # Delete index in Elastic search
        clientES = ESClient(self.__config)
        try:
            # Check if the index exists before deleting
            if clientES.has_index(self.__index_path):
                response = clientES.delete_index(self.__index_path)
                LOG.debug(response)
        except FelyxProcessorError as e:
            LOG.error('Failed to delete metrics index : {}'.format(
                self.__index_path))
            raise e

    def delete_metrics(self,
                             miniprods_ids : List[str]
                             ) -> int:
        """
            Delete metrics data refering to a list of miniprod ids

            Args:
                miniprods_ids: The list of miniprod ids.

            Returns:
                The number of deleted metrics

            Raises:
                FelyxProcessorError: If delete failed.
        """
        if 0 >= len(miniprods_ids):
            LOG.warning('Cannot delete metrics from an empty  miniprod list...')
            return 0

        # Index documents in Elastic search
        clientES = ESClient(self.__config)

        # Create search query
        terms_filters = dict()
        terms_filters['miniprod'] = miniprods_ids
        fields = ['miniprod']

        query = ESSearchQuery(fields, terms_filters)

        try:
            nb_metrics = clientES.delete_by_query(self.__index_path, query,
                                                  ES_METRICS_DELETE)
        except FelyxProcessorError as e:
            LOG.error('Failed to delete metrics')
            raise e

        return nb_metrics

    def get_metrics_ids_by_miniprods_ids(self,
                                         miniprods_ids: List[str]) -> List[str]:
        """
            Get metrics ids refering to a list of miniprod ids.

            Args:
                miniprods_ids: The list of miniprod ids.

            Returns:
                The list of metrics ids.

            Raises:
                FelyxProcessorError: If retrieve failed.
        """

        # Create search query
        terms_filters = dict()
        terms_filters['miniprod'] = miniprods_ids
        fields = ['miniprod']

        query = ESSearchQuery(fields, terms_filters)
        clientES = ESClient(self.__config)

        try:
            response = clientES.search(self.__index_path, query,
                                       ES_METRICS_SEARCH)

            metrics_ids = list(map(lambda x: x['_id'], response))

        except FelyxProcessorError as e:
            LOG.error('Failed to retrieve metrics ids for miniprods ids')
            LOG.debug('Request was limited to the following miniprods ids: {}'
                      .format(', '.join(miniprods_ids)))
            raise e

        return metrics_ids
