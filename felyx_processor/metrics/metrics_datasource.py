# encoding: utf-8
"""
metrics_datasource
--------------------------------------

Contains base class for retrieve metrics data from different sources

"""
import abc
import datetime
from typing import Dict, List, Union

from pandas import DataFrame

# COLUMNS MAPPING
TIME_COLUMN = 'time'
SITE_COLUMN = 'site'
DATASET_COLUMN = 'dataset'


class MetricsDataSource(metaclass=abc.ABCMeta):
    """Metrics Data Source Interface"""

    @abc.abstractmethod
    def get_metrics(
        self,
        datasets: List[str],
        sites_codes: List[str],
        start: datetime,
        stop: datetime,
        wkt_coords: List[List[float]] = None,
        constraints_list: List[Dict[str, str]] = None,
        metrics: List[str] = None,
        as_dataframe: bool = False
    ) -> Union[Dict, DataFrame]:
        """
        Search Metrics data in a list of datasets and list of sites over
            a timespan and spatial filter.

        Args:
            datasets: The list of datasets to search.
            sites_codes: The list of sites codes to search.
            start: The begin time coverage.
            stop: the end time coverage.
            wkt_coords: (optional) The spatial filter defined by a list of
            coordinates of a polygon.
            constraints_list: (optional) Additional data filters.
            metrics: (optional) The list of metrics parameters to extract.
            as_dataframe: If True, returns a dataframe,
            if False, returns a dictionary.

        Returns:
            if as_dataframe is set to True :
                a pandas dataframe with following columns :
                dataset, site, time, param1, param2 ...
            else :
                a dictionary formatted as following :
                    {'dataset_name':
                        {'site_code':
                            {'times': ['YYYY-MM-DD HH:MM:SS',...],
                            'metrics': {'measurement_1': [float_value,...],
                            'measurement_2': [float_value,...], ...
                            }
                        }
                    }
        """
        raise NotImplementedError

    @abc.abstractmethod
    def register_metrics(self,
                         metrics: List[Dict]
                         ):
        """ Register metrics data.

        Args:
            metrics: The list of metrics to register as a list of dictionaries
            formatted as following :
                {
                    'shape': 'POLYGON((Longitude Latitude, ...))',
                    'time_coverage': ['YYYY-MM-DD HH:MM:SS',
                    'YYYY-MM-DD HH:MM:SS'],
                    'site': 'site_code',
                    'dataset': 'dataset_name',
                    'collection': 'collection_code',
                    'data': [
                            {
                            'name': 'parameter_name',
                            'value': double_value
                            },
                            ...
                            ],
                    'miniprod': 'miniprod_id'
                }
        """
        raise NotImplementedError

    @abc.abstractmethod
    def delete_metrics(self,
                       miniprods_ids : List[str]) -> int:
        """ Delete metrics data refering to a list of miniprod ids.

            Args:
                miniprods_ids: the list of miniprod ids.

            Returns:
                The number of deleted metrics.
        """
        raise NotImplementedError

    @abc.abstractmethod
    def get_metrics_ids_by_miniprods_ids(self,
                                         miniprods_ids: List[str]
                                       ) -> List[str]:
        """ Get metrics ids refering to a list of miniprod ids.

            Args:
                miniprods_ids: the list of miniprod ids.

            Returns:
                The list of metrics ids.
        """
        raise NotImplementedError
