# -*- encoding: utf-8 -*-

"""
@author: <sylvain.herledan@oceandatalab.com>
@date: 2018-09-28
"""
import cerbere
from importlib_metadata import entry_points
import json
import logging
import os
from pathlib import Path

import numpy
import pandas as pd

import felyx_processor.export.miniprod
import felyx_processor.processors.data as processors_data
from felyx_processor.extraction import extract_found_subsets_from_eo_file
from felyx_processor.extraction.source import SourceFile
from felyx_processor.miniprod.manifest_miniprod_datasource import ManifestMiniprodDataSource
from felyx_processor.miniprod.writers import set_miniprod_attrs
from felyx_processor.sites.insitu_factory import InSituDataSourceFactory
from felyx_processor.utils.configuration import (
    FelyxProcessorConfig,
    FelyxSystemConfig,
    get_mdataset_config,
    get_metrics_from_mdataset_config,
)
from felyx_processor.utils.exceptions import FelyxProcessorError, MetricProcessingError

logger = logging.getLogger('felyx.processor')


_contrib_metrics_base = {
        entry_point.name: entry_point
        for entry_point
        in entry_points().select(group='felyx.felyxcontribs_metrics_base')
}

_contrib_metrics_ghrsst = {
        entry_point.name: entry_point
        for entry_point
        in entry_points().select(group='felyx.felyxcontribs_metrics_ghrsst')
}

_contrib_metrics_workflow = {
        entry_point.name: entry_point
        for entry_point
        in entry_points().select(group='felyx.felyxcontribs_metrics_workflow')
}

_contrib = {
    **_contrib_metrics_base,
    **_contrib_metrics_workflow,
    **_contrib_metrics_ghrsst
}


class PluginError(Exception):
    """"""

    def __init__(self, task_id, status):
        """"""
        self.task_id = task_id
        self.msg = status


def _get_metrics_by_extraction(felyx_processor_config, felyx_sys_config,
                               felyx_server, dataset_name, site,
                               allowed_metrics_list):
    """TODO : Not used TO DELETE """
    site_obj = felyx_server.site(site_code=site)

    # Get all amtrics related to the dataset
    metrics = felyx_server.metric(dataset_name)

    # Apply filter (if provided) to keep only allowed metrics
    if allowed_metrics_list is not None:
        metrics = [_ for _ in metrics if _['name'] in allowed_metrics_list]

    # Get collections associated with each metric
    name_cache = {}
    result = {}
    for metric in metrics:
        collections = metric['site_collection']  # numeric identifier
        for collection_id in collections:
            if collection_id not in name_cache:
                data_source = InSituDataSourceFactory.get_insitu_datasource(
                    felyx_sys_config=felyx_sys_config,
                    felyx_processor_config=felyx_processor_config,
                    collection_code=collection_id)
                sites_list = data_source.get_sites_list(collection_id)

                """
                collection_obj = felyx_server.site_collection(collection_id)
                if site_obj['id'] not in collection_obj['site_code']:
                """
                if site_obj['id'] not in sites_list:
                    logger.info('Skipping collection {} since it does not '
                                'contain site {}'.format(collection_id, site))
                    continue

                # name_cache[collection_id] = collection_obj['site_code']
            name_cache[collection_id] = sites_list

            collection_name = collection_id
            if collection_name not in result:
                result[collection_name] = [metric]
            else:
                result[collection_name].append(metric)

    return result


def _get_metrics_by_collection(felyx_server, dataset_name,
                               allowed_metrics_list):
    """TODO Not used TO DELETE"""
    # Get all amtrics related to the dataset
    metrics = felyx_server.metric(dataset_name)

    # Apply filter (if provided) to keep only allowed metrics
    if allowed_metrics_list is not None:
        metrics = [_ for _ in metrics if _['name'] in allowed_metrics_list]

    # Get collections associated with each metric
    name_cache = {}
    result = {}
    for metric in metrics:
        collections = metric['site_collection']  # numeric identifier
        for collection_id in collections:
            if collection_id not in name_cache:
                collection_obj = felyx_server.site_collection_id(collection_id)
                name_cache[collection_id] = collection_obj['code']

            collection_name = name_cache[collection_id]
            if collection_name not in result:
                result[collection_name] = [metric]
            else:
                result[collection_name].append(metric)

    return result


def _compute_from_def(metric_def, input_data):
    """"""
    target = input_data
    for step_n, step in enumerate(metric_def):
        task_id = list(step.keys())[0]
        if task_id not in _contrib:
            raise PluginError(task_id, f"metric plugin '{task_id}' not found")
        task_args = list(step.values())[0]

        result = _contrib[task_id].load().perform(task_id, step_n, target,
                                                  None, **task_args)

        success, outputs, status = result
        if success is False:
            raise PluginError(task_id, status)

    return outputs[0]


def _check_metric_value(value):
    """"""
    if value is None:
        return False, 'Value is None'

    if isinstance(value, numpy.ma.core.MaskedConstant):
        return False, 'Value is masked'

    is_float = isinstance(value, float) or isinstance(value, numpy.floating)
    if is_float and not numpy.isfinite(value):
        return False, 'Value is infinite'

    return True, None


def from_miniprods(felyx_processor_config,
                   felyx_sys_config,
                   miniprod_paths,
                   mdataset,
                   allowed_collections=None,
                   allowed_sites=None,
                   from_manifests=False):
    """"""
    errors = []
    computed_metrics = []

    # Retrieve the collection from the mdataset and check if allowed
    config_mdataset = get_mdataset_config(felyx_processor_config,
                                          mdataset)
    collection = config_mdataset.site_collection_id
    if ((allowed_collections is not None) and
            (collection not in allowed_collections)):
        msg = 'The collection of the mdataset is not allowed : ' \
              '{}'.format(collection)
        raise FelyxProcessorError(msg)

    # Retrieve the sites information of the collection
    ds = InSituDataSourceFactory.get_insitu_datasource(
        felyx_sys_config=felyx_sys_config,
        felyx_processor_config=felyx_processor_config,
        collection_code=collection)
    sites = ds.get_sites_list(collection)

    # For each miniprod to process
    for file_path in miniprod_paths:

        if from_manifests:
            results = metrics_from_manifests(
                felyx_processor_config=felyx_processor_config,
                felyx_sys_config=felyx_sys_config,
                file_path=file_path,
                mdataset=mdataset,
                collection=collection,
                sites=sites,
                config_mdataset=config_mdataset,
                allowed_sites=allowed_sites)
        else:
            results = metrics_from_miniprods(
                felyx_processor_config=felyx_processor_config,
                file_path=file_path,
                mdataset=mdataset,
                collection=collection,
                sites=sites,
                allowed_sites=allowed_sites)

        computed_metrics.extend(results[0])
        errors.extend(results[1])

    return computed_metrics, errors


def metrics_from_miniprods(felyx_processor_config,
                           file_path,
                           mdataset,
                           collection,
                           sites,
                           allowed_sites=None):
    """"""
    load_data = processors_data.DataManager.load  # local alias

    errors = []
    computed_metrics = []

    miniprod_data = load_data(
        cerbere.open_feature(file_path, reader='xarray'), 'miniprod')
    info = felyx_processor.export.miniprod.dict_from_Data(miniprod_data)

    site = info['site']
    if ((allowed_sites is not None) and (site not in allowed_sites)):
        return computed_metrics, errors

    if site not in sites:
        logger.info('Skipping miniprod file {} since site does not '
                    'belong to collection {}'.format(site, collection))
        return computed_metrics, errors

    if info['site_collection'] != collection:
        return computed_metrics, errors

    # List all the metrics that can be computed
    eo_dataset_name = info['dataset']
    metrics = get_metrics_from_mdataset_config(felyx_processor_config,
                                               mdataset,
                                               eo_dataset_name)

    result_metrics = []

    # Compute metrics
    for metric_id, metric in metrics.items():
        # Load metric definition
        _tasks_sequence = metric.processing_chain_description
        try:
            tasks_sequence = json.loads(_tasks_sequence)
        except ValueError:
            import sys
            e = sys.exc_info()
            _msg = ('Could not load processing chain description as '
                    'JSON: {}'.format(_tasks_sequence))
            exc = MetricProcessingError(metric_id,
                                        eo_dataset_name,
                                        collection,
                                        file_path,
                                        _msg, e)
            errors.append(exc)
            continue

        # Compute metric
        try:
            computed_value = _compute_from_def(tasks_sequence,
                                               miniprod_data)
        except Exception as err:
            import sys

            if isinstance(err, PluginError):
                _msg = err.msg
                e = ''
            else:
                _msg = 'Failed to compute metric'
                e = sys.exc_info()
            exc = MetricProcessingError(metric_id,
                                        eo_dataset_name,
                                        collection,
                                        file_path,
                                        _msg, e)
            errors.append(exc)
            continue

        # Format result
        ok, reason = _check_metric_value(computed_value)
        if not ok:
            logger.warning('Warning during {}/{} computation: {}'
                           .format(collection, metric_id, reason))

            # Invalid values are all replaced by None
            computed_value = None

        if isinstance(computed_value, numpy.float32):
            computed_value = numpy.float64(computed_value)

        result_metrics.append({'name': metric.label,
                               'value': computed_value.item()})
        # Store the result
        miniprod_name = os.path.basename(file_path)
        miniprod_metrics = {'shape': info['shape'],
                            'time_coverage': info['time_coverage'],
                            'site': info['site'],
                            'dataset': eo_dataset_name,
                            'collection': collection,
                            'data': result_metrics,
                            'miniprod': miniprod_name}

        computed_metrics.append(miniprod_metrics)

    # Close the miniproduct - no leak of file descriptors!
    miniprod_data.close()

    return computed_metrics, errors


def metrics_from_manifests(
        felyx_processor_config: FelyxProcessorConfig,
        felyx_sys_config: FelyxSystemConfig,
        file_path,
        mdataset,
        collection,
        sites,
        config_mdataset,
        allowed_sites=None,
):
    """"""
    errors = []
    config_eo_datasets = felyx_processor_config.EODatasets
    computed_metrics = []
    subsets = []

    miniprod_index = ManifestMiniprodDataSource(Path(''))
    data = miniprod_index.read_manifest_file(file_path)
    if data.empty:
        return computed_metrics, errors

    extractions = miniprod_index.as_extractions(data)
    for collections in extractions.values():
        for manifest_collection, manifest_sites in collections.items():
            if manifest_collection != collection:
                # manifest_collection.pop(manifest_collection)
                continue

            for site_infos in manifest_sites:
                manifest_site = site_infos.site

                if ((allowed_sites is not None) and (
                        manifest_site not in allowed_sites)):
                    continue
                if manifest_site not in sites:
                    logger.info('Skipping miniprod file {} since site does not '
                                'belong to collection {}'.format(manifest_site,
                                                                 collection))
                    # manifest_sites.pop(manifest_site)
                    continue

                eo_dataset_id = site_infos.dataset
                path_pattern = config_eo_datasets[eo_dataset_id].filenaming
                if path_pattern is None:
                    msg = 'Cannot restore full filename from source file for ' \
                          'eo-dataset : {}'.format(eo_dataset_id)
                    raise FelyxProcessorError(msg)

                file_date = pd.to_datetime(
                    site_infos.source_time_coverage_start)
                source_fullpath = Path(
                    file_date.strftime(str(Path(path_pattern).parent))) / str(
                    site_infos.source)

                with SourceFile(felyx_processor_config=felyx_processor_config,
                                url=source_fullpath,
                                dataset_id=eo_dataset_id).load() as source_file:
                    source_file.dataset_id = eo_dataset_id

                    # get found subsets for the current source file
                    children = extract_found_subsets_from_eo_file(
                        felyx_context=felyx_processor_config,
                        source_file=source_file,
                        extractions=collections)

                    for child in children:
                        subset, site, site_info, collection_id, collection = \
                            child
                        set_miniprod_attrs(felyx_sys_config=felyx_sys_config,
                                           source_file=source_file,
                                           subset=subset,
                                           site_info=site_info,
                                           site_collection_info=collection,
                                           site_collection_id=collection_id)
                        subsets.append(child)

        metrics = get_metrics_from_mdataset_config(felyx_processor_config,
                                                   mdataset,
                                                   eo_dataset_id)

        for subset in subsets:

            miniprod_data = processors_data.Data(
                processors_data.MiniProdNCFile(subset[0]),
                'miniprod')

            result_metrics = []

            for metric_id, metric in metrics.items():
                # Load metric definition
                _tasks_sequence = metric.processing_chain_description
                try:
                    tasks_sequence = json.loads(_tasks_sequence)
                except ValueError:
                    import sys
                    e = sys.exc_info()
                    _msg = ('Could not load processing chain description as '
                            'JSON: {}'.format(_tasks_sequence))
                    exc = MetricProcessingError(metric_id,
                                                eo_dataset_id,
                                                collection,
                                                file_path,
                                                _msg, e)
                    errors.append(exc)
                    continue

                # Compute metric
                try:
                    computed_value = _compute_from_def(tasks_sequence,
                                                       miniprod_data)
                except Exception as err:
                    import sys

                    if isinstance(err, PluginError):
                        _msg = err.msg
                        e = ''
                    else:
                        _msg = 'Failed to compute metric'
                        e = sys.exc_info()
                    exc = MetricProcessingError(metric_id,
                                                eo_dataset_id,
                                                collection,
                                                file_path,
                                                _msg, e)
                    errors.append(exc)
                    continue

                # Format result
                ok, reason = _check_metric_value(computed_value)
                if not ok:
                    logger.warning('Warning during {}/{} computation: {}'
                                   .format(collection, metric_id, reason))

                    # Invalid values are all replaced by None
                    computed_value = None

                if isinstance(computed_value, numpy.float32):
                    computed_value = numpy.float64(computed_value)

                result_metrics.append({'name': metric.label,
                                       'value': computed_value})
            # Store the result
            miniprod_metrics = {
                'shape': subset[2].shape.wkt,
                'time_coverage': [subset[2].time_coverage_start,
                                  subset[2].time_coverage_stop],
                'site': subset[1],
                'dataset': eo_dataset_id,
                'collection': collection_id,
                'data': result_metrics,
                'miniprod': subset[2].id}

            computed_metrics.append(miniprod_metrics)

    return computed_metrics, errors
