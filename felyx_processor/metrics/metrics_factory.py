# encoding: utf-8
"""
metrics_factory
--------------------------------------

Contains class for creating metrics datasource from various sources

"""

from felyx_processor.metrics.es_metrics_datasource import ESMetricsDataSource
from felyx_processor.metrics.metrics_datasource import MetricsDataSource
from felyx_processor.utils.configuration import FelyxSystemConfig


class MetricsDataSourceFactory:
    @classmethod
    def get_metrics_datasource(cls, felyx_sys_config: FelyxSystemConfig) \
            -> MetricsDataSource:
        """Create a datasource for metrics data according to the dataset
        configuration.

        Args:
            felyx_sys_config: Felyx configuration
        Returns:
            the metrics datasource
        """
        return ESMetricsDataSource(felyx_sys_config.elasticsearch)
