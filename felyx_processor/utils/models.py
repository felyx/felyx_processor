# coding=utf-8

"""
felyx_processor.utils.models
------------------------------

This package provides a common framework for data structures and their validation.

.. :copyright:
.. :license:

.. sectionauthor::
.. codeauthor::
"""

from enum import Enum


class DataModel(str, Enum):
    """
    Class containing the DataModel
    """
    Swath = 'Swath'
    Grid = 'Grid'
    Trajectory = 'Trajectory'
    PointCollection = 'PointCollection'
    PointTimeseries = 'PointTimeSeries'
    GridTimeseries = 'GridTimeSeries'


class LogLevel(str, Enum):
    """
    Class containing the LogLevel
    """
    debug = 'debug'
    info = 'info'
    warning = 'warning'
    error = 'error'


class CollectionLevel(str, Enum):
    """
    Class containing the CollectionLevel
    """
    community = 'community'
    local = 'local'
    custom = 'custom'


class Indexing(str, Enum):
    """where to dump the child products index metadata"""
    elasticsearch = 'elasticsearch'
    manifest = 'manifest'
    stdout = 'stdout'


class DataSourceAccess(str, Enum):
    elasticsearch = 'elasticsearch'
    parquet = 'parquet'
    inline = 'inline'
