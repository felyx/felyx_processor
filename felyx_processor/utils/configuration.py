# coding=utf-8

"""
felyx_processor.utils.configuration
------------------------------

This package provides a common framework for the loading of the various
felyx system configuration files.

.. :copyright:
.. :license:

.. sectionauthor::
.. codeauthor::
"""
from __future__ import annotations

import logging
import os
from decimal import Decimal
from enum import Enum
from pathlib import Path
from typing import Dict, List, Optional, Union
from typing_extensions import Self

import shapely
import shapely.wkt
import yaml
from pydantic import (AnyHttpUrl, BaseModel, HttpUrl, field_validator,
                      model_validator, ValidationInfo)
from pydantic_settings import BaseSettings

from felyx_processor.miniprod.shape import get_felyx_shape
from felyx_processor.utils.exceptions import FelyxProcessorError
from felyx_processor.utils.merger import deep_merge_dicts
from felyx_processor.utils.models import DataModel, DataSourceAccess, Indexing
from felyx_processor.miniprod.shape import GeographicPolygon


logger = logging.getLogger('felyx.processor')

# Environnement variable for Felyx system configuration file path
FELYX_CFG_SYS_ENV = 'FELYX_SYS_CFG'

# @TODO move stuff to felyx data


class FelyxSettings(BaseSettings):
    class Config:  # noqa: D106, WPS431
        env_prefix = 'felyx_processor_'
        env_nested_delimiter = '__'
        validate_default = True
        validate_assignment = True

    @classmethod
    def from_yaml_file(cls, file: Path):
        """"Load a model from an YAML file"""
        with open(file) as f:
            return cls(**yaml.safe_load(f))

    def to_yaml(self) -> str:
        """"Dump model to YAML"""
        return yaml.dump(self.model_dump())

    def merge(self, partial: Dict) -> FelyxSettings:
        """Merge from a partial dictionary"""
        if partial is None:
            return self

        return self.model_validate(deep_merge_dicts(self.model_dump(), partial))


class FelyxModel(BaseModel):
    def merge(self, partial: Dict) -> FelyxModel:
        """Merge from a partial dictionary"""
        if partial is None:
            return self

        return self.model_validate(deep_merge_dicts(self.model_dump(), partial))

    class Config:
        """Validate constraints on assignment"""
        validate_default = True
        validate_assignment = True
        extra = 'forbid'


class ConfigException(Exception):
    pass


class ElasticSearchOperationConfig(FelyxModel):
    """
    Class containing the Elastic Search operation configuration model
    """
    chunksize: Optional[int] = 5000
    timeout: Optional[str] = '10s'
    scroll_retention_time: Optional[str] = '5m'


class ElasticSearchConfig(FelyxModel):
    """
    Class containing the Elastic Search configuration model
    """
    url: AnyHttpUrl = AnyHttpUrl('http://127.0.0.1:9200')
    prefix: str = 'felyx_'
    chunksize: int = 5000
    timeout: str = '10s'
    scroll_retention_time: str = '5m'
    operations: Union[Dict[str, ElasticSearchOperationConfig], None] = None

    def get_operation_config(self, operation_key: str) \
            -> ElasticSearchOperationConfig:
        """ Return the ES configuration for a given ES operation """
        # Create a configuration with global ES settings
        config = ElasticSearchOperationConfig.model_validate(
            {'scroll_retention_time': self.scroll_retention_time,
             'timeout': self.timeout,
             'chunksize': self.chunksize}
        )
        # Check if there are specific settings
        # defined for this operation in ES config
        if self.operations is not None and operation_key in self.operations:
            operation_config = self.operations[operation_key]
            # Merge default config and operation settings
            config = config.model_copy(
                update=operation_config.model_dump(exclude_none=True)
            )

        return config


class ParquetConfig(FelyxModel):
    """
    Class containing the Parquet configuration model
    """
    filepath_pattern: str
    frequency: Union[str, None] = None


class FelyxSystemConfig(FelyxSettings):
    root_dir: Path = Path('./felyx')
    prepend_root_dir_to_relative_paths: bool = True
    shared_workspace_root: Path = Path('workspace')
    felyx_public_folder_name: str = 'public'
    default_felyx_miniprod_store: Path = Path('felyx/data')
    default_felyx_manifest_store: Optional[Union[str, Path]] = Path(
        'felyx/manifests')
    nc_format: str = 'NETCDF4'
    default_felyx_user: str = 'felyx'
    default_felyx_password: str = 'felyx'
    acknowledgement: Union[str, None] = None
    miniprod_license: Union[str, None] = None
    miniprod_format_version: str = 'felyx 2.0'
    miniprod_references: Union[str, None] = None
    link_mode: str = 'soft'
    output_derived_miniprod_dir: Path = Path('workspace')
    elasticsearch: ElasticSearchConfig = ElasticSearchConfig()

    # TODO: to be deleted
    metadata: Path = Path('conf/plugins/.metadata')
    insitu_mappings_dir: Path = Path('conf/insitu_mappings')
    plugins_root: Path = Path('conf/plugins')

    @field_validator(
        'shared_workspace_root', 'plugins_root',
        'output_derived_miniprod_dir', 'insitu_mappings_dir',
        'default_felyx_miniprod_store', 'default_felyx_manifest_store')
    @classmethod
    def build_paths(cls, path: Path, info: ValidationInfo):
        if path is None:
            return path

        if info.data['prepend_root_dir_to_relative_paths']:
            path = info.data['root_dir'] / path
        os.makedirs(path, exist_ok=True)

        return path


class DistributedProcessing(FelyxModel):
    # URl of the jobard orchestrator service
    url: HttpUrl = 'http://localhost:8000'
    # maximum memory required by a job
    memory: str = '2G'
    # maximum walltime required by a job
    walltime: str = '01:30:00'
    # main node for job submission
    connection: str = 'datarmor'
    # job division when processing multiple inputs
    split: str = '/40'
    # priority of jobs for this dataset
    priority: int = 10


class DatasetMonitoringConfig(FelyxModel):
    """Felyx report dataset monitoring configuration."""

    alert_check_nb_of_processed_eo_files: Optional[bool] = False


class DatasetConfig(FelyxModel):
    id: str
    name: str
    description: str
    # Cerbere reader class for input EO data
    dataset_class: str = 'xarray'
    feature_class: DataModel
    # pattern of the full path to the EO data files wrt their time coverage
    # start
    filenaming: Optional[str] = None
    level: str
    supplier: str
    keywords: Optional[str] = None

    # sibling dataset
    sibling: Optional[SiblingConfig] = None

    monitoring: Optional[DatasetMonitoringConfig] = DatasetMonitoringConfig()


class SiteConfig(FelyxModel):
    name: str
    shape: shapely.Geometry

    class Config:
        """Validate constraints on assignment"""
        validate_default = True
        validate_assignment = True
        arbitrary_types_allowed = True

    @field_validator('shape', mode='before')
    @classmethod
    def check_shape(cls, v):
        if isinstance(v, str):
            # convert WKT string to shapely geometry
            if ';' in v:
                # avoid issues with "SRID=4326;POLYGON ((..."
                v = v.split(';')[-1]
            return shapely.wkt.loads(v)
            # return get_felyx_shape(shapely.wkt.loads(v))

        return v


class MetricMonitoringConfig(FelyxModel):
    """Felyx report metric monitoring configuration."""

    alert_check_nb_metrics: bool = False


class MetricConfig(FelyxModel):
    dataset: str
    label: str
    processing_chain_description: str
    monitoring: Optional[MetricMonitoringConfig] = MetricMonitoringConfig()


class OwnerConfig(FelyxModel):
    username: Union[str, None] = None
    email: Union[str, None] = None
    url: Union[str, None] = None


class SiteCollectionMonitoringConfig(FelyxModel):
    """Felyx report site collection monitoring configuration."""

    alert_threshold_nb_of_in_situ_measurements: Optional[int] = -1


class SiteCollectionConfig(FelyxModel):
    # id of the site collection to which this configuration is for (filled in
    # when parsing configuration)
    id: str
    # full human-readable name of the site collection
    name: str
    # comprehensive human-readable description of the site collection
    description: str
    static: bool = False
    level: str = 'community'
    # site_code: Union[List, None] = None
    datasource_access: DataSourceAccess
    # sites: List[SiteConfig] = []
    sites: Optional[Dict[str, SiteConfig]] = None
    # @TODO put in data source instead ?
    parquet_config: Optional[ParquetConfig] = None
    monitoring: Optional[SiteCollectionMonitoringConfig] = (
        SiteCollectionMonitoringConfig())


class DynamicInsituAccessConfig(FelyxModel):
    elasticsearch: ElasticSearchConfig

    class Config:
        arbitrary_types_allowed = True


class MatchingCriteriaConfig(FelyxModel):
    # colocation time window, in minutes
    time_window: int
    # colocation search radius in km
    search_radius: float


class CommonExtractionOptionsConfig(FelyxModel):
    overwrite: bool = True
    # indexing of the extracted miniprods : stores the metadata of the
    # extracted miniprods either in elasticsearch [elasticsearch] or within
    # the manifest files [manifest] created for each processed
    # EO data file or both [elasticsearch_and_manifest].
    indexing: List[Indexing] = [Indexing.manifest]


class ContentSelectionMonitoringConfig(FelyxModel):
    """Felyx content selection monitoring configuration."""

    alert_threshold_nb_of_created_mmdb_files: Optional[int] = -1
    alert_check_nb_of_created_matchups: Optional[bool] = False
    dashboard_create_nb_of_created_matchups: Optional[bool] = True


class ContentSelectionConfig(FelyxModel):
    # list of variables to include in the assembled files (all of them by
    # default)
    variables: List[str] = ['.*']
    # variables NOT TO include in extracted child products (none of them by
    # default)
    except_variables: List[str] = []
    # list of global attributes to include in the assembled files (all of
    # them by default)
    attributes: List[str] = ['.*']
    # list of global attributes NOT TO include in the assembled files (none
    # of them by default)
    except_attributes: List[str] = []
    # list of attributes to convert to variables. This is applicable to
    # attributes that have different values from one child product to
    # another, and it allows to keep the traceability of this information
    # into the assembled MDB files.
    attributes_as_variables: List[str] = []
    # complementary hard-coded (constant) global attributes to include in all
    # output files
    custom_attributes: Dict[str, Union[str, Decimal]] = {}
    # prefix by which to rename all variables and global attributes
    # coming from this dataset (by default the dataset id is used)
    prefix: Union[str, None] = None
    # content selection config for monitoring report
    monitoring: Optional[ContentSelectionMonitoringConfig] = ContentSelectionMonitoringConfig()


class SiblingConfig(FelyxModel):
    """Defines a sibling dataset, that will allow faster multi-matchup
    processing"""
    # identifier of the sibling dataset
    identifier: str
    # multiplication factor to apply to scale slices from the
    # sibling dataset and obtain the corresponding slices in the current
    # dataset, as a dictionary (dimension, scaling factor)
    scaling: Dict[str, Union[int, float]]


class EODatasetExtractionConfig(FelyxModel):
    # id of the dataset to which this configuration is for (filled in when
    # parsing configuration)
    dataset_id: str
    # id of the matching dynamic site collection (filled in when
    #  parsing configuration)
    site_collection_id: str

    # size of the extracted subset array, in number of elements for the
    # spatial axis
    subset_size: int

    # padding with fill values the extracted subset when the slices are over
    # the file dimensions (so that extracted subset always have the same
    # size. Recommended for matchups.
    padding: bool = True

    matching_criteria: MatchingCriteriaConfig

    metrics: Union[Dict[str, MetricConfig], None] = None
    # content tailoring of the extracted child products
    content: ContentSelectionConfig = ContentSelectionConfig()

    non_empty_field: Union[List[str], None] = None
    reference: bool = False

    # distributed processing specific settings
    # TODO should not be here
    distributed_processing: Optional[DistributedProcessing] = None

    @field_validator('subset_size')
    @classmethod
    def check_subset_size(cls, value: int) -> int:
        if value % 2 == 0:
            raise ValueError(
                f'Found even child product size ({value}) in configuration. '
                f'Only odd values are permitted')

        return value

    @field_validator('dataset_id', 'site_collection_id', mode='after')
    @classmethod
    def check_id(cls, v):
        """these fields are filled in once whole configuration has been read"""
        return v


class ProductConfig(FelyxModel):
    # pattern for output filenames
    filenaming: Union[str, None] = None
    # content selected from each EO dataset in product
    content: Dict[str, ContentSelectionConfig]


class MDBOutputConfig(FelyxModel):
    # index to search the miniprods to be assembled
    # from [elasticsearch, manifest].
    search_from: Union[DataSourceAccess, None] = DataSourceAccess.parquet
    # the temporal extent of each created match-up file
    frequency: str
    # history of in situ data to include with the matched measurement, in
    # minutes before and after the matching time
    history: int
    # division of the output MMDB files in separate files
    products: Union[Dict[str, ProductConfig], None] = None
    # default time units in CF convention style (default is 'seconds since
    # 1950-01-01')
    default_time_units: str = 'seconds since 1950-01-01'


class MDatasetConfig(FelyxModel):
    # identifier of the in situ collection to match with
    site_collection_id: str
    # EO dataset(s) to match with SiteCollection
    eo_datasets: Dict[str, EODatasetExtractionConfig]
    common_options: CommonExtractionOptionsConfig = \
        CommonExtractionOptionsConfig()
    # optional, useful for the assemble matchup command
    output: Optional[MDBOutputConfig] = None
    # metrics: Dict[str, ConfigMDBMetrics]

    @field_validator('eo_datasets', mode='before')
    @classmethod
    def init_dataset(
            cls,
            value: Dict[str, EODatasetExtractionConfig],
            info: ValidationInfo
    ) -> Dict[str, EODatasetExtractionConfig]:
        for dataset_id in value:
            value[dataset_id]['dataset_id'] = dataset_id
            value[dataset_id]['site_collection_id'] = (
                info.data)['site_collection_id']
        return value


class RepeatIntervalUnitEnum(str, Enum):
    """Felyx report mail notification repeat interval unit enumeration."""

    seconds = 's'
    minutes = 'm'
    hours = 'h'
    days = 'd'
    weeks = 'w'


class RepeatIntervalConfig(FelyxModel):
    """Felyx report mail notification repeat interval configuration."""

    duration: Optional[int] = 4
    unit: Optional[RepeatIntervalUnitEnum] = RepeatIntervalUnitEnum.hours


class ContactPointConfig(FelyxModel):
    """Felyx report contact point configuration."""

    name: str
    addresses: List[str]
    disable_resolve_message: bool
    repeat_interval: Optional[RepeatIntervalConfig] = RepeatIntervalConfig()


class FelyxProcessorConfig(FelyxSettings):
    EODatasets: Dict[str, DatasetConfig]
    owner: List[OwnerConfig] = [OwnerConfig()]
    SiteCollections: Dict[str, SiteCollectionConfig]
    Metrics: List[MetricConfig] = []
    MMDatasets: Dict[str, MDatasetConfig]
    contact_points: Optional[List[ContactPointConfig]] = None

    @field_validator('SiteCollections', mode='before')
    @classmethod
    def fill_site_collection_id(
            cls,
            collections: Dict[str, SiteCollectionConfig]):
        # set the site collection identifier
        for cid, coll in collections.items():
            coll['id'] = cid

        return collections

    @field_validator('EODatasets', mode='before')
    @classmethod
    def fill_dataset_id(
            cls,
            datasets: Dict[str, DatasetConfig]):
        # set the dataset identifier
        for did, dst in datasets.items():
            dst['id'] = did

        return datasets

    @model_validator(mode='after')
    def check_validity(self) -> Self:
        # check a dataset is associated once only with a site collection
        mmd_count = {}
        for mmdataset in self.MMDatasets.values():
            for ds in mmdataset.eo_datasets.values():
                key = mmdataset.site_collection_id, ds.dataset_id,
                if key not in mmd_count:
                    mmd_count[key] = 0
                mmd_count[key] += 1
        for k, v in mmd_count.items():
            if v > 1:
                raise ConfigException(
                    f'They are multiple associates between site collection '
                    f'{k[0]} and dataset {k[1]} in felyx configuration')

        return self

    def related_extractions(
            self,
            dataset_id: str
    ):
        """ Return the miniprod processing configurations for a given dataset """
        configs = []
        for mdataset in self.MMDatasets.values():
            if dataset_id in mdataset.eo_datasets:
                configs.append(mdataset.eo_datasets[dataset_id])

        if len(configs) == 0:
            raise FelyxProcessorError(
                f'Cannot find miniprod processing configuration for dataset : '
                f'{dataset_id} in configuration file')

        return configs

    def related_site_collections(
            self, dataset_id, allowed_site_collections=None):
        res = []
        for extr in self.related_extractions(dataset_id):
            if allowed_site_collections is None:
                res.append(
                    self.SiteCollections[extr.site_collection_id])
                continue

            if extr.site_collection_id in allowed_site_collections:
                res.append(
                    self.SiteCollections[extr.site_collection_id])

        return res

    def child_product_sizes(self, dataset_id: str):
        processing_list = get_dataset_miniprod_extraction_config(
            felyx_processor_config=self,
            dataset_id=dataset_id)

        sizes = []
        for chain in processing_list:
            sizes.append(chain.subset_size)

        return set(sizes)


def load_felyx_processor_config(
    configuration_file: Path = None,
    override_data: Optional[Dict] = None,
) -> FelyxProcessorConfig:
    """Load Felyx processor configuration from a YAML File.

    Args:
        configuration_file: YAML configuration file
        override_data: override data

    Returns:
        FelyxProcessorConfig: an instance of FelyxProcessorConfig
    """
    if configuration_file is not None:
        felyx_processor_config = FelyxProcessorConfig.from_yaml_file(
            configuration_file)
    else:
        felyx_processor_config = FelyxProcessorConfig()

    felyx_processor_config.merge(override_data)

    return felyx_processor_config


def load_felyx_sys_config(
    configuration_file: Path = None,
    override_data: Optional[Dict] = None
) -> FelyxSystemConfig:
    """Load Felyx global configuration from a YAML File.

    Args:
        configuration_file: YAML configuration file
        override_data: override data

    Returns:
        FelyxProcessorConfig: an instance of FelyxProcessorConfig
    """
    if configuration_file is None and FELYX_CFG_SYS_ENV in os.environ:
        configuration_file = Path(os.environ[FELYX_CFG_SYS_ENV])
        if not configuration_file.exists():
            raise FelyxProcessorError(
                """
                The file {0} (provided by the environmental variable {1}
                is not available. As this file provides the
                configuration for this felyx instance, felyx will now
                exit. Please read the felyx documentation and ensure that
                your felyx instance is correctly installed.
                """
                .format(os.environ[FELYX_CFG_SYS_ENV], FELYX_CFG_SYS_ENV)
            )
    if configuration_file is None:
        return FelyxSystemConfig()
    return FelyxSystemConfig.from_yaml_file(configuration_file)


def get_dynamic_data_access_config(
    felyx_sys_config: FelyxSystemConfig,
    felyx_processor_config: FelyxProcessorConfig,
    collection_code: str,
) -> FelyxModel:
    """Retrieve the configuration model of dynamic data access for a given collection.

    Args:
        felyx_sys_config: Felyx systeme configuration
        felyx_processor_config: Felyx processor configuration
        collection_code: collection code

    Returns:
        FelyxModel: configuration model of dynamic data access for the given collection
    """
    model = None
    try:
        collection_config = felyx_processor_config.SiteCollections[collection_code]
    except KeyError:
        raise FelyxProcessorError(
            'Cannot find site collection : {0} in configuration file'.format(collection_code)
        )

    try:
        access = collection_config.datasource_access
        if access == 'elasticsearch':
            model = felyx_sys_config.elasticsearch
        elif access == 'parquet':
            model = collection_config.parquet_config
    except AttributeError:
        raise

    return model


def get_dataset_miniprod_extraction_config(
        felyx_processor_config: FelyxProcessorConfig,
        dataset_id: str
    ) -> List[EODatasetExtractionConfig]:
    """ Return the miniprod processing configurations for a given dataset """
    configs = []
    for mdataset in felyx_processor_config.MMDatasets.values():
        if dataset_id in mdataset.eo_datasets:
            configs.append(mdataset.eo_datasets[dataset_id])

    if len(configs) == 0:
        raise FelyxProcessorError(
            f'Cannot find miniprod processing configuration for dataset : '
            f'{dataset_id} in configuration file')

    return configs


def get_extraction_settings(
        felyx_processor_config: FelyxProcessorConfig,
        dataset_id: str,
        site_collection_id: str
) -> EODatasetExtractionConfig:
    """ Return the child product extraction settings for a
    given pair of dataset and site collection
    """
    for mdataset in felyx_processor_config.MMDatasets.values():
        if mdataset.site_collection_id != site_collection_id:
            continue
        if dataset_id in mdataset.eo_datasets:
            return mdataset.eo_datasets[dataset_id]

    raise FelyxProcessorError(
        f'Cannot find product extraction settings for dataset '
        f'{dataset_id} and site collection {site_collection_id} in '
        f'given felyx configuration')


def get_collection_config(
        felyx_processor_config: FelyxProcessorConfig,
        collection_id: str
    ) -> SiteCollectionConfig:
    """ Return the configuration for a given site collection"""
    try:
        return felyx_processor_config.SiteCollections[collection_id]
    except KeyError:
        msg = f'Cannot find configuration for collection : ' \
              f'{collection_id} in configuration file'
        raise FelyxProcessorError(msg)


def get_dataset_collection_extraction_config(
        felyx_processor_config: FelyxProcessorConfig,
        dataset_id: str,
        collection_id: str) -> Dict[str]:
    """ Return the miniprod processing configurations for a given dataset """

    options = {}
    for mdataset in felyx_processor_config.MMDatasets.values():

        if mdataset.site_collection != collection_id:
            continue

        for _ in mdataset.eo_datasets:
            if _ == dataset_id:
                dataset = mdataset.eo_datasets[_].model_dump()
                if 'indexing' in options:
                    if options['indexing'] != dataset['indexing']:
                        # TODO
                        options['indexing'] = \
                            Indexing.elasticsearch_and_manifest
                else:
                    options['indexing'] = dataset['indexing']
                if 'non_empty_field' in options:
                    options['non_empty_field'] |= dataset['non_empty_field']
                else:
                    options['non_empty_field'] = dataset['non_empty_field']

        common_options = mdataset.common_options.model_dump()
        if 'overwrite' in options:
            options['overwrite'] &= common_options['overwrite']
        else:
            options['overwrite'] = common_options['overwrite']
        if 'padding' in options:
            options['padding'] |= common_options['padding']
        else:
            options['padding'] = common_options['padding']

    if len(options) == 0:
        return

    return options


def get_mdataset_config(
        felyx_processor_config: FelyxProcessorConfig,
        dataset_id: str) -> MDatasetConfig:
    """ Return the mdataset config for a given mdataset id """

    config = None
    if dataset_id in felyx_processor_config.MMDatasets:
        config = felyx_processor_config.MMDatasets[dataset_id].model_copy()
    if config is not None:
        return config
    else:
        msg = 'Cannot find the mdataset configuration for id : ' \
              '{} in configuration file'.format(dataset_id)
        raise FelyxProcessorError(msg)


def get_metrics_from_mdataset_config(
        felyx_processor_config: FelyxProcessorConfig,
        mdataset_id: str,
        eodataset_id: str
    ) -> Dict[MetricConfig]:
    """Return the metrics configurations for a given mdataset id and eo
    dataset

    Args:
        felyx_processor_config: Felyx processor configuration
        mdataset_id: Mdataset id to find
        eodataset_id: EOdataset id to find

    Returns:
        a dict containing metric configuration
    """

    mdataset = None
    if mdataset_id in felyx_processor_config.MMDatasets:
        mdataset = felyx_processor_config.MMDatasets[mdataset_id].model_copy()
    if mdataset is not None:
        metrics = dict()
        if eodataset_id in mdataset.eo_datasets:
            processing = mdataset.eo_datasets[eodataset_id]
            metrics = processing.metrics.copy()

        if metrics is not None:
            return metrics
        else:
            msg = 'Cannot find metrics configuration for mdataset : ' \
                  '{} and eo dataset in configuration file'.format(mdataset_id,
                                                                   eodataset_id)
            raise FelyxProcessorError(msg)
    else:
        msg = 'Cannot find the mdataset configuration for id : ' \
              '{} in configuration file'.format(mdataset_id)
        raise FelyxProcessorError(msg)
