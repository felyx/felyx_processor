# coding=utf-8
"""
This package provides common classes for parsing felyx CLI parameters
"""
from __future__ import annotations

import argparse
from datetime import datetime
from pathlib import Path
from typing_extensions import Self
from typing import List, Optional, Tuple, Union, Type, TypeVar, Dict, Any
from dateutil import parser

from pydantic import DirectoryPath, field_validator, model_validator

import felyx_processor.utils.models as models
from felyx_processor.utils.configuration import FelyxModel, FelyxSystemConfig


def log_params(args: argparse.Namespace) -> Dict[str, Union[bool, str]]:
    return dict(
        verbose=args.verbose,
        quiet=args.quiet,
        silent=args.silence,
        logfile=args.logfile,
        logfile_level=args.logfile_level)

# @TODO Remove this
def add_log_params(yaml_data: dict, args: argparse.Namespace) -> dict:
    yaml_data['log']['verbose'] = args.verbose
    yaml_data['log']['quiet'] = args.quiet
    yaml_data['log']['silence'] = args.silence
    yaml_data['log']['logfile'] = args.logfile
    yaml_data['log']['logfile_level'] = args.logfile_level


class LogOptions(FelyxModel):
    verbose: Optional[bool] = False
    quiet: Optional[bool] = False
    silent: Optional[bool] = False
    logfile: Optional[Path] = None
    logfile_level: Optional[models.LogLevel] = models.LogLevel.info


class FelyxModelParameters(FelyxModel):
    log: LogOptions


class FilterSitesCollections(FelyxModel):
    allowed_collections: Union[Optional[List[str]], str] = []
    allowed_sites: Union[Optional[List[str]], str] = []
    # Area to process (any file not intersecting this area will return an empty
    # manifest and no child products), as a list latmin,lonmin,latmax,lonmax
    area: Optional[Tuple[float, float, float, float]] = None
    # input file attribute to get its spatial coverage from. Use `cf` to use the
    # standard bounding box attributes. If no attribute is provided, felyx will
    # use the file lat/lon which is likeky to be slower. Any other attribute
    # than `cf` must be a WKT string.
    area_attribute: Optional[str] = None

    @field_validator('area')
    @classmethod
    def area_validity(
            cls, v: List[float]
    ) -> Optional[Tuple[float, ...]]:
        if v is None:
            return v

        if len(v) != 4:
            raise ValueError(
                'the area must be provided as a list of 4 floats latmin,lonmin,'
                'latmax,lonmax')
        for i_lon in [1, 3]:
            if v[i_lon] > 360:
                v[i_lon] -= 360.
            if not (-180 <= v[i_lon] <= 180):
                raise ValueError(f'Invalid longitude in area: {v[i_lon]}')
        for i_lat in [0, 2]:
            if not (-180 <= v[i_lat] <= 180):
                raise ValueError(f'Invalid latitude in area: {v[i_lat]}')

        return tuple(v)


class InputFiles(FelyxModel):
    inputs: Optional[List[Path]] = None
    from_list: Optional[Path] = None

    @field_validator('inputs')
    @classmethod
    def check_inputs(cls, value: Any = None) -> Optional[List[Path]]:
        if value is not None:
            if isinstance(value, list):
                value = [Path(_) for _ in value]
            else:
                value = Path(value)

        return value

    @field_validator('from_list')
    @classmethod
    def check_list(cls, value: str = None) -> Optional[Path]:
        if value is not None:
            value = Path(value)
            assert (value.exists(), f'File listing {value} does not exist')
        return value

    @model_validator(mode='after')
    def check_consistency(self) -> Self:
        assert not (self.inputs is not None and self.from_list is not None)
        return self

    def get_file_list(self):
        if self.inputs is not None:
            return self.inputs

        inputs = []
        if self.from_list is not None:
            with open(self.from_list, 'r') as f:
                inputs.extend([
                    Path(_.strip()) for _ in f.readlines()
                    if 0 < len(_.strip())])
        return list(set(inputs))  # remove duplicates


class CollectEOFilesParams(FelyxModelParameters):
    # identifiers of MMDBs to process, as defined in config file
    mmdb_ids: List[str]
    # processing interval
    start: Union[str, datetime]
    end: Union[str, datetime]
    # output of the file list
    output: Optional[Path] = None
    # skip sibling datasets
    skip_siblings: bool = True

    @field_validator('start')
    @classmethod
    def complete_start_date_parameter(cls, v):
        return parser.parse(v)

    @field_validator('end')
    @classmethod
    def complete_end_date_parameter(cls, v):
        return parser.parse(v)

    @staticmethod
    def add_arguments(yaml_data: dict,
                      args: argparse.Namespace,
                      sys_config: FelyxSystemConfig) -> dict:
        yaml_data['mmdb_ids'] = \
            [_ for x in args.mmdb_ids for _ in x.split(',')]
        yaml_data['mmdb_ids'] = args.mmdb_ids
        yaml_data['start'] = args.start
        yaml_data['end'] = args.end
        yaml_data['output'] = args.output
        yaml_data['skip_siblings'] = args.skip_siblings
        add_log_params(yaml_data, args)


Parameters = TypeVar('Parameters', bound=FelyxModelParameters)
def get_parameters(
    params_type: Type[FelyxModelParameters],
    args: argparse.Namespace,
    felyx_sys_config: FelyxSystemConfig
) -> Parameters:
    """
    Args:
        params_type:
        args:
        felyx_sys_config:

    Returns:

    """
    yaml_data = {'log': LogOptions().model_dump()}

    params_type.add_arguments(yaml_data, args, felyx_sys_config)

    return params_type(**yaml_data)


def add_log_args(parser: argparse.ArgumentParser) -> argparse.ArgumentParser:
    """Add the CLI arguments related to log settings"""
    parser.add_argument(
        '--logfile', type=str, default=None,
        help='Path of the file where logs will be written')
    parser.add_argument(
        '--logfile-level', type=str, default='info',
        help='Minimal level that log messages must reach to be written in '
             'the log file')
    level_group = parser.add_mutually_exclusive_group()
    level_group.add_argument(
        '-v', '--verbose', action='store_true',
        help='Activate debug level logging - for extra feedback.')
    level_group.add_argument(
        '-q', '--quiet', action='store_true',
        help='Disable information logging - for reduced feedback.')
    level_group.add_argument(
        '-s', '--silence', action='store_true',
        help='Log ONLY the most critical or fatal errors. ')

    return parser
