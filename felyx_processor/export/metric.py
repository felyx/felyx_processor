# -*- encoding: utf-8 -*-

"""
@author: <sylvain.herledan@oceandatalab.com>
@date: 2018-09-28
"""

import json
import logging

import numpy

logger = logging.getLogger('felyx.processor')

MANDATORY_FIELDS = ('site', 'dataset', 'collection', 'shape', 'time_coverage',
                    'data', 'miniprod')


class MissingMetricFields(Exception):
    """"""
    def __init__(self, missing, *args, **kwargs):
        self.missing = missing
        _msg = 'The following attributes are required but were not found in ' \
               'the metric file: {}'.format(', '.join(missing))
        super(Exception, self).__init__(_msg, *args, **kwargs)


class UnknownMetricFields(Exception):
    """"""
    def __init__(self, extra, *args, **kwargs):
        self.extra = extra
        _msg = 'The following attributes are not required but were found in ' \
               'the metric file: {}'.format(', '.join(extra))
        super(Exception, self).__init__(_msg, *args, **kwargs)


class InvalidJSONMetric(Exception):
    """"""


def dict_from_JSON_file_handler(f_handler):
    """"""
    result = None
    try:
        result = json.load(f_handler)
    except ValueError:
        raise InvalidJSONMetric

    missing = [x for x in MANDATORY_FIELDS if x not in result.keys()]
    if 0 < len(missing):
        raise MissingMetricFields(missing=missing)

    extra = [x for x in result.keys() if x not in MANDATORY_FIELDS]
    if 0 < len(extra):
        raise UnknownMetricFields(extra=extra)

    return result


def dict_from_computed(computed):
    """"""

    result = {'site': computed['site'],
              'collection': computed['collection'],
              'dataset': computed['dataset'],
              'time_coverage': computed['time_coverage'],
              'shape': computed['shape'],
              'data': []}

    for metric in computed['data']:
        # @TODO what is all this for ?
        # _metric = {'name': metric['name']}
        #
        # # TODO: handle numpy types
        #
        # if metric['value'] is None:
        #     _metric['value'] = None
        # elif isinstance(metric['value'], numpy.floating):
        #     _metric['value'] = float(metric['value'])
        # elif isinstance(metric['value'], str):
        #     _metric['value'] = metric['value']
        # elif isinstance(metric['value'], bool):
        #     _metric['value'] = bool(metric['value'])
        # else:
        #     _msg = 'Type {} is not supported for metric value'
        #     logger.error(_msg.format(str(type(metric['value']))))
        #     # TODO: decide if this raises an error, is ignored or returned as
        #     # part of an errors list
        #     continue
        #
        # result['data'].append(_metric)
        result['data'].append(metric)

    return result
