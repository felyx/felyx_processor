# -*- encoding: utf-8 -*-

"""
@author: <sylvain.herledan@oceandatalab.com>
@date: 2018-09-26
"""
from dateutil import parser
import logging


import shapely.wkt


logger = logging.getLogger('felyx.processor')

TIME_FMT = '%Y-%m-%d %H:%M:%S'

MANDATORY_FIELDS = ('__date_created', '__date_modified', '__source',
                    '__geospatial_lon_min', '__geospatial_lon_max',
                    '__geospatial_lat_min', '__geospatial_lat_max',
                    '__time_coverage_start', '__time_coverage_end',
                    '__dataset_id', '__site_id',
                    '__percentage_coverage_of_site_by_miniprod')
DYNAMIC_FIELDS = ('__site_time', '__site_lon', '__site_lat',
                  '__source_center_index', '__time_difference', '__distance')


class MissingMiniprodFields(Exception):
    """"""
    def __init__(self, missing, *args, **kwargs):

        _msg = 'The following attributes are required but were not found in ' \
               'the miniprod file: {}'.format(', '.join(missing))
        super(Exception, self).__init__(_msg, *args, **kwargs)


def _parse_datetime(str_value):
    """"""
    return parser.parse(str_value)


def dict_from_Data(miniprod):
    """
    """
    attrs = miniprod.content.ds.attrs
    result = dict_from_attrs(attrs)
    return result


def dict_from_dataset(dataset):
    """
    Note: some datetime fields should already be in the right format, but it is
    better to check... It hurts performance a little but guarantees that the
    exported dictionary only contains valid data.
    """
    attrs = dataset.attrs
    result = dict_from_attrs(attrs)
    return result


def dict_from_attrs(attrs, for_indexing=False):
    """
    """
    # @TODO do something better here with classes
    # if not for_indexing:
    missing = [x for x in MANDATORY_FIELDS if x not in attrs]
    if 0 < len(missing):
        raise MissingMiniprodFields(missing=missing)

    dataset_id = attrs['__dataset_id']
    site_id = attrs['__site_id']
    source = attrs['__source']

    _x_min = attrs['__geospatial_lon_min']
    x_min = float(_x_min)

    _x_max = attrs['__geospatial_lon_max']
    x_max = float(_x_max)

    _y_min = attrs['__geospatial_lat_min']
    y_min = float(_y_min)

    _y_max = attrs['__geospatial_lat_max']
    y_max = float(_y_max)

    wkt = 'POLYGON(({} {},{} {},{} {},{} {},{} {}))'.format(x_min, y_min,
                                                            x_min, y_max,
                                                            x_max, y_max,
                                                            x_max, y_min,
                                                            x_min, y_min)

    _percentage_coverage = attrs['__percentage_coverage_of_site_by_miniprod']
    percentage_coverage = float(_percentage_coverage)

    _date_created = attrs['__date_created']
    date_created = _parse_datetime(_date_created)

    _date_modified = attrs['__date_modified']
    if _date_modified is None:
        date_modified = ''
    else:
        date_modified = _parse_datetime(_date_modified)

    slices = attrs['__source_slices']

    time_coverage_start = attrs['__time_coverage_start']
    if isinstance(time_coverage_start, str):
        time_coverage_start = _parse_datetime(time_coverage_start)

    time_coverage_stop = attrs['__time_coverage_end']
    if isinstance(time_coverage_stop, str):
        time_coverage_stop = _parse_datetime(time_coverage_stop)

    source_time_coverage_start = attrs['__source_time_coverage_start']
    if isinstance(source_time_coverage_start, str):
        source_time_coverage_start = _parse_datetime(source_time_coverage_start)

    source_time_coverage_end = attrs['__source_time_coverage_end']
    if isinstance(source_time_coverage_end, str):
        source_time_coverage_end = _parse_datetime(source_time_coverage_end)

    # @TODO use ChildMetadata here ?
    result = {'site': site_id,
              'site_collection': attrs['__site_collection_id'],
              'source': source,
              'shape': shapely.wkt.loads(wkt),
              'slices': slices,
              # @TODO change with coverage ?
              'percentage_coverage_of_site_by_miniprod': percentage_coverage,
              'date_created': date_created,
              'date_modified': date_modified,
              'dataset': dataset_id,
              'time_coverage': [time_coverage_start, time_coverage_stop],
              'source_time_coverage_start': source_time_coverage_start,
              'source_time_coverage_stop': source_time_coverage_end
              }

    # Handle attributes for miniprods produced by an extraction on a dynamic
    # site
    missing_dynamic = [x for x in DYNAMIC_FIELDS if x not in attrs]
    l = len(missing_dynamic)
    if 0 >= l:
        # All dynamic fields are available, add them in the result
        dynamic_lon = float(attrs['__site_lon'])
        dynamic_lat = float(attrs['__site_lat'])

        _dynamic_time = attrs['__site_time']
        dynamic_time = _parse_datetime(_dynamic_time)

        result['dynamic_target_longitude'] = dynamic_lon
        result['dynamic_target_latitude'] = dynamic_lat
        result['dynamic_target_time'] = dynamic_time

        result['source_center_index'] = str(attrs['__source_center_index'])
        result['dynamic_target_time_difference'] = attrs['__time_difference']
        result['dynamic_target_distance'] = attrs['__distance']

    elif l < len(DYNAMIC_FIELDS):
        # Only some fields are provided, this should not happen
        raise MissingMiniprodFields(missing=missing_dynamic)

    return result
