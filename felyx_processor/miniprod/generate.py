# encoding: utf-8
"""
felyx_processor.miniprod
-------------------

This package contains the code used to produce felyx miniprods. It includes the
code needed to search for data in source dataset files, and the code to
produce the actual miniprod files.

.. :copyright: Copyright 2013 Pelamis Scientific Software Ltd.
.. :license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: David Poulter <david.poulter@pelamis.co.uk>
.. codeauthor:: David Poulter <david.poulter@pelamis.co.uk>
"""

import datetime
import itertools
import logging
import os
from pathlib import Path
from typing import Dict, List, Optional, Tuple

import numpy
import pandas as pd
from cerbere.feature.cbasefeature import BaseFeature

import felyx_processor.extraction
import felyx_processor.storage
import felyx_processor.storage.miniprod
from felyx_processor.extraction import extract_found_subsets_from_eo_file, get_slices
from felyx_processor.extraction.source import SourceFile
from felyx_processor.miniprod import ChildMetadata
from felyx_processor.miniprod.writers import set_miniprod_attrs
from felyx_processor.utils.configuration import (
    FelyxProcessorConfig,
    FelyxSystemConfig,
    SiteCollectionConfig,
    get_dataset_collection_extraction_config,
)

LOGGER = logging.getLogger('felyx.processor')


class CannotCreateMiniprodDir(IOError):
    """"""
    def __init__(self, miniprod_dir):
        """"""
        self.path = miniprod_dir


def build_miniprod_path(
        source_file: SourceFile,
        collection_id: str,
        site: str,
        subset: BaseFeature,
        output) -> Path:
    """"""
    build_path = felyx_processor.storage.miniprod.get_full_path  # local alias

    times = subset.dataset.cb.time

    start_dt = pd.to_datetime(
        times.to_masked_array().compressed().min()).to_pydatetime()
    stop_dt = pd.to_datetime(
        times.to_masked_array().compressed().max()).to_pydatetime()

    try:
        meta = {'dataset': source_file.dataset_id,
                'site': site,
                'time_coverage': [start_dt, stop_dt]}
    except ValueError:
        pass

    full_path_name = build_path(output, meta, collection_id)

    return full_path_name


def create_miniprod(
        felyx_sys_config: FelyxSystemConfig,
        felyx_processor_config: FelyxProcessorConfig,
        source_file: SourceFile,
        subset: BaseFeature,
        site_id: str,
        site_info: ChildMetadata,
        site_collection_id: str,
        site_collection_info: SiteCollectionConfig,
        overwrite: bool,
        miniprod_dir: Path,
        creator: Dict = None,
        is_public: bool = False,
        save: bool = True) -> Tuple[Optional[str], Optional[str]]:
    """"""
    from felyx_processor.miniprod.writers import set_miniprod_attrs, write_miniprod_file

    if subset is False:
        return None, None

    # Ensure the subset has meaningful time information
    # noinspection PyUnresolvedReferences
    # TODO should not happen : remove ?
    check_times = subset.dataset.cb.time
    has_mask = isinstance(check_times, numpy.ma.masked_array)
    if has_mask and numpy.ma.getmaskarray(check_times).all():
        _msg = ('No data is available for site {}, which for this product '
                'means no time information is available either. No '
                'miniprod will be produced for this site.')
        LOGGER.debug(_msg.format(site_id))
        return None, None

    full_path = build_miniprod_path(
        source_file=source_file,
        collection_id=site_collection_id,
        site=site_id,
        subset=subset,
        output=miniprod_dir)

    miniprod_dir = os.path.dirname(full_path)
    miniprod_name = os.path.basename(full_path)

    now = datetime.datetime.now().isoformat()

    if save:
        site_info.path = full_path

        # Look for previous result
        if os.path.exists(full_path):
            if not overwrite:
                _msg = 'Not overwriting {} due to --no-overwrite argument'
                LOGGER.debug(_msg.format(full_path))

                # Consider the job done
                # @TODO is this usefull if file is not saved?
                set_miniprod_attrs(
                    felyx_sys_config=felyx_sys_config,
                    source_file=source_file,
                    subset=subset,
                    site_info=site_info,
                    site_collection_info=site_collection_info,
                    site_collection_id=site_collection_id)

                subset.ds.attrs['__date_modified'] = now

                return site_info
            else:
                _msg = 'Deleting old file {}'
                LOGGER.info(_msg.format(full_path))

                os.remove(full_path)

        ok = felyx_processor.storage.makedirs(miniprod_dir)
        if not ok:
            raise CannotCreateMiniprodDir(miniprod_dir)

        # Write the miniprod
        write_miniprod_file(
            felyx_sys_config=felyx_sys_config,
            source_file=source_file,
            full_path_name=full_path,
            subset=subset,
            site_info=site_info,
            creator=creator,
            site_collection_info=site_collection_info,
            site_collection_id=site_collection_id)

    else:
        set_miniprod_attrs(
            felyx_sys_config=felyx_sys_config,
            source_file=source_file,
            subset=subset,
            site_info=site_info,
            creator=creator,
            site_collection_info=site_collection_info,
            site_collection_id=site_collection_id)

    return site_info


def produce_miniprods(
        felyx_sys_config: FelyxSystemConfig,
        felyx_processor_config: FelyxProcessorConfig,
        source_file: SourceFile,
        miniprod_dir: Path,
        overwrite: bool,
        site_collections: Dict = None,
        allowed_sites: List[str] = None,
        save: bool = True) -> List[ChildMetadata]:
    """Produce all miniprods"""
    results = {}

    slices = get_slices(
        felyx_sys_config=felyx_sys_config,
        felyx_processor_config=felyx_processor_config,
        source_file=source_file,
        site_collections=site_collections,
        allowed_sites=allowed_sites)

    if not save:
        return list(itertools.chain.from_iterable(slices.values()))

    # Generators
    extractions = extract_found_subsets_from_eo_file(
        felyx_context=felyx_processor_config,
        source_file=source_file,
        extractions=slices)

    children = []
    for prod in extractions:
        # @TODO too many things returned here...
        subset, site, site_info, site_collection_id, site_collections_info = \
            prod

        site_info = create_miniprod(
            felyx_sys_config=felyx_sys_config,
            felyx_processor_config=felyx_processor_config,
            source_file=source_file,
            subset=subset,
            site_id=site,
            site_info=site_info,
            site_collection_id=site_collection_id,
            site_collection_info=site_collections_info,
            overwrite=overwrite,
            miniprod_dir=miniprod_dir,
            save=save)

        if site_info is not None:
            children.append(site_info)

    return children


def read_from_source(
        felyx_config: FelyxProcessorConfig,
        felyx_sys_config: FelyxSystemConfig,
        matchups: Dict[str, Dict],
        path_pattern: str
) -> List:
    """Extract child products directly from source file, using the indexes
    of the match-ups found by felyx-extraction

   Args:
       felyx_config : Felyx processor configuration
       felyx_sys_config: Felyx system configuration
       matchups: A dictionary of match-ups
       path_pattern: Path pattern
       use_padding: Use padding option
   Returns:
       The list of child products.
   Raises:
       FileExistsError: If more than one file matching the pattern
    """
    # get extraction from manifest dataframe
    subsets = []
    for source, extractions in matchups.items():

        if len(extractions) == 0:
            continue

        # get one result to access source file metadata
        sample = next(iter(next(iter(extractions.items()))[1]))

        # guess path to the source file
        file_date = pd.to_datetime(sample.source_time_coverage_start)

        source = Path(source)
        # TODO explain in documentation with/without pattern
        source_fullpath = source
        if path_pattern:
            # TODO should be same type - remove if this case is fixed
            if str(source.name) == str(source):
                # build full path from path pattern in configuration
                source_fullpath = Path(
                     file_date.strftime(str(Path(path_pattern).parent))
                ) / str(source)
            else:
                # TODO not correct; should be source_path for full path
                pass

            if any(i in '*?[]' for i in str(path_pattern)):
                # the path is a unix pattern matching. check for corresponding file
                files = list(source_fullpath.parent.glob(source_fullpath.name))
                if len(files) > 1:
                    raise FileExistsError(
                        f'there are more than on file matching the pattern '
                        f'{str(source_fullpath)}. Your pattern matching may not '
                        f'be precise enough. felyx can not guess which file to '
                        f'open')
                if len(files) == 0:
                    logging.warning(
                        f'no file matching the pattern {str(source_fullpath)} was '
                        f'found and no match-up will be extracted for this file.')
                    continue
                source_fullpath = files[0]
        else:
            if 'source_path' in sample:
                source_fullpath = sample.source_path
            else:
                logging.warning(
                    f'no file pattern was found in configuration file and there'
                    f' is no source path defined in the manifest, so no '
                    f'matchup will be extracted for this file.')
                continue

        dataset_id = sample.dataset

        try:
            with SourceFile(felyx_config, source_fullpath, dataset_id).load() \
                    as source_file:
                # get found subsets for the current source file
                children = extract_found_subsets_from_eo_file(
                        felyx_context=felyx_config,
                        source_file=source_file,
                        extractions=extractions)

                # add metadata attributes to each subset
                for child in children:
                    subset, site, site_info, collection_id, collection = child
                    set_miniprod_attrs(
                        felyx_sys_config=felyx_sys_config,
                        source_file=source_file,
                        subset=subset,
                        site_info=site_info,
                        site_collection_info=collection,
                        site_collection_id=collection_id)
                    subsets.append(child)
        except FileNotFoundError:
            logging.warning(
                f'the file {str(source_fullpath)} does not exist and no '
                f'matchup will be extracted for this file.')
            continue

    return subsets
