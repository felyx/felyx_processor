from datetime import datetime
from pathlib import Path
import typing as T

from pydantic import BaseModel, field_serializer, field_validator
import shapely


class ChildMetadata(BaseModel):
    """Metadata of an EO data subset sliced over a static or dynamic site"""
    # site information
    # @TODO site id ?
    site: str
    # @TODO site collection id ?
    site_collection: str

    # extracted data
    dataset: str
    # id: child product identifier
    id: str
    source: Path = None
    source_time_coverage_start: datetime
    # @TODO time_coverage_end ?
    source_time_coverage_stop: datetime

    shape: shapely.Geometry
    slices: T.Dict[str, slice]
    coverage: float
    time_coverage_start: datetime
    time_coverage_stop: datetime
    path: T.Optional[Path] = None
    source_path: T.Optional[Path] = None

    # for dynamic sites
    source_center_index: T.Optional[T.Dict[str, int]] = None
    # in km
    search_radius: T.Optional[float] = None
    # in minutes
    time_window: T.Optional[int] = None

    # distance in meter between dynamic site and matching observation
    dynamic_target_distance: T.Optional[int] = None
    # time difference in seconds between dynamic site and matching observation
    dynamic_target_time_difference: T.Optional[int] = None

    dynamic_target_latitude: T.Optional[float] = None
    dynamic_target_longitude: T.Optional[float] = None
    dynamic_target_time: T.Optional[datetime] = None

    date_created: T.Optional[datetime] = None
    date_modified: T.Optional[datetime] = None

    @field_validator('slices')
    @classmethod
    def validate_slices(cls, value):
        # remove numpy dtypes
        return {k: slice(int(v.start), int(v.stop))
                for k, v in value.items()}

    @field_serializer('shape')
    def serialize_shape(self, shape: shapely.Geometry, _info):
        return shape.wkt

    @field_serializer('slices')
    def serialize_slices(self, slices: T.Dict[str, slice], _info):
        return str(slices)

    class Config:
        """Validate constraints on assignment"""
        validate_default = True
        validate_assignment = True
        arbitrary_types_allowed = True


def build_child_id(date: datetime, site_id: str, dataset_id: str) -> str:
    """Build a child product identifier"""
    return f'{date.strftime("%Y%m%d%H%M%S")}_{site_id}_{dataset_id}'