# encoding: utf-8
"""
es_miniprod_datasource
--------------------------------------

Contains classe for retrieve Miniprod data from Elastic Search

"""
import logging
import math
from datetime import datetime
from pathlib import Path
from typing import Dict, List, Tuple, Union, Optional

import pandas as pd
import shapely
from pandas import DataFrame
from pydantic import (BaseModel, field_serializer, ValidationInfo,
                      field_validator)

from felyx_processor.index.es.es_client import ESClient, ESSearchQuery
from felyx_processor.miniprod import ChildMetadata
from felyx_processor.miniprod.miniprod_datasource import MiniprodDataSource
from felyx_processor.utils.configuration import ElasticSearchConfig
from felyx_processor.utils.exceptions import FelyxProcessorError

try:
    pass
except ImportError:
    pass

# ES_OPERATIONS : keys of the operations discribed in the ES Configuration for
# Miniprods data
ES_MINIPROD_SEARCH = 'miniprod_search'
ES_MINIPROD_INDEX = 'miniprod_index'
ES_MINIPROD_DELETE = 'miniprod_delete'

# MINIPRODS INDEX PATH
ES_INDEX_MINIPRODS_PATH = 'miniprods'

# RELATIVE PATH TO MINIPROD INDEX MAPPINGS
INDEX_MINIPRODS_MAPPINGS_RELATIVE_PATH = 'share/es/miniprods_mappings.json'

TIME_FMT = '%Y-%m-%d %H:%M:%S'

# MINIPROD_FIELDS_ES_MAPPING : list of miniprods fields mapping to


class ESChildDoc(BaseModel):
    """Metadata of an EO data subset sliced over a static or dynamic site"""
    # site information
    site_id: str
    site_collection_id: str

    # extracted data
    dataset_id: str

    source: str

    source_time_coverage_start: datetime
    source_time_coverage_end: datetime

    site_geometry: shapely.Geometry
    source_slices: Dict[str, slice]

    percentage_coverage_of_site_by_miniprod: Optional[float] = None

    time_coverage_start: datetime
    time_coverage_end: datetime

    # for dynamic sites
    source_center_index: Optional[Dict[str, int]] = None

    # distance in meter between dynamic site and matching observation
    distance: Optional[int] = None
    # time difference in seconds between dynamic site and matching observation
    time_difference: Optional[int] = None

    site_lat: Optional[float] = None
    site_lon: Optional[float] = None
    site_time: Optional[datetime] = None

    date_created: Optional[datetime] = None
    date_modified: Optional[datetime] = None

    class Config:
        """Validate constraints on assignment"""
        validate_default = True
        validate_assignment = True
        arbitrary_types_allowed = True

    @field_serializer('site_geometry')
    def serialize_shape(self, shape: shapely.Geometry, _info):
        return shape.wkt

    @field_serializer('time_coverage_start', 'time_coverage_end',
                      'source_time_coverage_start', 'source_time_coverage_end',
                      'date_created', 'date_modified', 'site_time')
    def serialize_datetime(self, value: datetime, _info):
        if value is not None:
            return value.strftime(TIME_FMT)

    @field_serializer('source_slices', 'source_center_index')
    def serialize_slices(self, slices: Dict[str, slice], _info):
        return str(slices)

    @field_validator('source', mode='before')
    @classmethod
    def validate_source(cls, path: Union[str, Path], info: ValidationInfo):
        # TODO should not be needed if source should not be a Path
        return Path(path).name


# ElasticSearch fields
# TODO replace with above class
MINIPROD_FIELDS_ES_MAPPING = {
    'date_created': 'date_created',
    'date_modified': 'date_modified',
    'dataset': 'dataset_id',
    'site': 'site_id',
    'source': 'source',
    'shape': 'site_geometry',
    'slices': 'source_slices',
    'time_coverage_start': 'time_coverage_start',
    'time_coverage_stop': 'time_coverage_end',
    'source_time_coverage_start': 'source_time_coverage_start',
    'source_time_coverage_stop': 'source_time_coverage_end',
    'dynamic_target_time': 'site_time',
    'dynamic_target_longitude': 'site_lon',
    'dynamic_target_latitude': 'site_lat',
    'percentage_coverage_of_site_by_miniprod':
        'percentage_coverage_of_site_by_miniprod',
    'site_collection': 'site_collection_id',
    'source_center_index': 'source_center_index',
    'dynamic_target_time_difference': 'time_difference',
    'dynamic_target_distance': 'distance'
}


LOG = logging.getLogger('felyx.processor')


class ESMiniprodDataSource(MiniprodDataSource):
    """Elastic Search Miniprods Data Source"""

    def __init__(self,
                 yaml_config: ElasticSearchConfig = None,
                 miniprod_root_dir: Path = None
                 ):
        """Elasticsearch Data Source constructor.

        Args:
            yaml_config: The YAML ES configuration.
            miniprod_root_dir: The miniprod root directory.

        Raises:
            FelyxProcessorError: If miniprod directory does not exist.
        """
        super().__init__()

        self.__config = yaml_config
        if self.__config is None:
            self.__config = ElasticSearchConfig()

        self.__index_path = '{}{}'.format(
            self.__config.prefix, ES_INDEX_MINIPRODS_PATH)

        if miniprod_root_dir is not None and not miniprod_root_dir.exists():
            msg = ('Miniprod directory does not exist : '
                   .format(str(miniprod_root_dir)))
            LOG.error(msg)
            raise FelyxProcessorError(msg)

        self.__miniprod_root_dir = miniprod_root_dir

        # Create miniprod index
        self.create_miniprods_index()

    def get_miniprods(self,
                      datasets: List[str],
                      sites_codes: List[str] = None,
                      site_collection: str = None,
                      start: datetime = None,
                      stop: datetime = None,
                      wkt_coords: List[Dict[str, str]] = None,
                      constraints_list: bool = None,
                      filter_on_site_time: bool = False,
                      as_dataframe: List[List[float]] = True
                      ) -> Union[Dict[str, Dict], pd.DataFrame]:
        """
        Search miniprod data in datasets and list of sites over a timespan and
        spatial filter

        Args:
            datasets: the list of datasets to search
            sites_codes: the list of sites codes to search
            site_collection: the collection from which to select the miniprods
            start: the begin time coverage
            stop: the end time coverage
            wkt_coords: (optional) the spatial filter defined by a list of
                coordinates of a polygon
            constraints_list: (optional) additional data filters
            filter_on_site_time: temporal filtering on the site data times
            as_dataframe: if True, returns a dataframe, if False, returns a dict

        Returns :
                if as_dataframe is set to True :
                    a pandas dataframe with columns are miniprods metadata
                else :
                    a dictionary with keys as source EO filenames and values
                    as miniprods metadata

        Raises:
            FelyxProcessorError:
        """

        # Check if miniprod directory is defined (used for computing miniprods
        # filenames)
        if self.__miniprod_root_dir is None:
            msg = ('Miniprod directory is undefined')
            LOG.error(msg)
            raise FelyxProcessorError(msg)

        # Create search query
        terms_filters = dict()

        # Set the filters
        if sites_codes is not None:
            terms_filters[MINIPROD_FIELDS_ES_MAPPING['site']] = sites_codes

        if site_collection is not None:
            terms_filters[MINIPROD_FIELDS_ES_MAPPING['site_collection']] = \
                [site_collection]

        if datasets is not None:
            terms_filters[MINIPROD_FIELDS_ES_MAPPING['dataset']] = datasets

        range_filters = dict()

        if filter_on_site_time is True:
            range_filters[MINIPROD_FIELDS_ES_MAPPING['dynamic_target_time']] = \
                (start, stop)
        else:
            range_filters[MINIPROD_FIELDS_ES_MAPPING['time_coverage_start']] = \
                (start, stop)

        fields = list(MINIPROD_FIELDS_ES_MAPPING.values())
        fields.extend('_id')
        sort_fields = dict()
        sort_fields[MINIPROD_FIELDS_ES_MAPPING['time_coverage_start']] = 'asc'

        query = ESSearchQuery(fields, terms_filters, range_filters, wkt_coords,
                              constraints_list, False, sort_fields)
        clientES = ESClient(self.__config)

        try:
            response = clientES.search(
                self.__index_path, query, ES_MINIPROD_SEARCH)
        except FelyxProcessorError as e:
            LOG.error('Failed to retrieve miniprods for datasets'.format(
                datasets))
            if sites_codes is not None:
                LOG.error(
                    'Request was limited to the following sites: {}'.format(
                        ', '.join(sites_codes)))
            raise e

        if len(response) == 0:
            LOG.info(f'No data found for {datasets} in [{start}, '
                     f'{stop}] time range')
            return response

        if as_dataframe is True:
            # Convert response to dataframe
            miniprods_df = self.convert_miniprods_to_dataframe(
                response, filter_on_site_time)
            return miniprods_df
        else:
            # Convert ES response to dict
            output = self.convert_miniprods_to_dict(response)

        return output

    def compute_miniprod_path(self,
                              dataset_id: str,
                              site_collection_id: str,
                              site_id: str,
                              time_coverage_start: datetime,
                              miniprod_filename: str
                              ) -> Path:
        """
           Compute the absolute path of a miniprod as following :
           <miniprod_dir>/<site_collection_id>/<site_id>/<dataset_id>/
           YYYY/DDD/<miniprod_filename>

            Args:
                miniprod_filename: The miniprod short filename.
                dataset_id: The dataset id of the miniprod.
                site_collection_id: The collection id of the miniprod.
                site_id: The site id of the miniprod.
                time_coverage_start: The time start of the miniprod.

            Returns:
                Returns the absolute path of the miniprod.
        """

        year = time_coverage_start.strftime('%Y')
        day = time_coverage_start.strftime('%j')

        filepath = Path.joinpath(self.__miniprod_root_dir,
            site_collection_id, site_id, dataset_id, year, day,
                                 miniprod_filename+'.nc')

        return filepath

    def convert_miniprods_to_dict(self,
                                 es_response: List[Dict]
                                  ) -> Dict[str, Dict]:
        """
           Convert ES response to dictionary.

            Args:
               es_response: ES response.

            Returns:
                Returns a dict where keys are the source EO filenames and
                values the found miniprods as extraction metadata objects.
        """
        result = dict()

        for miniprod in es_response:
            miniprod_id = miniprod['_id']
            es_metadata = miniprod['_source']

            # Convert the ES fields to miniprods fields
            metadata = {
                key: es_metadata[field_es]
                for key, field_es in MINIPROD_FIELDS_ES_MAPPING.items()
                if field_es in es_metadata
            }

            source = metadata['source']
            collection = metadata['site_collection']
            site = metadata['site']
            if source not in result:
                result[source] = dict()
            if collection not in result[source]:
                result[source][collection] = dict()
            if site not in result[source][collection]:
                result[source][collection][site] = list()

            # Add extra fields
            metadata['id'] = miniprod_id
            metadata['slice'] = eval(metadata['slices'])
            metadata['shape'] = shapely.wkt.loads(metadata['shape'])
            metadata['code'] = metadata['site']

            # Compute miniprod file path
            metadata['path'] = str(self.compute_miniprod_path(
                                    dataset_id=metadata['dataset'],
                                    site_collection_id=collection,
                                    site_id=site,
                                    time_coverage_start=datetime.strptime(
                                            metadata['time_coverage_start'],
                                            '%Y-%m-%d %H:%M:%S'),
                                    miniprod_filename=miniprod_id
            ).resolve())

            result[source][collection][site].append(metadata)

        return result

    def convert_miniprods_to_dataframe(
            self,
            es_response: List[Dict],
            filter_on_site_time: bool = False) \
            -> DataFrame:
        """
            Convert ES response to dictionary

            Args:
                es_response: ES response
                filter_on_site_time: (optional) filter dynamic time flag

            Returns:
                a pandas dataframe with columns as miniprods metadata
        """

        miniprod_list = list()
        for miniprod in es_response:
            miniprod_id = miniprod['_id']
            es_metadata = miniprod['_source']
            es_metadata['id'] = miniprod_id
            miniprod_list.append(es_metadata)

        # Create the dataframe
        miniprods_df = pd.DataFrame(miniprod_list)

        # Convert the ES fields to miniprods fields
        columns_dict = {value: key for key, value in
                        MINIPROD_FIELDS_ES_MAPPING.items()}
        miniprods_df = miniprods_df.rename(columns=columns_dict)

        # Sort
        if filter_on_site_time:
            column_time = 'dynamic_target_time'
        else:
            column_time = 'time_coverage_start'

        miniprods_df.sort_values(
            inplace=True,
            by=['site', column_time],
            ignore_index=True)

        # Compute miniprod file path
        miniprods_df['path'] = miniprods_df.apply(
            lambda x: str(self.compute_miniprod_path(
                dataset_id=x['dataset'],
                site_collection_id=x['site_collection'],
                site_id=x['site'],
                time_coverage_start=datetime.strptime(
                    x['time_coverage_start'], '%Y-%m-%d %H:%M:%S'),
                miniprod_filename=x['id']).resolve()), axis=1)

        # Convert time columns
        miniprods_df['time_coverage_start'] = pd.to_datetime(
            miniprods_df['time_coverage_start'])
        miniprods_df['time_coverage_stop'] = pd.to_datetime(
            miniprods_df['time_coverage_stop'])
        miniprods_df['source_time_coverage_start'] = pd.to_datetime(
            miniprods_df['source_time_coverage_start'])
        miniprods_df['source_time_coverage_stop'] = pd.to_datetime(
            miniprods_df['source_time_coverage_stop'])
        miniprods_df['dynamic_target_time'] = pd.to_datetime(
            miniprods_df['dynamic_target_time'])

        # Add some extra columns
        miniprods_df['slice'] = miniprods_df['slices']
        miniprods_df['shape'] = miniprods_df['shape']
        miniprods_df['code'] = miniprods_df['site']


        return miniprods_df

    def _timecoverage2doc(self,
                          time_coverage: Tuple
                          ) -> Dict:
        """
           Convert and check time coverage

           Args:
               time_coverage: a tuple with start and end date

           Returns:
               a dictonary with keys are time_coverage_start and
               time_coverage_stop and values are datetimes in string

            Raises:
                FelyxProcessorError: If invalid time coverage.
        """
        result = {}
        if 2 != len(time_coverage):
            msg = 'Invalid time coverage field'
            LOG.error(msg)
            raise FelyxProcessorError(msg)

        key = 'time_coverage_start'
        value = time_coverage[0]
        if isinstance(value, datetime):
            result[key] = value.strftime(TIME_FMT)
        else:
            result[key] = value

        key = 'time_coverage_stop'
        value = time_coverage[1]
        if isinstance(value, datetime):
            result[key] = value.strftime(TIME_FMT)
        else:
            result[key] = value

        return result

    def convert_child_to_esdoc(
            self,
            child: ChildMetadata
    ) -> ESChildDoc:
        """
        Convert a child metadata to ES doc item

        Paramaters
        ----------
        child:
            a child metadata object

        Returns
        -------
        dict:
            a ES ready doc item

        Raises
        ------
        FelyxProcessorError:
            if invalid child metadata
        """
        return ESChildDoc(**dict(
            date_created=child.date_created,
            date_modified=child.date_modified,
            dataset_id=child.dataset,
            site_id=child.site,
            source=child.source,
            site_geometry=child.shape,
            source_slices=child.slices,
            time_coverage_start=child.time_coverage_start,
            time_coverage_end=child.time_coverage_stop,
            source_time_coverage_start=child.source_time_coverage_start,
            source_time_coverage_end=child.source_time_coverage_stop,
            site_time=child.dynamic_target_time,
            site_lon=child.dynamic_target_longitude,
            site_lat=child.dynamic_target_latitude,
            percentage_coverage_of_site_by_miniprod=child.coverage,
            site_collection_id=child.site_collection,
            source_center_index=child.source_center_index,
            time_difference=child.dynamic_target_time_difference,
            distance=child.dynamic_target_distance))

    def register_miniprods(
            self,
            children: List[ChildMetadata],
            **kwargs
    ):
        """
        Register miniprods data

        Args:
            children: the list of miniprods to register as a list of tuples
             of dictionaries formatted as following :
            (
            'filenane',
            {
                'site': 'site_code',
                'source': 'source_filename',
                'shape': 'POLYGON((Longitude Latitude, ...))',
                'percentage_coverage_of_site_by_miniprod': double_value,
                'date_created': datetime(year, month, day, hour, minute, sec),
                'date_modified': datetime(year, month, day, hour, minute, sec),
                'dataset': 'dataset_name',
                'time_coverage': ['YYYY-MM-DD HH:MM:SS', 'YYYY-MM-DD HH:MM:SS'],
                'dynamic_target_longitude': longitude,
                'dynamic_target_latitude': latitude,
                'dynamic_target_time':
                            datetime(year, month, day, hour, minute, second)
            }
        )

        Raises:
            FelyxProcessorError: If register failed.
        """
        if 0 >= len(children):
            LOG.warning('No child products to register')
            return

        LOG.debug('Convert dicts to indexable documents')
        # Path(x.source).name should not be necessary
        docs = [(child.id, self.convert_child_to_esdoc(child).model_dump(),)
                for child in children]

        # Index  documents in Elastic search
        clientES = ESClient(self.__config)

        try:
            response = clientES.index(
                self.__index_path, docs, ES_MINIPROD_INDEX)
            LOG.debug(response)
        except FelyxProcessorError as e:
            LOG.error('Failed to register miniprods')
            raise e

    def create_miniprods_index(self):
        """
            Create the miniprods index if it does not exist

            Raises:
                FelyxProcessorError: If create index failed.
        """

        # Create miniprods index in Elastic search
        clientES = ESClient(self.__config)

        mappings_path = Path(__file__).parent.parent / \
                        INDEX_MINIPRODS_MAPPINGS_RELATIVE_PATH

        try:
            # Check if the index already exists, if not, create it
            if not clientES.has_index(self.__index_path):
                response = clientES.create_index(self.__index_path,
                                                 mappings_path)
                LOG.debug(response)
        except FelyxProcessorError as e:
            LOG.error('Failed to create miniprods index : {}'.format(
                self.__index_path))
            raise e

    def delete_miniprods_index(self):
        """
            Delete the miniprods index

            Raises:
                FelyxProcessorError: If delete failed.
        """
        # Delete index in Elastic search
        clientES = ESClient(self.__config)
        try:
            # Check if the index exists before deleting
            if clientES.has_index(self.__index_path):
                response = clientES.delete_index(self.__index_path)
                LOG.debug(response)
        except FelyxProcessorError as e:
            LOG.error('Failed to delete miniprods index : {}'.format(
                self.__index_path))
            raise e

    def delete_miniprods(self,
                        miniprods_ids : List[str]
                    ) -> int:
        """
            Delete miniprods data refering to a list of miniprod ids

            Args:
                miniprods_ids: the list of miniprod ids
            Returns:
                The number of deleted miniprods

            Raises:
                FelyxProcessorError: If delete failed.
        """
        if 0 >= len(miniprods_ids):
            LOG.warning(
                'Cannot delete miniprods from an empty miniprod id list...')
            return 0

        # Index documents in Elastic search
        clientES = ESClient(self.__config)
        try:
            nb_docs = clientES.delete_by_ids(self.__index_path,
                                                miniprods_ids,
                                                ES_MINIPROD_DELETE)
        except FelyxProcessorError as e:
            LOG.error('Failed to delete miniprods')
            raise e

        return nb_docs

    def convert_datasets_coverage_to_dict(self,
                                          es_response: Dict,
                                          fieldname: str,
                                          start_fieldname: str,
                                          end_fieldname: str) \
            -> Dict[str, Tuple[datetime, datetime]]:
        """
           Convert datasets coverage ES response to dictionary

           Args:
                es_response: ES response
                fieldname: name of the field to search
                start_fieldname: name of the coverage start field
                end_fieldname: name of the coverage end field

           Returns:
                Returns a dict where keys are the dataset ids and
                values a tuple containing the coverage start and end dates
        """
        coverages = dict()
        facets = es_response['aggregations'][fieldname]['buckets']
        if len(facets) == 0:
            return coverages

        for i in range(0, len(facets)):
            dataset_name = facets[i]['key']
            start = facets[i][start_fieldname]['value']
            if math.isinf(float(start)):
                continue
            timestamp = 0.001 * start
            start_date = datetime.utcfromtimestamp(timestamp)

            stop = facets[i][end_fieldname]['value']
            if math.isinf(float(stop)):
                continue
            timestamp = 0.001 * stop
            stop_date = datetime.utcfromtimestamp(timestamp)

            coverages[dataset_name] = (start_date, stop_date)

        result = {k: v for k, v in coverages.items() if 1 < len(v)}
        return result

    def get_datasets_coverage(self) \
            -> Dict[str, Tuple[datetime, datetime]]:
        """
            Search datasets time coverages in miniprods data.

            Returns:
                Returns a dict where keys are the dataset ids and
                values a tuple containing the coverage start and end dates.

            Raises:
                FelyxProcessorError: If retrieve failed.
        """
        clientES = ESClient(self.__config)

        try:
            response = clientES.search_coverages(
                self.__index_path, ES_MINIPROD_SEARCH,
                'dataset_id', 'time_coverage_start', 'time_coverage_end')
        except FelyxProcessorError as e:
            LOG.error('Failed to retrieve coverages for datasets')
            raise e

        if len(response) == 0:
            LOG.info(f'No data found for datasets')
            return dict()

        result = self.convert_datasets_coverage_to_dict(response,
                    'dataset_id', 'time_coverage_start', 'time_coverage_end')
        return result
