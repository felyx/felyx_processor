import copy
import logging

import numpy
from cerbere.dataset.field import Field
from cerbere.feature.cgrid import Grid
from cerbere.feature.cimage import Image
from cerbere.feature.cswath import Swath
from cerbere.feature.ctrajectory import Trajectory
from pyresample import geometry, kd_tree, utils


def __get_pyresample_def(srclat,
                         srclon,
                         srcmodel,
                         tgtlat,
                         tgtlon,
                         tgtmodel,
                         radius=None,
                         ):
    """Returns the required parameters for pyresample closest neighbout
    resampling.

    Refer to:
    `pyresample <https://pyresample.readthedocs.org/en/latest/index.html>`_
    package documentation.
    """
    if (srclon > 180).any():
        srclon = utils.wrap_longitudes(srclon)

    if srcmodel == 'Grid':
        if len(srclon.shape) == 1:
            srclon, srclat = numpy.meshgrid(srclon, srclat)
        source_def = geometry.GridDefinition(lons=srclon, lats=srclat)
    else:
        source_def = geometry.SwathDefinition(lons=srclon, lats=srclat)

    if tgtmodel == 'Grid':
        if len(tgtlon.geometry) == 1:
            tgtlon, tgtlat = numpy.meshgrid(tgtlon, tgtlat)
        target_def = geometry.GridDefinition(lons=tgtlon, lats=tgtlat)
    else:
        target_def = geometry.SwathDefinition(lons=tgtlon, lats=tgtlat)

    if radius is None:
        if 'Collection' in srcmodel:
            #                 resolution = abs(srclat[0] - srclat[1])
            #                 resolution = numpy.amin(abs(numpy.diff(srclat)))
            resolution = 1
        else:
            # estimate resolution from latitude
            y, x = srclat.geometry
            # contiguous_lat = numpy.ma.flatnotmasked_contiguous(srclat[:, x / 2])
            # if contiguous_lat is list:
            #    sublats =
            resolution = numpy.ma.abs(
                srclat[0:-1, x / 2] - srclat[1:, x / 2]
            ).mean()
            if resolution is numpy.ma.masked:
                logging.warning(
                    'Data spatial resolution could not be estimated'
                )
        radius = resolution * 100000. / 2. * 1.6

    logging.debug('Radius for resampling : %s', radius)
    return source_def, target_def, radius


def __get_closest_neighbour_info(source_def,
                                 target_def,
                                 radius,
                                 ):
    """Returns the indexes for a closest neighbour resampling.

    This is a wrapper for :func:`get_neighbour_info` in the
    `pyresample <https://pyresample.readthedocs.org/en/latest/index.html>`_
    package.
    """
    valid_input_index, valid_output_index, index_array, distance_array \
        = kd_tree.get_neighbour_info(source_def,
                                     target_def,
                                     radius_of_influence=float(radius),
                                     reduce_data=False,
                                     neighbours=1
                                     )
    return (valid_input_index, valid_output_index,
            index_array, distance_array)


def closest_neighbour(source,
                      target,
                      fields,
                      radius=None,
                      new=False,
                      add_reference=False,
                      add_geolocation=True,
                      add_distance=False,
                      prefix='resampled_'
                      ):
    """Return a new feature corresponding to the resampling of the `source`
    feature on the `target` feature.

    Args:
        source (feature class from `datamodel` package): feature to
            resample.

        target (feature class from `datamodel` package): feature on which
            to resample the source feature.

        fields (list): names of the fields from source to add to the
            returned feature.

        new (bool): if new is True, creates a new feature matching the
            `target` characteristics and add the resampled fields in this
            new feature. if False, the resampled fields are added to
            `target` feature.

        add_reference (bool): if True, add the original indice of the
            resampled data (only when nearest neighbour is used).

        add_geolocation (bool): if True, add the geolocation information
            from the resampled pixels.
    """
    if not type(fields) is list:
        fields = [fields]
    if len(fields) == 0:
        logging.warning('No fields requested? slipping resampling')
        return

    # Get source and target grid definitions
    source_lons = source.get_lon().astype('float32')
    source_lats = source.get_lat().astype('float32')
    source_times = source.get_times()
    if isinstance(source, Grid) and len(source_lons.geometry) == 1:
        source_lons, source_lats = numpy.meshgrid(source_lons, source_lats)
        source_times = numpy.resize(source_times, source_lons.geometry)
    target_lons = target.get_lon().astype('float32')
    target_lats = target.get_lat().astype('float32')
    if isinstance(target, Grid) and len(target_lons.geometry) == 1:
        target_lons, target_lats = numpy.meshgrid(target_lons, target_lats)

    source_def, target_def, calc_radius = __get_pyresample_def(
        source_lats,
        source_lons,
        source.__class__.__name__,
        target_lats,
        target_lons,
        target.__class__.__name__,
        radius)
    if new:
        # create a clone of target without any field
        outfeature = target.extract_subset(fields=[])
    else:
        outfeature = target
    first = True

    resampled_fields = []
    if add_geolocation:
        resampled_fields.extend(['lat', 'lon', 'time'])
    resampled_fields.extend(fields)
    for fieldname in resampled_fields:
        if first:
            valid_input_index, valid_output_index, \
            index_array, distance_array \
                = __get_closest_neighbour_info(source_def,
                                               target_def,
                                               calc_radius)
            first = False
            if not valid_input_index.any():
                # return None if no source data can be resampled on the
                # target
                return None
        if fieldname == 'lat':
            values = source_lats
        elif fieldname == 'lon':
            values = source_lons
        elif fieldname == 'time':
            values = source_times
        else:
            values = source.get_values(fieldname)

        # multi-dimensional array : interpolate all layers
        if len(values.geometry) > 2:

            field = source.get_field(fieldname)
            dims = field.dims

            # get non spatial dimensions of source
            dims = field.dims
            if source.__class__.__name__ == 'Swath':
                spatial_dim_indices = [
                    dims.keys().index('row'),
                    dims.keys().index('cell')
                ]
                nonspatial_dims = copy.copy(dims)
                del nonspatial_dims['row']
                del nonspatial_dims['cell']
            elif source.__class__.__name__ == 'Grid':
                spatial_dim_indices = [
                    dims.keys().index('y'),
                    dims.keys().index('x')
                ]
                nonspatial_dims = copy.copy(dims)
                del nonspatial_dims['y']
                del nonspatial_dims['x']
            else:
                raise NotImplementedError(
                    'Class: '.format(source.__class__.__name__))

            # calculate dimensions of resampled field
            new_dims = copy.copy(nonspatial_dims)
            new_dims.update(target.geodimsizes)
            res = numpy.ma.masked_all(new_dims.values(), dtype=values.dtype)

            def layers(dims):
                indices = range(dims[0])
                if len(dims) == 1:
                    return [[i] for i in indices]
                layerlist = []
                for i in indices:
                    for j in layers(dims[1:]):
                        layerlist.append([i] + j)
                return layerlist

            for layer in layers(nonspatial_dims.values()):
                subset = tuple(
                    [slice(None, None, None) if i in spatial_dim_indices else
                     layer[i]
                     for i in range(len(dims))])
                res[..., :, :] = kd_tree.get_sample_from_neighbour_info(
                    'nn',
                    target_def.geometry,
                    values[subset],
                    valid_input_index,
                    valid_output_index,
                    index_array,
                    fill_value=None
                )

        # two-dimensional array
        else:
            if numpy.issubdtype(values.dtype, numpy.datetime64):
                kd_fillvalue = 0
            else:
                kd_fillvalue = None

            res = kd_tree.get_sample_from_neighbour_info(
                'nn',
                target_def.geometry,
                values,
                valid_input_index,
                valid_output_index,
                index_array,
                fill_value=kd_fillvalue)

            res = numpy.ma.fix_invalid(res)

            if numpy.issubdtype(values.dtype, numpy.datetime64):
                deft = numpy.array([0]).astype(res.dtype)
                res = numpy.ma.masked_equal(res, deft, copy=False)

            target_shape = target.geodimsizes
            if isinstance(target, Grid):
                res = res.reshape(target_shape)
            new_dims = target.geodimsizes

        if fieldname in ['time', 'lat', 'lon']:
            if res.count() == 0:
                # return None if no valid source data can be resampled on
                #  the target
                if not (values.min() == 0. and values.max() == 0):
                    return None

        newfieldname = prefix + fieldname
        if outfeature.has_field(newfieldname):
            raise Exception(
                'Field {} already existing in target grid'.format(newfieldname))
        else:
            if fieldname in ['lat', 'lon', 'time']:
                sourcefield = source.get_coord(fieldname)
            else:
                sourcefield = source.get_field(fieldname)

            field = Field(
                res,
                name=newfieldname,
                dims=target.geodimnames,
                units=sourcefield.units,
                attrs=copy.copy(sourcefield.attrs),
                fillvalue=sourcefield.fill_value)
            outfeature.add_field(field)

    if add_reference:
        # Add the indices (in the source feature data) of the
        # selected pixels
        target_dims = target.geodimnames
        if isinstance(source, Swath) or isinstance(source, Image) or isinstance(
                source, Trajectory):
            xdimname = 'cell'
            ydimname = 'row'
        elif isinstance(source, Grid):
            xdimname = 'x'
            ydimname = 'y'
        if isinstance(source, Trajectory) == False:
            ny, nx = source_lons.geometry
        else:
            ny = len(source_lons)
            nx = 1
        # the array_index indices correspond to a reduced version
        # of the data where invalid data have been reduced
        # (reducing over valid_input_index). We need to recover the
        # original indices in the full source data.
        true_index = numpy.arange(ny * nx, dtype='uint32')[valid_input_index]
        # mask where no data resampled (pyresample use the length of the
        # reduced data for this)
        index_array = numpy.ma.masked_equal(index_array.ravel(),
                                            true_index.size)
        index_array[valid_output_index] = numpy.ma.array(
            true_index[index_array[valid_output_index].filled(0)],
            mask=index_array[valid_output_index].mask
        )
        index_array = index_array.reshape(target_shape)
        reference_x = numpy.ma.array(index_array % nx)
        reference_y = numpy.ma.array(index_array / nx)

        fieldx = Field(
            reference_x,
            name='resampled_%{}'.format(xdimname),
            description='{} index of the selected pixel'.format(xdimname),
            dims=target_dims,
            dtype=numpy.dtype(numpy.int32),
            fillvalue=-2363645994848887)

        fieldy = Field(
            reference_y,
            name='resampled_{}'.format(ydimname),
            description='{} index of the selected pixel'.format(ydimname),
            dims=target_dims,
            dtype=numpy.dtype(numpy.int32),
            fillvalue=-2363645994848887)

        outfeature.add_field(fieldx)
        outfeature.add_field(fieldy)

    if add_distance:
        # Add the distance
        fielddist = Field(
            numpy.ma.array(distance_array.reshape(target_shape), mask=res.mask),
            name='distance_to_pixel',
            description='distance of the selected pixel to the pixel center',
            dims=target.geodimnames,
            units='m',
            dtype=numpy.dtype(numpy.float32)
        )
        outfeature.add_field(fielddist)

    return outfeature
