# encoding: utf-8
"""
manifest_miniprod_datasource
--------------------------------------

Contains classe for retrieve Miniprod data from manifest files

"""
import logging
from datetime import datetime
from pathlib import Path
from typing import Dict, List, Tuple, Union

import pandas as pd
import shapely.wkt

from felyx_processor.miniprod import miniprod_datasource, ChildMetadata
from felyx_processor.miniprod.miniprod_datasource import MiniprodDataSource
from felyx_processor.utils.exceptions import FelyxProcessorError

try:
    pass
except ImportError:
    pass

# Manifest file pattern
MANIFEST_FILEPATH_PATTERN = '/%Y/%j/*.manifest'

# Columns mapping for Pandas dataframe
COLUMNS_MAP = {
    0: miniprod_datasource.SITE_COLUMN,
    1: miniprod_datasource.TIME_COLUMN,
    2: miniprod_datasource.ID_COLUMN,
    3: miniprod_datasource.DATASET_COLUMN,
    4: 'time_coverage_start'
}

TIME_FMT = '%Y-%m-%d %H:%M:%S'

LOG = logging.getLogger('felyx.processor')


class ManifestMiniprodDataSource(MiniprodDataSource):
    """Manifest Miniprods Data Source"""

    def __init__(
            self,
            manifest_root_dir: Path

    ):
        """Manifest Data Source constructor.

        Parameters
        ----------
        manifest_root_dir: Path
            the root dir where are stored the manifest files

        Raises
        ------
        FelyxProcessorError: if manifest directory does not exist.
        """
        super().__init__()

        if manifest_root_dir is None:
            msg = ('Cannot instanciate Miniprod datasource from manifest '
                   'because manifest directory is not defined')
            LOG.error(msg)
            raise FelyxProcessorError(msg)

        if not manifest_root_dir.exists():
            msg = ('Manifest directory does not exist : '
                   .format(str(manifest_root_dir)))
            LOG.error(msg)
            raise FelyxProcessorError(msg)

        self._manifest_root_dir = manifest_root_dir

    def set_filename(self, filename: Path):
        """ Initialize the data source with a single file (for testing)

        Args:
            filename: Path of the file.
        """
        self.filename = filename

    def _compute_file_to_read(
            self,
            datasets: List[str],
            start: datetime,
            stop: datetime
    ) -> List[Path]:
        """
        Compute the list of files of a dataset list to read from the time
        coverage.

        Args:
            datasets: the dataset list of the files
            start : the begin time coverage
            stop : the end time coverage

        Returns:
            A the file names list (absolute paths)
        """

        # Compute the time range
        start = pd.date_range(start, periods=1, freq='1D')[0]
        stop = pd.date_range(stop, periods=1, freq='1D')[0]
        date_list = pd.date_range(
            start=min(start, stop), end=max(stop, start), freq='1D')

        # For each dataset
        file_list = list()
        for dataset in datasets:
            file_sublist = list()

            filepath_pattern = Path.joinpath(
                self._manifest_root_dir,
                dataset + MANIFEST_FILEPATH_PATTERN)
            logging.info(
                f'searching {dataset} manifests with pattern:'
                f' {filepath_pattern}')

            # Compute file names
            for date in date_list:
                path = Path(date.strftime(str(filepath_pattern)))
                file_sublist.extend(Path(path.anchor).glob(
                    str(path.relative_to(path.anchor))))

            # keep only existing and non empty files
            file_sublist = [_ for _ in file_sublist
                            if (_.exists() and _.stat().st_size > 0)]
            file_sublist.sort()
            file_list.extend(file_sublist)

        for file in file_list:
            LOG.debug('Manifest file to read : {}'.format(file))

        if len(file_list) == 0:
            LOG.info(
                f'There are no matching manifest files for datasets : '
                f'{datasets}')

        return file_list

    def _apply_temporal_filter(
            self,
            df: pd.DataFrame,
            start: datetime = datetime(1900, 1, 1),
            stop: datetime = datetime(3000, 1, 1),
            filter_on_site_time: bool = False
    ) -> pd.DataFrame:
        """
        Extract a part of a data frame using a temporal filter

        Args:
            df: The data frame
            start (optional) : the begin time coverage
            stop (optional) : the end time coverage
            filter_on_site_time: temporal filtering on the site data times

        Returns:
            a extracted data frame
        """
        start = start.replace(tzinfo=None)
        stop = stop.replace(tzinfo=None)

        if filter_on_site_time is True:
            time_field = 'dynamic_target_time'
        else:
            time_field = 'time_coverage_start'

        return df[((df[time_field] >= start) & (df[time_field] <= stop))]

    def read(self,
             datasets: List[str],
             start: datetime,
             stop: datetime,
             filename: Path = None):
        """
        Read Manifest files

        Args:
            datasets: the dataset list of the files
            start: the start time
            stop: the end time
            filename: direct path to the manifest file (for testing)
        """
        empty_res = pd.DataFrame(
            [], columns=miniprod_datasource.MINIPROD_COLUMNS)

        if filename is not None:
            # Read a single file : for testing
            return self.read_manifest_file(filename)

        # Compute the files to read from the time coverage
        files = self._compute_file_to_read(datasets, start, stop)
        if len(files) == 0:
            LOG.info(
                f'No child products found for {datasets} in [{start}, {stop}] '
                f'time range')
            return empty_res

        return pd.concat([self.read_manifest_file(file) for file in files])

    def read_manifest_file(self, file: Path) -> pd.DataFrame:
        """
        Read a manifest file and convert it to a dataframe

        Args:
            file: the path to the file to read

        Returns:
            a dataframe containing the miniprod metadata with the following
            columns : site, date, id, dataset
        """
        # @TODO decide if __ or not for all columns
        df = pd.read_json(
            file, lines=True,
            convert_dates=['dynamic_target_time', 'time_coverage_start',
                           'time_coverage_end', 'source_time_coverage_start',
                           'source_time_coverage_end',
                           # @TODO remove ?
                           'date_created', 'date_created'
                           ])

        if df.empty:
            return df

        # convert some fields to proper dtype
        if 'shape' in df.columns:
            df['shape'] = df["shape"].apply(lambda d: shapely.wkt.loads(d))
        df['slices'] = df["slices"].apply(lambda d: eval(d))

        df = df.rename(
            columns={'time_difference': '__time_difference',
                     'distance': '__distance'})

        return df

    def get_miniprods(self,
                      datasets: List[str],
                      sites_codes: List[str] = None,
                      site_collection: str = None,
                      start: datetime = None,
                      stop: datetime = None,
                      wkt_coords: List[Dict[str, str]] = None,
                      constraints_list: bool = None,
                      filter_on_site_time: bool = True,
                      as_dataframe: bool = True
                      ) -> Union[Dict[str, Dict], pd.DataFrame]:
        """
        Search miniprod data in datasets and list of sites over a timespan and
        spatial filter

        Args:
         datasets: the list of datasets to search
         sites_codes: the list of sites codes to search
         site_collection: the collection from which to select the miniprods
         start: the begin time coverage
         stop: the end time coverage
         wkt_coords: (optional) the spatial filter defined by a list of
             coordinates of a polygon
         constraints_list: (optional) additional data filters
         filter_on_site_time: temporal filtering on the site data times
         as_dataframe: if True, returns a dataframe, if False, returns a dict

        Returns :
             if as_dataframe is set to True :
                 a pandas dataframe with columns are miniprods metadata
             else :
                 a dictionary with keys as source EO filenames and values
                 as miniprods metadata

        Raises:
            NotImplementedError: for spatial filter and additional data filters

        Note:
            Result as a dict instead of dataframe is not yet implemented.
        """
        # Check input parameters
        if wkt_coords is not None:
            raise NotImplementedError(
                'The spatial filter is not yet implemented for manifest files')

        if constraints_list is not None:
            raise NotImplementedError(
                'The additional data filters is not yet implemented for '
                'manifest files')

        # Read the Manifest files into a Pandas dataset
        df = self.read(datasets=datasets, start=start, stop=stop)
        if df.empty:
            return df

        # the site collection to select the miniprods extracted with
        if site_collection is not None:
            df = df[df.site_collection == site_collection]
            if df.empty:
                LOG.info(f'No child products for site collection :'
                         f' {site_collection}')
                return df

        # Then keep only the miniprods matching the sites codes
        if sites_codes is not None:
            df = df[df.site.isin(sites_codes)]
            if df.empty:
                LOG.info(f'No data matching the sites : {sites_codes}')
                return df

        # Extract observations with temporal filtering
        df = self._apply_temporal_filter(df, start, stop, filter_on_site_time)
        if df.empty:
            LOG.info(f'No data found for {datasets} in [{start}, '
                     f'{stop}] time range')
            return df

        # main time column
        if filter_on_site_time:
            column_time = 'dynamic_target_time'
        else:
            column_time = 'time_coverage_start'

        df.sort_values(
            inplace=True,
            by=[miniprod_datasource.SITE_COLUMN, column_time],
            ignore_index=True)

        if not as_dataframe:
            # Convert response to dict
            return self.as_extractions(df)

        return df

    def manifest_path(
            self,
            source_path: Path,
            dataset: str,
            date: datetime
    ) -> Path:
        """Construct the path to a manifest file from an EO granule (file)
        properties

        Parameters
        ----------
        source_path: Path
            the path to the input EO granule
        dataset: str
            the identifier of the dataset the granule belongs to
        date: datetime
            the date associated with the granule (start date/time)

        Returns
        -------
        manifest_path: Path
            the constructed theoretical path to a manifest file
        """
        manifest_dir = self._manifest_root_dir.joinpath(dataset, date.strftime(
            '%Y/%j/'))

        return manifest_dir / f'{source_path.name}.manifest'

    def register_miniprods(
            self,
            children: List[ChildMetadata],
            manifest_path: Path,
            **kwargs
    ):
        """
        Register children products metadata into a manifest file

        Parameters
        ----------
        children: List[ChildMetadata]
            the list of child products to save in the manifest file

        """
        # Create manifest directory
        manifest_path.parent.mkdir(parents=True, exist_ok=True)

        with open(manifest_path, 'w') as output_file:
            for child in children:
                # @TODO how to properly serialize slices in json ?
                # @TODO use full json ? here only a list of json elements
                line = child.model_dump_json(
                    exclude_none=True, exclude={'mask'})
                output_file.write(f'{line}\n')

    def delete_miniprods(self, miniprods_ids: List[str]) -> int:
        """
        Delete miniprods data refering to a list of miniprod ids

        Args:
          miniprods_ids: the list of miniprod ids

        Returns:
          the number of deleted miniprods

        Raises:
            NotImplementedError
        """
        raise NotImplementedError

    def get_datasets_coverage(self) -> Dict[str, Tuple[datetime, datetime]]:
        """
        Search datasets time coverages in miniprods data.

        Returns:
         Returns a dict where keys are the dataset ids and
         values a tuple containing the coverage start and end dates.

        Raises:
            NotImplementedError
        """
        raise NotImplementedError
