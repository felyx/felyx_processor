# encoding: utf-8
"""Helper functions for computing footprint geometries  with geohashes."""
import typing as T

import numpy as np
from polygon_geohasher.polygon_geohasher import geohashes_to_polygon
import shapely
import shapely.ops

from cerbere.feature.cbasefeature import BaseFeature


# number of level+1 geohashes for a geohash of a given precision level
GH_COUNT = 32


def latlon_to_geohash(
        lat: np.ndarray,
        lon: np.ndarray,
        precision: int = 4,
        mask: np.ndarray = None
) -> np.ndarray:
    """Returns the list of geohashes for a list of coordinates"""
    try:
        import pyinterp.geohash as geohash
    except ModuleNotFoundError:
        raise ModuleNotFoundError(
            'geohash package is not installed. Optional geohash based tiling '
            'is not available. Install geohash package.'
        )

    if mask is not None:
        lat = np.ma.masked_where(mask, lat, copy=False)
        lon = np.ma.masked_where(mask, lon, copy=False)

    clat = np.ma.masked_where(lat >= 90., lat).compressed()
    clon = np.ma.masked_where(lat >= 90., lon).compressed()
    geo = np.unique(geohash.encode(clon, clat, precision=precision))

    return geo.astype(np.str_)


def to_geohash(
        feature: BaseFeature,
        precision: int = 4,
        mask: np.ndarray = None) -> np.ndarray:
    """Returns the list of geohashes covered by a cerbere feature"""
    lat = feature.ds.cb.latitude
    lon = feature.ds.cb.longitude

    if mask is not None:
        lat = np.ma.masked_where(mask, lat, copy=False)
        lon = np.ma.masked_where(mask, lon, copy=False)

    return latlon_to_geohash(lat, lon, precision)


def latlon_to_footprint(
        lat: np.ndarray,
        lon: np.ndarray,
        precision: int = 4) -> shapely.Geometry:
    """Returns the footprint polygon matching a list of positions, based on
    geohash extraction method.

    Any arguments to `latlon_to_geohash`, `reduce` or `geohashes_to_polygon` can
    be used here.
    """
    geohashes = latlon_to_geohash(lat, lon, precision)
    reduced_geohashes = reduce(geohashes)

    return to_polygon(reduced_geohashes)


def footprint(feature: BaseFeature,
              precision: int = 4,
              mask: np.ndarray = None) -> shapely.Geometry:
    """Returns the footprint polygon matching a cerbere feature, based on
    geohash extraction method

    Any arguments to `latlon_to_geohash`, `reduce` or `geohashes_to_polygon` can
    be used here.

    Args:
        mask: mask of invalid measurements (to remove these locations from the
            footprint computation)
    """
    lat = feature.dataset.cb.latitude
    lon = feature.dataset.cb.longitude
    if mask is not None:
        lat = np.ma.masked_where(mask, lat, copy=False)
        lon = np.ma.masked_where(mask, lon, copy=False)

    return latlon_to_footprint(lat, lon, precision=precision)


def reduce(geohashes: np.ndarray,
            min_level: int = 2) -> np.ndarray:
    """
    Reduce a array of geohashes so that level+1 geohashes are
    replaced with a level geohash whenever possible.

    Args:
        geohashes: array of geohashes of identical level
        min_level: minimum precision to which geohashes are reduced

    Return:
        a reduced list of geohashes of different precision
    """
    if len(geohashes) == 0:
        return geohashes

    geohashes.sort()

    reduced = []
    remainders = []
    k = 0
    while True:
        if k == len(geohashes):
            break
        if k + GH_COUNT >= len(geohashes):
            remainders.extend(geohashes[k:])
            break

        next_geo = set(
            [_[:-1] for _ in
             geohashes[k:min(k + GH_COUNT, len(geohashes))]])
        if len(next_geo) == 1:
            reduced.append(geohashes[k][:-1])
            k += GH_COUNT
        else:

            remainders.append(geohashes[k])
            k += 1

    if len(geohashes[0]) > min_level and len(reduced) > 0:
        reduced = reduce(np.array(reduced))

    reduced.extend(remainders)
    return reduced


def to_polygon(
        geohashes: np.ndarray[str],
        convex: bool = True
        ) -> T.Union[shapely.Polygon, shapely.MultiPolygon]:
    """
    Returns the polygon matching a set of geohashes.

    Contiguous geohashes are considered as belonging to the same polygon. Apply
    only on a reduced list of geohashes as it can be extremely inefficient on a
    large number of geohashes.

    Args:
        geohashes: the list of geohashes to merge into a (Multi)Polygon
        convex: simplify by taking the convex hull of the returned polygon to
            avoid aliasing effect. Uses with caution.
    """
    polygon = geohashes_to_polygon(geohashes)

    # simplify by taking the convex hull
    if convex:
        if isinstance(polygon, shapely.MultiPolygon):
            return shapely.ops.unary_union(
                [_.convex_hull for _ in polygon.geoms])
        else:
            return polygon.convex_hull

    return polygon
