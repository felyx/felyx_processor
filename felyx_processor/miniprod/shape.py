# encoding: utf-8
"""
felyx_processor.miniprod.shape
-------------------------

Contains the code for the implementation of shape objects as needed by the
felyx backend. This includes code for the correct handling of sites that
cross the international date line, and shape classes with certain
optimisations.

.. :copyright: Copyright 2013 Pelamis Scientific Software Ltd.
.. :license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: David Poulter <david.poulter@pelamis.co.uk>
.. codeauthor:: David Poulter <david.poulter@pelamis.co.uk>
"""

import logging
import typing as T

import numpy as np
import shapely.geometry.base
from shapely.geometry import (LineString, MultiPolygon, Polygon,
                              MultiLineString)
from shapely.ops import cascaded_union

from .geohash import latlon_to_footprint


LOGGER = logging.getLogger('felyx.processor')


class InvalidShapeType(TypeError):
    """Error raised when the type of the geometry object representing the shape
    of a Felyx site is not Polygon."""
    def __init__(self, geom_type):
        """"""
        self.geom_type = geom_type


class GeographicShape(object):
    """
    A base class  that implements has a crosses_dateline and
    crosses_prime_meridian properties for shapely geometries.
    """
    _PM = LineString(((0, 90), (0, -90)))
    _IDT = LineString(((180, 90), (180, -90)))
    _shape = None
    _adjusted_shape = None
    _crosses_dateline = None
    _crosses_greenwich = None
    _width = None
    _use_adjusted = None

    @property
    def geometry(self):
        return self._shape

    @property
    def crosses_dateline(self):
        """
        True if the shape cross the 180 degrees longitude line else False.
        """
        if self._crosses_dateline is None:
            self._crosses_dateline = not self.geometry.disjoint(self._IDT)

        return self._crosses_dateline

    @property
    def crosses_greenwich(self):
        """
        True if the shape crosses the 0 degrees longitude line else False.
        """
        if self._crosses_greenwich is None:
            self._crosses_greenwich = not self.geometry.disjoint(self._PM)

        return self._crosses_greenwich

    @property
    def adjusted_shape(self):
        """
        This shape referenced to a 0 -> 360 longitude scale instead of
        -180 -> 180.
        """
        raise NotImplementedError

    @property
    def width(self):
        """
        The width of the shape, in native units.
        """
        if self._width is None:
            self._width = self.geometry.bounds[2] - self.geometry.bounds[0]
        return self._width

    @property
    def use_adjusted(self):
        """
        True if the shape will be better evaluated on the adjusted longitude
        scale, instead of the native one.
        """
        # If a shape crosses the dateline, it will appear to double back
        # on itself, and cross the prime meridian instead. In this case, it
        # will cause many more miniprod sites to be identified than should be.
        # Worse, it may cancel out (through negative overlay) valid regions.
        # In this event, the unadjusted shape will cross the prime meridian,
        # and the adjusted shape will cross the deadline.

        # If the shape doesn't cross the prime meridian, then there is no
        # issue, and the main shape can be used.
        if self._use_adjusted is not None:
            return self._use_adjusted

        if not self.crosses_greenwich:
            self._use_adjusted = False
            return self._use_adjusted

        # If the shape and adjusted shape have the same area, then there is
        # no need to use the adjusted shape.
        if self.geometry.area == self.adjusted_shape.area:
            self._use_adjusted = False
            return self._use_adjusted

        if self.crosses_greenwich and self.adjusted_shape.crosses_dateline:
            # The shape may be distorted, or may be a full orbit.

            if (self.width + self.adjusted_shape.width) == 360:
                # The shape is a global grid, with pixels measured form the
                # centre point. The adjusted shape is simply the remains of the
                # coverage of the original shape. It is safe to use the original
                # shape.
                self._use_adjusted = False
                return self._use_adjusted

            elif self.width == self.adjusted_shape.width:
                # If the width of the original and adjusted shapes are the
                # same, it is safe to use the original shape.
                self._use_adjusted = False
                return self._use_adjusted

            elif self.adjusted_shape.width / self.width < 0.99:
                # The adjusted shape is SIGNIFICANTLY thinner (smaller in
                # longitude) than the original shape. This indicates a valid
                # swath segment, but less than a full orbit.
                self._use_adjusted = True
                return False

            else:
                self._use_adjusted = False
                return self._use_adjusted

        self._use_adjusted = False
        return self._use_adjusted


class GeographicPolygon(GeographicShape):
    """
    A class inheriting from GeographicShape for (Multi)Polygon.

    Args:
        shape: a shapely Polygon or MultiPolygon, or a list of tuples (lons,
            lats)
    """
    def __init__(
            self,
            shape: T.Union[Polygon, MultiPolygon, T.List[T.Tuple[float, float]]]
    ) -> None:
        if isinstance(shape, (Polygon, MultiPolygon)):
            self._shape = shape

        elif isinstance(shape, list):
            geoms = []
            for lons, lats in shape:
                # @TODO adapt to true resolution
                geoms.append(latlon_to_footprint(lats, lons))

            if len(geoms) > 1:
                footprint = cascaded_union(geoms)
            elif len(geoms) == 0:
                footprint = shapely.geometry.base.EmptyGeometry
            else:
                footprint = geoms[0]

            self._shape = footprint
        else:
            raise ValueError('Bad input args for GeographicPolygon')

    @property
    def adjusted_shape(self):
        """
        This shape referenced to a 0 -> 360 longitude scale instead of
        -180 -> 180.
        """
        if self._adjusted_shape is None:

            if len(self.geometry.geoms) == 1:
                lons, lats = self.geometry.geoms[0].boundary.xy
                lons = np.array(lons)
                lons[lons < 0] += 360
                coords = np.array((lons, lats)).T
                self._adjusted_shape = GeographicPolygon([Polygon(coords)])
            else:
                self._adjusted_shape = self.geometry

        return self._adjusted_shape


class GeographicLine(GeographicShape):
    """
    A class inheriting from GeographicShape and LineString.
    """
    def __init__(
            self,
            shape: T.Union[LineString, MultiLineString,
                T.List[T.Tuple[float, float]]]
    ) -> None:
        if isinstance(shape, (LineString, MultiLineString)):
            self._shape = shape

        elif isinstance(shape, list):
            if len(shape) > 1:
                self._shape = MultiLineString(
                    np.array(_).T for _ in shape)
            elif len(shape) == 0:
                self._shape = shapely.geometry.base.EmptyGeometry
            else:
                self._shape = LineString(np.array(shape[0]).T)

        else:
            raise TypeError(f'Bad input type for GeographicLine: {type(shape)}')

    @property
    def adjusted_shape(self):
        """
        This shape referenced to a 0 -> 360 longitude scale instead of
        -180 -> 180.
        """
        if self._adjusted_shape is None:
            lons, lats = self.geometry.xy
            lons = np.array(lons)
            lons[lons < 0] += 360
            coords = np.array((lons, lats)).T
            self._adjusted_shape = GeographicLine(coords)

        return self._adjusted_shape


def get_felyx_shape(
        geos_shape: Polygon,
) -> GeographicShape:
    """
    Return a GeographicShape object from a Polygon

    Args:
        geos_shape (Polygon): A shapely shape object

    Returns:
        A GeographicShape object.
    """
    if geos_shape.type != 'Polygon':
        LOGGER.critical(
            str(
                'The shape object received was not of the correct type. '
                "The 'type' attribute of the shape must be 'Polygon' instead "
                'of {}. Exiting.'
            ).format(geos_shape.type)
        )
        raise InvalidShapeType(geos_shape.type)

    return GeographicPolygon(geos_shape)

