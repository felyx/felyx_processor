# encoding: utf-8
"""
miniprod_datasource
--------------------------------------

Contains base classe for retrieve miniprods data from different sources

"""
import abc
from datetime import datetime
from pathlib import Path
from typing import Dict, List, Tuple, Union

import pandas as pd
import shapely.wkt

from felyx_processor.miniprod import ChildMetadata

# COLUMNS MAPPING

TIME_COLUMN = 'time'
SITE_COLUMN = 'site'
DATASET_COLUMN = 'dataset'
ID_COLUMN = 'id'

# required columns for miniprod data
MINIPROD_COLUMNS = [SITE_COLUMN, TIME_COLUMN, ID_COLUMN, DATASET_COLUMN]


class MiniprodDataSource(metaclass=abc.ABCMeta):
    """Miniprod Data Source Interface"""

    @abc.abstractmethod
    def get_miniprods(self,
                      datasets: List[str],
                      sites_codes: List[str] = None,
                      site_collection: str = None,
                      start: datetime = None,
                      stop: datetime = None,
                      wkt_coords: List[Dict[str, str]] = None,
                      constraints_list: bool = None,
                      filter_on_site_time: bool = True,
                      as_dataframe: bool = True
                    ) -> Union[Dict[str, Dict], pd.DataFrame]:
        """
        Search miniprod data in datasets and list of sites over a timespan and
        spatial filter

        Args:
            datasets: the list of datasets to search
            sites_codes: the list of sites codes to search
            site_collection: the collection from which to select the miniprods
            start: the begin time coverage
            stop: the end time coverage
            wkt_coords: (optional) the spatial filter defined by a list of
                coordinates of a polygon
            constraints_list: (optional) additional data filters
            filter_on_site_time: temporal filtering on the site data times
            as_dataframe: if True, returns a dataframe, if False, returns a dict
        Returns :
                if as_dataframe is set to True :
                    a pandas dataframe with columns are miniprods metadata
                else :
                    a dictionary with keys as source EO filenames and values
                    as miniprods metadata
        """
        raise NotImplementedError

    @abc.abstractmethod
    def register_miniprods(
            self,
            children: List[Tuple[str, Dict]],
            **kwargs
    ):
        """
        Register children products metadata into a manifest file

        Parameters
        ----------
        children: List[ChildMetadata]
            the list of child products to save in the manifest file
        """
        raise NotImplementedError

    @abc.abstractmethod
    def delete_miniprods(self, miniprods_ids: List[str]) -> int:
        """
        Delete miniprods data refering to a list of miniprod ids

        Args:
            miniprods_ids: the list of miniprod ids

        Returns:
            the number of deleted miniprods
        """
        raise NotImplementedError

    def miniprods_from_sibling(
            self,
            dataset_id: str,
            sibling_miniprods: pd.DataFrame,
            scaling: Dict[str, Union[int, float]],
            file_pattern: str,
            as_dataframe: bool = True
    ) -> Union[Dict[str, Dict], pd.DataFrame]:
        """
        Return the list of matchups inferred for a dataset from the list
        retrieved for a sibling dataset

         Args:
            dataset_id: The dataset id.
            sibling_miniprods: The pandas dataframe containing siblings.
            scaling: The scale factors to apply.
            file_pattern: The file pattern.
            as_dataframe: if True, returns a dataframe, if False, returns a dict

         Returns :
                if as_dataframe is set to True, a pandas dataframe,
                a dictionary otherwise
        """
        miniprods = sibling_miniprods.copy()

        if miniprods.empty:
            return miniprods

        # transform str to slices and adjust slices if a scale factor is to be
        # applied
        if any(s != 1 for s in scaling.values()):
            miniprods['slices'] = [
                str({d: slice(sl.start * scaling.get(d, 1),
                              sl.stop * scaling.get(d, 1))
                for d, sl in eval(sli).items()})
                for sli in miniprods.slices]

        # source file name
        miniprods['source'] = [
            Path(start.strftime(file_pattern)).name
            for start in miniprods.source_time_coverage_start
        ]

        miniprods['dataset'] = [dataset_id] * miniprods.shape[0]

        if not as_dataframe:
            return self.as_extractions(miniprods)

        return miniprods

    @staticmethod
    def as_extractions(miniprods: pd.DataFrame) -> Dict[str, Dict]:
        """
        Returns result from index search as a dict where keys are the
        source EO filenames and values the found miniprods as extraction
        metadata objects.

        Args:
            miniprods: The dataframe containing the miniprods.

        Returns:
            A dictionary.
        """

        # group matchups per source file to open a source file only once
        result = {}
        for source, df in miniprods.groupby('source'):
            # get extraction from manifest dataframe
            extractions = {}
            for collection, coll_matchups in df.groupby('site_collection'):
                extractions[collection] = []
                for id, item in coll_matchups.to_dict(orient='index').items():
                    # if item['site'] not in extractions[collection]:
                    #     extractions[collection][item['site']] = []

                    # @TODO retrieve obj from dataframe instead of dicts ?
                    # @TODO or return a dataframe?
                    # @TODO do type conversion when reading dataframe
                    item.update(dict(
                        slices=item['slices'],
                        source_center_index=item['source_center_index'],
                        shape=item['shape'],
                        # @TODO use better naming (percent coverage covered
                        #  by miniprod)
                        coverage=item['coverage'],
                        dynamic_target_time=item['dynamic_target_time']
                    ))

                    child = ChildMetadata(**item)

                    extractions[collection].append(child)
            result[str(source)] = extractions

        return result

    @abc.abstractmethod
    def get_datasets_coverage(self) -> Dict[str, Tuple[datetime, datetime]]:
        """
            Search datasets time coverages in miniprods data.

            Returns:
                Returns a dict where keys are the dataset ids and
                values a tuple containing the coverage start and end dates.
        """
        raise NotImplementedError
