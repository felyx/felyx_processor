# encoding: utf-8
"""
felyx_processor.miniprod.tools
-------------------------

Contains helper functions for the felyx_processor miniprod extraction module.

.. :copyright: Copyright 2013 Pelamis Scientific Software Ltd.
.. :license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: David Poulter <david.poulter@pelamis.co.uk>
.. codeauthor:: David Poulter <david.poulter@pelamis.co.uk>
"""

import logging

LOGGER = logging.getLogger('felyx.processor')


class lazy_property(object):
    """A class to evaluate a property method of another, and replace that
    property with the actual return value. To be used as a method decorator.

    :param function function: The decorated method.
    """

    def __init__(self, function):
        """Initialise a property of a class, and replace that property with the
        value.
        """
        self.function = function
        self.function_name = function.__name__

    # noinspection PyUnusedLocal
    def __get__(self, instance, class_instance):
        """Evaluate the value and replace the property with that value as an
        attribute.

        :param instance: The object being decorated.
        :param class_instance: The owner (unused).
        """
        if instance is None:
            return None
        value = self.function(instance)
        setattr(instance, self.function_name, value)
        return value
