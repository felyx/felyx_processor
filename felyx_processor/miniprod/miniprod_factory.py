# encoding: utf-8
from pathlib import Path
from typing import Optional

from felyx_processor.miniprod.es_miniprod_datasource import ESMiniprodDataSource
from felyx_processor.miniprod.manifest_miniprod_datasource import ManifestMiniprodDataSource
from felyx_processor.miniprod.miniprod_datasource import MiniprodDataSource
from felyx_processor.utils.configuration import FelyxSystemConfig
from felyx_processor.utils.exceptions import FelyxProcessorError
from felyx_processor.utils.models import Indexing


class MiniprodDataSourceFactory:
    """Class for accessing the index storage of registered child product"""

    @classmethod
    def get_miniprod_datasource(
            cls,
            felyx_sys_config: FelyxSystemConfig,
            source: Indexing = 'elasticsearch',
            manifest_root_dir: Optional[Path] = None,
            miniprod_root_dir: Optional[Path] = None
    ) -> MiniprodDataSource:
        """
        Returns an object to access the registered child products.

        Parameters
        ----------
        felyx_sys_config: FelyxSystemConfig
            Felyx system configuration
        source: str
            the storage source for the index: "elasticsearch" or "manifest"
        manifest_root_dir: Path
            if source is manifest, the root path of the manifest files
        miniprod_root_dir: Path
            if source is elasticsearch, the root path of the miniprod files
            for computing absolute miniprod filenames

        Returns
        -------
        MiniprodDataSource
            the miniprods datasource

        Raises
        ------
        FelyxProcessorError
            if cannot instanciate Miniprod datasource for source
        """
        if source == Indexing.elasticsearch:
            return ESMiniprodDataSource(
                felyx_sys_config.elasticsearch, miniprod_root_dir)

        if source == Indexing.manifest:
            return ManifestMiniprodDataSource(manifest_root_dir)

        raise FelyxProcessorError(
            f'Cannot instanciate Miniprod datasource for source : {source}')
