# encoding: utf-8
"""
felyx_processor.miniprod.extract.writers
-----------------------------------

Contains functions used to write output data files.

.. :copyright: Copyright 2014 Pelamis Scientific Software Ltd.
.. :license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: David Poulter <david.poulter@pelamis.co.uk>
.. codeauthor:: David Poulter <david.poulter@pelamis.co.uk>
"""
import datetime
import logging
import os
from typing import Dict

import numpy
import xarray as xr

from cerbere.feature.cbasefeature import BaseFeature

from felyx_processor.extraction.source import SourceFile
from felyx_processor.utils.configuration import FelyxSystemConfig, SiteCollectionConfig
from felyx_processor.miniprod import ChildMetadata


LOGGER = logging.getLogger('felyx.processor')


class ContentMaskError(Exception):
    """"""
    def __init__(self, file_path, *args):
        self.file_path = file_path
        msg = f'Failed to add content mask to file {file_path}'
        super(ContentMaskError, self).__init__(msg, *args)


def write_miniprod_file(
        felyx_sys_config: FelyxSystemConfig,
        source_file: SourceFile,
        full_path_name: str,
        subset: BaseFeature,
        site_info: ChildMetadata,
        site_collection_id: str,
        site_collection_info: SiteCollectionConfig,
        creator: Dict = None) -> None:
    """Create a miniprod from a cerbere subset.

    Args:
        felyx_sys_config: felyx system configuration
        source_file: An instance of a SourceFile class.
        full_path_name: The output path and name of the miniprod.
        subset: The cerbere feature to be recorded in the miniprod.
        site_info: A dictionary of information about the site.
        site_collection_id: The name of the felyx site collection.
        site_collection_info:
        creator: A dictionary with information about the creator of this subset.

    Returns:
        None
    """
    LOGGER.info(f'Creating file {full_path_name}')

    set_miniprod_attrs(
        felyx_sys_config=felyx_sys_config,
        source_file=source_file,
        subset=subset,
        site_info=site_info,
        site_collection_id=site_collection_id,
        site_collection_info=site_collection_info,
        creator=creator)

    subset.ds.cb.save(full_path_name, format=felyx_sys_config.nc_format)


def set_miniprod_attrs(
        felyx_sys_config: FelyxSystemConfig,
        source_file: SourceFile,
        subset: BaseFeature,
        site_info: ChildMetadata,
        site_collection_id: str,
        site_collection_info: SiteCollectionConfig,
        creator: Dict = None) -> None:
    """Complete cerbere subset.attrs.

    Args:
        felyx_sys_config: Felyx system configuration
        source_file: An instance of a SourceFile class.
        subset: The cerbere feature to be recorded in the miniprod.
        site_info: A dictionary of information about the site.
        site_collection_id: The name of the felyx site collection.
        site_collection_info ():
        creator: A dictionary with information about the owner of the site
        collection this site belongs in.

    Returns:
        None
    """
    # Add global metadata:
    lon_min, lat_min, lon_max, lat_max = site_info.shape.bounds

    if creator is None:
        # @TODO replace with class ?
        try:
            creator = {'username': os.getlogin(), 'email': '', 'url': ''}
        except OSError:
            # fails on HPC nodes
            creator = {'username': '', 'email': '', 'url': ''}

    now = datetime.datetime.now().isoformat()

    new_global_attributes = {
        '__id': f'{source_file.dataset_cfg.name}_{site_info.site}',
        '__title': f'child product derived from {source_file.input_url.name} '
                   f'over felyx site {site_info.site}',
        '__summary': str(
            'child product created by felyx on {date} from '
            '{url} provided by {provider}. Contains data over {code} '
            '(a polygon with bounds from {lat_min} to {lat_max} degrees '
            'North and from {lon_min} to {lon_max} degrees East).'
        ).format(
            **{
                'date': datetime.datetime.now().strftime('%Y%m%dT%H%M%S'),
                'url': source_file.input_url,
                'code': site_info.site,
                'provider': source_file.dataset_cfg.supplier,
                'lat_min': lat_min, 'lat_max': lat_max,
                'lon_min': lon_min, 'lon_max': lon_max
            }
        ),
        '__history': r'Created on {} with {}'.format(
            datetime.datetime.now().strftime('%Y%m%dT%H%M%S'),
            source_file.input_url
        ),
        '__date_created': now,
        '__date_modified': now,
        '__keywords': source_file.dataset_cfg.keywords,
        '__acknowledgement': felyx_sys_config.acknowledgement,
        '__license': felyx_sys_config.miniprod_license,
        '__format_version': felyx_sys_config.miniprod_format_version,
        '__creator_name': creator['username'],
        '__creator_email': creator['email'],
        '__creator_url': creator['url'],
        '__references': felyx_sys_config.miniprod_references,
        '__time_coverage_resolution': '',
        '__source': os.path.split(source_file.input_url)[-1],
        '__source_slices': subset.ds.attrs.pop('slices'),
        '__site_id': site_info.site,
        '__site_name': site_info.site,
        '__site_geometry': site_info.shape.wkt,
        '__dataset_id': source_file.dataset_id,
        '__dataset_name': source_file.dataset_cfg.name,
        '__site_collection_id': site_collection_id,
        '__site_collection_name': site_collection_info.name,
        '__site_collection_summary': site_collection_info.description,
        '__geospatial_lon_min': lon_min,
        '__geospatial_lon_max': lon_max,
        '__geospatial_lat_min': lat_min,
        '__geospatial_lat_max': lat_max,
        '__percentage_coverage_of_site_by_miniprod': 0,
    }

    # remove None in attributes
    for k, v in new_global_attributes.items():
        if v is None:
            new_global_attributes[k] = ''

    # warning: compress masked array or NaT may be returned!
    times_coverage = subset.ds.cb.time.to_masked_array().compressed()
    time_coverage_start = times_coverage.min()
    time_coverage_end = times_coverage.max()

    source_time_coverage_start = subset.ds.attrs['time_coverage_start']
    source_time_coverage_end = subset.ds.attrs['time_coverage_end']
    new_global_attributes['__time_coverage_start'] = \
        time_coverage_start.astype('datetime64[us]').astype(datetime.datetime)
    new_global_attributes['__time_coverage_end'] = \
        time_coverage_end.astype('datetime64[us]').astype(datetime.datetime)
    new_global_attributes['__source_time_coverage_end'] = \
        source_time_coverage_end
    new_global_attributes['__source_time_coverage_start'] = \
        source_time_coverage_start

    if 'coverage' in site_info:
        new_global_attributes[str(
            '__percentage_coverage_of_site_by_miniprod'
        )] = site_info.coverage

    # Define the additional dynamic site attributes that maybe in the
    # site_info dictionary.
    if site_info.dynamic_target_time is not None:
        new_global_attributes['__site_lat'] = site_info.dynamic_target_latitude
        new_global_attributes['__site_lon'] = site_info.dynamic_target_longitude
        new_global_attributes['__site_time'] = \
            site_info.dynamic_target_time.isoformat()
        new_global_attributes['__time_difference'] = \
            site_info.dynamic_target_time_difference
        new_global_attributes['__distance'] = \
            site_info.dynamic_target_distance
        new_global_attributes['__source_center_index'] = \
            str(site_info.source_center_index)

    subset.ds.attrs.update(new_global_attributes)
