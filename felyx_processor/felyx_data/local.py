# encoding: utf-8
"""
Contains a class for the representation of felyx data, in a local disk based
cache.
"""

import logging
import typing as T

import shapely.geometry.base
from shapely.wkt import loads as geometry

from felyx_processor.felyx_data.base import FelyxData
from felyx_processor.miniprod.shape import get_felyx_shape
from felyx_processor.utils.configuration import (
    get_collection_config,
    get_extraction_settings,
    get_dataset_miniprod_extraction_config,
)
import felyx_processor.utils.configuration as cfg


LOGGER = logging.getLogger('felyx.processor')
FELYX_CONFIG = None
COLLECTION_LEVELS = {'public': ['community', 'local'], 'private': ['custom']}


class DuplicateNameCollection(ValueError):
    """Error raised upon detection of several site collections sharing the same
    name."""


class SiteCollectionNotFound(ValueError):
    """Error raised when an unknown site collection has been requested."""


class InvalidMatchingDatasetsCount(ValueError):
    """Error raised when the number of datasets found is not exactly 1."""


class LocalFelyxData(FelyxData):
    """
    Implements a data extractor for use with a local copy of the Felyx server
    information.


    :param datafile_path: The path to the locally stored cache file
      representing the felyx_server information [#f1]_ .
    :type datafile_path: str or unicode
    """

    def __init__(self, felyx_processor_config: cfg.FelyxProcessorConfig):
        """Initialise the class, with any special features of the super-class.

        Args:
            felyx_processor_config:
        """
        self._site_collection = {}
        self._site_id = None
        self._site_code = None
        self._config = felyx_processor_config
        # TODO add position (dynamic) data.

    def dataset(
            self,
            dataset_pid: T.Optional[int] = None,
            dataset_id: T.Optional[str] = None
    ) -> T.Union[cfg.DatasetConfig, T.Dict[str, cfg.DatasetConfig]]:
        try:
            if dataset_pid:
                # Return values for a given dataset name, exiting if the name
                # exists for multiple datasets.
                result = [
                    _ for _ in self._config.dataset
                    if _['name'] == dataset_pid
                ]
                if len(result) == 0:
                    raise ValueError(f'Dataset {dataset_pid} is not defined '
                                     f'in felyx configuration.')
                if len(result) == 1:
                    return result[0]
                else:
                    LOGGER.critical(
                        str(
                            'Found {} and not 1 instances of dataset '
                            'with code {}, please review the state of your '
                            'felyx_server instance. Exiting.'
                        ).format(len(result), dataset_pid)
                    )
                    raise InvalidMatchingDatasetsCount()

            elif dataset_id is not None:
                # Return values for the unique dataset_id
                try:
                    return self._config.EODatasets[dataset_id]
                except KeyError:
                    raise ValueError(f'Dataset {dataset_pid} is not defined '
                                     f'in felyx configuration.')

            elif dataset_pid is None and dataset_id is None:
                result = self._config.EODatasets
                return result

        except (IOError, IndexError):
            LOGGER.critical(
                str(
                    'The dataset name or number provided ({}) does not '
                    'exist in this felyx instance. Please try another '
                    'dataset. Exiting'
                ).format(
                    dataset_id if dataset_id is not None else dataset_pid
                )
            )
            raise

    def metric(self, dataset_name):
        """
        Return a dictionary representation of the metrics for a dataset.

        :param dataset_name: The dataset_name of the dataset in the server
          database.
        :type dataset_name: str or unicode
        :return: All information pertaining to the metrics applied to this
          dataset.
        :rtype: dict
        """
        result = []

        for _ in self._config.MMDatasets.values():
            metric = {'site_collection': [_['site_collection']]}
            for id, dset in _['eo_datasets'].items():
                if id == dataset_name:
                    metric['eo-dataset'] = id
                    if dset['metrics'] is not None:
                        for key, value in dset['metrics'].items():
                            metric['name'] = key
                            metric['label'] = value['label']
                            metric['processing_chain_description'] = \
                                value['processing_chain_description']
                            result.append(metric.copy())

        return result

    def sites(self, static=None):
        """
        Return a list representation of all the sites.

        :param static: Return only static sites if True, only dynamic sites
          if False, all sites if None.
        :type static: NoneType | bool
        :return: All information pertaining to the chosen sites.
        :rtype: list
        """
        if static is None:
            raise NotImplementedError
        return [_ for _ in self._config.site if _['static'] == static]

    def site(self, site_id=None, site_code=None):
        """
        Return a dictionary representation of a single site.

        :param site_id: The ID of the site in the server database.
        :type site_id: int
        :param site_code: The code of the site in the server
        :type site_code: basestring
        :return: All information pertaining to the chosen individual site.
        :rtype: dict
        """
        # ============================================v A supprimer
        if 'site' not in self._config:
            if site_id is None:
                site_id = site_code
            return {'id': site_id}
        # ============================================^ A supprimer

        if site_code is not None:
            if self._site_code is None:
                self._site_code = {
                    site['code']: index for index, site
                    in enumerate(self._config.site)
                }
            return self._config.site[self._site_code[site_code]]

        if site_id is not None:
            if self._site_id is None:
                self._site_id = {
                    site['id']: index for index, site
                    in enumerate(self._config.site)
                }
            return self._config.site[self._site_id[site_id]]

        raise ValueError(
            'At least one of site_id or site_code must be provided.'
        )

    # noinspection PyTypeChecker
    def site_collection_info(
            self,
            site_collection_id: str,
    ) -> cfg.SiteCollectionConfig:
        """Return the description of a site collection.

        Parameters
        ----------
        site_collection_id: str
            The identifier of the site_collection

        Returns
        -------
        site_collection: SiteCollectionConfig
            All information pertaining to that site collection.
        """
        return self._config.SiteCollections[site_collection_id]

    def set_sites_list(
            self,
            collection_name: str,
            site_codes: T.List[str]
        ):
        """
        Set the site codes for a collection
        :param str collection_name: The name of the site_collection
        :param site_codes: The site codes list
        """
        if collection_name in self._site_collection:
            # @TODO remove _site_collection as attribute
            self._site_collection[collection_name].sites = [
                cfg.SiteConfig(
                    name=code, shape=shapely.geometry.GeometryCollection())
                for code in site_codes]
        else:
            raise SiteCollectionNotFound(
                f'No site collection with the name {collection_name}.')

    def site_collection_id(self, site_collection_name):
        """
        Return the info of a site collection.

        :param str site_collection_name: The name of the site_collection in the
          server database.
        :return: All information pertaining to that site collection.
        :rtype: dict
        """
        collection = [
            _ for _ in self._config.SiteCollections
            if _.name == site_collection_name
        ]
        if len(collection) > 1:
            LOGGER.critical(
                'Multiple site collections with the name {}. Exiting.'.format(
                    site_collection_name
                )
            )
            raise DuplicateNameCollection()
        elif not len(collection):
            LOGGER.critical(
                'No site collection with the name {}. Exiting.'.format(
                    site_collection_name
                )
            )
            raise SiteCollectionNotFound()
        else:
            return collection[0]['id']

    def site_collection_name(self, site_collection_code):
        """
        Return the name of a site collection.

        :param str site_collection_code: The code of the site_collection in the
          server database (string identifier).
        :return: the name of the collection
        :rtype: string
        """
        collection = [
            _ for _ in self._config.ite_collection
            if _['code'] == site_collection_code
        ]
        if len(collection) > 1:
            LOGGER.critical(
                'Multiple site collections with the name {}. Exiting.'.format(
                    site_collection_code
                )
            )
            raise DuplicateNameCollection()
        elif not len(collection):
            LOGGER.critical(
                'No site collection with the name {}. Exiting.'.format(
                    site_collection_code
                )
            )
            raise SiteCollectionNotFound()
        else:
            return collection[0]['name']

    def related_site_collections(
            self, dataset_id, allowed_site_collections=None):
        res = []
        for extr in self.related_extractions(dataset_id):
            if allowed_site_collections is None:
                res.append(
                    self._config.SiteCollections[extr.site_collection_id])
                continue

            if extr.site_collection_id in allowed_site_collections:
                res.append(
                    self._config.SiteCollections[extr.site_collection_id])

        return res

    def related_extractions(
            self,
            dataset_id: str
    ):
        """ Return the miniprod processing configurations for a given dataset """
        configs = []
        for mdataset in self._config.MMDatasets.values():
            if dataset_id in mdataset.eo_datasets:
                configs.append(mdataset.eo_datasets[dataset_id])

        if len(configs) == 0:
            raise cfg.FelyxProcessorError(
                f'Cannot find miniprod processing configuration for dataset : '
                f'{dataset_id} in configuration file')

        return configs

    # noinspection PyTypeChecker
    def site_collections(
            self,
            dataset_id: str,
            dynamic: bool = False,
            site_collection_id: str = None
    ) -> T.Dict:
        """
        Return a dictionary representation of the site collections, defined
        by the contents of miniprod processing.

        Args:
            dataset_id: dataset id to search collections for.
            dynamic: limit to dynamic sites, or limit to static sites.
            site_collection_id: limit search to this site collection.
        """

        # Retrieve the miniprod processing configuration for the dataset
        # Issue 106 : Check the existence of a dataset in the configuration
        processing_configs = get_dataset_miniprod_extraction_config(
            felyx_processor_config=self._config,
            dataset_id=dataset_id)

        result = {}

        extractions = []
        for cfg in processing_configs:
            if (site_collection_id is not None and
                    cfg.site_collection_id not in site_collection_id):
                continue

            site_collection = self.site_collection_info(cfg.site_collection_id)

            if not site_collection:
                continue

            if site_collection.static != (not dynamic):
                continue

            extractions.append(cfg)

        LOGGER.debug(f'Found collection id(s) '
                     f'{str([_.site_collection_id for _ in extractions])}')

        for cfg in extractions:
            collection_id = cfg.site_collection_id

            collection = self.site_collection_info(collection_id)

            result[collection_id] = cfg

            # fix for unconsistent local cache (bug workaround)
            unfound_sites = []

            if collection.static:
                sites = {}
                for site in collection['sites']:
                    try:
                        site_info = collection['sites'][site]
                        # site_info = self.site(site_code=site)
                    except KeyError:
                        unfound_sites.append(site)
                        continue
                    wkt = site_info['shape']
                    site_code = site
                    if ';' in wkt:
                        # avoid issues with "SRID=4326;POLYGON ((..."
                        wkt = wkt.split(';')[-1]

                    geos_shape = geometry(wkt)
                    felyx_shape = get_felyx_shape(geos_shape)
                    sites.update({site_code: wkt})
                result[collection_id]['sites'] = sites

            # remove unknown sites from collections
            for site in unfound_sites:
                LOGGER.error(f'Site {site} was not found in local cache')

                # remove from collection
                collection['site'].pop(collection['site'].index(site))

        return result

    def child_product_sizes(self, dataset_id: str):
        processing_list = get_dataset_miniprod_extraction_config(
            felyx_processor_config=self._config,
            dataset_id=dataset_id)

        sizes = []
        for chain in processing_list:
            sizes.append(chain.subset_size)

        return set(sizes)

    def constraints(self, dataset_id: str):
        """Return the largest dynamic site constraints for each site for a
        dataset.

        :param dataset_id: The id of the dataset in question (as a
          SourceFile.dataset_id object).
        :type dataset_id: dict
        :return: A dictionary of colocation radii, colocation period and
          miniprod_size for each site.
        :type: dict
        """
        result = {}
        processing_list = get_dataset_miniprod_extraction_config(
            felyx_processor_config=self._config,
            dataset_id=dataset_id)

        for chain in processing_list:
            for site in self.site_collection_info(chain.site_collection_id).sites:

                if site.name not in result.keys():
                    result[site.name] = {
                        'collocation_period':
                            chain.matching_criteria.time_window,
                        'collocation_radius':
                            chain.matching_criteria.search_radius,
                        'miniprod_size': chain.subset_size
                    }
                else:
                    if result[site.name]['collocation_period'] < \
                            chain.matching_criteria.time_window:
                        result[site.name]['collocation_period'] = \
                            chain.matching_criteria.time_window
                    if result[site.name]['collocation_radius'] < \
                            chain.matching_criteria.search_radius:
                        result[site.name]['collocation_radius'] = \
                            chain.matching_criteria.search_radius
                    if result[site.name]['miniprod_size'] < chain.subset_size:
                        result[site.name]['miniprod_size'] = chain.subset_size

        res = {}
        for key, value in result.items():
            try:
                # site = self.site(key)
                res[key] = value
            except KeyError:
                # LOGGER.error('Site {} was not found'.format(site))
                continue

        return res
