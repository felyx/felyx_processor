# encoding: utf-8
"""
felyx_processor.felyx_data
---------------------------------

Contains the code for the generation of a local copy of the felyx_server
database, which contains site, dataset and metric definitions, amongst other
information.

.. :copyright: Copyright 2014 Pelamis Scientific Software Ltd.
.. :license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: David Poulter <david.poulter@pelamis.co.uk>
.. codeauthor:: David Poulter <david.poulter@pelamis.co.uk>
"""
import logging

from felyx_processor.utils.configuration import FelyxProcessorConfig

LOGGER = logging.getLogger('felyx.processor')


from felyx_processor.felyx_data.base import BadLocalCache, FelyxData, NoLocalCache
from felyx_processor.felyx_data.local import LocalFelyxData

#Global access to Felyx server data
FELYX_SERVER_DATA = None


def get_felyx_data(
    felyx_processor_config: FelyxProcessorConfig,
    reset: bool = False,
) -> FelyxData:
    """Create an access to Felyx server data.

    Args:
        felyx_processor_config:
        reset : if True, instanciate a new server data
    Returns:
        an access to Felyx server data
    """
    global FELYX_SERVER_DATA
    if FELYX_SERVER_DATA is None or reset is True:
        FELYX_SERVER_DATA = felyx_server_data(felyx_processor_config=felyx_processor_config)
    return FELYX_SERVER_DATA


def felyx_server_data(felyx_processor_config: FelyxProcessorConfig):
    """Return an object which can return information from the felyx server.
    Causes a system exit if neither local cache or remote services are
    available.

    .. note:: The absence of a local cache will force the return of an
        object linked to a felyx_server instance. However, to save bandwidth,
        that connection will not be tested. Failure to read form that
        connection will be caught and logged, but not by this function.
    """

    try:
        return LocalFelyxData(felyx_processor_config=felyx_processor_config)
    except (NoLocalCache, BadLocalCache) as problem:
        LOGGER.warning(
            str(
                'Failed to read local cache file. Internally the problem '
                'was recorded as: \n{}\nNow attempting to read remote '
                'instance.'
            ).format(str(problem))
        )
