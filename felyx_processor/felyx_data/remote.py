# encoding: utf-8
"""
felyx_processor.felyx_data.remote
----------------------------------------

Contains a class for the representation of the information on a felyx_server
instance. Functionally equivalent to any class that inherits from
:class:`felyx_processor.felyx_data.base.FelyxServerData` .

.. :copyright: Copyright 2014 Pelamis Scientific Software Ltd.
.. :license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: David Poulter <david.poulter@pelamis.co.uk>
.. codeauthor:: David Poulter <david.poulter@pelamis.co.uk>
"""
import datetime
import logging
import sys
from typing import Dict, List

from felyx_processor.miniprod.shape import get_felyx_shape

LOGGER = logging.getLogger('felyx.processor')

import urllib.parse as urlparse
from collections import OrderedDict

import requests
from shapely.wkt import loads as geometry

from felyx_processor.felyx_data.base import FelyxData
from felyx_processor.utils.configuration import get_felyx_config

FELYX_CONFIG = None


def ensure_json_format(query):
    """Add 'format=json' if necessary"""
    if 'format=json' not in query:
        if '?' in query:
            return '{}&format=json'.format(query)
        return '{}?format=json'.format(query)
    else:
        return query

class RemoteFelyxData(FelyxData):
    """Implements a data extractor for use with a remote, REST interface to the
    Felyx server information.

    :param felyx_api: The URL of the RESTful interface of the felyx server.
    :type felyx_api: str or unicode or NoneType
    :param auth: The username and password for the REST connection
    :type auth: str or unicode or NoneType
    """

    def __init__(self, felyx_api=None, auth=None):
        """Return a RemoteFelyxData object.

        """
        super(RemoteFelyxData, self).__init__()

        global FELYX_CONFIG
        if FELYX_CONFIG is None:
            FELYX_CONFIG = get_felyx_config()

        # Load the felyx API variable
        if felyx_api is None:
            if 'default_felyx_api' in FELYX_CONFIG:
                self._felyx_api = FELYX_CONFIG['default_felyx_api']
            else:
                LOGGER.critical(
                    'default_felyx_api absent from felyx config file. Exiting.'
                )
                sys.exit(1)
        else:
            self._felyx_api = felyx_api

        if not self._felyx_api.endswith('/'):
            self._felyx_api = '{}/'.format(self._felyx_api)

        # Configure the authority information for remote access, by default
        # we have the authority of the felyx user.
        if auth is None:
            self._auth = (
                FELYX_CONFIG['default_felyx_user'],
                FELYX_CONFIG['default_felyx_password']
            )
        else:
            self._auth = auth

        # setup a simple cache
        self.__cache = {}
        self.__sites_data = {'static': None, 'dynamic': None}

    def __query(self, *args, **kwargs):
        """Helper function for extracting REST queries."""
        # Get the query query_string
        given_string = args[0]

        # Add any data to the query string
        if 'data' in kwargs:
            data = kwargs.pop('data')
            q_string = given_string.format(**data)
            query = urlparse.urljoin(self._felyx_api, q_string)
        else:
            query = urlparse.urljoin(self._felyx_api, given_string)

        #Lazily evaluate the query
        if not query in self.__cache:
            LOGGER.debug('{} not in object instance cache'.format(query))
            # Start with and empty result, and assume more data is available.
            result = []
            more_data_available = True

            # Loop while tastypie server advises that more data data is
            # available
            while more_data_available:
                LOGGER.debug('Making API request {}'.format(query))
                try:
                    headers = {'Content-Type': 'application/json'}
                    query = ensure_json_format(query)
                    LOGGER.debug('request.get("{}")'.format(query))
                    return_data = requests.get(
                        query, auth=self._auth, headers=headers, verify=False
                    )
                    LOGGER.info(
                        str(
                            'Server {} responded with a {} status code.'
                        ).format(self._felyx_api, return_data.status_code)
                    )
                except requests.ConnectionError:
                    LOGGER.exception(
                        str(
                            "Couldn't perform the query {}, no connection "
                            'to that server was possible. Exiting.'
                        ).format(query)
                    )
                    sys.exit(1)

                if return_data.status_code != requests.codes.ok:
                    LOGGER.debug(
                        str(
                            'The server {} responded with an unexpected HTTP '
                            'status code ({}). '
                        ).format(self._felyx_api, return_data.status_code)
                    )
                    if return_data.status_code >= 500:
                        LOGGER.critical(
                            str(
                                'The server responded with a status code {}. '
                                'This implies an error in the server itself, '
                                'or that the felyx server is not properly '
                                'installed. '
                                'Please contact the administrator for the '
                                'server. Exiting.'
                            ).format(return_data.status_code)
                        )
                        sys.exit(1)
                    elif return_data.status_code >= 400:
                        LOGGER.critical(
                            str(
                                'The server responded with a status code {}. '
                                'This implies that the client has requested '
                                'an invalid, missing or hidden resource. '
                                'Please contact the felyx developers to '
                                'report this. The query being made was: {}\n'
                                ' Exiting.'
                            ).format(return_data.status_code, query)
                        )
                        sys.exit(1)
                    else:
                        LOGGER.debug(
                            str(
                                'The server responded with an unexpected, but '
                                'not erroneous status of {}. Continuing.'
                            )
                        )

                try:
                    return_data = return_data.json()
                except ValueError:
                    LOGGER.exception(
                        'The server did not respond with an error, but the '
                        'response was not valid JSON. This is an error. Please '
                        'contact the felyx developers, with the details of '
                        'the following text. Now exiting.'
                    )
                    sys.exit(1)

                # Log any errors as returned from tastypie
                if 'error' in return_data:
                    LOGGER.critical(
                        str(
                            'Execution of query {} returned invalid results. '
                            'This was reported as follows: \n{}'
                            '\nExiting.'
                        ).format(query, return_data['error'])
                    )
                    sys.exit(1)

                # Add results
                if 'objects' in return_data:
                    result += return_data['objects']

                    # Check to see if more data is available.
                    if 'meta' in return_data:
                        if 'next' in return_data['meta']:
                            if return_data['meta']['next']:
                                query = 'http://{}{}'.format(
                                    urlparse.urlsplit(query)[1],
                                    return_data['meta']['next']
                                )
                            else:
                                more_data_available = False

                else:
                    more_data_available = False
                    result = return_data

            # Cache the results
            self.__cache[query] = result

        else:
            LOGGER.debug('{} found in object instance cache'.format(query))
            result = self.__cache[query]

        return result

    def dataset(self, dataset_pid=None, dataset_id=None):
        """
        Return a dictionary representation of the 'dataset' information for
        that entry. Or for all datasets.

        :param dataset_pid: The ID of the dataset in the server database.
        :type dataset_pid: int
        :param dataset_id: The dataset_name of the dataset in the server
          database.
        :type dataset_id: str or unicode
        :return: All information pertaining to the dataset.
        :rtype: dict
        """
        try:
            if dataset_id:
                result = self.__query(
                    'dataset/?name={id}', data={'id': dataset_id}
                )
                if len(result) == 1:
                    return result[0]
                else:
                    LOGGER.error(
                        str(
                            'Found {} and not 1 instances of dataset '
                            'with code {}.'
                        ).format(len(result), dataset_id)
                    )

            elif dataset_pid is not None:
                result = self.__query(
                    'dataset/{id}/', data={'id': dataset_pid}
                )
                if isinstance(result, dict):
                    return result
            elif dataset_pid is None and dataset_id is None:
                result = self.__query('dataset/')
                return result
        except IOError:
            LOGGER.critical(
                str(
                    'The dataset name or number provided ({}) does not '
                    'exist in this felyx instance. Please try another '
                    'dataset.'
                ).format(
                    dataset_id if dataset_id is not None else dataset_pid
                )
            )
            sys.exit(1)

    def metric(self, dataset_name):
        """
        Return a dictionary representation of the metrics for a dataset.

        :param dataset_name: The dataset_name of the dataset in the server
          database.
        :type dataset_name: str or unicode
        :return: All information pertaining to the metrics applied to this
          dataset.
        :rtype: dict
        """
        results = []
        tmp_results = self.__query(
            'metric/?dataset__name={}'.format(dataset_name)
        )

        # Replace API links with table ID numbers
        for element in tmp_results:
            element['site_collection'] = [
                self._id_from_api(_) for _ in element['site_collection']
                if isinstance(_, str)
            ]
            element['dataset'] = self._id_from_api(element['dataset'])
            results.append(element)

        return results

    def sites(self, static=None):
        """
        Return a dictionary representation of all the sites.

        :param static: Return only static sites if True, only dynamic sites
          if False, all sites if None.
        :type static: NoneType | bool
        :return: All information pertaining to the chosen sites.
        :rtype: list
        """
        if static:
            if not self.__sites_data['static']:
                self.__sites_data['static'] = self.__query(
                    'site/?static=true&limit=500'
                )
            return self.__sites_data['static']
        elif static is False:
            if not self.__sites_data['dynamic']:
                self.__sites_data['dynamic'] = self.__query(
                    'site/?static=false&limit=500'
                )
            return self.__sites_data['dynamic']
        elif static is None:
            result = self.sites(static=True) + self.sites(static=False)
            return result

    def site(self, site_id=None, site_code=None):
        """
        Return a dictionary representation of a single site.

        :param site_id: The ID of the site in the server database.
        :type site_id: int
        :param site_code: The code of the site in the server
        :type site_code: basestring
        :return: All information pertaining to the chosen individual site.
        :rtype: dict
        """
        result = []
        if site_code:
            if not isinstance(site_code, str):
                raise ValueError(
                    'site_code must be a string type object'
                )

            if self.__sites_data['static']:
                result = [
                    _ for _ in self.sites(True) if _['code'] == site_code
                ]

            if (not result) and self.__sites_data['dynamic']:
                result = [
                    _ for _ in self.sites(False) if _['code'] == site_code
                ]

            if not result:
                result = self.__query('site/?code={}'.format(site_code))

            if len(result) != 1:
                LOGGER.error(
                    str(
                        'Found {} and not 1 instances of site with code {}.'
                    ).format(len(result), site_code)
                )
                sys.exit(1)
            return result[0]

        elif site_id is not None:
            if not isinstance(site_id, int):
                raise ValueError(
                    'site_id must be an integer'
                )
            result = self.__query('site/{}/'.format(site_id))
            if not isinstance(result, dict):
                LOGGER.error(
                    str(
                        'Found {} and not 1 instances of site with id {}.'
                    ).format(len(result), site_id)
                )
                sys.exit(1)
            return result
        else:
            return None

    # noinspection PyTypeChecker
    def site_collection_info(self,
                             site_collection_id,
                             start: datetime = None,
                             stop: datetime = None
                             ) -> Dict:
        """
        Return a dictionary representation of the sites in a site collection.

        :param start: (optional) the begin time coverage
        :param stop: (optional) the end time coverage
        :param site_collection_id: The ID of the site_collection in the server
          database.
        :type site_collection_id: int
        :return: All information pertaining to that site collection.
        :rtype: dict
        """
        result = self.__query(
            'site_collection/{}/'.format(site_collection_id)
        )

        # Replace API links with full user information
        if not isinstance(result['owner'], dict):
            user_id = self._id_from_api(result['owner'])
            user_info = self.__query(
                'user/{}/'.format(user_id)
            )
            result['owner'] = user_info
        return result

    def site_collection_id(self, site_collection_name):
        """
        Return the info of a site collection.

        :param str site_collection_name: The name of the site_collection in the
          server database.
        :return: All information pertaining to that site collection.
        :rtype: dict
        """
        result = self.__query(
            'site_collection/?name={}'.format(site_collection_name)
        )

        return result['id']

    # noinspection PyTypeChecker
    def site_collections(self,
                         dataset: List,
                         start: datetime = None,
                         stop: datetime = None,
                         dynamic: bool = False,
                         only_site_collection: str = None
        ) -> Dict:
        """
        Return a dictionary representation of the site collections, defined
        by the contents of miniprod processing.
        Automatically include a manual 'public' collection, which is a
        superset off all sites owned by 'felyx'.
        :param dataset: The integer dataset id to search for collections for.
        :param start: (optional) the begin time coverage
        :param stop: (optional) the end time coverage
        :param dynamic: Boolean limit to dynamic sites, or limit to static
          sites.
        :param str only_site_collection: The optional code of the only valid
          site collection.
        """

        site_collections = OrderedDict([('public', {}), ('private', {})])

        LOGGER.debug('Establishing list of site collections.')
        queries = {
            'public': str(
                'miniprod_processing/?site_collection__level__in'
                '=community&level__in=local&dataset={}&'
                'site_collection__static={}'.format(
                    dataset['id'],
                    str(not dynamic).lower()
                )
            ),
            'private': str(
                'miniprod_processing/?site_collection__level__in'
                '=custom&dataset={}&site_collection__static={}'.format(
                    dataset['id'],
                    str(not dynamic).lower()
                )
            )
        }

        for group in queries.keys():

            #Identify the public/private collections required
            collections = [
                self.__query(_['site_collection']) for _ in self.__query(
                    queries[group]
                )
            ]

            if only_site_collection is not None:
                LOGGER.debug(
                    "Restricting site collections to code '{}' in group "
                    '{}.'.format(
                        only_site_collection, group
                    )
                )
                collections = [
                    _ for _ in collections if _['code'] == only_site_collection
                ]

            LOGGER.debug('Found collection id(s) {} for {} users.'.format(
                str([_['id'] for _ in collections]), group))
            # Make a dict of site information for each site in each public
            # collection
            for collection in collections:
                site_collections[group][collection['id']] = {}
                for site in collection['site']:
                    site_info = self.__query(site)
                    geos_shape = geometry(site_info['shape'])
                    felyx_shape = get_felyx_shape(geos_shape)
                    site_collections[group][collection['id']].update(
                        {site_info['code']: felyx_shape}
                    )
        return site_collections

    def local_copy(self):
        """
        Create a JSON copy of the felyx server information, which may can then
        be accessed without making a request to the server.

        Tables extracted are core_site, core_dataset, core_metric,
        core_miniprod_processing, core_site_collection and core_user. Not all
        information from each table is extracted.
        """
        raise NotImplementedError

    def miniprod_size(self, dataset, site_collection_id):
        """Return the single miniprod size for a dataset, collection pair.

        :param dataset: The id of the dataset in question (as a
        SourceFile.dataset object).
        :param site_collection_id: The integer id of the site collection.
        """
        raise NotImplementedError

    def constraints(self, dataset):
        """Return the largest dynamic site constraints for each site for a
        dataset.

        :param dataset: The id of the dataset in question (as a
        SourceFile.dataset object).
        """
        raise NotImplementedError

    def default_options(self, dataset, site_collection_id):
        """Return the default arguments dataset, collection pair.

        :param dataset: The id of the dataset in question (as a
        SourceFile.dataset object).
        :param int site_collection_id: The integer id of the site collection.
          database.
        :return: The default arguments for that dataset / site_collection
          pair.
        :rtype: str
        """
        raise NotImplementedError
