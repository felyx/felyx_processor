# encoding: utf-8
"""
Contains base classes for the representation, and collection, of data
as required by felyx_processor.
"""
import abc
import typing as T

from pydantic import BaseModel

import felyx_processor.utils.configuration as cfg


class NoLocalCache(Exception):
    """Raised when no local cache file exists."""


class BadLocalCache(Exception):
    """Raised when the local cache file is unreadable."""


# @TODO Usage ?
class Site(BaseModel):
    # @TODO put in configuration model ?
    site_collection: T.Dict = {}
    site_id: str = None
    site_code: str = None


class FelyxData(abc.ABC):
    """An abstract base class for getting required felyx input data"""

    @staticmethod
    def _id_from_api(resource_url):
        """Return the ID of an element in the form of a resource URL

        :param resource_url: The resource URL
        :type resource_url: str or unicode
        :return: The id of the resource
        :rtype: int
        """
        if isinstance(resource_url, int):
            return resource_url
        return int(resource_url.split('/')[-2])

    @abc.abstractmethod
    def dataset(
            self,
            dataset_pid: T.Optional[int] = None,
            dataset_id: T.Optional[str] = None
    ) -> T.Union[cfg.DatasetConfig, T.Dict[str, cfg.DatasetConfig]]:
        """
        Return a dictionary representation of the 'dataset' information for
        that entry. Or for all datasets.

        Parameters
        ----------
        dataset_pid: str, optional
            The numeric identifier of the dataset in the server database.
        dataset_id: str, optional
            The dataset identifier of the dataset in the server database.

        Returns
        -------
        config:  DatasetConfig or dict of DatasetConfig
            All information pertaining to the dataset(s).
        """


    @abc.abstractmethod
    def metric(self, dataset_name):
        """
        Return a dictionary representation of the metrics for a dataset.

        :param dataset_name: The dataset_name of the dataset in the server
          database.
        :type dataset_name: str or unicode
        :return: All information pertaining to the metrics applied to this
          dataset.
        :rtype: dict
        """
        return

    @abc.abstractmethod
    def sites(self, static=None):
        """
        Return a list representation of all the sites.

        :param static: Return only static sites if True, only dynamic sites
          if False, all sites if None.
        :type static: NoneType | bool
        :return: All information pertaining to the chosen sites.
        :rtype: list
        """
        return

    @abc.abstractmethod
    def site(self, site_id=None, site_code=None):
        """
        Return a dictionary representation of a single site.

        :param site_id: The ID of the site in the server database.
        :type site_id: int
        :param site_code: The code of the site in the server
        :type site_code: str or unicode
        :return: All information pertaining to the chosen individual site.
        :rtype: dict
        """
        return

    @abc.abstractmethod
    def related_site_collections(
            self,
            dataset_id,
            allowed_site_collections=None):
        raise NotImplementedError

    @abc.abstractmethod
    def site_collection_info(
            self,
            site_collection_id: str,
        ) -> cfg.SiteCollectionConfig:
        """Return a description of a site collection.

        Parameters
        ----------
        site_collection_id: str
            The ID of the site_collection

        Returns
        -------
        site_collection: SiteCollectionConfig
            All information pertaining to that site collection.
        """

    @abc.abstractmethod
    def site_collection_id(self, site_collection_name):
        """
        Return the info of a site collection.

        :param str site_collection_name: The name of the site_collection in the
          server database.
        :return: All information pertaining to that site collection.
        :rtype: dict
        """
        return

    @abc.abstractmethod
    def site_collections(self,
                         dataset: T.List,
                         dynamic: bool = False,
                         only_site_collection: str = None
        ) -> T.Dict:
        """
        Return a dictionary representation of the site collections, defined
        by the contents of miniprod processing.
        Automatically include a manual 'public' collection, which is a
        superset off all sites owned by 'felyx'.
        :param dataset: The integer dataset id to search for collections for.
        :param dynamic: Boolean limit to dynamic sites, or limit to static
          sites.
        :param str only_site_collection: The optional code of the only valid
          site collection.
        """

    @abc.abstractmethod
    def constraints(self, dataset):
        """Return the largest dynamic site constraints for each site for a
        dataset.

        :param dataset: The id of the dataset in question (as a
        SourceFile.dataset object).
        """
