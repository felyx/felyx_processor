# -*- encoding: utf-8 -*-

"""
@author: <sylvain.herledan@oceandatalab.com>
@date: 2018-09-26

Main program of Felyx felyx-get-coverage command. This command is used for
retrieving datasets time coverages from Elasticsearch miniprods index.
"""

import argparse
import datetime
import json
import logging
import os
from pathlib import Path
from typing import List, NoReturn, Tuple, Optional

import felyx_processor.index.es
import felyx_processor.storage
from felyx_processor.miniprod.miniprod_factory import MiniprodDataSourceFactory
from felyx_processor.utils.arguments import parse_all_args
from felyx_processor.utils.configuration import (
    FelyxProcessorConfig,
    FelyxSystemConfig,
    load_felyx_processor_config,
    load_felyx_sys_config,
)
from felyx_processor.utils.exceptions import FelyxProcessorError
from felyx_processor.utils.logging import setup_logging
from felyx_processor.utils.parameters import FelyxModelParameters, log_params
from . import exit_success, exit_failure


logger = logging.getLogger('felyx.processor')


class GetCoverageParams(FelyxModelParameters):
    output_path: Optional[Path] = None


def parse_args(cli_args: List[str]) -> \
        Tuple[FelyxSystemConfig, FelyxProcessorConfig, GetCoverageParams]:
    """Parse the command line arguments.

    Args:
      cli_args: The command line arguments.

    Returns:
      Tuple[FelyxSystemConfig, FelyxProcessorConfig, GetCoverageParams]:
      A tuple containing the felyx system configuration, the felyx processor
       configuration and the command parameters.

    Raises:
      FileNotFoundError: If configuration file does not exist.
    """
    coverage_parser = argparse.ArgumentParser()
    coverage_parser.add_argument(
        '-c', '--configuration-file', type=str, required=True,
        help='Path of the configuration file')

    coverage_parser.add_argument(
        '--output_path', type=str, default=None,
        help=('If provided, path of the file where the '
              'results will be saved in JSON format'))

    coverage_parser.add_argument(
        '--logfile', type=str, default=None,
        help='Path of the file where logs will be written')
    coverage_parser.add_argument(
        '--logfile_level', type=str, default=None,
        help='Minimal level that log messages must reach to be written '
             'in the log file')
    level_group = coverage_parser.add_mutually_exclusive_group()
    level_group.add_argument(
        '-v', '--verbose', action='store_true',
        help='Activate debug level logging - for extra feedback.')
    level_group.add_argument(
        '-q', '--quiet', action='store_true',
        help='Disable information logging - for reduced feedback.')
    level_group.add_argument(
        '-s', '--silence', action='store_true',
        help='Log ONLY the most critical or fatal errors. ')

    args = parse_all_args(coverage_parser, cli_args)

    felyx_processor_config = load_felyx_processor_config(
        configuration_file=args.configuration_file)
    felyx_sys_config = load_felyx_sys_config()

    params = GetCoverageParams(**dict(
        output_path=args.output_path,
        log=log_params(args)
    ))

    return felyx_sys_config, felyx_processor_config, params,


def default_converter(item_object):
    """
     Convert an item object of dictionary to JSON compatible format.

     Args:
         item_object: The object to convert.

     Returns:
         If the item is a datetime, a string to time iso format,
         a float otherwise.
    """
    if isinstance(item_object, datetime.datetime):
        return item_object.isoformat()
    else:
        return float(item_object)


def felyx_get_coverage(cli_args: List[str] = None):
    """
      Felyx get_coverage command. This command is used for retrieving datasets
      time coverages from Elasticsearch miniprods index.

      Args:
          cli_args: The command arguments.
      """
    # Setup logging
    main_logger = logging.getLogger()

    felyx_sys_config, felyx_processor_config, args = parse_args(cli_args)

    # Setup logging
    if not setup_logging(main_logger, args.log):
        exit_failure(args)

    # Retrieve datasets coverage from miniprod datasource
    ds = MiniprodDataSourceFactory.get_miniprod_datasource(
        felyx_sys_config=felyx_sys_config
    )
    try:
        result = ds.get_datasets_coverage()
    except FelyxProcessorError:
        logger.error('Failed to retrieve datasets coverages')
        exit_failure(args)

    if result is None:
        logger.error('Failed to retrieve datasets coverage')
        exit_failure(args)

    if args.output_path is not None:
        output_dir = os.path.dirname(args.output_path)
        ok = felyx_processor.storage.makedirs(output_dir)
        if not ok:
            logger.error(
                'Could not create output directory {}'.format(output_dir))
            exit_failure(args)

        with open(args.output_path, 'wt') as f:
            json.dump(result, f, default=default_converter)
    else:
        print(json.dumps(result, default=default_converter, indent=2))

    exit_success(args)
