# -*- encoding: utf-8 -*-

import logging
import shutil
import sys
from pathlib import Path
from typing import Union

from felyx_processor.utils.configuration import FelyxSystemConfig, load_felyx_sys_config
from felyx_processor.utils.exceptions import FelyxProcessorError

logger = logging.getLogger(__name__)
handler = logging.StreamHandler()
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)


def create_directory(path: Union[str, Path]):
    """
         Check existence of a directory
         Create it if non exist and set full rwx rights
         Args:
             path : the path to the directory to create
    """
    if isinstance(path, str):
        path = Path(path)

    create_dir = not path.exists()
    if create_dir:
        try:
            path.mkdir(parents=True, exist_ok=True)
        except OSError as e:
            msg = 'Could not create directory : {} error: {}'.format(str(path), e.strerror)
            logger.error(msg)
            raise FelyxProcessorError(msg)

    try:
        path.chmod(0o777)
    except OSError as e:
        msg = 'Could not set a+w rights to directory : {} error: {}'.format(str(path), e.strerror)
        logger.error(msg)
        raise FelyxProcessorError(msg)

    if create_dir:
        logger.info('"{}" directory successfully created'.format(str(path)))


def felyx_install(felyx_config: FelyxSystemConfig, force_reinstall: bool = False):
    """
       - Check existence of directories stored in Felyx system configuration
         Create them if non exist and set full rw rights
       - Create Elasticsearch indexes for miniprods and metrics
    """
    if felyx_config.root_dir.is_dir() and force_reinstall:
        logging.info('Remove directory {0}'.format(felyx_config.root_dir))
        shutil.rmtree(felyx_config.root_dir)

    try:
        create_directory(felyx_config.root_dir)

        directories = (
            felyx_config.shared_workspace_root,
            felyx_config.default_felyx_miniprod_store,
            felyx_config.output_derived_miniprod_dir,
            felyx_config.insitu_mappings_dir
        )
        for directory in directories:
            create_directory(directory)

    except FelyxProcessorError:
        sys.exit(1)


def main():
    # load configuration
    felyx_config = load_felyx_sys_config()
    # install with the good configuration
    felyx_install(felyx_config=felyx_config)


if '__main__' == __name__:
    main()
