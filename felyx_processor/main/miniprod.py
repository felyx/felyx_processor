# -*- encoding: utf-8 -*-

"""
@author <sylvain.herledan@oceandatalab.com>
@date 2019-02-22
"""
import json
import logging
import os
import sys
from argparse import ArgumentParser
from datetime import datetime
from pathlib import Path
from typing import List, NoReturn, Tuple, Optional

from pydantic import field_validator, DirectoryPath
import shapely.geometry
import shapely.wkt

import felyx_processor.export.miniprod
import felyx_processor.index.es
import felyx_processor.storage
from felyx_processor.extraction.source import SourceFile
from felyx_processor.felyx_data import get_felyx_data
from felyx_processor.miniprod.generate import produce_miniprods
from felyx_processor.miniprod.miniprod_factory import MiniprodDataSourceFactory
from felyx_processor.miniprod.es_miniprod_datasource import ESMiniprodDataSource
from felyx_processor.miniprod.manifest_miniprod_datasource import (
    ManifestMiniprodDataSource)
from felyx_processor.utils.arguments import get_override_args, parse_all_args
from felyx_processor.utils.configuration import (
    FelyxModel,
    FelyxProcessorConfig,
    FelyxSystemConfig,
    SiteCollectionConfig,
    get_dataset_collection_extraction_config,
    load_felyx_processor_config,
    load_felyx_sys_config,
)
from felyx_processor.utils.exceptions import FelyxProcessorError
from felyx_processor.utils.logging import setup_logging
from felyx_processor.utils.models import Indexing
from felyx_processor.utils.parameters import (
    FelyxModelParameters, InputFiles, FilterSitesCollections, log_params,
    add_log_args)
from felyx_processor.miniprod import ChildMetadata
from . import exit_success, exit_failure


logger = logging.getLogger('felyx.processor')


class MiniprodsOption(FelyxModel):
    no_overwrite: bool = False
    indexing: List[Indexing] = ['manifest']
    create_miniprod: bool = False
    exit_flag: Optional[str] = None
    child_product_dir: Optional[Path] = Path('felyx/data')
    manifest_dir: Optional[Path] = Path('felyx/manifests')


class MiniprodsParams(FelyxModelParameters):
    dataset_id: str
    input: InputFiles
    filters: Optional[FilterSitesCollections] = None
    options: Optional[MiniprodsOption] = None


def add_child_extraction_args(parser: ArgumentParser) -> ArgumentParser:

    parser.add_argument(
        '--allowed-collections', type=str, default=None,
        help='Site collection identifier or list of site collection '
             'identifiers. The extraction will only be generated for these '
             'collections')
    parser.add_argument(
        '--allowed-sites', type=str, default=None,
        help='Site identifier or list of site identifiers. '
             'The extraction will only be generated for these sites.')

    # arguments for spatial filtering
    parser.add_argument(
        '--area', type=float, nargs=4, default=None,
        help='Area to process (any file not intersecting this area will '
             'return an empty manifest and no child products), as a list '
             'latmin,lonmin,latmax,lonmax')
    parser.add_argument(
        '--area-attribute', type=str, default='cf',
        help='input file attribute to get its spatial coverage from. Use `cf` '
             'to use the standard bounding box attributes. If no attribute is '
             'provided, felyx will use the file lat/lon which is likeky to be '
             'slower. Any other attribute than `cf` must be a WKT string.')

    parser.add_argument(
        '--create-miniprod', action='store_true', default=False,
        help=str('create miniprod files. Miniprods are not created by default')
    )
    parser.add_argument(
        '--no-overwrite', action='store_true', default=False,
        help=str('no overwrite already existing child products')
    )
    parser.add_argument(
        '--indexing', type=str, nargs='*', default=['manifest'],
        choices=['stdout', 'manifest', 'elasticsearch'],
        help='index the extracted child products metadata. Default is to a '
             'manifest file.')

    return parser


def args_as_dict(args, felyx_sys_config):
    vargs = {k: v  for k, v in vars(args).items() if v is not None}

    if 'inputs' in vargs:
        vargs['inputs'] = [_ for x in vargs['inputs'] for _ in x.split(',')]

    return dict(
        dataset_id=args.dataset_id,
        input=dict(
            inputs=vargs.get('inputs'),
            from_list=args.from_list),
        filters=dict(
            allowed_collections=args.allowed_collections,
            allowed_sites=args.allowed_sites,
            area=args.area,
            area_attribute=args.area_attribute),
        options=dict(
            create_miniprod=args.create_miniprod,
            indexing=args.indexing,
            no_overwrite=args.no_overwrite,
            # @TODO does not work - remove system defaults ?
            child_product_dir=vargs.get(
                'child_product_dir',
                felyx_sys_config.default_felyx_miniprod_store),
            manifest_dir=vargs.get(
                'manifest_dir',
                felyx_sys_config.default_felyx_manifest_store)),
        log=log_params(args)
    )

def parse_args(cli_args: List[str]) -> Tuple[FelyxSystemConfig,
                                             FelyxProcessorConfig,
                                             MiniprodsParams]:
    """"""
    extraction_parser = ArgumentParser()
    extraction_parser.add_argument(
        '-c', '--configuration-file', type=str, required=True,
        help='Path of the configuration file')

    extraction_parser.add_argument(
        '--dataset-id', type=str, required=True,
        help='The identifier of the dataset for the extraction')

    extraction_parser.add_argument(
        '--inputs', type=str, default=None, nargs='*',
        help='The path of the input file or a list of the paths of the input '
             'files')
    extraction_parser.add_argument(
        '--from-list', type=str, default=None,
        help='The path of a file containing one input path per line')

    extraction_parser.add_argument(
        '--child-product-dir', type=str, default=None,
        help='Path of the directory that will be used as root '
             'when building the path of the files containing the extractions')
    extraction_parser.add_argument(
        '--manifest-dir', type=str, default=None,
        help='Path of the directory that will be used as root to store '
             'manifest.\nIf set to "-" (default), the content of the manifest '
             'file is printed on stdout')

    extraction_parser = add_child_extraction_args(extraction_parser)
    extraction_parser = add_log_args(extraction_parser)

    args = parse_all_args(extraction_parser, cli_args)

    felyx_processor_config = load_felyx_processor_config(
        configuration_file=args.configuration_file)
    felyx_sys_config = load_felyx_sys_config()

    params = MiniprodsParams(**args_as_dict(args, felyx_sys_config))

    return felyx_sys_config, felyx_processor_config, params


def format_manifest(
        miniprod_path: Path,
        miniprod_info: dict) -> (Path, str):
    """"""
    manifest_line = {'id': os.path.basename(miniprod_path)}
    for key in miniprod_info.keys():
        if key == 'time_coverage':
            manifest_line['time_coverage_start'] = miniprod_info[key][0]
            manifest_line['time_coverage_stop'] = miniprod_info[key][1]
        elif key == 'dynamic_target_time':
            manifest_line[key] = \
                miniprod_info[key].strftime('%Y-%m-%d %H:%M:%S')
        else:
            manifest_line[key] = miniprod_info[key]

    # return f'{miniprod_path}; {json.dumps(manifest_line)}'
    return miniprod_path, manifest_line


def register_children(
        felyx_sys_config,
        felyx_processor_config,
        input_path: Path,
        children: List[ChildMetadata],
        manifest_dir: Path,
        indexing: List[Indexing],
        dataset_id: str = None,
        granule_date: datetime = None
) -> None:
    """Save the metadata of the children products into an index."""
    if Indexing.stdout in indexing:
        # dump manifest to stdout
        for rec in children:
            print(f'{rec.model_dump_json()}')

    if Indexing.manifest in indexing:
        # save manifest to disk
        manifest_saver = ManifestMiniprodDataSource(manifest_dir)
        manifest_path = manifest_saver.manifest_path(
            input_path, dataset_id, granule_date)
        manifest_saver.register_miniprods(children, manifest_path)

    if Indexing.elasticsearch in indexing:
        # @TODO felyx_sys_config.miniprod_root_dir use args override if defined
        # @TODO do we really ,need this ? (defined in ChildMetadata?)
        manifest_saver = ESMiniprodDataSource(
            felyx_sys_config.elasticsearch,
            felyx_sys_config.default_felyx_miniprod_store)
        # @TODO addapt method for ES
        manifest_saver.register_miniprods(children)

    # # export to another store if configured
    # options = get_dataset_collection_extraction_config(
    #     felyx_processor_config, dataset_id, children[0].site_collection)
    #     # miniprod_info[1]['__dataset_id'],
    #     # miniprod_info[1]['__site_collection_id'])

    # if indexing is not None:
    #     options['indexing'] = indexing
    # if options['indexing'] != Indexing.elasticsearch:
    #     miniprods.append(format_manifest(miniprod_info[0], doc))
    # if options['indexing'] != Indexing.manifest:
    #     docs.append((os.path.basename(miniprod_info[0]), doc,))

    # # Reformat results
    # miniprods = []
    # docs = []
    # for site in static_miniprods.keys():
    #     for miniprod_info in static_miniprods[site].values():
    #         doc = felyx_processor.export.miniprod.dict_from_attrs(
    #             miniprod_info[1], True
    #         )
    #         options = get_dataset_collection_extraction_config(
    #             felyx_processor_config,
    #             miniprod_info[1]['__dataset_id'],
    #             miniprod_info[1]['__site_collection_id'])
    #         if options is None and indexing is None:
    #             continue
    #         if indexing is not None:
    #             options['indexing'] = indexing
    #         if options['indexing'] != Indexing.elasticsearch:
    #             miniprods.append(format_manifest(miniprod_info[0], doc))
    #         if options['indexing'] != Indexing.manifest:
    #             docs.append((os.path.basename(miniprod_info[0]), doc,))
    #
    # for site in children.keys():
    #     for miniprod_info in children[site].values():
    #         doc = felyx_processor.export.miniprod.dict_from_attrs(
    #             miniprod_info[1], True
    #         )
    #         options = get_dataset_collection_extraction_config(
    #             felyx_processor_config,
    #             miniprod_info[1]['__dataset_id'],
    #             miniprod_info[1]['__site_collection_id'])
    #         if options is None and indexing is None:
    #             continue
    #         if indexing is not None:
    #             options['indexing'] = indexing
    #         if options['indexing'] != Indexing.elasticsearch:
    #             miniprods.append(format_manifest(miniprod_info[0], doc))
    #         if options['indexing'] != Indexing.manifest:
    #             docs.append((os.path.basename(miniprod_info[0]), doc,))
    #
    # if len(miniprods) + len(docs) == 0:
    #     logger.warning('No subsets extracted for this input file')
    #
    # # Write or print manifest
    # if manifest_dir is None:
    #     for miniprod_path, manifest in miniprods:
    #         print(f'{miniprod_path}; {json.dumps(manifest)}')
    # else:
    #     write_manifest(
    #         manifest_dir, input_path, miniprods, dataset_id, granule_date)
    #
    # if len(docs) > 0:
    #     ds = MiniprodDataSourceFactory.get_miniprod_datasource(felyx_sys_config)
    #     ds.register_miniprods(docs)
    #
    # return True


def in_area(source: SourceFile,
            area: Tuple[float, float, float, float],
            area_attribute: str = None) -> bool:
    """Check if a source file's spatial coverage intersects a given area.

    Args:
        source: file to check the spatial coverage from
        area: bounding box of the intersection area, as a list latmin,
            lonmin, latmax, lonmax
        area_attribute: attribute to read the file's spatial coverage from.
            If `cf`, uses the standard CF convention spatial coverage
            attributes. If None, uses the file lat/lon.
    """
    area = shapely.geometry.box(area[1], area[0], area[3], area[2])

    if area_attribute is None or area_attribute == 'cf':
        coverage = source.feature.ds.cb.bbox
    else:
        try:
            coverage = shapely.wkt.loads(
                source.feature.ds.attrs[area_attribute])
        except Exception:
            raise ValueError(f'attribute {area_attribute} does not contain a '
                             f'WKT string')

    return coverage.intersects(area)


def find_children(
        site_collections: List[SiteCollectionConfig],
        felyx_sys_config,
        felyx_processor_config,
        source_file,
        args
):
    # Produce child products for static sites
    if len(site_collections) == 0:
        raise ValueError(
            f'No site collection associated with dataset'
            f' {source_file.dataset_id}. Felyx can not process the files from '
            f'this dataset. Check your configuration.')

    results = produce_miniprods(
        felyx_sys_config=felyx_sys_config,
        felyx_processor_config=felyx_processor_config,
        source_file=source_file,
        miniprod_dir=args.options.child_product_dir,
        overwrite=not args.options.no_overwrite,
        site_collections=site_collections,
        allowed_sites=args.filters.allowed_sites,
        save=args.options.create_miniprod)

    if len(results) == 0:
        logger.info(f'No child products found for {source_file.input_url}')

    return results


def run_extraction(
        felyx_sys_config,
        felyx_processor_config,
        inputs: List[Path],
        args: MiniprodsParams,
) -> None:
    """"""
    # Get the static and the dynamic sites for the EO dataset to process
    # felyx_data = get_felyx_data(felyx_processor_config)

    site_collections = felyx_processor_config.related_site_collections(
        args.dataset_id, args.filters.allowed_collections)

    # Check that the dataset of the source file appears at least in one
    # configured extraction
    if len(site_collections) == 0:
        logger.warning(
            f'No static or dynamic site collection found for dataset '
            f'{args.dataset_id}')

    for input_path in inputs:
        logger.info('Processing {}'.format(input_path))
        _source_file = SourceFile(
            felyx_processor_config=felyx_processor_config,
            url=input_path,
            dataset_id=args.dataset_id)

        child_products = None
        with _source_file.load() as source_file:

            # spatial filter
            if args.filters.area is not None:
                is_in_area: bool = in_area(
                    source_file, args.filters.area, args.filters.area_attribute)
                if not is_in_area:
                    # return empty manifest, skip file processing
                    child_products = []

            if child_products is None:
                child_products = find_children(
                    site_collections,
                    felyx_sys_config,
                    felyx_processor_config,
                    source_file,
                    args)

        register_children(
            felyx_sys_config=felyx_sys_config,
            felyx_processor_config=felyx_processor_config,
            input_path=input_path,
            children=child_products,
            manifest_dir=args.options.manifest_dir,
            indexing=args.options.indexing,
            dataset_id=args.dataset_id,
            granule_date=_source_file.time_coverage_start
        )


def felyx_miniprod(cli_args: List[str] = None) -> NoReturn:
    """"""

    main_logger = logging.getLogger()

    felyx_sys_config, felyx_processor_config, args = parse_args(cli_args)

    # Setup logging
    if not setup_logging(main_logger, args.log):
        exit_failure(args)

    # Aggregate inputs from all possible sources
    inputs = args.input.get_file_list()
    # if args.input.inputs is not None:
    #     if isinstance(args.input.inputs, list):
    #         inputs.extend(args.input.inputs)
    #     else:
    #         inputs = [args.input.inputs]
    # # if not sys.stdin.isatty():  # command receives inputs from a pipe
    # #     inputs.extend([_.strip() for _ in sys.stdin.readlines()
    # #                    if 0 < len(_.strip())])
    # if args.input.from_list is not None:
    #     if not os.path.isfile(args.input.from_list):
    #         logger.error(f'Listing {args.input.from_list} does not exist')
    #         exit_failure(args)
    #
    #     with open(args.input.from_list, 'r') as f:
    #         inputs.extend([Path(_.strip()) for _ in f.readlines()
    #                        if 0 < len(_.strip()) and _[0] != '#'])
    # inputs = list(set(inputs))  # remove duplicates

    if 0 >= len(inputs):
        logger.error('No input file to process!')
        exit_failure(args)

    _msg = '{} input files to process for the {} dataset'
    logger.debug(_msg.format(len(inputs), args.dataset_id))

    try:
        run_extraction(
            felyx_sys_config=felyx_sys_config,
            felyx_processor_config=felyx_processor_config,
            inputs=inputs,
            args=args)

    except Exception:
        # Error sink
        logger.error('An error occurred and could not be handled by felyx',
                     exc_info=sys.exc_info())
        print(sys.exc_info())
        exit_failure(args)

    exit_success(args)
