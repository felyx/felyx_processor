# -*- encoding: utf-8 -*-

"""
@author: <sylvain.herledan@oceandatalab.com>
@date: 2018-09-26

Main program of Felyx felyx-register-miniprods command. This command is used for
indexing miniprods into Elasticsearch index.
"""

import argparse
import logging
import os
import sys
from typing import List, NoReturn, Tuple

from cerbere.dataset import ncdataset

import felyx_processor.export.miniprod
import felyx_processor.index.es
from felyx_processor.miniprod.miniprod_factory import MiniprodDataSourceFactory
from felyx_processor.utils.arguments import get_override_args, parse_all_args
from felyx_processor.utils.configuration import (
    FelyxProcessorConfig,
    FelyxSystemConfig,
    load_felyx_processor_config,
    load_felyx_sys_config,
)
from felyx_processor.utils.exceptions import FelyxProcessorError
from felyx_processor.utils.logging import setup_logging
from felyx_processor.utils.parameters import (
    FelyxModelParameters, log_params, InputFiles)
from . import exit_success, exit_failure


logger = logging.getLogger('felyx.processor')


class RegisterMiniprodsParams(FelyxModelParameters):
    input: InputFiles


def parse_args(
        cli_args: List[str]
) -> Tuple[FelyxSystemConfig, FelyxProcessorConfig, RegisterMiniprodsParams]:
    """Parse the command line arguments.

    Args:
      cli_args: The command line arguments.

    Returns:
      Tuple[FelyxSystemConfig, FelyxProcessorConfig, RegisterMiniprodsParams]:
      A tuple containing the felyx system configuration, the felyx processor
       configuration and the command parameters.

    Raises:
      FileNotFoundError: If configuration file does not exist.
    """

    register_parser = argparse.ArgumentParser()
    register_parser.add_argument(
        '-c', '--configuration-file', type=str, default=None,
        help='Path of the configuration file')

    register_parser.add_argument(
        '--inputs', type=str, default=None, nargs='*', required=True,
        help='The path of the input file or a list of the paths of '
             'the input files')
    register_parser.add_argument(
        '--from_list', type=str, default=None,
        help='The path of a file containing one input path per line')
    register_parser.add_argument(
        '--logfile', type=str, default=None,
        help='Path of the file where logs will be written')
    register_parser.add_argument(
        '--logfile_level', type=str, default=None,
        help='Minimal level that log messages must reach to be written '
             'in the log file')
    level_group = register_parser.add_mutually_exclusive_group()
    level_group.add_argument(
        '-v', '--verbose', action='store_true',
        help='Activate debug level logging - for extra feedback.')
    level_group.add_argument(
        '-q', '--quiet', action='store_true',
        help='Disable information logging - for reduced feedback.')
    level_group.add_argument(
        '-s', '--silence', action='store_true',
        help='Log ONLY the most critical or fatal errors. ')

    args = parse_all_args(register_parser, cli_args)

    felyx_processor_config = load_felyx_processor_config(
        configuration_file=args.configuration_file)
    felyx_sys_config = load_felyx_sys_config()

    params = RegisterMiniprodsParams(**dict(
        input=dict(
            inputs=[_ for x in args.inputs for _ in x.split(',')],
            from_list=args.from_list),
        log=log_params(args)
    ))

    return felyx_sys_config, felyx_processor_config, params,


def felyx_register_miniprods(cli_args=None):
    """
    Felyx_register_miniprods command. This command is used for indexing
    miniprods into Elasticsearch index.

    Args:
     cli_args: The command arguments.
   """
    main_logger = logging.getLogger()

    felyx_sys_config, felyx_processor_config, args = parse_args(cli_args)

    # Setup logging
    if not setup_logging(main_logger, args.log):
        exit_failure(args)

    # Aggregate inputs from all possible sources
    inputs = []
    if args.input.inputs is not None:
        if isinstance(args.input.inputs, list):
            inputs.extend(args.input.inputs)
        else:
            inputs = [args.input.inputs]
    # if not sys.stdin.isatty():  # command receives inputs from a pipe
    #     inputs.extend([_.strip() for _ in sys.stdin.readlines()
    #                    if 0 < len(_.strip())])
    if args.input.from_list is not None:
        if not os.path.isfile(args.input.from_list):
            logger.error('Listing {} does not exist'.format(
                args.input.from_list))
            exit_failure(args)
        with open(args.input.from_list, 'r') as f:
            inputs.extend([_.strip() for _ in f.readlines()
                           if 0 < len(_.strip())])
    inputs = list(set(inputs))  # remove duplicates

    if 0 >= len(inputs):
        logger.error('No input file to process!')
        exit_failure(args)

    _msg = '{} input files to process'
    logger.debug(_msg.format(len(inputs)))

    docs = []
    for input_path in inputs:
        if not os.path.exists(input_path):
            logger.warning('Skipped input "{}": not found'.format(input_path))
            continue

        dataset = ncdataset.Dataset(input_path)
        try:
            miniprod_name = os.path.basename(input_path)
            doc = felyx_processor.export.miniprod.dict_from_dataset(dataset)
            docs.append((miniprod_name, doc,))
        except felyx_processor.export.miniprod.MissingMiniprodFields:
            e = sys.exc_info()
            _msg = 'Skipped input: "{}" file lacks mandatory fields ({})'
            logger.warning(_msg.format(input_path, ', '.join(e[1].missing)))
            logger.debug('Invalid miniproduct', exc_info=e)
        finally:
            dataset.close()

    if 0 >= len(docs):
        logger.error('No document to register in the index!')
        exit_failure(args)

    datasource = MiniprodDataSourceFactory.get_miniprod_datasource(
        felyx_sys_config=felyx_sys_config
    )
    try:
        datasource.register_miniprods(docs)
    except FelyxProcessorError:
        logger.error('Failed to register miniprods in the index')
        exit_failure(args)

    logger.info('Done')
    exit_success(args)
