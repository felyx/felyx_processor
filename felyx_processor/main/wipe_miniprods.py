# -*- encoding: utf-8 -*-
"""
Main program of Felyx felyx-wipe-miniprods command. This command is used for
removing miniprods and associated metrics from Elasticsearch.
"""

import argparse
from datetime import datetime
from dateutil import parser
import logging
import sys
from pathlib import Path
from typing import List, NoReturn, Tuple, Optional, Union

from pydantic import field_validator

from felyx_processor.metrics.metrics_factory import MetricsDataSourceFactory
from felyx_processor.miniprod.miniprod_datasource import ID_COLUMN
from felyx_processor.miniprod.miniprod_factory import MiniprodDataSourceFactory
from felyx_processor.utils.arguments import get_override_args, parse_all_args
from felyx_processor.utils.configuration import (
    FelyxProcessorConfig,
    FelyxSystemConfig,
    load_felyx_processor_config,
    load_felyx_sys_config,
)
from felyx_processor.utils.logging import setup_logging
from felyx_processor.utils.parameters import FelyxModelParameters, log_params
from . import exit_success, exit_failure


logger = logging.getLogger('felyx.processor')


class WipeMiniprodsParams(FelyxModelParameters):
    dataset_id: Optional[str] = None
    collection: Optional[str] = None
    site: Optional[str] = None
    start: Union[str, datetime, None] = None
    end: Union[str, datetime, None] = None
    dry_run: bool = False
    no_prompt: bool = False

    @field_validator('start')
    @classmethod
    def complete_start_date_parameter(cls, v: str) -> datetime:
        return parser.parse(v)

    @field_validator('end')
    @classmethod
    def complete_end_date_parameter(cls, v: str) -> datetime:
        return parser.parse(v)


def parse_args(
        cli_args: List[str]
) -> Tuple[FelyxSystemConfig, FelyxProcessorConfig, WipeMiniprodsParams]:
    """Parse the command line arguments.

      Args:
          cli_args: The command line arguments.

      Returns:
          Tuple[FelyxSystemConfig, FelyxProcessorConfig, WipeMiniprodsParams]:
          A tuple containing the felyx system configuration, the felyx processor
          configuration and the command parameters.

      Raises:
          FileNotFoundError: If configuration file does not exist.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-c', '--configuration-file', type=Path,
        help=str(
            'A configuration file with the settings of the match-up dataset '
            'to assemble'
        )
    )

    parser.add_argument('--dataset_id', type=str, default=None,
        help='The identifier of the dataset for the extraction')
    parser.add_argument('--collection', type=str, default=None,
                        help='The collection name')
    parser.add_argument('--site', type=str, default=None,
                        help='The site code')
    parser.add_argument('--start', type=str, default=None,
                        help='start date YYYY-mm-ddTHH:MM:SS format')
    parser.add_argument('--end', type=str,  default=None,
                        help='end date YYYY-mm-ddTHH:MM:SS format')
    parser.add_argument('--dry-run', action='store_true', default=False)
    parser.add_argument('--no-prompt', action='store_true', default=False)

    parser.add_argument(
        '--logfile', type=str, default=None,
        help='Path of the file where logs will be written')
    parser.add_argument(
        '--logfile_level', type=str, default=None,
        help='Minimal level that log messages must reach to be written in the log file')
    level_group = parser.add_mutually_exclusive_group()
    level_group.add_argument(
        '-v', '--verbose', action='store_true',
        help='Activate debug level logging - for extra feedback.')
    level_group.add_argument(
        '-q', '--quiet', action='store_true',
        help='Disable information logging - for reduced feedback.')
    level_group.add_argument(
        '-s', '--silence', action='store_true',
        help='Log ONLY the most critical or fatal errors. ')

    args = parse_all_args(parser, cli_args)

    felyx_processor_config = load_felyx_processor_config(
        configuration_file=args.configuration_file)
    felyx_sys_config = load_felyx_sys_config()

    params = WipeMiniprodsParams(**dict(
        dataset_id=args.dataset_id,
        collection=args.collection,
        site= args.site,
        start= args.start,
        end=args.end,
        dry_run=args.dry_run,
        no_prompt=args.no_prompt,
        log=log_params(args)
    ))

    return felyx_sys_config, felyx_processor_config, params,


def confirm(prompt: str = None, resp: bool = False):
    """
    Prompts for yes or no response from the user.

    Args:
        prompt: The message to prompt.
        resp: The default response.

    Returns:
        True for yes and False for no.
    """
    if prompt is None:
        prompt = 'Confirm'

    prompt = '%s [%s]|%s:' % (prompt, 'n', 'y')

    while True:
        ans = input(prompt)
        if not ans:
            return resp
        if ans not in ['y', 'Y', 'n', 'N']:
            print('please enter y or n.')
            continue
        if ans == 'y' or ans == 'Y':
            return True
        if ans == 'n' or ans == 'N':
            return False


def wipe_miniprods(cli_args: List[str] = None):
    """
    Felyx wipe_miniprods command. This command is used for removing
    miniprods and associated metrics from Elasticsearch.

    Args:
        cli_args: The command arguments.
    """

    main_logger = logging.getLogger()

    felyx_sys_config, felyx_processor_config, args = parse_args(cli_args)

    # Setup logging
    if not setup_logging(main_logger, args.log):
        exit_failure(args)

    # Check if there at list a dataset or site filter
    if args.dataset_id is None and args.site is None:
        logger.error('Option --dataset_id or --site must be set !')
        exit_failure(args)

    dataset_ids = None
    if args.dataset_id is not None:
        dataset_ids = [args.dataset_id]

    site_codes = None
    if args.site is not None:
        site_codes = [args.site]

    # TODO : For the moment collection filter is not available
    if args.collection is not None:
        logger.error('Option --collection is not yet implemented !')
        exit_failure(args)

    try:
        # Find the miniprods
        miniprods_ds = MiniprodDataSourceFactory.get_miniprod_datasource(
            felyx_sys_config=felyx_sys_config,
            miniprod_root_dir=felyx_sys_config.default_felyx_miniprod_store
        )

        res = miniprods_ds.get_miniprods(
            datasets=dataset_ids,
            sites_codes=site_codes,
            start=args.start,
            stop=args.end,
            filter_on_site_time=False,
            as_dataframe=True)

        miniprod_ids = list()
        if ID_COLUMN in res.columns:
            miniprod_ids = (res[ID_COLUMN]).to_list()

        if len(miniprod_ids) == 0:
            logger.warning('No miniprods found')
            exit_success(args)

        if not args.dry_run and not args.no_prompt:
            confirm_msg = '\nYou are about to delete miniprods and metrics for:'
            if args.dataset_id is not None:
                confirm_msg += '\n\t* dataset : {} '.format(args.dataset_id)
            if args.collection is not None:
                confirm_msg += '\n\t* collection : {}'.format(args.collection)
            if args.site is not None:
                confirm_msg += '\n\t* site : {}'.format(args.site)

            confirm_msg += '\n\nAre you sure you want to continue?'
            if not confirm(confirm_msg):
                exit_success(args)

        metrics_ds = MetricsDataSourceFactory.get_metrics_datasource(
            felyx_sys_config=felyx_sys_config)

        if not args.dry_run:
            # Delete metrics from miniprods ids
            nb_metrics = metrics_ds.delete_metrics(miniprod_ids)

            # Delete miniprods from miniprods ids
            nb_miniprods = miniprods_ds.delete_miniprods(miniprod_ids)

            delete_msg = 'have been deleted'
        else:
            nb_miniprods = len(miniprod_ids)
            # Find the metrics
            res = metrics_ds.get_metrics_ids_by_miniprods_ids(miniprod_ids)
            nb_metrics = len(res)
            delete_msg = 'will be deleted'

        logger.info('{} miniproducts {}'.format(nb_miniprods, delete_msg))
        logger.info('{} metrics {}'.format(nb_metrics, delete_msg))

    except Exception:
        # Error sink
        logger.error('An error occurred and could not be handled by felyx',
                     exc_info=sys.exc_info())
        exit_failure(args)

    exit_success(args)
