# -*- encoding: utf-8 -*-

"""
@author: <sylvain.herledan@oceandatalab.com>
@date: 2018-09-26
Main program of Felyx felyx-get-trajectories command. This command is used for
retrieving trajectories data from Elasticsearch index.
"""

import argparse
from datetime import datetime
from dateutil import parser
import json
import logging
import os
from pathlib import Path
from typing import List, NoReturn, Tuple, Union, Optional

from pydantic import field_validator

import felyx_processor.index.es
import felyx_processor.storage
from felyx_processor.sites.insitu_factory import InSituDataSourceFactory
from felyx_processor.utils.arguments import parse_all_args
from felyx_processor.utils.configuration import (
    FelyxProcessorConfig,
    FelyxSystemConfig,
    load_felyx_processor_config,
    load_felyx_sys_config,
)
from felyx_processor.utils.logging import setup_logging
from felyx_processor.utils.parameters import FelyxModelParameters, log_params
from . import exit_success, exit_failure


logger = logging.getLogger('felyx.processor')


class GetTrajectoriesParams(FelyxModelParameters):
    collection: str
    sites: Optional[List[str]] = None
    start: datetime
    end: datetime
    #TODO For a later use
    # tolerance: float
    output_path: Optional[Path]

    @field_validator('sites')
    @classmethod
    def complete_sites(cls, v: str) -> Optional[List[str]]:
        if v is None:
            return
        return [_ for x in v for _ in x.split(',')]


def parse_args(
        cli_args: List[str]
) -> Tuple[FelyxSystemConfig, FelyxProcessorConfig, GetTrajectoriesParams]:
    """Parse the command line arguments.

    Args:
        cli_args: The command line arguments.

    Returns:
        Tuple[FelyxSystemConfig, FelyxProcessorConfig, GetTrajectoriesParams]:
        A tuple containing the felyx system configuration, the felyx processor
        configuration and the command parameters.

    Raises:
        FileNotFoundError: If configuration file does not exist.
    """
    get_trajectories_parser = argparse.ArgumentParser()
    get_trajectories_parser.add_argument(
        '-c', '--configuration-file', type=str, required=True,
        help='Path of the configuration file')

    get_trajectories_parser.add_argument(
        '--collection', type=str,  required=True,
        help='Site collection identifier')
    get_trajectories_parser.add_argument(
        '--sites', type=str,  nargs='*',
        help='Site identifier or list of site identifiers for searching '
             'for trajectory information')
    get_trajectories_parser.add_argument(
        '--start', type=str, required=True,
         help='Beginning of the time coverage (YYYY-mm-ddTHH:MM:SS format)')
    get_trajectories_parser.add_argument(
        '--end', type=str, required=True,
        help='End of the time coverage (YYYY-mm-ddTHH:MM:SS format)')
    # TODO for a later use
    # get_trajectories_parser.add_argument(
    #     '--tolerance', type=float, default=-1.0,
    #     help=('Accepted tolerance for the geometry '
    #           'simplification algorithm'))
    get_trajectories_parser.add_argument(
        '--output_path', type=str, default=None,
        help=('If provided, path of the file where the '
              'results will be saved in JSON format'))

    get_trajectories_parser.add_argument(
        '--logfile', type=str, default=None,
        help='Path of the file where logs will be written')
    get_trajectories_parser.add_argument(
        '--logfile_level', type=str, default=None,
        help='Minimal level that log messages must reach to be written '
             'in the log file')
    level_group = get_trajectories_parser.add_mutually_exclusive_group()
    level_group.add_argument(
        '-v', '--verbose', action='store_true',
        help='Activate debug level logging - for extra feedback.')
    level_group.add_argument(
        '-q', '--quiet', action='store_true',
        help='Disable information logging - for reduced feedback.')
    level_group.add_argument(
        '-s', '--silence', action='store_true',
        help='Log ONLY the most critical or fatal errors. ')

    args = parse_all_args(get_trajectories_parser, cli_args)

    felyx_processor_config = load_felyx_processor_config(
        configuration_file=args.configuration_file)
    felyx_sys_config = load_felyx_sys_config()

    params = GetTrajectoriesParams(**dict(
        collection=args.collection,
        sites=args.sites,
        start=args.start,
        end=args.end,
        output_path=args.output_path,
        log=log_params(args)
    ))

    return felyx_sys_config, felyx_processor_config, params,


def default_converter(item_object):
    """
    Convert an item object of dictionary to JSON compatible format.

    Args:
        item_object: The object to convert.

    Returns:
        If the item is a datetime, a string to time iso format,
        a float otherwise.
    """
    if isinstance(item_object, datetime):
        return item_object.isoformat()
    else:
        return float(item_object)


def felyx_get_trajectories(cli_args=None) -> NoReturn:
    """
    Felyx get_trajectories command. This command is used for retrieving
    trajectories of a site collection from insitu data.

    Args:
        cli_args: The command arguments.
    """
    # Setup logging
    main_logger = logging.getLogger()

    felyx_sys_config, felyx_processor_config, args = parse_args(cli_args)

    # Setup logging
    if not setup_logging(main_logger, args.log):
        exit_failure(args)

    collection = args.collection
    sites = args.sites
    start = args.start
    stop = args.end
    # TODO For a later use
    #tolerance = args.tolerance
    output_path = args.output_path

    ds = InSituDataSourceFactory.get_insitu_datasource(
        felyx_sys_config=felyx_sys_config,
        felyx_processor_config=felyx_processor_config,
        collection_code=collection,
    )
    results = ds.get_trajectories(
        collection_code=collection,
        sites_codes=sites,
        start=start,
        stop=stop,
        as_dataframe=False
    )

    if results is None:
        logger.error('Failed to retrieve trajectories from the index')
        exit_failure(args)

    if not results:
        logger.warning('There is no trajectories for this collection : {} and \
        sites : {}.'.format(collection, sites))
        exit_success(args)

    if output_path is not None:
        output_dir = os.path.dirname(output_path)
        ok = felyx_processor.storage.makedirs(output_dir)
        if not ok:
            logger.error(
                'Could not create output directory {}'.format(output_dir))
            exit_failure(args)

        with open(output_path, 'wt') as f:
            json.dump(results, f, default=default_converter)
    else:
        print(json.dumps(results, default=default_converter, indent=2))

    exit_success(args)
