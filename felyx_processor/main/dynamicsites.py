"""
Main program of Felyx felyx-convert-site-data command. This command is used for
converting old Felyx csv files to Parquet files.
"""
import argparse
import logging
import sys
from pathlib import Path
from typing import List

from felyx_processor.sites.dynamic_site_data import from_csv

logger = logging.getLogger('felyx.processor')


def parse_args(cli_args: List[str]) -> argparse.Namespace:
    """Parse the command line arguments.
       Args:
           cli_args: The command line arguments.

       Returns:
           The parsed arguments.
    """
    parser = argparse.ArgumentParser(
        description=str(
            'convert dynamic site data from old felyx csv format to parquet')
    )

    parser.add_argument(
        'input', type=Path,
        help=str('file to convert, in csv format'))

    parser.add_argument(
        'collection', type=str,
        help=str('name of the collection'))

    parser.add_argument(
        '-m', '--metadata', type=Path,
        help=str('json metadata file associated with the csv files'))

    parser.add_argument(
        '-d', '--duration', type=str,
        help=str('duration covered by a single output file, as a pandas '
                 'frequency code.')
    )

    parser.add_argument(
        '-p', '--pathname', type=Path,
        help=str('full pathname of output files, in parquet format')
    )

    return parser.parse_args(cli_args)


def convert(cli_args: List[str] = None):
    """Convert files of dynamic site data from csv (old format) to parquet
        Args:
            cli_args: The command arguments.
    """
    args = parse_args(cli_args)

    sd = from_csv(args.input, dataset_id=args.collection,
                  metadata_file=args.metadata)

    output = args.pathname
    if output is None:
        output = args.input.replace('.csv', '.parquet')

    output.parent.mkdir(parents=True, exist_ok=True)

    sd.save(output, frequency=args.duration)

    sys.exit(0)
