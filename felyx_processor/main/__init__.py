import argparse
import logging
import sys
from typing import List, NoReturn, Tuple


logger = logging.getLogger('felyx.processor')


def exit_success(args: argparse.Namespace) -> NoReturn:
    # @TODO what is this exit flag for ?
    if hasattr(args, 'options') and args.options.exit_flag is not None:
        with open('{}.ok'.format(args.option.exit_flag), 'w'):
            pass

    logger.info('Done (success)')
    sys.exit(0)


def exit_failure(args: argparse.Namespace) -> NoReturn:
    if hasattr(args, 'options') and args.options.exit_flag is not None:
        with open('{}.nok'.format(args.options.exit_flag), 'w'):
            pass

    logger.error('Done (failure)')
    sys.exit(1)
