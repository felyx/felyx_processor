"""
"""
import argparse
import datetime
import glob
import logging
import os.path
import re
import sys
from pathlib import Path
from typing import List, Tuple

from dateutil.rrule import DAILY, rrule

import felyx_processor.utils.configuration as cfg
from felyx_processor.utils.arguments import parse_all_args
from felyx_processor.utils.logging import setup_logging
from felyx_processor.utils.parameters import CollectEOFilesParams, log_params

logger = logging.getLogger('felyx.eo_files')


def parse_args(cli_args: List[str]) -> \
        Tuple[cfg.FelyxSystemConfig, cfg.FelyxProcessorConfig, CollectEOFilesParams]:
    """Parse the command line arguments.

    Args:
      cli_args: The command line arguments.

    Returns:
      Tuple[FelyxSystemConfig, FelyxProcessorConfig, CollectEOFilesParams]:
      A tuple containing the felyx system configuration, the felyx processor
       configuration and the command parameters.

    Raises:
      FileNotFoundError: If configuration file does not exist.
    """

    parser = argparse.ArgumentParser()

    # mandatory args
    parser.add_argument(
        type=Path, dest='configuration_file',
        help=str(
            'A configuration file with the settings of the match-up dataset '
            'to generate'
        )
    )
    parser.add_argument(
        '--mmdb-ids', type=str, dest='mmdb_ids', nargs='+',
        help=str(
            'the list of identifiers, within the MMDB configuration file of '
            'the matchup dataset to generate'
        )
    )
    parser.add_argument(
        '--start', type=str,
        help='start date of the data interval')
    parser.add_argument(
        '--end', type=str,
        help='end date of the data interval')
    parser.add_argument(
        '--skip-siblings', action='store_true',
        help='skip sibling datasets')

    parser.add_argument(
        '--output', help=str(
            'The directory where the EO files lists are generated.'
            'The EO file list filename pattern is : <eo_dataset_id>.list.'
            'If not specified, the EO files list are printed on the standard output.'
        )
    )

    # logs and verbosity
    parser.add_argument(
        '--logfile', type=str, default=None,
        help='Path of the file where logs will be written')
    parser.add_argument(
        '--logfile-level', type=str, default='info',
        help='Minimal level that log messages must reach to be written in the '
             'log file')
    level_group = parser.add_mutually_exclusive_group()
    level_group.add_argument(
        '-v', '--verbose', action='store_true',
        help='Activate debug level logging - for extra feedback.')
    level_group.add_argument(
        '-q', '--quiet', action='store_true',
        help='Disable information logging - for reduced feedback.')
    level_group.add_argument(
        '-s', '--silence', action='store_true',
        help='Log ONLY the most critical or fatal errors.')

    args = parse_all_args(parser, cli_args)

    try:
        felyx_processor_config = cfg.load_felyx_processor_config(configuration_file=args.configuration_file)
        felyx_sys_config = cfg.load_felyx_sys_config()
        params = CollectEOFilesParams(**dict(
            mmdb_ids=args.mmdb_ids,
            start=args.start,
            end=args.end,
            output=args.output,
            skip_siblings=args.skip_siblings,
            log=log_params(args)
        ))

        return felyx_sys_config, felyx_processor_config, params
    except FileNotFoundError:
        raise FileNotFoundError('{0} configuration file does not exists'.format(
            args.configuration_file))
    except Exception as e:
        logger.exception(e)
        sys.exit(1)


def exit_success():
    """"""
    logger.info('Done (success)')
    sys.exit(0)


def exit_failure():
    """"""
    logger.error('Done (failure)')
    sys.exit(1)


def collect_input_files(dataset_id: str,
                        config: cfg.FelyxProcessorConfig,
                        start: datetime,
                        end: datetime) -> List[Path]:
    """
    Collect the files of an EO dataset.
    Args:
      dataset_id: The EO datatset id as specified in the Felyx configuration.
      config: The Felyx configuration.
      start: The files time interval start.
      end: The files time interval end.
    Returns:
        The list of EO files paths.
    """

    logger.info(
        f'Collect EO files for dataset {dataset_id} for time range from : {start.isoformat()} to : {end.isoformat()}')

    filenaming = config.EODatasets[dataset_id].filenaming

    # discard all time markers < day
    # TODO improve for files not stored by day
    filenaming = re.sub('\*+', '*', re.sub(r'%([HIMSfcx])', '*', filenaming))

    files = []

    # TODO improve for files not stored by day
    # add last file of previous day (to manage overlap over 00:00 UTC)
    prev_files = glob.glob(
        (start - datetime.timedelta(days=1)).strftime(filenaming))
    if len(prev_files) > 0:
        files.append(prev_files[-1])

    # get files over requested days
    for dt in rrule(
            DAILY, dtstart=start, until=end - datetime.timedelta(days=1)):
        logger.debug(f'search path for {dataset_id}: {dt.strftime(filenaming)}')
        files.extend(
            glob.glob(dt.strftime(filenaming))
        )

    files = list(set(files))
    files.sort()

    return files


def felyx_collect_eo_files(cli_args: List[str] = None):
    """
     Felyx_collect_eo_files command. This command is used for compute the EO files lists from
     the datasets used by the MMDBs specified in input.
     An EO files list is produced for each dataset and stored in text file or just written on
     stdout if no output directory is specified.
     Args:
       cli_args: The command arguments.
     """
    # get command line args
    felyx_sys_config, felyx_processor_config, args = parse_args(cli_args)

    # Setup logging
    if not setup_logging(logger, args.log):
        exit_failure()

    if args.output is not None and not os.path.isdir(args.output):
        logger.error(f'Output directory {args.output} does not exist')
        exit_failure()

    try:
        # skip datasets not part of the produced MMDBs or siblings of a parent
        # dataset
        datasets_to_process = []
        logger.info('Determining the list of datasets to collect')
        for mmdb_id in args.mmdb_ids:
            logger.info(f'Checking MMDB {mmdb_id}')
            if mmdb_id not in felyx_processor_config.MMDatasets:
                logger.error(f'MMDB {mmdb_id} is not configured in the given Felyx configuration file')
                exit_failure()
            for dataset_id in felyx_processor_config.MMDatasets[mmdb_id].eo_datasets:
                # skip dataset if it is a sibling of the reference dataset
                dst_config = felyx_processor_config.MMDatasets[mmdb_id].eo_datasets[dataset_id]
                if args.skip_siblings and \
                        dst_config.sibling is not None:
                    logger.info(f'Skipping sibling dataset {dataset_id}')
                    continue

                if dataset_id not in datasets_to_process:
                    datasets_to_process.append(dataset_id)

        # collect input files for dataset
        for dataset_id in datasets_to_process:
            collfiles = collect_input_files(
                dataset_id, felyx_processor_config, args.start, args.end)
            if len(collfiles) == 0:
                logger.warning(f'No input files found for {dataset_id}')
                continue
            # if output path, write the EO files list in a file
            if args.output is not None:
                with open(args.output / f'{dataset_id}.list', 'wt') as f:
                    f.write("\n".join(f'{str(eo_file)}' for eo_file in collfiles))
            else:
                for eo_file in collfiles:
                    print(f'{dataset_id} | {eo_file}')

    except Exception:
        # Error sink
        logger.error('An error occurred and could not be handled by felyx',
                     exc_info=sys.exc_info())
        exit_failure()

    exit_success()
