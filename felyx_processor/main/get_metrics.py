# -*- coding: utf-8 -*-

"""
@author <sylvain.herledan@oceandatalab.com>
@date 2019-01-10
Main program of Felyx felyx-get-metrics command. This command is used for
retrieving metrics data from Elasticsearch index.
"""

import argparse
from datetime import datetime
import json
import logging
import os
from pathlib import Path
import sys
from typing import List, NoReturn, Tuple, Union, Optional

from pydantic import field_validator

import felyx_processor.storage
import felyx_processor.utils.configuration
from felyx_processor.metrics.metrics_factory import MetricsDataSourceFactory
from felyx_processor.utils.arguments import get_override_args, parse_all_args
from felyx_processor.utils.configuration import (
    FelyxProcessorConfig,
    FelyxSystemConfig,
    load_felyx_processor_config,
    load_felyx_sys_config,
)
from felyx_processor.utils.logging import setup_logging
from felyx_processor.utils.parameters import FelyxModelParameters, log_params
from . import exit_success, exit_failure


logger = logging.getLogger('felyx.processor')


class GetMetricsParams(FelyxModelParameters):
    datasets: Union[List[str], str]
    sites: Optional[List[str]] = None
    metrics: Optional[List[str]] = None
    start: datetime
    end: datetime
    wkt: Optional[List[List[float]]] = None
    output_path: Optional[Path] = None

    @field_validator('sites', 'metrics', 'datasets', mode='before')
    @classmethod
    def complete_lists(cls, v: str) -> Optional[List[str]]:
        if v is None:
            return
        return [_ for x in v for _ in x.split(',')]

    @field_validator('wkt', mode='before')
    @classmethod
    def complete_wkt(cls, v: str) -> Optional[List[List[float]]]:
        if v is None:
            return
        return [[float(y) for y in x.split(',')] for x in v.split(' ')]


def parse_args(
        cli_args: List[str]
) -> Tuple[FelyxSystemConfig, FelyxProcessorConfig, GetMetricsParams]:
    """Parse the command line arguments.

         Args:
             cli_args: The command line arguments.

         Returns:
             Tuple[FelyxSystemConfig, FelyxProcessorConfig, GetMetricsParams]:
             A tuple containing the felyx system configuration, the felyx
             processor configuration and the command parameters.

         Raises:
             FileNotFoundError: If configuration file does not exist.
    """
    get_metrics_parser = argparse.ArgumentParser()
    get_metrics_parser.add_argument(
        '-c', '--configuration-file', type=str, required=True,
        help='Path of the configuration file')
    get_metrics_parser.add_argument(
        '--datasets', type=str, nargs='*', required=True,
        help='Dataset identifier or list of dataset identifiers to extract '
             'from the index')
    get_metrics_parser.add_argument(
        '--sites', type=str, nargs='*',
        help='Site identifier or list of site identifiers to extract '
             'from the index')
    get_metrics_parser.add_argument(
        '--metrics', type=str, nargs='*',
        help='Metric identifier or list of metric identifiers to extract '
             'from the index')
    get_metrics_parser.add_argument(
        '--start', type=str, required=True,
         help='Beginning of the time coverage (YYYY-mm-ddTHH:MM:SS format)')
    get_metrics_parser.add_argument(
        '--end', type=str, required=True,
        help='End of the time coverage (YYYY-mm-ddTHH:MM:SS format)')
    get_metrics_parser.add_argument(
        '--wkt', type=str, default=None,
        help='Restrict search to a polygon defined by the provided coordinates '
             '("lon1,lat1 ...  lonN,latN lon1,lat1")')
    get_metrics_parser.add_argument(
        '--output_path', type=str, default=None,
        help=('If provided, path of the file where the '
              'results will be saved in JSON format'))

    get_metrics_parser.add_argument(
        '--logfile', type=str, default=None,
        help='Path of the file where logs will be written')
    get_metrics_parser.add_argument(
        '--logfile_level', type=str, default=None,
        help='Minimal level that log messages must reach to be written '
             'in the log file')
    level_group = get_metrics_parser.add_mutually_exclusive_group()
    level_group.add_argument(
        '-v', '--verbose', action='store_true',
        help='Activate debug level logging - for extra feedback.')
    level_group.add_argument(
        '-q', '--quiet', action='store_true',
        help='Disable information logging - for reduced feedback.')
    level_group.add_argument(
        '-s', '--silence', action='store_true',
        help='Log ONLY the most critical or fatal errors. ')

    args = parse_all_args(get_metrics_parser, cli_args)

    felyx_processor_config = load_felyx_processor_config(
        configuration_file=args.configuration_file)
    felyx_sys_config = load_felyx_sys_config()

    params = GetMetricsParams(**dict(
        datasets=args.datasets,
        sites=args.sites,
        metrics=args.metrics,
        start=args.start,
        end=args.end,
        wkt=args.wkt,
        output_path=args.output_path,
        log=log_params(args)
    ))

    return felyx_sys_config, felyx_processor_config, params,


def default_converter(item_object):
    """
         Convert an item object of dictionary to JSON compatible format.

         Args:
             item_object: The object to convert.

         Returns:
             If the item is a datetime, a string to time iso format,
             a float otherwise.
    """
    if isinstance(item_object, datetime):
        return item_object.isoformat()
    else:
        return float(item_object)


def felyx_get_metrics(cli_args=None):
    """
        Felyx get_metrics command. This command is used for retrieving metrics
        data from Elasticsearch index.

        Args:
            cli_args: The command arguments.
    """
    # Setup logging
    main_logger = logging.getLogger()

    felyx_sys_config, felyx_processor_config, args = parse_args(cli_args)

    # Setup logging
    if not setup_logging(main_logger, args.log):
        exit_failure(args)

    # TODO: let users define constraints from the command line
    constraints = None

    ds = MetricsDataSourceFactory.get_metrics_datasource(
        felyx_sys_config=felyx_sys_config)
    results = ds.get_metrics(
        datasets=args.datasets,
        sites_codes=args.sites,
        start=args.start,
        stop=args.end,
        metrics=args.metrics,
        wkt_coords=args.wkt,
        constraints_list=constraints,
        as_dataframe=False
    )

    if results is None:
        logger.error('Failed to retrieve metrics from the index')
        exit_failure(args)

    if not results:
        logger.warning('There is no metrics data for these datasets : {} and \
          sites : {}.'.format(args.datasets, args.sites))
        exit_success(args)

    if args.output_path is not None:
        output_dir = os.path.dirname(args.output_path)
        ok = felyx_processor.storage.makedirs(output_dir)
        if not ok:
            logger.error(
                'Could not create output directory {}'.format(output_dir))
            exit_failure()

        with open(args.output_path, 'wt') as f:
            json.dump(results, f, default=default_converter)
    else:
        print(json.dumps(results, default=default_converter, indent=2))

    exit_success(args)
