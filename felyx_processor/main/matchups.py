# -*- encoding: utf-8 -*-

"""
felyx-assemble-matchups
=======================

A command line tool to assemble full match-up dataset files from the EO child
products and measurements associated with the dynamic extraction sites.

:copyright: Copyright 2015 Ifremer.
:license: Released under GPL v3 license, see :ref:`license`.
.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>

"""
from argparse import ArgumentParser
import datetime
import dateutil
from functools import partial
import logging
import sys
from pathlib import Path
from typing import Dict, List, Union, Optional, Tuple

import dateutil.parser as parser
import numpy as np
import pandas as pd
from pydantic import field_validator
import xarray as xr

import felyx_processor.utils.configuration as cfg
from felyx_processor.extraction import tailor
from felyx_processor.miniprod.generate import read_from_source
from felyx_processor.miniprod.miniprod_datasource import MiniprodDataSource
from felyx_processor.miniprod.miniprod_factory import MiniprodDataSourceFactory
from felyx_processor.sites.insitu_factory import InSituDataSourceFactory
from felyx_processor.utils.arguments import get_override_args, parse_all_args
from felyx_processor.utils.logging import setup_logging
from felyx_processor.utils.models import Indexing
from felyx_processor.utils.parameters import (
    FelyxModelParameters, DirectoryPath, log_params, add_log_args)


# variables that must be identical for paired matchups
INVARIANT_VARS = ['__site_id', '__site_name', '__site_time', '__site_lon',
                  '__site_lat']

# Felyx variables not to filter while building products
FELYX_VARS_TO_KEEP = ['__time_difference', '__distance', '__source',
                      '__source_center_index', '__source_slices'] +\
                     INVARIANT_VARS

logger = logging.getLogger('felyx.processor')



class MatchupsParams(FelyxModelParameters):
    matchup_dataset: str
    output_dir: DirectoryPath
    child_product_dir: Optional[DirectoryPath] = None
    from_manifests: bool = False
    extract_from_source: bool = False
    manifest_dir: Optional[DirectoryPath] = None
    start: datetime.datetime
    end: datetime.datetime
    dry_run: bool = False


def add_assembling_args(parser: ArgumentParser) -> ArgumentParser:

    parser.add_argument(
        '-o', '--output-dir', default='.', help=str(
            'The directory to write the output match-up files'
        )
    )
    parser.add_argument(
        '--from-manifests', action='store_true', default=False,
        help=str('collect the child products to assemble from the manifest '
                 'files created by `felyx-extraction`. Avoids querying an '
                 'index engine such as Elasticsearch.')
    )
    parser.add_argument(
        '--extract-from-source', action='store_true', default=False,
        help=str('Extract the child products to assemble directly from the EO '
                 'source data files. Prevents from saving the child products '
                 'to disk in `felyx-extraction`.')
    )
    parser.add_argument('--start', type=str,
                                help='start date (YYYY-mm-ddTHH:MM:SS format)')
    parser.add_argument('--end', type=str,
                                help='end date (YYYY-mm-ddTHH:MM:SS format)')

    parser.add_argument('--dry-run', action='store_true', default=False)

    return parser


def args_as_dict(args, felyx_sys_config):
    return dict(
        matchup_dataset=args.matchup_dataset,
        output_dir=args.output_dir,
        child_product_dir=vars(args).get(
            'child_product_dir',
            felyx_sys_config.default_felyx_miniprod_store),
        from_manifests=args.from_manifests,
        extract_from_source=args.extract_from_source,
        manifest_dir=vars(args).get(
            'manifest_dir',
            felyx_sys_config.default_felyx_manifest_store),
        start=args.start,
        end=args.end,
        dry_run=args.dry_run,
        log=log_params(args))


def parse_args(
        cli_args: List[str]
) -> Tuple[cfg.FelyxSystemConfig, cfg.FelyxProcessorConfig, MatchupsParams]:
    """"""
    matchup_parser = ArgumentParser(
        description=str(
            'assemble full match-up dataset files from the EO child products '
            'and measurements associated with the dynamic extraction sites')
    )

    matchup_parser.add_argument(
        '-c', '--configuration-file', type=Path,
        help=str(
            'A configuration file with the settings of the match-up dataset '
            'to assemble'
            )
        )
    matchup_parser.add_argument(
        '--matchup-dataset', help=str(
            'The identifier of the of the match-up dataset to assemble, '
            'as defined in the settings configuration file'
        )
    )
    matchup_parser.add_argument(
        '--child-product-dir', help=str(
            'The directory where child products are stored (uses the value in '
            'felyx system configuration file by default). Only used if '
            'assembling the match-ups from pre-extracted child products.'
        )
    )
    matchup_parser.add_argument(
        '--manifest-dir', help=str(
            'The directory where manifests are stored (uses the value in '
            'felyx system configuration file by default). Only used if '
            'assembling the match-ups from manifest'
        )
    )

    matchup_parser = add_assembling_args(matchup_parser)
    matchup_parser = add_log_args(matchup_parser)

    args = parse_all_args(matchup_parser, cli_args)

    felyx_processor_config = cfg.load_felyx_processor_config(
        configuration_file=args.configuration_file)
    felyx_sys_config = cfg.load_felyx_sys_config()

    params = MatchupsParams(**dict(
        matchup_dataset=args.matchup_dataset,
        output_dir=args.output_dir,
        child_product_dir=args.child_product_dir,
        from_manifests=args.from_manifests,
        extract_from_source=args.extract_from_source,
        manifest_dir=args.manifest_dir,
        start=args.start,
        end=args.end,
        dry_run=args.dry_run,
        log=log_params(args)))

    return felyx_sys_config, felyx_processor_config, params



# temporay
def get_miniprods(dataset, site_collection, start, end, miniprod_path):
    miniprod_path = miniprod_path.joinpath(site_collection, '*', dataset)
    print(miniprod_path)

    miniprods = []
    date = start
    import glob
    while date.date() <= end.date():
        search_path = miniprod_path.joinpath(
            str(date.year), date.strftime('%j'))
        logging.debug('Search path: {}'.format(search_path))
        miniprods.extend(glob.glob(str(search_path / '*.nc')))

        date += datetime.timedelta(days=1)

    # filter on file date
    miniprods = [
        _ for _ in miniprods if start < parser.parse(Path(_).name[:14]) < end
    ]

    return miniprods


def preprocess(
        ds: xr.Dataset,
        selection_config: cfg.ContentSelectionConfig) -> xr.Dataset:

    # apply variable and attribute selection
    ds = tailor(ds, selection_config)

    # remove indexes as child products will be stacked along new axis
    ds = ds.reset_index(list(ds.indexes))

    # variables (like 'lat') that have more than 1-dimension and the same
    # name as one of its dimensions ('obs', 'lat'). xarray disallows such
    # variables because they conflict with the coordinates used to label
    # dimensions (which happens next when stacking the child products)
    for d in ds.dims:
        if d in ds.variables and d in ds[d].dims:
            ds = ds.rename_dims({d: '_' + d})

    # add felyx attributes as variables
    ds['__site_id'] = xr.DataArray(
        data=str(ds.attrs.pop('__site_id')),
        attrs={
            'long_name': 'identifier of the site matched with the observation'
        }
    )
    ds['__site_name'] = xr.DataArray(
        data=str(ds.attrs.pop('__site_name')),
        attrs={
            'long_name': 'full name of the site matched with the observation'
        }
    )
    ds['__site_time'] = xr.DataArray(
        data=np.datetime64(
            parser.parse(ds.attrs.pop('__site_time')).isoformat()),
        attrs={
            'standard_name': 'time',
            'long_name': 'time of the nearest site data'
        }
    )
    ds['__site_lon'] = xr.DataArray(
        data=float(ds.attrs.pop('__site_lon')),
        attrs={
            'standard_name': 'longitude',
            'long_name': 'longitude of the nearest site data'
        }
    )
    ds['__site_lon'].encoding['_FillValue'] = 1e20
    ds['__site_lat'] = xr.DataArray(
        data=float(ds.attrs.pop('__site_lat')),
        attrs={
            'standard_name': 'latitude',
            'long_name': 'latitude of the nearest site data'
        }
    )
    ds['__site_lat'].encoding['_FillValue'] = 1e20
    ds['__time_difference'] = xr.DataArray(
        data=float(ds.attrs.pop('__time_difference')),
        attrs={
            'long_name': 'time difference between observation and the nearest '
                         'data of the matched site',
            'units': 's'
        }
    )
    ds['__time_difference'].encoding['_FillValue'] = 1e20
    ds['__distance'] = xr.DataArray(
        data=float(ds.attrs.pop('__distance')),
        attrs={
            'long_name': 'distance between observation and the nearest '
                         'data of the matched site',
            'units': 'm'
        },

    )
    ds['__distance'].encoding['_FillValue'] = 1e20
    ds['__source'] = xr.DataArray(
        data=str(ds.attrs.pop('__source')),
        attrs={
            'long_name': 'source file the observation was extracted from'
        }
    )
    ds['__source_center_index'] = xr.DataArray(
        data=str(ds.attrs.pop('__source_center_index')),
        attrs={
            'long_name': 'indices of the observation within the source file '
                         'it was extracted from'
        }
    )
    ds['__source_slices'] = xr.DataArray(
        data=str(ds.attrs.pop('__source_slices')),
        attrs={
            'long_name': 'slices of the data subset within the source file '
                         'it was extracted from'
        }
    )

    return ds


def reference(mdb_config):
    if len(mdb_config.eo_datasets) == 1:
        dst_ref = next(iter(mdb_config.eo_datasets.keys()))
    else:
        dst_ref = [k for k, v in mdb_config.eo_datasets.items() if v.reference]
        if len(dst_ref) == 0:
            raise ValueError(
                'There must be one dataset in EODatasets defined as the  '
                'reference dataset')
        elif len(dst_ref) > 1:
            raise ValueError(
                'There must be only one dataset in EODatasets defined as the  '
                'reference dataset')
        else:
            dst_ref = dst_ref[0]

    return dst_ref


def remove_duplicate_obs(
        dst: xr.Dataset, time_tolerance: int = 300) -> xr.Dataset:
    """Remove the duplicate matchups, i.e. having the same (site_id,
    site_time) anchor key.

    Keep the closest in time, unless they are within `time_tolerance` (in
    seconds) in which case distance is prefered.
    """
    # remove duplicate keys
    uniques, uniq_idx, counts = np.unique(
        dst['obs'], return_index=True, return_counts=True)

    if counts.max() > 1:
        dropped = []
        duplicates = uniques[counts >= 2]
        for key in duplicates:
            # keep the closest match-up in time
            time_diff = dst.__time_difference.where(dst.obs == key)
            closest = int(time_diff.argmin(skipna=True))

            if np.fabs(time_diff - time_diff[closest]).max() < 300:
                # all duplicates have more or less the same time,
                # use distance instead
                distance = dst.__distance.where(dst.obs == key)
                closest = int(distance.argmin(skipna=True))

            indices = set(np.where(~np.isnan(time_diff))[0]) - {closest}
            dropped.extend(list(indices))

        all_indices = np.arange(dst.sizes['obs']).tolist()
        dst = dst.isel(obs=[x for x in all_indices if x not in dropped])

    return dst


def mmdb_tailor(dst: xr.Dataset,
                content_cfg: cfg.ContentSelectionConfig,
                dataset_id: str
                ) -> xr.Dataset:
    """Returns a tailored stack of EO child products from a given EO dataset.

    Only the requested variables and attributes are returned (from `variables`
    and `except_variables` configuration settings.

    Returned variables and attributes are possibly prefixed (`prefix` setting).
    """
    # @TODO put somewhere else ====================

    # remove invariant variables
    dst = dst.drop_vars(INVARIANT_VARS)

    # remove felyx attributes not converted to variables
    felyx_attrs = [f'__{_}' for _ in [
        'source_time_coverage_start', 'source_time_coverage_end',
        'time_coverage_start', 'time_coverage_end',
        'date_created', 'date_modified',
        'geospatial_lon_min', 'geospatial_lon_max',
        'geospatial_lat_min', 'geospatial_lat_max',
        'summary', 'site_geometry', 'site_collection_name',
        'id', 'title', 'history', 'percentage_coverage_of_site_by_miniprod']]

    for attr in felyx_attrs:
        if attr in dst.attrs:
            dst.attrs.pop(attr)
    # ========================

    # global attributes have already been stacked as variables
    m_cfg = content_cfg.model_copy()
    m_cfg.variables.extend(m_cfg.attributes_as_variables)
    m_cfg.attributes_as_variables = []

    # Add to variable selection the Felyx variables to keep in products
    m_cfg.variables.extend(FELYX_VARS_TO_KEEP)

    return tailor(dst, m_cfg, prefix=f'{dataset_id}_')


def save(
        mmdb: xr.Dataset,
        time_units: str,
        filenaming: str,
        output_dir: Path = Path()):
    """Save a MMDB product.

    Format the output match-up dataset, remove dummy dims and restore in
    situ data type and attributes (removed in merge operations).
    """
    # clean temporary variables used for jointures
    mmdb = mmdb.rename(
        {'__site_time': 'time', '__site_lon': 'lon', '__site_lat':  'lat'}
    ).drop_vars(['obs', 'site_obs'])

    # set site as main coordinates
    mmdb = mmdb.reset_coords().set_coords(['lat', 'lon', 'time'])

    # formatting rules for variables
    for v in mmdb.variables:
        if np.issubdtype(mmdb[v].dtype, np.datetime64):
            # fixes an issue with xarray
            mmdb[v] = mmdb[v].astype('datetime64[ns]')

            # set consistent time units
            mmdb[v].encoding['units'] = time_units
            mmdb[v].encoding['dtype'] = 'float64'
            mmdb[v].encoding['_FillValue'] = 0

        elif np.issubdtype(mmdb[v].dtype, str) or \
                np.issubdtype(mmdb[v].dtype, object):
            # xarray/NetCDF4 does not accept _FillValue for strings with
            # variable length
            if '_FillValue' in mmdb[v].encoding:
                mmdb[v].encoding.pop('_FillValue')

    # sort attributes
    mmdb.attrs = dict(sorted(mmdb.attrs.items()))

    # check attribute types before writing
    for attr, value in mmdb.attrs.items():
        if isinstance(value, datetime.datetime):
            mmdb.attrs[attr] = value.isoformat()

    # save
    outfname = mdb_filename(mmdb, filenaming, output_dir)
    logger.info(
        f'Saving MMDB product {mmdb.attrs["__id"]}:'
        f' {outfname}')
    outfname.parent.mkdir(parents=True, exist_ok=True)

    mmdb.to_netcdf(outfname, engine='h5netcdf')


def join_eo_datasets(
        mdf: xr.Dataset,
        matchups_dst: Dict[str, xr.Dataset],
        product_config: cfg.ProductConfig):
    """Performs the jointure between the multiple datasets"""
    attrs = mdf.attrs

    # filter variables and attributes wrt configuration
    for dst_id in matchups_dst:
        if dst_id not in product_config.content:
            logging.info(f'{dst_id} has no entry in MMDB product '
                         f'configuration so no variables or attributes from '
                         f'this dataset will be added in the output MMDB files')
    matchups_dst = {
        dst_id: mmdb_tailor(xarr, product_config.content[dst_id], dst_id)
        for dst_id, xarr in matchups_dst.items()
        if dst_id in product_config.content and xarr is not None
    }

    # reset coords to avoid xarray MergeError
    for k, v in matchups_dst.items():
        matchups_dst[k] = v.reset_coords()

    # join on (__site_id,__site_time) key
    joined_mdf = xr.merge(
        [mdf, *(list(matchups_dst.values()))], join='left', compat='override')

    # xarray merge discards attributes: copy them back
    for dst, xarr in matchups_dst.items():
        joined_mdf.attrs.update(xarr.attrs)
    joined_mdf.attrs.update(attrs)

    return joined_mdf


def build_products(
        mdf: xr.Dataset,
        matchups_dst: Dict[str, xr.Dataset],
        mdb_config: cfg.MDatasetConfig,
        output_dir: Path = Path()):
    """
    Join and save the EO and in situ data into one or more subproducts
    """
    time_units = mdb_config.output.default_time_units

    # @TODO add in situ data from source files
    for sub_id, sub_cfg in mdb_config.output.products.items():

        # join eo datasets specified in subproduct definition
        eo_datasets = list(sub_cfg.content.keys())
        if len(eo_datasets) == 0:
            raise ValueError(
                'At least one dataset must be defined in the `products` '
                'section')

        product = join_eo_datasets(mdf, matchups_dst, sub_cfg)

        save(product,
             filenaming=sub_cfg.filenaming,
             time_units=time_units,
             output_dir=output_dir)


def mdb_filename(
        mdb: xr.Dataset,
        pattern: Union[str, None],
        output_dir: Path = Path()):
    """Return the filename of a mdb product"""
    start = parser.parse(mdb.attrs['__time_coverage_start'])
    end = parser.parse(mdb.attrs['__time_coverage_end'])

    if pattern is None:
        return output_dir / f'{start.strftime("%Y%m%dT%H%M%S")}_' \
               f'{end.strftime("%Y%m%dT%H%M%S")}_{mdb.attrs["id"]}.nc'
    else:
        return output_dir / start.strftime(pattern)


def add_site_data(
        felyx_sys_config: cfg.FelyxSystemConfig,
        felyx_processor_config: cfg.FelyxProcessorConfig,
        site_collection: str,
        mdb: xr.Dataset,
        mdb_config: cfg.MDatasetConfig):
    """Collect the site data in time window around each found match-up.

        Args:
            felyx_sys_config: The Felyx system configuration
            felyx_processor_config: The Felyx configuration
            site_collection: The site collection name
            mdb: The mdb dataset
            mdb_config: The mdb configuration
        Returns:
            The mdb
    """
    # **** @TODO TEMPORARY ***
    site_data_source = InSituDataSourceFactory.get_insitu_datasource(
        felyx_sys_config=felyx_sys_config,
        felyx_processor_config=felyx_processor_config,
        collection_code=site_collection
    )
    # ******************

    site_data = site_data_source.get_raws_in_window(
        mdb_config.site_collection_id,
        mdb.__site_id.values,
        pd.to_datetime(mdb['__site_time'].values),
        time_window=np.timedelta64(mdb_config.output.history, 'm'),
        prefix=mdb_config.site_collection_id,
        as_dataset=True
    )

    # join with EO child product dataframe (on 'obs' index)
    site_data['obs'] = mdb['obs']
    site_metadata = site_data.attrs
    mdb = mdb.merge(site_data)
    mdb.attrs.update(site_metadata)

    # remove redundant variables
    mdb = mdb.drop_vars([f'{mdb_config.site_collection_id}_id'])

    # add indice of match-up nearest site data in each time series of site data
    if mdb_config.output.history not in [0, None]:
        nearest = np.ma.abs(
            np.ma.fix_invalid(
                mdb.__site_time - mdb[f'{mdb_config.site_collection_id}_time'])
        ).argmin(axis=1)
        mdb['__site_matchup_indice'] = xr.DataArray(
            data=nearest.astype(np.int32),
            dims=('obs',),
            attrs={
                'long_name': 'indice of the matched site data with the '
                             'observation, along site_obs axis'
            }
        )

    return mdb


def __get_attr_to_var(mdb_config: cfg.MDBOutputConfig, dst_id: str):
    """collect the list of child product attributes to convert to variables"""
    attrs = []
    for product, prod_config in mdb_config.products.items():
        if dst_id in prod_config.content:
            attrs.extend(prod_config.content[dst_id].attributes_as_variables)
    return attrs


def matchups_from_manifests(dataset, site_collection, start, end, manifest_dir):
    # @TODO TEMPORARY
    pass


def stack_from_source(
        felyx_config: cfg.FelyxProcessorConfig,
        selection_config: cfg.ContentSelectionConfig,
        felyx_sys_config: cfg.FelyxProcessorError,
        matchups: pd.DataFrame,
        path_pattern: str
):
    """Extract child products directly from source file using the indices
    of the match-ups found by felyx-extraction.
    """
    # extract data subsets from the list of found extractions
    subsets = read_from_source(
        felyx_config=felyx_config,
        felyx_sys_config=felyx_sys_config,
        matchups=MiniprodDataSource.as_extractions(matchups),
        path_pattern=path_pattern)

    if len(subsets) == 0:
        return

    # preprocess and stack
    dst_stack = xr.concat(
        [preprocess(_[0].ds, selection_config=selection_config)
         for _ in subsets], dim='obs')

    return dst_stack


def collect_matchups(
        config: cfg.MDatasetConfig,
        eo_data_path: Dict[str, str],
        miniprod_index: MiniprodDataSource,
        site_collection: str,
        start: datetime.datetime = None,
        end: datetime.datetime = None,
        sibling_optimization: bool = False) -> Dict[str, pd.DataFrame]:
    """Return the list of match-ups collected from the child product indexes.

    """
    # build a dictionary of the matchups to assemble
    # sort out the datasets from which matchups will be inferred from a
    # sibling dataset
    datasets = []
    datasets_from_sibling = []
    if sibling_optimization:
        for dst_id in config.eo_datasets.keys():
            if config.eo_datasets[dst_id].sibling is None:
                datasets.append(dst_id)
            else:
                datasets_from_sibling.append(dst_id)
    else:
        datasets = list(config.eo_datasets.keys())

    # check configuration validity
    for dst_id in datasets_from_sibling:
        if config.eo_datasets[dst_id].sibling.identifier not in datasets:
            raise ValueError(
                f'the dataset {dst_id} refers to a sibling dataset '
                f'{config.eo_datasets[dst_id].sibling.identifier} which is '
                f'not defined in the configuration file'
            )

    # first collect the matchups from the datasets for which they were
    # extracted
    matchups = {
        dst: miniprod_index.get_miniprods(
            datasets=[dst],
            site_collection=site_collection,
            start=start,
            stop=end,
            as_dataframe=True) for dst in datasets}

    # then infer for the remaining datasets (attached to a sibling)
    if len(datasets_from_sibling) == 0:
        return matchups

    for dst in datasets_from_sibling:
        if dst not in config.eo_datasets:
            raise ValueError(f'Missing dataset {dst} in config.eo_datasets')
        if dst not in eo_data_path:
            raise ValueError(f'Missing path for dataset {dst}')

    matchups.update({
        dst: miniprod_index.miniprods_from_sibling(
            dataset_id=dst,
            sibling_miniprods=matchups[
                config.eo_datasets[dst].sibling.identifier],
            scaling=config.eo_datasets[dst].sibling.scaling,
            file_pattern=eo_data_path[dst],
            as_dataframe=True)
        for dst in datasets_from_sibling})

    return matchups


def build(mmdb_id: str,
          felyx_config: cfg.FelyxProcessorConfig,
          felyx_sys_config: cfg.FelyxSystemConfig,
          start: datetime.datetime = None,
          end: datetime.datetime = None,
          from_manifests: bool = False,
          extract_from_source: bool = False,
          manifest_dir: Path = None,
          output_dir: Path = Path(),
          add_dynamic_data=True,
          child_product_dir: Path = None,
          eo_data_path: Dict[str, str] = None,
          ):
    """build a complete match-up file

    Args:
        mmdb_id: The mmdb identifier
        felyx_config: The Felyx configuration
        felyx_sys_config: The Felyx system configuration
        start (datetime): start date/time of search interval
        end (datetime): end date/time of search interval
        from_manifests: Option for building from manifests
        manifest_dir: Root path of the manifest files
        output_dir: Path of the output file
        child_product_dir: Path of the child product
        datasets (list<str>): the list of additional datasets matched with the
            reference dataset (for multi-dataset matchups or cross-overs)
        add_dynamic_data (bool): add the data attached to the dynamic sites over
            which the miniprods were extracted (these are the in situ
            measurements from drifters or ships defined as dynamic sites for
            instance)
        extract_from_source: extract the child products from the source EO
            files instead of the extracted child products (in case the found
            child products matching a collection of site were not saved to
            disk). This allows to save disk space when not needing the
            individual child products.
        eo_data_path: a dictionary where keys are EO dataset identifiers and
            values the corresponding path pattern to its data files. Used
            only if `extract_from_source` is set.
    """
    mdb_config = felyx_config.MMDatasets[mmdb_id]

    site_collection = mdb_config.site_collection_id

    # reference dataset
    ref_dataset = reference(mdb_config)

    # build a dictionary of the child products to assemble where keys are the
    # dataset identifiers and values the related child products
    if from_manifests:
        # retrieves the list of child products to assemble from extraction
        # manifest files
        miniprod_index = MiniprodDataSourceFactory.get_miniprod_datasource(
                            felyx_sys_config=felyx_sys_config,
                            source=Indexing.manifest,
                            manifest_root_dir=manifest_dir)
    else:
        # retrieves the list of child products to assemble from the
        # Elasticsearch index
        miniprod_index = MiniprodDataSourceFactory.get_miniprod_datasource(
            felyx_sys_config=felyx_sys_config,
            miniprod_root_dir=child_product_dir)

    matchups = collect_matchups(
        config=mdb_config,
        eo_data_path=eo_data_path,
        miniprod_index=miniprod_index,
        site_collection=site_collection,
        start=start,
        end=end)
    datasets = matchups.keys()

    # checks
    if len(matchups[ref_dataset]) == 0:
        logger.warning(f'No match-ups collected for reference dataset'
                       f' {ref_dataset} in [{start}, {end}] interval. No MDB '
                       f'file can be created for this time interval.')
        return
    for dst in datasets:
        if len(matchups[dst]) == 0:
            logger.warning(f'No match-ups collected for {dst}')

    # concatenate match-ups
    matchups_dst = {}
    for dst_id in datasets:
        if len(matchups[dst_id]) == 0:
            continue

        if extract_from_source:
            # variable and attribute selection before stacking the
            # extracted dataset into a unique Dataset
            selection_config = mdb_config.eo_datasets[dst_id].content
            # converts global attributes that need to be stacked to variables.
            selection_config.attributes_as_variables = list(
                set(selection_config.attributes_as_variables).union(
                    set(__get_attr_to_var(mdb_config.output, dst_id))))

            # extract the child products from the source EO files and stack them
            matchups_dst[dst_id] = stack_from_source(
                felyx_config=felyx_config,
                selection_config=selection_config,
                felyx_sys_config=felyx_sys_config,
                matchups=matchups[dst_id],
                path_pattern=eo_data_path[dst_id])

        else:
            # global for the preprocessing by xarray of the netcdf files before
            # stacking them into a unique Dataset => converts global
            # attributes that need to be stacked to variables.
            selection_config = cfg.ContentSelectionConfig(
                attributes_as_variables=__get_attr_to_var(
                    mdb_config.output,
                    dst_id))
            partial_func = partial(
                preprocess, selection_config=selection_config)

            # read the child products saved on disk and stack them
            child_product_list = matchups[dst_id].path.values.tolist()
            matchups_dst[dst_id] = xr.open_mfdataset(
                child_product_list,
                parallel=False,
                combine='nested',
                concat_dim='obs',
                preprocess=partial_func)

        if matchups_dst[dst_id] is None:
            continue

        if matchups_dst[dst_id] is None:
            continue

        # create key for jointure
        matchups_dst[dst_id]['obs'] = xr.DataArray(
            data=matchups_dst[dst_id].__site_id.str.cat(
                matchups_dst[dst_id].__site_time, sep='_'),
            dims=('obs',))

        # remove duplicates
        matchups_dst[dst_id] = remove_duplicate_obs(matchups_dst[dst_id])

    # init mdb with reference dataset's sites
    if matchups_dst[ref_dataset] is None:
        logger.warning(
            f'No matchups wre found for the reference dataset {ref_dataset}. '
            'No matchup file will be produced')
        return

    mdb = matchups_dst[ref_dataset][[*INVARIANT_VARS, 'obs']]
    mdb.attrs = {}

    # collect the dynamic data in time window around each found match-up
    if add_dynamic_data:
        mdb = add_site_data(felyx_processor_config=felyx_config,
                            felyx_sys_config=felyx_sys_config,
                            site_collection=site_collection,
                            mdb=mdb, mdb_config=mdb_config)

    # set attributes
    mdb.attrs['__time_coverage_start'] = start.isoformat()
    mdb.attrs['__time_coverage_end'] = end.isoformat()
    mdb.attrs['__id'] = mmdb_id

    # format and save as one or multiple files
    build_products(mdb, matchups_dst, mdb_config, output_dir=output_dir)


def _eo_data_path(
        config: cfg.FelyxProcessorConfig,
        mmdataset: str,
        from_source: bool):
    if not from_source:
        return

    eo_data_path = {}
    for eo_dst in config.MMDatasets[mmdataset].eo_datasets:
        try:
            eo_data_path[eo_dst] = config.EODatasets[eo_dst].filenaming
        except (KeyError, ValueError):
            msg = (
                f'The path to the EO data files for dataset {eo_dst} in '
                f'multi-matchup dataset {mmdataset} is not defined ('
                f'`filenaming` field). It is required for extracting the '
                f'match-ups from the EO data files during the assembly '
                f'process.')
            logger.error(msg)
            raise cfg.ConfigException(msg)

    return eo_data_path


def assemble_matchups(cli_args: List[str] = None):
    """"""
    # Setup logging
    main_logger = logging.getLogger()

    felyx_sys_config, felyx_processor_config, args = parse_args(cli_args)

    # Setup logging
    if not setup_logging(main_logger, args.log):
        logger.error('Done (failure)')
        sys.exit(1)

    # prepare configuration for the collation of the result
    config = felyx_processor_config
    if config.MMDatasets is None:
        raise ValueError(
            'The MDB Dataset is not defined in your configuration file')
    mdb_config = config.MMDatasets[args.matchup_dataset]

    # time selection
    try:
        start = None
        if args.start:
            start = parser.parse(str(args.start))
        end = None
        if args.end:
            end = parser.parse(str(args.end))
    except ValueError:
        _, e, _ = sys.exc_info()
        logger.error(str(e))
        sys.exit(1)

    # path for pre-extracted child products
    child_product_dir = args.child_product_dir
    if child_product_dir is None:
        child_product_dir = \
            Path(felyx_sys_config.default_felyx_miniprod_store).joinpath(
                felyx_sys_config.default_felyx_user)

    # break down extraction period into regular intervals
    frequency = f'{mdb_config.output.frequency}'
    start = pd.date_range(end=start, periods=1, freq=frequency)[0]
    stop = pd.date_range(start=end, periods=1, freq=frequency)[0]
    intervals = pd.date_range(
        start=min(start, stop), end=max(stop, start), freq=frequency)

    logging.debug('Match-up files coverages: {}'.format(intervals))

    # produces an assembled file over each covered interval
    path_patterns = _eo_data_path(
        config, args.matchup_dataset, args.extract_from_source)

    for i in range(len(intervals)-1):
        try:
            build(args.matchup_dataset,
                  config,
                  felyx_sys_config,
                  intervals[i],
                  intervals[i+1],
                  output_dir=args.output_dir,
                  child_product_dir=child_product_dir,
                  from_manifests=args.from_manifests,
                  manifest_dir=args.manifest_dir,
                  extract_from_source=args.extract_from_source,
                  eo_data_path=path_patterns
                  )
        except cfg.ConfigException as e:
            raise SystemExit(e)

    sys.exit(0)
