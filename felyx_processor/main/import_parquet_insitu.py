# -*- encoding: utf-8 -*-
"""
Main program of Felyx felyx_import_parquet_insitu command.
This command is used for importing insitu data from parquet files into
Elasticsearch raw collection index"""

import argparse
import logging
import os
import sys
from typing import List, NoReturn, Tuple

from pandas import DataFrame

from felyx_processor.sites import insitu_datasource
from felyx_processor.sites.es_insitu_datasource import ESInSituDataSource
from felyx_processor.sites.insitu_datasource import LOCATION_COLUMNS, Z_AXIS
from felyx_processor.sites.insitu_factory import InSituDataSourceFactory
from felyx_processor.sites.parquet_insitu_datasource import ParquetInSituDataSource
from felyx_processor.utils.arguments import get_override_args, parse_all_args
from felyx_processor.utils.configuration import (
    FelyxProcessorConfig,
    FelyxSystemConfig,
    load_felyx_processor_config,
    load_felyx_sys_config,
)
from felyx_processor.utils.exceptions import FelyxProcessorError
from felyx_processor.utils.logging import setup_logging
from felyx_processor.utils.parameters import (
    FelyxModelParameters, InputFiles, log_params)
from . import exit_success, exit_failure


logger = logging.getLogger('felyx.processor')


class ImportParquetInSituParams(FelyxModelParameters):
    input: InputFiles
    collection: str


def parse_args(
        cli_args: List[str]
) -> Tuple[FelyxSystemConfig, FelyxProcessorConfig, ImportParquetInSituParams]:
    """Parse the command line arguments.

    Args:
        cli_args: The command line arguments.

    Returns:
        Tuple[FelyxSystemConfig, FelyxProcessorConfig,
                ImportParquetInSituParams]:
        A tuple containing the felyx system configuration, the felyx processor
        configuration and the command parameters.

    Raises:
    FileNotFoundError: If configuration file does not exist.
    """
    import_parser = argparse.ArgumentParser()
    import_parser.add_argument(
        '-c', '--configuration-file', type=str, default=None,
        help='Path of the configuration file')
    import_parser.add_argument(
        '--collection', type=str,
        help='Site collection identifier')
    import_parser.add_argument(
        '--inputs', type=str, default=None, nargs='*', required=True,
        help='The path of the input file or a list of the paths of '
             'the input files')
    import_parser.add_argument(
        '--from_list', type=str, default=None,
        help='The path of a file containing one input path per line')
    import_parser.add_argument(
        '--logfile', type=str, default=None,
        help='Path of the file where logs will be written')
    import_parser.add_argument(
        '--logfile_level', type=str, default=None,
        help='Minimal level that log messages must reach to be written '
             'in the log file')
    level_group = import_parser.add_mutually_exclusive_group()
    level_group.add_argument(
        '-v', '--verbose', action='store_true',
        help='Activate debug level logging - for extra feedback.')
    level_group.add_argument(
        '-q', '--quiet', action='store_true',
        help='Disable information logging - for reduced feedback.')
    level_group.add_argument(
        '-s', '--silence', action='store_true',
        help='Log ONLY the most critical or fatal errors. ')

    args = parse_all_args(import_parser, cli_args)

    felyx_processor_config = load_felyx_processor_config(
        configuration_file=args.configuration_file)
    felyx_sys_config = load_felyx_sys_config()

    params = ImportParquetInSituParams(**dict(
        collection=args.collection,
        input=dict(
            inputs=[_ for x in args.inputs for _ in x.split(',')],
            from_list=args.from_list),
        log=log_params(args)
    ))

    return felyx_sys_config, felyx_processor_config, params,


def felyx_import_parquet_insitu(cli_args=None):
    """
      Felyx command for importing insitu data from parquet files into
      Elasticsearch raw collection index

      Args:
          cli_args: The command arguments.
    """
    main_logger = logging.getLogger()

    felyx_sys_config, felyx_processor_config, args = parse_args(cli_args)

    # Setup logging
    if not setup_logging(main_logger, args.log):
        exit_failure(args)

    inputs = args.input.get_file_list()
    if 0 >= len(inputs):
        logger.error('No input file to process!')
        exit_failure(args)

    _msg = '{} input files to process'
    logger.debug(_msg.format(len(inputs)))

    # Retrieve ElasticSearch datasource
    es_insitu_ds = InSituDataSourceFactory.get_insitu_datasource(
        felyx_sys_config=felyx_sys_config,
        felyx_processor_config=felyx_processor_config,
        collection_code=args.collection
    )
    if not isinstance(es_insitu_ds, ESInSituDataSource):
        msg = \
            'The collection must be configured as ElasticSearch datasource : ' \
            '{}'.format(args.collection)
        logger.error(msg)
        exit_failure(args)

    # For each file to process
    insitu_ds = None
    raws_df = DataFrame()
    check_index = False

    for input_path in inputs:

        logger.info('Processing file : {}'.format(input_path))
        if not os.path.exists(input_path):
            logger.warning('Skipped input "{}": not found'.format(input_path))
            continue

        # Read the parquet insitu data
        if insitu_ds is None:
            insitu_ds = ParquetInSituDataSource(filename=input_path)
        else:
            insitu_ds.set_filename(input_path)
        try:
            raws_df = insitu_ds.get_raws(collection_code=args.collection,
                                      sites_codes=None,
                                      start=None,
                                      stop=None,
                                      as_dataframe=True)
            # If data have z dimension, keep only first level
            index = raws_df.index.names
            if Z_AXIS in index:
                raws_df = raws_df.xs(0, level=Z_AXIS, drop_level=False)
                raws_df = raws_df.reset_index()
                raws_df.drop([Z_AXIS], axis=1, inplace=True)
            else:
                raws_df = raws_df.reset_index()
            # Check the mandatory fields for Elasticsearch indexing
            if not all(item in list(raws_df.columns)
                        for item in LOCATION_COLUMNS):
                _msg = 'Skipped input: "{}" missing mandatory fields ({})'
                logger.warning(_msg.format(input_path, LOCATION_COLUMNS))
                continue

            if 0 >= len(raws_df):
                _msg = 'Skipped input: "{}" no data to read in the file'
                logger.warning(_msg.format(input_path))
                continue

        except FelyxProcessorError:
            _msg = 'Error when reading input: "{}"'
            logger.error(_msg.format(input_path))
            exit_failure(args)

        # For each site of the file, register the insitu data into
        # Elastic Search collection raw index
        for site_code, raws_subdf in raws_df.groupby(
                insitu_datasource.SITE_COLUMN):
            try:
                # Create if needed the collection raw index
                if check_index is False:
                    es_insitu_ds.create_raws_index(args.collection)
                    check_index = True

                # Register the data
                es_insitu_ds.register_raws(args.collection, site_code,
                                           raws_subdf,
                                           clean=True)
            except FelyxProcessorError:
                logger.error(
                    'Failed to register insitu data in the index for input : {} '
                    'and site : {}'.format(input_path, site_code))
                exit_failure(args)
        logger.info('File {} has been successfully imported'.format(input_path))

    logger.info('Done')
    exit_success(args)
