# -*- encoding: utf-8 -*-

"""
@author: <sylvain.herledan@oceandatalab.com>
@date: 2018-09-28
"""

from __future__ import print_function

import argparse
import json
import logging
import os
import sys
from typing import List, NoReturn, Optional, Tuple

from pydantic import field_validator, DirectoryPath

import felyx_processor.export.metric
import felyx_processor.index.es
import felyx_processor.storage
import felyx_processor.storage.metric
from felyx_processor.metrics.metrics_factory import MetricsDataSourceFactory
from felyx_processor.utils.arguments import get_override_args, parse_all_args
from felyx_processor.utils.configuration import (
    FelyxProcessorConfig,
    FelyxSystemConfig,
    load_felyx_processor_config,
    load_felyx_sys_config,
)
from felyx_processor.utils.logging import setup_logging
from felyx_processor.utils.parameters import (
    FelyxModelParameters, FelyxModel, InputFiles, FilterSitesCollections,
    log_params)
from . import exit_success, exit_failure


logger = logging.getLogger('felyx.processor')


class MetricsOption(FelyxModel):
    no_overwrite: bool = False
    index_results: bool = False
    exit_flag: Optional[str] = None
    metric_dir: Optional[DirectoryPath] = None
    from_manifests: bool = False


class MetricsParams(FelyxModelParameters):
    mdataset: str
    input: InputFiles
    filters: Optional[FilterSitesCollections] = None
    options: Optional[MetricsOption] = None


def parse_args(
        cli_args: List[str]
) -> Tuple[FelyxSystemConfig, FelyxProcessorConfig, MetricsParams]:
    """"""
    metric_parser = argparse.ArgumentParser()
    metric_parser.add_argument(
        '-c', '--configuration-file', type=str, default=None,
        help='Path of the configuration file')
    metric_parser.add_argument(
        '--mdataset', type=str, default=None, required=True,
        help='The dataset used for metrics computation')
    metric_parser.add_argument(
        '--inputs', type=str, default=None, nargs='*', required=True,
        help='The path of the input file or a list of the paths of '
             'the input files')
    metric_parser.add_argument(
        '--from-list', type=str, default=None,
        help='The path of a file containing one input path per line')
    metric_parser.add_argument(
        '--allowed-collections', type=str, default=None,
        help='Site collection identifier or list of site collection '
             'identifiers. The extraction will only be generated for these '
             'collections')
    metric_parser.add_argument(
        '--allowed-sites', type=str, default=None,
        help='Site identifier or list of site identifiers. '
             'The extraction will only be generated for these sites.')
    metric_parser.add_argument(
        '--metric-dir', type=str, default=None,
        help='Path of the directory that will be used as root '
             'when building the path of the files containing the metric')
    metric_parser.add_argument(
        '--from-manifests', action='store_true', default=False,
        help='Input files to generate metrics are manifest files created '
             'by `felyx-extraction`. Avoids querying an index engine such as '
             'Elasticsearch.')
    metric_parser.add_argument(
        '--no-overwrite', action='store_true', default=False,
        help=str('do not overwrite already existing child products')
    )

    metric_parser.add_argument(
        '--logfile', type=str, default=None,
        help='Path of the file where logs will be written')
    metric_parser.add_argument(
        '--logfile-level', type=str, default=None,
        help='Minimal level that log messages must reach to be written '
             'in the log file')
    level_group = metric_parser.add_mutually_exclusive_group()
    level_group.add_argument(
        '-v', '--verbose', action='store_true',
        help='Activate debug level logging - for extra feedback.')
    level_group.add_argument(
        '-q', '--quiet', action='store_true',
        help='Disable information logging - for reduced feedback.')
    level_group.add_argument(
        '-s', '--silence', action='store_true',
        help='Log ONLY the most critical or fatal errors. ')

    args = parse_all_args(metric_parser, cli_args)

    felyx_processor_config = load_felyx_processor_config(
        configuration_file=args.configuration_file)
    felyx_sys_config = load_felyx_sys_config()

    params = MetricsParams(**dict(
        mdataset=args.mdataset,
        input=dict(
            inputs=[_ for x in args.inputs for _ in x.split(',')],
            from_list=args.from_list),
        filters=dict(
            allowed_collections=args.allowed_collections,
            allowed_sites=args.allowed_sites),
        options=dict(
            metric_dir=args.metric_dir,
            from_manifests=args.from_manifests,
            no_overwrite=args.no_overwrite),
        log=log_params(args)
        ))

    return felyx_sys_config, felyx_processor_config, params,


def _to_stdout(meta, *args, **kwargs) -> bool:
    """"""
    json_str = None
    try:
        json_str = json.dumps(meta, indent=2)
    except ValueError:
        logger.error('Failed to parse metadata as JSON',
                     exc_info=sys.exc_info())
        return False

    print(json_str)
    return True


def _to_json_file(meta, output_dir, overwrite) -> bool:
    """"""
    ok = felyx_processor.storage.metric.write_json(meta, output_dir, overwrite)
    return ok


def export_metrics(felyx_sys_config: FelyxSystemConfig,
                   results, metric_dir, index_results, overwrite) -> bool:
    """ Export metrics to JSON files and index in Elasticsearch if needed

    Args:
        felyx_sys_config: The Felyx system configuration
        results: The metrics.
        metric_dir: The directory where are stored the metrics.
        index_results: The indexing results option.
        overwrite: The overwrite existing files option.

    Returns:
        The execution status.
    """
    # Write or print metrics documents
    if '-' == metric_dir:
        export_method = _to_stdout
    else:
        export_method = _to_json_file

    for doc in results:
        miniprod_name = doc['miniprod']
        exported = felyx_processor.export.metric.dict_from_computed(doc)
        relative_dir = felyx_processor.storage.metric.get_relative_path(
            exported)
        doc_path = os.path.join(metric_dir, relative_dir,
                                '{}.json'.format(miniprod_name))
        ok = export_method(doc, doc_path, overwrite)
        if not ok:
            return False

    if index_results is False:
        return True

    # Handle indexation
    ds = MetricsDataSourceFactory.get_metrics_datasource(
        felyx_sys_config=felyx_sys_config)
    ds.register_metrics(results)

    return ok


def run_metric(felyx_config: FelyxProcessorConfig,
               felyx_sys_config: FelyxSystemConfig,
               inputs,
               args,
               metric_dir,
               allowed_collections,
               allowed_sites) -> bool:
    """
    Runs the metrics computation.

    Args:
        felyx_config: The Felyx configuration.
        felyx_sys_config: The Felyx system configuration.
        inputs: The list of input files.
        args: The command arguments.
        metric_dir: The directory of the generated metrics.
        allowed_collections: The list of allowed collections.
        allowed_sites: The list of allowed sites.

    Returns:
        A boolean indicating the execution status.
    """
    # can raise an exception so import once log is ok
    import felyx_processor.metrics
    result, errors = felyx_processor.metrics.from_miniprods(
        felyx_processor_config=felyx_config,
        felyx_sys_config=felyx_sys_config,
        miniprod_paths=inputs,
        mdataset=args.mdataset,
        allowed_collections=allowed_collections,
        allowed_sites=allowed_sites,
        from_manifests=args.options.from_manifests)

    # Print all errors to stderr
    if 0 < len(errors):
        for e in errors:
            logger.error('Error while processing metrics for {}'.format(
                e.miniprod))
            logger.error('\tCollection: {}'.format(e.collection))
            logger.error('\tDataset: {}'.format(e.dataset))
            logger.error('\tMetric: {}'.format(e.name))
            logger.error('\tError: {}'.format(e.msg), exc_info=e.exception)
        return False

    if 0 >= len(result):
        # No error but nothing to export
        logger.warning('No metric has been produced from the miniprods')
        return True

    ok = export_metrics(felyx_sys_config=felyx_sys_config,
                        results=result,
                        metric_dir=metric_dir,
                        index_results=args.options.index_results,
                        overwrite=not args.options.no_overwrite)

    return ok


def felyx_metric(cli_args=None) -> NoReturn:
    """"""
    # local alias

    main_logger = logging.getLogger()

    felyx_sys_config, felyx_processor_config, args = parse_args(cli_args)

    # Setup logging
    if not setup_logging(main_logger, args.log):
        exit_failure(args)

    # Aggregate inputs from all possible sources
    inputs = args.input.get_file_list()

    if 0 >= len(inputs):
        logger.error('No input file to process!')
        exit_failure(args)

    _msg = '{} input files to process'
    logger.debug(_msg.format(len(inputs)))

    # Use default directory to save miniprods if the user did not define one
    metric_dir = args.options.metric_dir
    if metric_dir is None:
        # Get path for miniprods repository from Felyx config (metrics are
        # saved in the same place as miniprods)
        metric_dir = felyx_sys_config.default_felyx_miniprod_store

    allowed_sites = None
    allowed_collections = None

    if args.filters is not None:
        if args.filters.allowed_sites is not None and \
                0 < len(args.filters.allowed_sites):
            allowed_sites = args.filters.allowed_sites
        if args.filters.allowed_collections is not None and \
                0 < len(args.filters.allowed_collections):
            allowed_collections = args.filters.allowed_collections
    ok = False
    try:
        ok = run_metric(
            felyx_processor_config,
            felyx_sys_config,
            inputs,
            args,
            metric_dir,
            allowed_collections,
            allowed_sites)

    except Exception:
        # Error sink
        logger.error('An error occurred and could not be handled by felyx',
                     exc_info=sys.exc_info())
        exit_failure(args)

    if not ok:
        exit_failure(args)

    exit_success(args)
