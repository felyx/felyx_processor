import os

# Constants
# -----------------------------------------------------------------------------
MINIPRODUCT = 'miniprod'
METRICS = 'metrics'
MINIPROD_CONTENT_MASK = 'miniprod_content_mask'


# Classes
# -----------------------------------------------------------------------------
class URI(str):
    """ Universal Resource Identifier """
    def __new__(cls, value, datatype, **kwargs):
        obj = str.__new__(cls, value)
        obj.datatype = datatype
        obj.metadata = kwargs
        return obj

    @staticmethod
    def from_dict(dict_obj):
        """Instanciate an URI object from a dict."""
        if 'uri' not in dict_obj or 'datatype' not in dict_obj:
            raise Exception('URI.fromDict expects a dict with "uri" and "datatype" keys')

        if 'metadata' in dict_obj:
            return URI(dict_obj['uri'],
                       dict_obj['datatype'],
                       **dict_obj['metadata'])
        return URI(dict_obj['uri'], dict_obj['datatype'])

    def available(self):
        """ """
        return os.path.exists(self)


class URIs(list):
    """ List of URIs """

    def available(self):
        """ """
        return all(map(lambda x: x.available(), self))

    def common_path(self):
        return os.path.commonprefix(self)
