"""
felyx_processor.processors.celery
----------------------------

@author: Sylvain Herledan
"""

from __future__ import with_statement

import logging
import os

try:
    import json
except ImportError:
    import simplejson as json

logger = logging.getLogger('felyx.processor')
PLUGINS_PATH = None
METADATA_PATH = None

# Initialization
# -----------------------------------------------------------------------------

# Methods
# -----------------------------------------------------------------------------


def operator_new(category, cost, *args):
    def _operator(func):
        return func
    return _operator


def operator(category, cost, *args):
    """ Decorator for operators """
    def _operator(func):
        func.category = category
        func.cost = cost
        func.is_operator = True
        func.argstypes = args
        return func
    return _operator


def merge_metadata_old(path):
    """ """
    merged = {}
    for filename in os.listdir(path):
        if not filename.endswith('.json'):
            continue
        fullpath = os.path.join(path, filename)
        with open(fullpath, 'r') as f:
            meta = json.loads(f.read())
        for operator in meta.keys():
            if operator in merged.keys():
                raise Exception('Operator "%s" is defined in several plugins: "%s" and "%s"' % ( operator
                                                  , merged[operator]['plugin']
                                                  , meta[operator]['plugin']))
            merged[operator] = meta[operator]
    return merged


class PluginMetadata(dict):
    """ """
    def __init__(self, plug_name, class_name, capabilities=None):
        """ """
        dict.__init__(self)
        self['name'] = plug_name
        self['class'] = class_name
        if capabilities is not None:
            self['capabilities'] = capabilities
        else:
            self['capabilities'] = []
