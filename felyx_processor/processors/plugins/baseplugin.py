# encoding: utf-8
"""
felyx_processor.processors.plugins.baseplugin
----------------------------------------

This module contains base classes for the plugins, the contents are intended
to be inherited by plugin classes, and not to be implemented themselves.

As such, it doesn't contain functioning plugins.

.. :copyright: Copyright 2013 Pelamis Scientific Software Ltd.
.. :license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: David Poulter <david.poulter@pelamis.co.uk>
.. codeauthor:: David Poulter <david.poulter@pelamis.co.uk>
"""
from __future__ import with_statement

import collections
import hashlib
import json
import logging
import os

import felyx_processor.storage
from felyx_processor.basetypes import MINIPRODUCT
from felyx_processor.processors.data import Data, DataManager
from felyx_processor.query import format_return
from felyx_processor.query.finders import get_miniprod_root

LOGGER = logging.getLogger('felyx.processor')

class PluginError(Exception):
    """An exception class for errors raised in Plugins."""

class JSONNaNEncoder(json.JSONEncoder):
    """A JSON Encoder class that can accept NaNs, but convert them to
    null in JSON output. Significantly (~ 8*) faster than regex method."""
    def encode(self, input_object):
        if isinstance(input_object, str):
            if isinstance(input_object, str):
                _encoding = self.encoding
                if (_encoding is not None
                        and not (_encoding == 'utf-8')):
                    input_object = input_object.decode(_encoding)
            if self.ensure_ascii:
                return json.encoder.encode_basestring_ascii(input_object)
            else:
                return json.encoder.encode_basestring(input_object)

        chunks = self.iterencode(input_object, _one_shot=True)
        if not isinstance(chunks, (list, tuple)):
            chunks = list(chunks)
        return ''.join(_ if _ != 'NaN' else 'null' for _ in chunks)

def create_derived_path(plugin_path, **kwargs):
    """Return a directory to store the remapped miniprods in.

    :param str plugin_path: Path component identifying the plugin used.
    :return: A directory path.
    """
    keys = sorted(kwargs.keys())
    values = collections.OrderedDict()
    for key in keys:
        values[key] = kwargs[key]
    hashable = json.dumps(values)
    hasher = hashlib.md5()
    hasher.update(hashable)
    unique_path = hasher.hexdigest()[:16]

    name = os.path.join(plugin_path, unique_path)
    path = os.path.join(
        get_miniprod_root(),
        name
    )
    felyx_processor.storage.makedirs(path)

    return name, path


class BasePlugin(object):
    """
    A base class for all plugins. Provides the default values for plugins.
    """
    acceptable_arguments = ['testing']
    # All plugins accept the '*args' argument.
    number_of_inputs_per_task = [1]

    @classmethod
    def check_task_id(cls, task_id):
        """
        Check the validity of the task requests, return the task if it checks
        out.

        :param str task_id: The id of the task requested.
        """
        if not hasattr(cls, task_id):
            return False, 'Task ID not known'

        if not hasattr(getattr(cls, task_id), '__call__'):
            return False, 'Task ID not executable'

        return getattr(cls, task_id), None

    @classmethod
    def report_unused_kwargs(cls, kwargs):
        """
        Log any keywords that are not explicitly handled by the class.

        :param kwargs: The remaining kwargs to be reported
        """
        if len(kwargs):
            LOGGER.warning(
                'The following arguments were not passed to the plugin '
                'as they are not explicitly coded for: {}.'.format(
                    str(', '.join(kwargs.keys()))
                )
            )

    @classmethod
    def get_task_data(cls, args):
        """
        Return the input in a form for plugins

        :param args: The data to be handled.
        """
        raise NotImplementedError

    @classmethod
    def filter_keywords(cls, keys, kwargs):
        """
        Strip out any unexpected keywords from the arguments.

        :param list keys: A list of acceptable keys for this class of plugin.
        :param kwargs: The kwargs sent to the plugin.
        """
        return_kwargs = {}
        kwargs_copy = kwargs.copy()
        for key in kwargs.keys():
            if key in keys:
                return_kwargs[key] = kwargs_copy.pop(key, None)

        cls.report_unused_kwargs(kwargs_copy)

        return return_kwargs

    @classmethod
    def check_args_length(cls, args):
        """
        Return False with an unexpected length of arguments.

        :param args: the arguments to check
        """
        if len(args) not in cls.number_of_inputs_per_task:
            print(cls.number_of_inputs_per_task)
            print(len(args))
            print()
            return False
        return True

    @classmethod
    def get_task_data(cls, args):
        """
        Formats the input data to a form for Univariate plugins.

        :param args: The input input arguments to be passed to the task.
        """
        data = TypeError()
        if isinstance(args[0], Data):
            # input is a single Data object - return as a single element list
            data = [args[0]]

        elif isinstance(args[0], str):
            if os.path.exists(args[0]):
                #Input is a string with a file location that exists.
                data = [DataManager().load(args[0], MINIPRODUCT)]

        elif isinstance(args[0], collections.Container):
            # input is a sequence of Data objects - pass it on.
            if all(isinstance(_, Data) for _ in args[0]):
                data = args[0]

        return data

    @classmethod
    def perform(cls, task_id, task_name, *args, **kwargs):
        """
        Performs the operation defined in statistical_operator.

        :param str task_id: The id of the task to perform.
        :param str task_name: The name of the task to perform.
        :param args: The input input arguments to be passed to the task.
        :param kwargs: The keyword arguments to be passed to the task.
        """

        # Identify the task_id, is it a callable method of this class?
        task_function, task_error = cls.check_task_id(task_id)
        if task_error:
            return False, None, task_error

        # Limit to specified arguments, and report on unexpected ones.
        kwargs = cls.filter_keywords(cls.acceptable_arguments, kwargs)

        # Check there is only one argument
        if not cls.check_args_length(args):
            return False, None, 'Unexpected positional (*args) arguments.'

        # Get input data
        data = cls.get_task_data(args)
        if isinstance(data, Exception):
            LOGGER.exception('Plugin input data not of correct type.')
            return False, None, 'Failed'

        # Perform plugin task.
        # noinspection PyBroadException
        try:
            result = task_function(data, **kwargs)
            return True, result, 'Success'
        except Exception as problem:
            if 'outfile' in kwargs:
                format_return(
                    'Plugin task <{}> raised an exception. Internally it was '
                    'reported as: {}'.format(task_id, str(problem)),
                    kwargs['outfile'],
                    level=LOGGER.exception
                )
            else:
                LOGGER.exception('Plugin task raised an exception.')
            return False, None, 'Failed'


class BaseStatisticPlugin(BasePlugin):
    """
    A base class for statistical plugins. Provides the default methods that
    are set for all statistics plugins.
    """
    # Statistics operators take a set number of inputs. This is
    # defined in this attribute. For example, a Univariate statistic can only
    # accept a single input. A Bivariate plugin can accept either 1 (two fields
    # from a single file), or 2 (a single field form two separate files).
    number_of_inputs_per_task = BasePlugin.number_of_inputs_per_task + [2]

    # All Statistics classes accept the limit_to_site keyword
    acceptable_arguments = BasePlugin.acceptable_arguments + [
        'limit_to_site', 'axis', 'field']

    # noinspection PyUnusedLocal
    @staticmethod
    def is_reducer(task_id):
        """
        Is the task a reducer? Default to False for statistics.

        :param str task_id: The id of the task.
        """
        return False

    # noinspection PyUnusedLocal
    @staticmethod
    def expects_uri(task_id):
        """
        Does the task expect a URI? Defaults to False for statistics.

        :param str task_id: The id of the task.
        """
        return False


class UnivariateStatisticPlugin(BaseStatisticPlugin):
    """
    A class for univariate statistical plugins. These are functions that accept
    only the input field, for example mean, max, stdev etc. Arguments may
    provide filtering, but typically, a single value is yielded per input.
    """
    acceptable_arguments = BaseStatisticPlugin.acceptable_arguments + \
        ['confidence_value', 'must_have']


class VariableUnivariateStatistic(BaseStatisticPlugin):
    """
    A class for univariate statistics which require a coefficient or choice
    variable. For example, percentile (Qth) or Nth moment.
    """
    acceptable_arguments = BaseStatisticPlugin.acceptable_arguments + \
        ['confidence_value', 'must_have', 'percentile_value']


class BivariateStatisticPlugin(BaseStatisticPlugin):
    """
    A class for bivariate statistical plugins. These are plugins that accept
    two sets of input data, for example a difference calculation.
    """
    acceptable_arguments = BaseStatisticPlugin.acceptable_arguments + \
        ['confidence_value', 'must_have']
    number_of_inputs_per_task = \
        BaseStatisticPlugin.number_of_inputs_per_task + [3]


class BaseQueryPlugin(BasePlugin):
    """
    A base class for the query system and it's operators. Query plugins are
    expected to be called directly from user queries.
    """

    # noinspection PyUnusedLocal
    @staticmethod
    def expects_uri(task_id):
        """
        The query system expects a URI as input.

        :param str task_id: The id of the task.
        """
        return True

        # noinspection PyUnusedLocal

    @staticmethod
    def is_reducer(task_id):
        """
        Is the task a reducer? Default to False for statistics.

        :param str task_id: The id of the task.
        """
        assert task_id
        return False

    # noinspection PyUnusedLocal
    @classmethod
    def perform(cls, task_id, task_name, *args, **kwargs):
        """
        Performs the query segment required.

        :param str task_id: The id of the task to perform.
        :param str task_name: The name of the task to perform.
        :param args: The input input arguments to be passed to the task.
        :param kwargs: The keyword arguments to be passed to the task.
        """
        # Identify the task_id, is it a callable method of this class?
        return_value = None

        task_function, task_error = cls.check_task_id(task_id)
        if task_error:
            LOGGER.error(
                str(
                    'The plugin class {} has no task named {}. It is probable '
                    "that the user requested the wrong 'operation_name' in a "
                    'query request. The plugin reported the '
                    "error message: \n'{}'. \nExiting query execution."
                ).format(cls.__name__, task_id, task_error)
            )
            return False, return_value, task_error

        # Get input data
        data = cls.get_task_data(args)
        if isinstance(data, Exception):
            message = str(
                "The plugin class {} (task name '{}') received input data "
                'in an incorrect format. This is likely to be caused by '
                'an error in the selection step of the query, or in the '
                'operation of a previous plugin. Exiting query execution.'
            ).format(cls.__name__, task_id)

            LOGGER.error(message)
            return False, return_value, message

        # noinspection PyBroadException
        try:
            # Breakpoint this location in order to force step into the plugin.
            # label: FORCEPLUGIN
            return_value = task_function(*data, **kwargs)
            LOGGER.debug(
                str(
                    "The plugin {} (task name '{}') executed without error."
                ).format(cls.__name__, task_id)
            )
            return True, return_value, 'Success'

        except PluginError as problem:
            message = str(
                "The plugin {} (task name '{}') reported the following known "
                'error: {}.'
            ).format(cls.__name__, task_id, str(problem))
            LOGGER.error(message)

        except Exception as problem:
            message = str(
                "The plugin {} (task name '{}') reported the following unknown "
                'error: {}.'
            ).format(cls.__name__, task_id, str(problem))
            LOGGER.error(message)

        return False, None, message

class QueryPlugin(BaseQueryPlugin):
    """
    A plugin for the felyx query system.
    """

    @classmethod
    def get_inputs(cls, values, name=None):
        """
        Unified method to handling the inputs to query based plugins.

        :param values: Values form a selection, or another plugin.
        :param name: The name of the calling plugin step.
        :return: Common plugin format return values.
        """
        # Both selection steps and any plugin should return the common format
        # dictionary where results are presented as:
        # {'results': {...}, 'arguments': {...}, step_name: '...'}
        # OR:
        # {'error': '...'}

        if not isinstance(values, dict):
            raise PluginError(
                str(
                    "This plugin '{}' recieved values in an unexpected format. "
                    'Please report this issue to the felyx development team.'
                ).format(cls.__name__)
            )

        arguments = values.pop('arguments', {})

        if name is not None:
            arguments[name] = {}

        results = values['results']
        old_name = values.pop('step_name', None)

        return results, arguments, old_name

    @classmethod
    def dumps(cls, values):
        """
        Returns a JSON representation of the input values.

        :param values: Python objects to be encoded
        :return:
        """
        return json.dumps(values, cls=JSONNaNEncoder)

    @classmethod
    def get_task_data(cls, args):
        """
        Formats the input data to a form for Univariate plugins.

        :param args: The input input arguments to be passed to the task.
        """
        return [_ for _ in args if _ is not None]
