# -*- coding: utf-8 -*-
"""Input system for felyx.

Exported classes:
    Data -- Holds data and metadata together.
    DataManager -- Loads and saves data.
    UnknownDataTypeException -- Exception raised when the user tries to load
    data with an unsupported type.
"""
import json
import operator
from pathlib import Path

import numpy
import xarray as xr

from felyx_processor.basetypes import METRICS, MINIPROD_CONTENT_MASK, MINIPRODUCT

FUNCS = [
    'eq', 'ge', 'gt', 'mod', 'le', 'lt', 'equal', 'not_equal', 'less',
    'less_equal', 'greater', 'greater_equal'
]


class MiniProdNCFile():
    """
    A wrapper class for Felyx Miniprod files. Provides an augmented
    NCFile class (as all miniprods are netCDF4) with methods which
    plugins may use to analyse the contents of the file.

    :param args: Arguments to be passed to NCFile
    :param kwargs: Keyword arguments to be passed to NCFile

    .. codeauthor:: David Poulter <david.poulter@pelamis.co.uk>
    """

    def __init__(self, fpath: Path, **kwargs):
        """Initialise the child product object."""
        #self.ds = xr.open_dataset(fpath, **kwargs)
        self.ds = fpath.ds

    @staticmethod
    def flag_bit_set(field_values, bit_number):
        """
        Return the inverse of a mask where a flag bit must be set. That is,
        for a value to be returned from read_values, the flag MUST be set.

        :param numpy.ndarray field_values: The values of the field.
        :param int or list bit_number: The bit number of the flag that must be
          set.
        :return: A mask of where the values are not set (to be later inverted).
        :rtype: numpy.ndarray
        """
        if isinstance(bit_number, list):
            result = None
            for element in bit_number:
                if result is not None:
                    result *= numpy.bitwise_and(field_values, 2 ** element)
                else:
                    result = numpy.bitwise_and(field_values, 2 ** element)
        elif isinstance(bit_number, int):
            result = numpy.bitwise_and(field_values, 2 ** bit_number)

        else:
            raise TypeError(
                'Value(s) supplied to flag_bit_set must be a single integer, '
                'or a list of integers.'
            )

        return result.astype(numpy.bool)

    @staticmethod
    def flag_bit_not_set(field_values, bit_number):
        """
        Return the of a mask where a flag bit must be set. That is,
        for a value to be returned from read_values, the flag MUST NOT be set.

        :param numpy.ndarray field_values: The values of the field.
        :param int or list bit_number: The bit number of the flag that must NOT
          be set.
        :return: A mask of where the values are not set (to be later inverted).
        :rtype: numpy.ndarray
        """

        if isinstance(bit_number, list):
            result = None
            for element in bit_number:
                if result is not None:
                    result *= numpy.invert(
                        numpy.bitwise_and(
                            field_values, 2 ** element
                        )

                    )

                else:
                    result = numpy.invert(
                        numpy.bitwise_and(
                            field_values, 2 ** element
                        )
                    )
        elif isinstance(bit_number, int):
            result = numpy.invert(
                numpy.bitwise_and(
                    field_values, 2 ** bit_number
                )
            )

        else:
            raise TypeError(
                'Value(s) supplied to flag_bit_not_set must be a single '
                'integer, or a list of integers.'
            )

        return result.astype(numpy.bool)

    def read_values(
            self, field, slices=None, limit_to_site=True, must_have=None
        ):
        """
        Read the values within a field, limiting to only the values within the
        miniprod site if requested

        :param str field: The name of the field to read.
        :param list slices: A list of slices to extract, if requested.
        :param bool limit_to_site: True to mask the values outside of the felyx
          site. Set to True by default.
        :param list must_have: A list of restriction dictionary elements.
        :return: The field values after slicing, limiting and checking if
          these steps are requested.
        :rtype: numpy.ndarray
        """
        # Set slices in case they are needed later...

        result = self.ds[field].cb.isel(index=slices)
        if limit_to_site:
            mask = self.ds[MINIPROD_CONTENT_MASK].cb.isel(index=slices)
            result = numpy.ma.masked_where(mask == 0, result)

        if must_have:
            for restriction in must_have:
                # Check all elements are provided
                if not all(_ in restriction.keys() for _ in [
                    'field', 'operator', 'value'
                ]):
                    raise ValueError(
                        str(
                            'Invalid restriction (missing values). '
                            'The must_have clause must contain a field, an'
                            'operator and a value key.'
                        )
                    )

                field = restriction.get('field')
                operator_function = restriction.get('operator')
                value = restriction.get('value')

                if hasattr(numpy, operator_function) and \
                        operator_function in FUNCS:
                    function = getattr(numpy, operator_function)
                elif hasattr(operator, operator_function) and \
                        operator_function in FUNCS:
                    function = getattr(operator, operator_function)
                elif hasattr(self, operator_function):
                    function = getattr(self, operator_function)
                else:
                    raise ValueError('Operator not known')

                mask = self.ds[field].cb.isel(index=slices)

                result = numpy.ma.masked_where(
                    numpy.invert(
                        function(mask, value)
                    ), result
                )

        return result


class UnknownDataTypeException(TypeError):
    """Exception raised when the user tries to load data with an unsupported
    type.
    """

    def __init__(self, datatype):
        message = 'Data type "%s" is not supported' % datatype
        TypeError.__init__(self, message)


class Data(object):
    """ Holds data and metadata together."""

    def __init__(self, data, datatype, **kwargs):
        self.content = data
        self.datatype = datatype
        self.metadata = {}
        for keyword in kwargs.keys():
            self.metadata[keyword] = kwargs[keyword]

    def close(self):
        """
        Try to code the file, letting exceptions fail silently.
        """
        if hasattr(self.content, '_isopen'):
            # noinspection PyProtectedMember
            if self.content._isopen:
                self.content.close()
        else:
            try:
                self.content.close()
            except AttributeError:
                pass
            except RuntimeError:
                pass


class DataManager(object):
    """Provides routines for loading and saving data.

    Public functions:
        load -- Returns a Data object with the content loaded from URI.

        save -- Serializes a Data object and save it a the provided URI.
    """

    @staticmethod
    def load(uri, datatype, **kwargs):
        """Returns a Data object with the content loaded from URI.
            >>>from felyx_processor.processors.data import DataManager
            >>>manager = DataManager()
            >>>d = manager.load('/tmp/toto.nc', 'miniprod')
            >>>d.datatype
            'miniprod'

        :param kwargs: Keyword arguments to be stored as metadata.
        :type uri: str
        :param uri: The URI of the data to load.
        :type datatype: string
        :param datatype: The type of data to load.
        :rtype: :class:`Data`
        :returns: A Data object with the content loaded from URI.
        """
        if MINIPRODUCT == datatype:
            result = Data(MiniProdNCFile(uri), datatype, **kwargs)

        elif METRICS == datatype:
            with open(uri, 'rb') as outfile:
                result = json.load(outfile)['data']

        else:
            raise UnknownDataTypeException(datatype)

        return result

    # noinspection PyUnusedLocal
    @staticmethod
    def save(uri, data, **kwargs):
        """Serializes data and save the result at the specified URI.

        :param kwargs: Ignored extra parameters
        :type uri: URI
        :param uri: The URI where data will be saved.
        :type data: Data
        :param data: The Data object to save.
        """
        if MINIPRODUCT == data.datatype:
            target_file = MiniProdNCFile(uri, mode='w')
            data.content.save(target_file)
            target_file.close()

        elif METRICS == data.datatype:
            with open(uri, 'wb') as outfile:
                json.dump({'data': data.content}, outfile)

        else:
            raise UnknownDataTypeException(data.datatype)
