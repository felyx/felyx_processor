# -*- coding: utf-8 -*-
"""
Module to store/read/write dynamic site data.

:copyright: Copyright 2021 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>

@TODO: merge with datasource ?
"""
import csv
import datetime
import json
import logging
import warnings
from pathlib import Path
from typing import Dict, Optional, Union

import numpy
import pandas as pd
import pyarrow as pa
import pyarrow.parquet as pq
from pydantic import BaseModel

DEFAULT_TIME_UNITS = 'seconds since 1981-01-01 00:00:00'

# standard coordinate names for felyx in situ format
SITES_REQ_COLUMNS = {
    'lat': 'Latitude', 'lon': 'Longitude', 'time': 'Time', 'id': 'Site'}
DEFAULT_METADATA = {
    'lat': {'long_name': 'latitude',
            'standard_name': 'latitude', 'units': 'degree_north'},
    'lon': {'long_name': 'longitude',
            'standard_name': 'longitude', 'units': 'degree_east'},
    'time': {'long_name': 'time',
             'standard_name': 'time',
             'units': 'seconds since 1981-01-01 00:00:00'},
    'id': {'long_name': 'dynamic site identifier'},
}


class NumpyJSONEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, numpy.ndarray) and obj.ndim == 1:
            return obj.tolist()
        elif hasattr(obj, 'dtype'):
            return obj.item()

        return json.JSONEncoder.default(self, obj)


class Variable(BaseModel):
    long_name: Optional[str] = None
    standard_name: Optional[str] = None
    units: Optional[str] = None
    dtype: str = None
    _FillValue: Union[int, float] = None

    class ConfigDict:
        extra = 'allow'


class Metadata(BaseModel):
    vars: Dict[str, Variable]
    globals: Dict = {}


def from_csv(filename, dataset_id, read_metadata=True, metadata_file=None):
    """Return the data from a felyx csv file in a DynamicSiteData object"""
    # read fields
    with open(filename, 'r', newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter=';', quotechar='#')
        for row in reader:
            items = [_.strip() for _ in row]
            if 0 < len(items) and 'fields' == items[0]:
                fields = items[1:]
                break

    # read metadata
    var_attrs = {}
    global_attrs = {}
    if read_metadata:
        if metadata_file is None:
            raise ValueError('You must provide the path to the metadata json '
                             'file in order to read the metadata')
        var_attrs, global_attrs = DynamicSiteData.read_metadata_from_json(
            metadata_file)

    # read data
    with open(filename, 'r', newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter=';', quotechar='#')
        data = pd.DataFrame(
            DynamicSiteData._read_csv_rows(reader),
            columns=['id', *fields]
        )

    # remove unnamed columns
    if '' in data.columns:
        data = data.drop(columns=[''])
    # guess types and fill some metadata
    for col in data.columns:
        if col == 'Time':
            data[col] = data[col].astype('datetime64[ns]')
            continue
        elif col in ['Latitude', 'Longitude']:
            data[col] = data[col].astype(numpy.float64)
            continue
        elif col == 'id':
            continue

        if col not in var_attrs:
            var_attrs[col] = Variable(long_name=col)

        dtype = var_attrs[col].model_dump().get(
            'dtype', numpy.dtype(type(var_attrs[col]._FillValue)))

        if dtype is None:
            try:
                dtype = numpy.dtype(type(eval(data[col].min())))
            except NameError:
                dtype = data[col].dtype

        # replaces nulls and convert to expected dtype
        if dtype in ['float32', 'float64']:
            data[col] = pd.to_numeric(
                data[col], errors='coerce').astype('Float64')
        else:
            data[col] = pd.to_numeric(
                data[col], errors='coerce').astype('Int64')

        logging.info(f'Guessed type for field {col}: {dtype}')

    data = data.rename(columns={
        'Latitude': 'lat', 'Longitude': 'lon', 'Time': 'time'
    })

    data = data.reset_index().set_index(['id', 'time']).drop(columns='index')

    return DynamicSiteData(
        dataset_id, data, var_attrs=var_attrs, global_attrs=global_attrs)


def from_parquet(filename, dataset_id='unknown', columns_filter=None,
                 read_metadata=True):
    """Return the data from a felyx parquet file in a DynamicSiteData object"""
    df = pd.read_parquet(
        filename, engine='pyarrow', columns=columns_filter)

    metadata = {'vars': {}, 'globals': {}}
    if read_metadata:
        schema = pa.parquet.read_schema(filename)

        # read globals
        globmetadata = schema.metadata
        for k, v in globmetadata.items():
            k = k.decode('utf-8')
            v = v.decode('utf-8')
            if k == 'pandas':
                continue
            if k == 'collection':
                dataset_id = v
            else:
                metadata['globals'][k] = v

        # read var attrs
        for col in df.columns:
            metadata['vars'][col] = {}

            if schema.field(col).metadata is None:
                logging.debug(f'no attributes defined in parquet file for '
                              f'variable {col}')
                continue

            for k, v in schema.field(col).metadata.items():
                k = k.decode('utf-8')
                if k.startswith('PARQUET'):
                    continue
                try:
                    v = json.loads(v.decode('utf-8'))
                except json.decoder.JSONDecodeError:
                    v = v.decode('utf-8')
                metadata['vars'][col][k] = v

    return dataset_id, df, metadata['vars'], metadata['globals']


class DynamicSiteData(object):
    """
    Class to store and format dynamic sites (trajectories) with their
    associated data.
    """

    def __init__(
            self,
            collection_id: str,
            data: pd.DataFrame = None,
            var_attrs: Dict[str, Variable] = None,
            global_attrs: Dict = None):
        """Intialize a set of dynamic site data belonging to the same
        collection of dynamic sites.

        Args:
            collection_id (string): identifier to which the dynamic site data
                belong to.
        """
        self.collection = collection_id
        self.data = self.check_data(data)
        self.metadata:Optional[Metadata] = None
        # = Metadata(**{'globals': {}, 'vars':  []})
        self.add_metadata(var_attrs, global_attrs)

    @staticmethod
    def check_data(
            data: Optional[pd.DataFrame] = None
    ) -> Optional[pd.DataFrame]:
        """verify the conformity of the submitted dynamic site data"""
        if data is None:
            return data

        if not isinstance(data, pd.DataFrame):
            raise TypeError('data must be provided as a dataframe')

        headers = list(data.columns) + list(data.index.names)
        if len(set(headers).intersection(['lat', 'lon', 'time'])) != 3:
            raise ValueError('One of the following columns is missing in the '
                             'dataframe: lat, lon, time')

        for column in ['id', 'lat', 'lon', 'time']:
            if column not in headers:
                raise ValueError(f'column {column} is missing in the dataframe')

        # enforce types
        # this check requires to reset index and set it again after
        index = data.index.names
        if None in index:
            # tries to fix index
            logging.warning(
                'Index in DataFrame is not valid. Trying to infer it from '
                'columns')
            if len(set(data.columns).intersection({'id', 'time', 'z'})) == 3:
                index = ['id', 'time', 'z']
            elif len(set(data.columns).intersection({'id', 'time'})) == 2:
                index = ['id', 'time']
            else:
                raise ValueError('Could not infer index from DataFrame')

        data = data.reset_index()

        if data.id.size > 0 and type(data.id.iloc[0]) is not str:
            data.id = data.id.str.decode('utf-8')
        data.lon = data.lon.astype(float)
        data.lat = data.lat.astype(float)
        if not numpy.issubdtype(data.time.dtype, numpy.datetime64):
            data.time = data.time.astype('datetime64[s]')

        data = data.set_index(index)

        return data

    def add_data(self, data: pd.DataFrame):
        """
        Add complementary data to an existing set of dynamic site data.

        Args:
            data: provided as a dataframe, which must contain the columns
                `lat`, `lon`, `time`, `id` and other variables.
        """
        data = self.check_data(data)
        if self.data is None:
            self.data = data
        else:
            self.data = self.merge_data(data)

    def add_metadata(
            self,
            var_attrs: Dict[str, Variable] = None,
            global_attrs: Dict[str, Union[str, int, float]] = None):
        """Add the metadata associated with dynamic site data. This consists
        in the attributes describing each coordinate and variable, and the
        global attributes for the whole data collection, both provided as
        dictionaries.

        variable attributes are provided as a dictionary, with keys being the
        names of the variables and the values dictionaries of attributes.
        """
        if var_attrs is None:
            var_attrs = {}
        if global_attrs is None:
            global_attrs = {}

        # check that lat, lon and time are there
        for v in ['lat', 'lon', 'time', 'id']:
            if v not in var_attrs:
                var_attrs[v] = DEFAULT_METADATA[v]

        self.metadata = Metadata(vars=var_attrs, globals=global_attrs)

    @staticmethod
    def read_metadata_from_json(filename):
        """read old style metadata in json format"""
        with open(filename, 'r') as f:
            metadata = json.load(f)

        var_attrs = metadata['variable']
        # convert key variables to standard felyx names
        for k, v in SITES_REQ_COLUMNS.items():
            if v in var_attrs:
                var_attrs[k] = var_attrs.pop(v)

        var_attrs = {v: Variable(**k) for v, k in var_attrs.items()}
        for v in var_attrs:
            if v not in SITES_REQ_COLUMNS and \
                    'dtype' not in var_attrs[v].model_dump():
                raise ValueError(f'Missing dtype attribute for variable {v} '
                                 f'in metadata.json file')

        global_attrs = metadata['global']

        return var_attrs, global_attrs

    def save_metadata_to_json(self, filename):
        """Save the metadata of a collection of in situ data to felyx json

        Deprecated: for old csv felyx format only
        """
        warnings.warns(
            'Deprecated function. Save data as parquet instead',
            DeprecationWarning
        )
        jsonstr = {
            'variable': {
                SITES_REQ_COLUMNS.get(_.name, _.name): _.model_dump(
                    exclude_none=True)
                for _ in self.metadata.vars},
            'global': self.metadata.globals}

        if 'units' not in jsonstr['variable']['Time']:
            jsonstr['variable']['Time']['units'] = DEFAULT_TIME_UNITS

        # write result
        json.dump(jsonstr, open(filename, 'w'), indent=4,
                  cls=NumpyJSONEncoder, skipkeys=True)

    def merge_data(self, data):
        return pd.concat([self.data, data])

    def _save_csv(self, filename, df):
        with open(filename, 'w') as f:
            # for each site
            columns = list(df)
            columns.remove('id')
            for sid, data in df.groupby('id'):
                if data.size > 0:
                    f.write('target ; %s\n' % sid)
                    f.write('fields;')
                    data.loc[data['id'] != sid].to_csv(f, header=True, sep=';',
                                             index=False, columns=columns)
                    f.write('position\n')
                    data.to_csv(
                        f, header=False, date_format='%Y-%m-%dT%H:%M:%S',
                        sep=';', index=False, columns=columns)

    def save_to_csv(self, filename, metadata=True, frequency=None):
        """Save the data into felyx csv format for ingestion into the server

        Args:
            metadata: save the metadata in a distinct metadata.json file
        """
        warnings.warns(
            'Deprecated function. Save data as parquet instead',
            DeprecationWarning
        )

        drange = None
        if frequency is not None:
            drange = self.date_range(
                self.data['time'].min(), self.data['time'].max(),
                frequency=frequency
            )

        # Latitude, Longitude, Time must be first
        coords = list(SITES_REQ_COLUMNS.keys())
        df = self.data[[*coords, *list(set(self.data.columns) - set(
            coords))]].rename(columns=SITES_REQ_COLUMNS)

        if drange is not None:
            for i, date in enumerate(drange[:-1]):
                mask = (df['Time'] >= date) & \
                       (df['Time'] < drange[i+1])
                if not mask.any():
                    logging.warning('No data to save for {}'.format(date))
                    continue

                self._save_csv(date.strftime(str(filename)), df[mask])
        else:
            self._save_csv(filename, df)

        if metadata:
            self.save_metadata_to_json('metadata.json')

    @staticmethod
    def set_metadata(tbl, col_meta={}, tbl_meta={}):
        """Store table- and column-level metadata as json-encoded byte strings.

        Table-level metadata is stored in the table's schema.
        Column-level metadata is stored in the table columns' fields.

        To update the metadata, first new fields are created for all columns.
        Next a schema is created using the new fields and updated table metadata.
        Finally a new table is created by replacing the old one's schema, but
        without copying any data.

        Args:
            tbl (pyarrow.Table): The table to store metadata in
            col_meta: A json-serializable dictionary with column metadata in the
              form
                {
                    'column_1': {'some': 'data', 'value': 1},
                    'column_2': {'more': 'stuff', 'values': [1,2,3]}
                }
            tbl_meta: A json-serializable dictionary with table-level metadata.
        """
        # Create updated column fields with new metadata
        if col_meta or tbl_meta:
            fields = []
            for col in tbl.schema.names:
                if col in col_meta:
                    # Get updated column metadata
                    metadata = tbl.field(col).metadata or {}
                    for k, v in col_meta[col].items():
                        metadata[k] = json.dumps(v, cls=NumpyJSONEncoder)

                    # force having a dtype attribute for saving to netcdf
                    # later
                    if 'dtype' not in metadata:
                        dtype = tbl.field(col).type.to_pandas_dtype()
                        if numpy.issubdtype(dtype, numpy.datetime64):
                            metadata['dtype'] = json.dumps(
                                'int64', cls=NumpyJSONEncoder)
                        else:
                            metadata['dtype'] = json.dumps(
                                dtype.__name__, cls=NumpyJSONEncoder)

                    # Update field with updated metadata
                    fields.append(tbl.field(col).with_metadata(metadata))
                else:
                    fields.append(tbl.field(col))

            # Get updated table metadata
            tbl_metadata = tbl.schema.metadata or {}
            for k, v in tbl_meta.items():
                if type(v) == bytes or isinstance(v, str):
                    tbl_metadata[k] = v
                else:
                    tbl_metadata[k] = json.dumps(v, cls=NumpyJSONEncoder)

            # Create new schema with updated field metadata and updated table
            # metadata
            schema = pa.schema(fields, metadata=tbl_metadata)

            # With updated schema build new table (shouldn't copy data)
            # tbl = pa.Table.from_batches(tbl.to_batches(), schema)
            tbl = tbl.cast(schema)

        return tbl

    def save_to_parquet(self, filename, add_metadata=True, frequency=None):
        drange = None
        if frequency is not None:
            times = self.data.index.levels[1]
            drange = self.date_range(
                times.min(), times.max(),
                frequency=frequency
            )

        # transform to pyarrow table to get the dataframe schema
        pdf = pa.Table.from_pandas(self.data.head(1))

        # add felyx metadata to schema
        metadata = self.metadata.model_dump(exclude_none=True)
        metadata['globals']['collection'] = self.collection

        # save
        if drange is not None:
            for r in drange:
                df = self.data.xs(
                    slice(r.left, r.right - numpy.timedelta64(1, 'ns')),
                    level='time', drop_level=False)
                if df.empty:
                    logging.warning(
                        f'No data to save for interval {r.left} to {r.right}')
                    continue

                Path(r.left.strftime(str(filename))).parent.mkdir(
                    parents=True, exist_ok=True)

                pdf = pa.Table.from_pandas(df)
                if add_metadata:
                    pdf = DynamicSiteData.set_metadata(
                        pdf,
                        col_meta=metadata['vars'],
                        tbl_meta=metadata['globals'])

                pq.write_table(pdf, r.left.strftime(str(filename)))
        else:
            Path(filename).parent.mkdir(parents=True, exist_ok=True)
            pdf = pa.Table.from_pandas(self.data)

            if add_metadata:
                pdf = DynamicSiteData.set_metadata(
                    pdf,
                    col_meta=metadata['vars'],
                    tbl_meta=metadata['globals'])

            pq.write_table(pdf, filename)

    def save(self, filename, format='parquet', metadata=True, frequency=None):
        """Save the data into felyx for ingestion into the server. Data can
        be save in csv or parquet format

        Args:
            frequency: split the data into multiple files wrt the time of the
                data. Provided as a pandas frequency.
        """
        if format == 'csv':
            self.save_to_csv(filename, metadata=metadata, frequency=frequency)
        elif format == 'parquet':
            self.save_to_parquet(
                filename, add_metadata=metadata, frequency=frequency)

    @staticmethod
    def _read_csv_rows(reader):
        for row in reader:
            items = [_.strip() for _ in row]
            if 1 < len(items) and 'target' == items[0]:
                # Switch to another site, save previous work
                site_info = items
                position_flag = False
            elif 0 < len(items) and 'position' == items[0]:
                # Do nothing, this is just a flag
                position_flag = True
                continue
            elif 0 < len(items) and 'fields' == items[0]:
                # Do nothing, this is just a flag
                fields = items[1:]
                continue
            else:
                if not position_flag:
                    logging.warning(
                        'CSV row with data found but the position '
                        'delimiter row has not been found.')
                # This must be a lon;lat;time line
                yield [site_info[1], *items]

    @staticmethod
    def date_range(
            start: datetime.datetime,
            stop: datetime.datetime,
            frequency: str,
            as_interval: bool = True
    ) -> Union[pd.IntervalIndex, pd.DatetimeIndex]:
        """Compute the time range framing two dates wrt a fixed frequency.

        Used to split dynamic sites data in several periodic subsets.

        Args:
            start : the first date of the interval to frame
            stop : the last date of the interval to frame
            frequency: frequency code, as in pandas
            as_interval: return as a list of pandas intervals, otherwise as a
                date range if False
        """
        dleft = pd.date_range(start, periods=1, normalize=True, freq=frequency)
        if dleft[0] > start:
            dleft = dleft.shift(-1)
        start = dleft[0]

        dright = pd.date_range(stop, periods=1, normalize=True, freq=frequency)
        while dright[-1] < stop:
            dright = dright.shift(1)
        stop = dright[-1]

        if as_interval:
            return pd.interval_range(start, stop, freq=frequency)

        return pd.date_range(start, stop, freq=frequency)
