# encoding: utf-8
"""
parquet_insitu_datasource
--------------------------------------

Contains classe for retrieve In Situ data from Parquet

"""
import collections
import json
import logging
from datetime import datetime
from pathlib import Path
from typing import Dict, List, OrderedDict, Union

import numpy as np
import pandas as pd
import pyarrow as pa
from pandas import DataFrame, Timestamp

import felyx_processor.sites.insitu_datasource as insitu_datasource
from felyx_processor.sites.dynamic_site_data import DynamicSiteData
from felyx_processor.sites.insitu_datasource import DEFAULT_FILLVALS, InSituDataSource
from felyx_processor.utils.configuration import ParquetConfig

TIME_FMT = '%Y-%m-%d %H:%M:%S'

""" LOG """
LOG = logging.getLogger('felyx.processor')


class ParquetInSituDataSource(InSituDataSource):
    """Parquet In Situ Data Source"""

    def __init__(self,
                 yaml_config: ParquetConfig = None,
                 filename: Path = None):
        """
        Parquet Data Source constructor
        Args:
            yaml_config: the YAML Parquet configuration
            filename: For testing, the path to the Parquet file.
        """
        super().__init__()

        self.filename = filename
        self.__config = yaml_config

        if self.__config is None and self.filename is None:
            self.__config = ParquetConfig()

        self.dataframe = None

    def set_filename(self, filename: Path):
        """ Initialize the data source with a single file (for testing)
        Args:
            filename: The path to the Parquet file.
        """
        self.filename = filename

    @staticmethod
    def read_parquet_metadata(filename: Union[str, Path], df: pd.DataFrame) -> Dict[str, Dict]:
        """Return the metadata from a felyx parquet file

        Args:
                df: The Pandas data frame which contains the parquet data.
                filename: The Parquet file path to read the metadata.
            Returns:
                The metadata read from the Parquet file as a dictionary with the following format :
                {
                    'collection': str => The collection name.
                    'vars': dict      => The variables and their attributes read from the parquet file.
                    'globals': dict   => The global attributes of the parquet file.
                }
        """

        metadata = {'vars': {}, 'globals': {}}
        schema = pa.parquet.read_schema(filename)

        # read globals
        globmetadata = schema.metadata
        for k, v in globmetadata.items():
            k = k.decode('utf-8')
            v = v.decode('utf-8')
            if k == 'pandas':
                continue
            if k == 'collection':
                metadata['collection'] = v
                continue
            if k == 'felyx_globals':
                dict_felyx_globals = json.loads(v)
                if 'globals' in dict_felyx_globals:
                    metadata['globals'] = dict_felyx_globals['globals']
                if 'vars' in dict_felyx_globals:
                    metadata['vars'] = dict_felyx_globals['vars']
                if 'collection' in dict_felyx_globals:
                    metadata['collection'] = dict_felyx_globals['collection']
                continue
            else:
                metadata['globals'][k] = v

        # read var attrs
        for col in df.columns:
            field = schema.field(col)
            if field.metadata is not None:
                metadata['vars'][col] = {}
                for k, v in field.metadata.items():
                    k = k.decode('utf-8')
                    try:
                        v = json.loads(v.decode('utf-8'))
                    except json.decoder.JSONDecodeError:
                        v = v.decode('utf-8')
                    if k.startswith('PARQUET'):
                        continue
                    metadata['vars'][col][k] = v

        return metadata

    @staticmethod
    def _add_metadata(df: pd.DataFrame, filepath: Union[str, Path]):
        """Add to the dataframe the metadata from the source parquet file
        Args:
            df: The Pandas data frame to update.
            filepath: The file path to the metadata.
        """
        metadata = {'felyx_globals': {'vars': {}, 'globals': {}}}
        try:
            metadata['felyx_globals'].update(ParquetInSituDataSource.read_parquet_metadata(filepath, df))
        except KeyError:
            logging.warning('No metadata found in parquet file')

        # fill in metadata
        all_vars = set(metadata['felyx_globals']['vars'].keys()).union(
            df.columns
        )
        for v in all_vars:
            if v not in df:
                continue

            v_attrs = metadata['felyx_globals']['vars'].get(v, {})

            # @TODO Temporary (fix CMEMS format)
            if 'attrs' in v_attrs:
                v_attrs.pop('attrs')

            if 'dtype' not in v_attrs:
                v_attrs['dtype'] = df[v].dtype
            else:
                # from string to dtype
                v_attrs['dtype'] = np.dtype(v_attrs['dtype'])

            if '_FillValue' not in v_attrs:
                try:
                    v_attrs['_FillValue'] = DEFAULT_FILLVALS[v_attrs['dtype']]
                except KeyError:
                    v_attrs['_FillValue'] = np.ma.default_fill_value(
                        v_attrs['dtype'])
            metadata['felyx_globals']['vars'][v] = v_attrs

        # @TODO Fix : Remove empty attrs {} for id and time which does not belong to df.columns because they are index.
        # @TODO The empty attrs {} comes from some malformed parquet files => Fix this
        for v in metadata['felyx_globals']['vars']:
            v_attrs = metadata['felyx_globals']['vars'].get(v, {})
            if 'attrs' in v_attrs:
                v_attrs.pop('attrs')
            metadata['felyx_globals']['vars'][v] = v_attrs

        df.attrs.update(metadata)

    def read(self,
             collection_code: str,
             start: datetime,
             stop: datetime,
             columns_filter: List[str] = None):
        """
        Read Parquet file
        Args:
            collection_code: the collection code to read
            start: the start time
            stop: the end time
            columns_filter: the columns to read
        """

        date_filter = [[
            ('time', '>=', start.replace(tzinfo=None)),
            ('time', '<', stop.replace(tzinfo=None))]] if start and stop else None

        if self.filename is not None:
            # Read a single file
            df = pd.read_parquet(
                self.filename,
                engine='pyarrow',
                columns=columns_filter,
                filters=date_filter
            )
            files = [self.filename]
        else:
            # Compute the files to read from the time coverage
            files = self._compute_file_to_read(collection_code, start, stop)
            if len(files) == 0:
                LOG.info(f'No data found for {collection_code} in [{start}, '
                         f'{stop}] time range')
                return

            df = pd.concat(
                [pd.read_parquet(
                    file,
                    engine='pyarrow',
                    columns=columns_filter,
                    filters=date_filter)
                 for file in files])

        if df.empty:
            LOG.info(f'No data found for {collection_code} in [{start}, '
                     f'{stop}] time range')

        if ['id', 'time'] != df.index.names[:2]:
            df.sort_values(inplace=True, by=['id', 'time'], ignore_index=True)
            df = df.set_index(['id', 'time'])

        # convert sites to string
        # @TODO JFP
        # if self.dataframe.id.size > 0 and \
        #         type(self.dataframe.id.iloc[0]) is not str:
        #     self.dataframe[insitu_datasource.SITE_COLUMN] = \
        #         self.dataframe[insitu_datasource.SITE_COLUMN].str.decode(
        #             "utf-8")

        # add felyx metadata to dataframe
        self._add_metadata(df, files[0])

        return df

    def get_trajectories(self,
                         collection_code: str,
                         sites_codes: List[str],
                         start: datetime,
                         stop: datetime,
                         as_dataframe: bool = True
                         ) -> Union[Dict, DataFrame]:
        """
        Search trajectories in a collection and list of sites over a timespan

        Args:
            collection_code : the collection to search
            sites_codes : the list of sites codes to search
            start : the begin time coverage
            stop : the end time coverage
            as_dataframe : if True, returns a dataframe, if False, returns a
                dict

        Returns:
            if as_dataframe is set to True :
                a pandas dataframe with columns : time, site, lat, lon
            else :
                a dict containing the trajectories with key equals to site code
                    and value equal to a list of lat, lon, date
        """
        # empty result for request output type
        empty_res = {}
        if as_dataframe:
            empty_res = DataFrame(
                [], columns=insitu_datasource.LOCATION_COLUMNS)

        logging.info('Read {}'.format(collection_code))

        # Read the Parquet files into a Pandas dataset
        df = self.read(
            collection_code=collection_code, start=start, stop=stop,
            columns_filter=insitu_datasource.LOCATION_COLUMNS)

        if df is None:
            return empty_res

        # remove z level if present. lat/lon/time must be the same at any
        # z level
        if 'z' in df.index.names:
            df = df.xs(0, level='z')

        # Extract observations with temporal filtering
        df = self._apply_temporal_filter(df, start, stop)
        if df.empty:
            LOG.info(f'No data found for {collection_code} in [{start}, '
                     f'{stop}] time range')
            return empty_res

        # Then keep only the observations matching the sites codes
        if sites_codes is not None:
            if not isinstance(sites_codes, list):
                sites_codes = [sites_codes]
            df = df.loc[sites_codes]
            if df.empty is True:
                LOG.info(f'No data matching the sites : {sites_codes}')
                return empty_res

        # Add the collection code to dataframe metadata
        df._metadata = ['collection']

        df.collection = collection_code
        trajectories = df

        if as_dataframe is False:
            # Create a dictionary from the dataframe
            trajectories = dict()

            codes = set(df.index.get_level_values(0))
            for code in codes:
                locations = df.loc[code].reset_index()[[
                    'lon', 'lat', 'time']].to_dict(orient='split')['data']

                # Convert datetime64 to datetime
                trajectories[code] = [
                    [_[0], _[1], _[2].to_pydatetime()] for _ in locations]

        return trajectories

    def get_raws(self,
                 collection_code: str,
                 sites_codes: List[str],
                 start: datetime,
                 stop: datetime,
                 wkt_coords: List[List[float]] = None,
                 constraints_list: List[Dict[str, str]] = None,
                 measurements: List[str] = None,
                 as_dataframe: bool = True
                 ) -> Union[Dict, DataFrame]:
        """
            Search In situ data in a collection and list of sites over a
            timespan and spatial filter
            Args:
                collection_code : the collection to search
                sites_codes : the list of sites codes to search
                start : the begin time coverage
                stop : the end time coverage
                wkt_coords (optional) : the spatial filter defined by a list of
                coordinates of a polygon
                constraints_list (optional) : additional data filters
                measurements (optional) : list of physical parameters to extract
                as_dataframe : if True, returns a dataframe,
                if False, returns a dict
            Returns:
                if as_dataframe is set to True :
                    a pandas dataframe with following columns :
                    time, site, param1, param2, ...,paramN
                else :
                    a output dict formatted as following :
                        {'collection_code':
                            {'site_code':
                                {'times': ['YYYY-MM-DD HH:MM:SS'],
                                'metrics': {'measurement_1': [float_value],
                                    'measurement_2': [float_value], ...
                                'Longitude': [float_value],
                                'Latitude': [float_value],
                                'Time': ['YYYY-MM-DD HH::MM:SS']}
                                }
                            }
                        }
            Raises:
                NotImplementedError: If spatial or additional data filters
                parameters are set to True.
        """

        # empty result for request output type
        empty_res = dict()
        if as_dataframe:
            empty_res = DataFrame(
                [], columns=insitu_datasource.LOCATION_COLUMNS)

        # Check input parameters
        if wkt_coords is not None:
            raise NotImplementedError(
                'The spatial filter is not yet implemented for Parquet')

        if constraints_list is not None:
            raise NotImplementedError(
                'The additional data filters is not yet implemented for '
                'Parquet')

        if measurements is not None:
            columns_filter = [insitu_datasource.TIME_COLUMN,
                              insitu_datasource.SITE_COLUMN]
            columns_filter = columns_filter + measurements
        else:
            columns_filter = None

        # Read the Parquet files into a Pandas dataset
        df = self.read(
            collection_code=collection_code, start=start, stop=stop,
            columns_filter=columns_filter)

        # Extract observations with temporal filtering
        df = self._apply_temporal_filter(df, start, stop)
        if df.empty:
            LOG.info(f'No data found in [{start}, {stop}] time range')
            return empty_res

        # Then keep only the observations matching the sites codes

        if sites_codes is not None:
            df = df.loc[sites_codes]
            if df.empty is True:
                LOG.info(f'No data matching the sites : {sites_codes}')
                return empty_res

        # Rename the spatiotemporal columns and sort
        #df.sort_values(inplace=True, by=['id', 'time'])

        # Add the collection code to dataframe metadata
        df._metadata = ['collection']

        df.collection = collection_code

        if as_dataframe is False:
            # Create a dictionary from the dataframe
            # Group the observations by sites and create dict output
            output = dict()
            output[df.collection] = dict()

            for sdf in df.groupby(insitu_datasource.SITE_COLUMN):
                site_code = sdf[0]
                LOG.info(
                    'site : {} data : {}'.format(sdf[0], (sdf[1]).to_string()))

                # Fix: Convert the indexes to columns for dict conversion
                sdf[1].reset_index(inplace=True)
                times = list(
                    map(lambda x: datetime.utcfromtimestamp(x // 10 ** 9),
                        sdf[1][insitu_datasource.TIME_COLUMN].values.astype(
                            np.int64)))

                # If measurement filter is set, drop the time column
                if measurements is not None:
                    sdf[1].drop(insitu_datasource.TIME_COLUMN, axis=1,
                                inplace=True)
                # Rename time, lat, lon columns and remove site column
                sdf[1].drop(insitu_datasource.SITE_COLUMN, axis=1, inplace=True)
                raws_df = sdf[1].rename(
                    columns={insitu_datasource.TIME_COLUMN: 'Time',
                             insitu_datasource.LATITUDE_COLUMN: 'Latitude',
                             insitu_datasource.LONGITUDE_COLUMN: 'Longitude'})

                # Convert pandas dataframe to dict
                raws_df.replace(to_replace={np.nan: None}, inplace=True)
                site_data = {'metrics': raws_df.to_dict('list')}
                site_data['times'] = times
                if measurements is None:
                    site_data['metrics']['Time'] = times

                output[df.collection][site_code] = site_data

            return output

        index = df.index.names
        if index != ['id', 'time'] and index != ['id', 'time', 'z']:
            # the dataframe is reindexed
            # only authorized index vars are: 'id', 'time' (mandatory) and
            # 'depth' (optional). Other indexes are transformed to normal
            # columns
            new_index = ['id', 'time']
            if 'z' in index:
                new_index.append('z')

            df = df.reset_index(index).set_index(new_index)

        return df

    def _apply_temporal_filter(
            self,
            df: pd.DataFrame,
            start: datetime = None,
            stop: datetime = None
    ) -> pd.DataFrame:
        """
        Extract a part of a data frame using a temporal filter

        @TODO : Check this. This method seems useless since the temporal filter is applied in the read method.
        Args:
            start (optional) : the begin time coverage
            stop (optional) : the end time coverage
        Returns:
            the extracted data frame
        """
        if start is not None:
            start = start.strftime('%Y-%m-%d %H:%M:%S')
        if stop is not None:
            stop = stop.strftime('%Y-%m-%d %H:%M:%S')

        return df.xs(slice(start, stop), level='time', drop_level=False)

    def _compute_file_to_read(
            self,
            collection_code: str,
            start: datetime,
            stop: datetime
    ) -> List[Path]:
        """
        Compute the list of files of a collection to read from the time coverage
        Args:
            collection_code : the name of the collection
            start : the begin time coverage
            stop : the end time coverage
        Returns:
            a the file names list (absolute paths)
        """
        freq = self.__config.frequency
        if freq is None:
            if datetime.now().strftime(self.__config.filepath_pattern) != \
                    self.__config.filepath_pattern:
                raise ValueError('The parquet file patterns expects some '
                                 'date. You must define the frequency setting')
            # just one parquet file, no time splitting
            return [Path(self.__config.filepath_pattern)]

        date_list = DynamicSiteData.date_range(
            start, stop, frequency=freq, as_interval=False)

        # Compute file names
        file_list = list(set([
            Path(date.strftime(self.__config.filepath_pattern))
            for date in date_list]))

        # keep only existing files
        file_list = [_ for _ in file_list if _.exists()]
        file_list.sort()

        for file in file_list:
            LOG.info('Parquet file to read : {}'.format(file))

        if len(file_list) == 0:
            LOG.warning(
                f'There are no matching parquet file names for collection : '
                f'{collection_code}')

        return file_list

    def get_sites_list(
            self,
            collection_code: str,
            start: datetime = datetime(1950, 1, 1),
            stop: datetime = datetime.now()
    ) -> List[str]:
        """
        Retrieve the site list of a collection
        Args:
            collection_code : the name of the collection
            start (optional) : the begin time coverage
            stop (optional) : the end time coverage
        Returns:
            a the site list (names)
        """
        # Issue 106 : Compute the files to read from the time coverage
        files = self._compute_file_to_read(collection_code, start, stop)
        sites = pd.concat([
            pd.read_parquet(file, engine='pyarrow',
                            columns=[insitu_datasource.SITE_COLUMN])
            for file in files])

        # convert sites to string
        if sites.id.size > 0 and type(sites.id.iloc[0]) is not str:
            sites.id = sites.id.str.decode('utf-8')

        return sites[insitu_datasource.SITE_COLUMN].unique()

    def get_raws_count_by_day(self,
                              collection_code: str,
                              start: datetime,
                              stop: datetime,
                              ) -> OrderedDict[Timestamp, int]:
        """
        Search the raws number in a collection over a timespan and group them
        by day.
        Args:
            collection_code : the collection to search
            start : the begin time coverage
            stop : the end time coverage
        Returns:
            A dictionary with days as keys and raws count as values.
        Raises:
            FelyxProcessorError: If fails to extract raws count.
        """

        # Create the list of the days to compute
        time_range = pd.date_range(start=start, end=stop, freq='D')
        nb_raws = dict.fromkeys(time_range, 0)
        nb_raws = collections.OrderedDict(sorted(nb_raws.items()))

        # Read the Parquet files into a Pandas dataset
        df = self.read(collection_code=collection_code, start=start, stop=stop,
                  columns_filter=insitu_datasource.LOCATION_COLUMNS
                  )

        if df is None:
            return nb_raws

        # remove z level if present. lat/lon/time must be the same at any
        # z level
        if 'z' in df.index.names:
            df = df.xs(0, level='z')

        # Extract observations with temporal filtering
        df = self._apply_temporal_filter(df, start, stop)
        if df.empty:
            LOG.info(f'No data found for {collection_code} in [{start}, '
                     f'{stop}] time range')
            return nb_raws

        # Group the observations by day
        df = df.reset_index(). \
            set_index(insitu_datasource.TIME_COLUMN).resample('1D').count()
        # print('nb_measurements df : {}'.format(df))

        # Set the dict with the raws counts
        for index, row in df.iterrows():
            nb_raws[index] = row['id']

        return nb_raws
