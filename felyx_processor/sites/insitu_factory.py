# encoding: utf-8
"""
insitu_factory
--------------------------------------

Contains class for creating insitu datasource from various sources (ES or Parquet)

"""
from felyx_processor.sites.es_insitu_datasource import ESInSituDataSource
from felyx_processor.sites.insitu_datasource import InSituDataSource
from felyx_processor.sites.parquet_insitu_datasource import ParquetInSituDataSource
from felyx_processor.utils.configuration import (
    ElasticSearchConfig,
    FelyxProcessorConfig,
    FelyxSystemConfig,
    ParquetConfig,
    get_dynamic_data_access_config,
)
from felyx_processor.utils.exceptions import FelyxProcessorError


class InSituDataSourceFactory:
    @classmethod
    def get_insitu_datasource(
        cls,
        felyx_sys_config: FelyxSystemConfig,
        felyx_processor_config: FelyxProcessorConfig,
        collection_code: str,
    ) -> InSituDataSource:
        """Create a datasource for insitu data according to the collection
        configuration.

        Args:
           felyx_sys_config: felyx system configuration
           felyx_processor_config: felyx processor configuration
           collection_code : the name of the collection
        Returns:
           the insitu datasource
        Raises:
            FelyxProcessorError: If datasource cannot be instanciated.
        """
        config_model = get_dynamic_data_access_config(
            felyx_sys_config=felyx_sys_config,
            felyx_processor_config=felyx_processor_config,
            collection_code=collection_code
        )
        if isinstance(config_model, ElasticSearchConfig):
            return ESInSituDataSource(config_model)

        if isinstance(config_model, ParquetConfig):
            return ParquetInSituDataSource(config_model)

        raise FelyxProcessorError(
            'Cannot instanciate Insitu datasource for collection : {0}'.format(
                collection_code)
        )
