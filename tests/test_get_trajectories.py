import json
import time
from pathlib import Path

import pytest

from felyx_processor.main.get_trajectories import felyx_get_trajectories
from felyx_processor.main.import_parquet_insitu import felyx_import_parquet_insitu
from felyx_processor.sites.insitu_factory import InSituDataSourceFactory

TEST_INSITU_FILES = (
        'insitu/parquet/cmems_wave/daily/20180701_cmems_wave.parquet',
)

COLLECTION_ES = 'test_cmems_wave'
COLLECTION_PARQUET = 'cmems_wave_parquet_daily'


@pytest.fixture
def process_import_args(tmpdir, data_dir, felyx_processor_config_file):
    return [
        '--inputs', ','.join([str(data_dir / path) for path in
                              TEST_INSITU_FILES]),
        '--collection', COLLECTION_ES,
        '--configuration-file', str(felyx_processor_config_file),
    ]


@pytest.fixture
def import_parquet_insitu(felyx_sys_config_file, process_import_args):
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        felyx_import_parquet_insitu(process_import_args)
        time.sleep(10)

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0

@pytest.fixture
def process_args_es(tmpdir, felyx_processor_config_file):
    return [
        '--collection', COLLECTION_ES,
        '--sites', 'cw1300130,cw1300131',
        '--start', '2018-07-01T00:00:00',
        '--end', '2018-07-01T23:59:59',
        '--configuration-file', str(felyx_processor_config_file),
        '--output_path', str(tmpdir / 'trajectories_es.json')
    ]

@pytest.fixture
def process_args_parquet(tmpdir, felyx_processor_config_file):
    return [
        '--collection', COLLECTION_PARQUET,
        '--sites', 'cw1300130,cw1300131',
        '--start', '2018-07-01T00:00:00',
        '--end', '2018-07-01T23:59:59',
        '--configuration-file', str(felyx_processor_config_file),
        '--output_path', str(tmpdir / 'trajectories_parquet.json')
    ]

@pytest.fixture
def expected_res():
    return {'cw1300130': 24, 'cw1300131': 24}


def check_trajectories(json_file, sites, expected_res):

    assert json_file.exists()

    with open(json_file.resolve()) as file:
        data = json.load(file)
    assert len(data) > 0

    # Check sites
    sites_list = [k for (k, v) in data.items()]
    sites_args = sites.split(',')
    assert sorted(sites_list) == sorted(sites_args)

    # Check trajectories length
    for (k, v) in data.items():
        assert len(v) == expected_res[k]


def test_get_trajectories_es(import_parquet_insitu,
                             felyx_sys_config, felyx_processor_config,
                             process_args_es,
                             expected_res):
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        felyx_get_trajectories(process_args_es)

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0

    # check trajectories
    check_trajectories(json_file=Path(process_args_es[11]),
                       sites=process_args_es[3],
                       expected_res=expected_res)

    # delete test data
    collection = process_args_es[1]
    ds = InSituDataSourceFactory.get_insitu_datasource(
        felyx_sys_config=felyx_sys_config,
        felyx_processor_config=felyx_processor_config,
        collection_code=collection,
    )
    ds.delete_raws_index(collection)


def test_get_trajectories_parquet(process_args_parquet,
                                  expected_res):
    print(process_args_parquet)
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        felyx_get_trajectories(process_args_parquet)

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0

    # check trajectories
    check_trajectories(json_file=Path(process_args_parquet[11]),
                        sites=process_args_parquet[3],
                       expected_res=expected_res)
