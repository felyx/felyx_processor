import datetime
import time
from datetime import datetime
from pathlib import Path
import shapely.wkt

import dateutil
import numpy as np
from pydantic import ValidationError
import pytest

from felyx_processor.miniprod import miniprod_datasource, ChildMetadata
from felyx_processor.miniprod.miniprod_factory import MiniprodDataSourceFactory
from felyx_processor.utils.exceptions import FelyxProcessorError
from felyx_processor.utils.models import Indexing


@pytest.fixture(autouse=True)
def teardown(es_miniprod_datasource):
    yield
    # Remove miniprods index from elasticsearch
    es_miniprod_datasource.delete_miniprods_index()


@pytest.fixture
def register_miniprods(es_miniprod_datasource):

    children = [
        ChildMetadata(**{
            'id': '20211004000000_site1_dataset1.nc',
            'site': 'site1',
            'site_collection': 'collection1',
            'source': '20211004000000-file1.nc',
            'shape': shapely.wkt.loads(
                'POLYGON((-84.83 45.76,-84.83 45.88, -84.70 45.88,-84.70 '
                '45.76,-84.83 45.76))'),
            'slices': {'time': slice(1040, 1061, None)},
            'source_center_index': {'time': 1050},
            'coverage': 100.0,
            'date_created': datetime(2021, 10, 4, 0, 0, 0),
            'date_modified': datetime(2021, 10, 4, 0, 0, 0),
            'dataset': 'dataset1',
            'time_coverage_start': datetime(2021, 10, 4),
            'time_coverage_stop': datetime(2021, 10, 5),
            'source_time_coverage_start': datetime(2018, 7, 9, 1, 9, 15),
            'source_time_coverage_stop': datetime(2018, 7, 9,1,9,43),
            'dynamic_target_longitude': -84.77,
            'dynamic_target_latitude': 45.82,
            'dynamic_target_time': datetime(2021, 10, 4, 12, 0, 0),
            'dynamic_target_time_difference': 12777,
            'dynamic_target_distance': 7585,
        }),
        ChildMetadata(**{
            'id': '20211004000000_site2_dataset1.nc',
            'site': 'site2',
            'site_collection': 'collection2',
            'source': '20211005000000-file1.nc',
            'shape': shapely.wkt.loads(
                'POLYGON((-84.83 45.76,-84.83 45.88, -84.70 45.88,-84.70 '
                '45.76,-84.83 45.76))'),
            'slices': {'time': slice(1040, 1061, None)},
            'source_center_index': {'time': 1050},
            'coverage': 100.0,
            'date_created': datetime(2021, 10, 5, 0, 0, 0),
            'date_modified': datetime(2021, 10, 5, 0, 0, 0),
            'dataset': 'dataset1',
            'time_coverage_start': datetime(2021, 10, 5),
            'time_coverage_stop': datetime(2021, 10, 6),
            'source_time_coverage_start': datetime(2018, 7, 9, 1, 9, 15),
            'source_time_coverage_stop': datetime(2018, 7, 9,1,9,43),
            'dynamic_target_longitude': -84.77,
            'dynamic_target_latitude': 45.82,
            'dynamic_target_time': datetime(2021, 10, 5, 12, 0, 0),
            'dynamic_target_time_difference': 12777,
            'dynamic_target_distance': 7585,
        }),
        ChildMetadata(**{
            'id': '20211004000000_site1_dataset2.nc',
            'site': 'site1',
            'site_collection': 'collection1',
            'source': '20211005000000-file3.nc',
            'shape': shapely.wkt.loads(
                'POLYGON((-84.83 45.76,-84.83 45.88, -84.70 45.88,-84.70 '
                '45.76,-84.83 45.76))'),
            'slices': {'time': slice(1040, 1061, None)},
            'source_center_index': {'time': 1050},
            'coverage': 100.0,
            'date_created': datetime(2021, 10, 29, 0, 0, 0),
            'date_modified': datetime(2021, 10, 29, 0, 0, 0),
            'dataset': 'dataset2',
            'time_coverage_start': datetime(2021, 10, 29),
            'time_coverage_stop': datetime(2021, 10, 31),
            'source_time_coverage_start': datetime(2018, 7, 9, 1, 9, 15),
            'source_time_coverage_stop': datetime(2018, 7, 9,1,9,43),
            'dynamic_target_longitude': -84.77,
            'dynamic_target_latitude': 45.82,
            'dynamic_target_time': datetime(2021, 10, 29, 12, 0, 0),
            'dynamic_target_time_difference': 12777,
            'dynamic_target_distance': 7585,
        }),
        ChildMetadata(**{
            'id': '20211004000000_site2_dataset2.nc',
            'site': 'site2',
            'site_collection': 'collection1',
            'source': '20211005000000-file4.nc',
            'shape': shapely.wkt.loads(
                'POLYGON((-84.83 45.76,-84.83 45.88, -84.70 45.88,-84.70 '
                '45.76,-84.83 45.76))'),
            'slices': {'time': slice(1040, 1061, None)},
            'source_center_index': {'time': 1050},
            'coverage': 100.0,
            'date_created': datetime(2021, 10, 29, 0, 0, 0),
            'date_modified': datetime(2021, 10, 29, 0, 0, 0),
            'dataset': 'dataset2',
            'time_coverage_start': datetime(2021, 10, 1),
            'time_coverage_stop': datetime(2021, 10, 31),
            'source_time_coverage_start': datetime(2018, 7, 9, 1, 9, 15),
            'source_time_coverage_stop': datetime(2018, 7, 9,1,9,43),
            'dynamic_target_longitude': -84.77,
            'dynamic_target_latitude': 45.82,
            'dynamic_target_time': datetime(2021, 10, 29, 12, 0, 0),
            'dynamic_target_time_difference': 12777,
            'dynamic_target_distance': 7585,
        })
    ]

    es_miniprod_datasource.register_miniprods(children)

    # Wait the ES refresh between register and delete
    time.sleep(10)


def check_dataframe(dataframe, **kwargs):
    assert len(dataframe) > 0

    # Check the site list
    if 'site_codes' in kwargs and kwargs['site_codes'] is not None:
        site_list = list(set(dataframe[miniprod_datasource.SITE_COLUMN].values))
        print('SITE LIST : {}'.format(site_list))
        assert any(elem in site_list for elem in kwargs['site_codes'])

    # Check the dates
    time_col = 'dynamic_target_time'
    min_date = min(dataframe[time_col].values.astype(np.int64))
    min_date = datetime.utcfromtimestamp(min_date // 10 ** 9)
    assert (min_date >= kwargs['start'])

    max_date = max(dataframe[time_col].values.astype(np.int64))
    max_date = datetime.utcfromtimestamp(max_date // 10 ** 9)
    assert (max_date <= kwargs['stop'])

    print('TIME COVERAGE : {} -> {}'.format(min_date, max_date))

    # Check the expected results
    if kwargs['expected_res'] is not None:
        if kwargs['expected_res']['nb_rows'] is not None:
            assert(len(dataframe) == kwargs['expected_res']['nb_rows'])


@pytest.mark.parametrize(
    'datasets,site_collection, site_codes,start_date,end_date,expected_res,'
    'as_dataframe',
    [
        (
            ['dataset2'],
            None,
            ['site1', 'site2'],
            dateutil.parser.parse('2021-10-29 00:00:00'),
            dateutil.parser.parse('2021-10-30 00:00:00'),
            {'nb_rows': 2},
            False
        ),
        (
            ['dataset2'],
            None,
            ['site1', 'site2'],
            dateutil.parser.parse('2021-10-29 00:00:00'),
            dateutil.parser.parse('2021-10-30 00:00:00'),
            {'nb_rows': 2},
            True
        ),
    ]
)
def test_get_miniprods(register_miniprods,
                       felyx_sys_config, tmpdir,
                       datasets,
                       site_collection,
                       site_codes,
                       start_date, end_date, expected_res, as_dataframe):

    ds = MiniprodDataSourceFactory.get_miniprod_datasource(
        felyx_sys_config=felyx_sys_config,
        miniprod_root_dir=Path(tmpdir))

    res = ds.get_miniprods(
        datasets=datasets,
        site_collection=site_collection,
        sites_codes=site_codes,
        start=start_date,
        stop=end_date,
        filter_on_site_time=True,
        as_dataframe=as_dataframe)

    print('------------MINIPRODS : {}'.format(datasets))
    print('result len : {}'.format(len(res)))
    print('result : {}'.format(res))

    if as_dataframe:
        check_dataframe(
            res,
            datasets=datasets,
            site_codes=site_codes,
            start=start_date,
            stop=end_date,
            expected_res=expected_res)


@pytest.mark.parametrize(
    'datasets,site_collection,start_date,end_date,expected_res',
    [
        (
            ['dataset1'],
            'collection1',
            dateutil.parser.parse('2021-10-04 00:00:00'),
            dateutil.parser.parse('2021-10-04 12:00:00'),
            {'nb_rows': 1}
        ),
    ]
)
def test_get_miniprods_collection(register_miniprods,
                                  felyx_sys_config, tmpdir,
                                  datasets, site_collection,
                                  start_date, end_date, expected_res):

    ds = MiniprodDataSourceFactory.get_miniprod_datasource(
        felyx_sys_config=felyx_sys_config,
        miniprod_root_dir=Path(tmpdir))

    res = ds.get_miniprods(
        datasets=datasets,
        start=start_date,
        stop=end_date,
        filter_on_site_time=True,
        as_dataframe=True)

    print('------------MINIPRODS : {}'.format(datasets))
    print('dataframe len : {}'.format(len(res)))
    print('dataframe : {}'.format(res))

    check_dataframe(
            res,
            datasets=datasets,
            site_collection=site_collection,
            start=start_date,
            stop=end_date,
            expected_res=expected_res)

@pytest.mark.parametrize(
    'docs',
    [[
        ChildMetadata(**{
            'id': '20211004000000_site1_dataset3.nc',
            'site_collection': 'collection1',
            'site': 'site1',
            'source': '20211004000000-file1.nc',
            'shape': shapely.wkt.loads(
                'POLYGON((-84.83 45.76,-84.83 45.88, -84.70 45.88,-84.70 '
                '45.76,-84.83 45.76))'),
            'coverage': 100.0,
            'slices': {'time': slice(1040, 1061, None)},
            'source_center_index': {'time': 1050},
            'date_created': datetime(2021, 10, 4, 0, 0, 0),
            'date_modified': datetime(2021, 10, 4, 0, 0, 0),
            'dataset': 'dataset3',
            'time_coverage_start': datetime(2021, 10, 4),
            'time_coverage_stop': datetime(2021, 10, 5),
            'source_time_coverage_start': datetime(2018, 7, 9, 1, 9, 15),
            'source_time_coverage_stop': datetime(2018, 7, 9, 1, 9, 43),
            'dynamic_target_longitude': -84.77,
            'dynamic_target_latitude': 45.82,
            'dynamic_target_time': datetime(2021, 10, 4, 12, 0, 0)}),
        ChildMetadata(**{
            'id': '20211004000000_site2_dataset3.nc',
            'site_collection': 'collection1',
            'site': 'site2',
            'source': '20211005000000-file1.nc',
            'shape': shapely.wkt.loads(
                'POLYGON((-84.83 45.76,-84.83 45.88, -84.70 45.88,-84.70 '
                '45.76,-84.83 45.76))'),
            'coverage': 100.0,
            'slices': {'time': slice(1040, 1061, None)},
            'source_center_index': {'time': 1050},
            'date_created': datetime(2021, 10, 5, 0, 0, 0),
            'date_modified': datetime(2021, 10, 5, 0, 0, 0),
            'dataset': 'dataset3',
            'time_coverage_start': datetime(2021, 10, 5),
            'time_coverage_stop': datetime(2021, 10, 6),
            'source_time_coverage_start': datetime(2018, 7, 9, 1, 9, 15),
            'source_time_coverage_stop': datetime(2018, 7, 9, 1, 9, 43),
            'dynamic_target_longitude': -84.77,
            'dynamic_target_latitude': 45.82,
            'dynamic_target_time': datetime(2021, 10, 5, 12, 0, 0)
        })
    ]]
)
def test_register_miniprods(es_miniprod_datasource, docs):
    es_miniprod_datasource.register_miniprods(docs)


@pytest.mark.parametrize(
    'miniprod_ids,expected_res',
    [
        (
                ['20211004000000_site1_dataset1.nc',
                 '20211004000000_site2_dataset1.nc'],
                {'nb_docs': 2}
        ),
    ]
)
def test_delete_miniprods(register_miniprods, es_miniprod_datasource,
                          miniprod_ids, expected_res):

    nb_miniprods = es_miniprod_datasource.delete_miniprods(miniprod_ids)

    print('------------MINIPRODS DELETED : {}'.format(nb_miniprods))

    # Check the expected results
    if expected_res is not None:
        if expected_res['nb_docs'] is not None:
            assert (nb_miniprods == expected_res['nb_docs'])
        else:
            assert nb_miniprods > 0


@pytest.mark.parametrize(
    'miniprod_ids,expected',
    [
        (
            ['unknown_id'],
            FelyxProcessorError
        ),
    ]
)
def test_delete_miniprods_error(register_miniprods, es_miniprod_datasource,
                                miniprod_ids, expected):
    with pytest.raises(expected):
        es_miniprod_datasource.delete_miniprods(miniprod_ids)


@pytest.mark.parametrize(
    'datasets,site_codes,start_date,end_date,expected_res',
    [
        (
                ['ESACCI-SEASTATE-L2P-SWH-Sentinel-3_A_PLRM'],
                None,
                dateutil.parser.parse('2018-07-09 00:00:00'),
                dateutil.parser.parse('2018-07-09 12:00:00'),
                {'nb_rows': 54}
        ),
        (
                ['ESACCI-SEASTATE-L2P-SWH-Sentinel-3_A_PLRM'],
                None,
                dateutil.parser.parse('2018-07-09 00:00:00'),
                dateutil.parser.parse('2018-07-10 12:00:00'),
                {'nb_rows': 85}
        ),
        (
                ['ESACCI-SEASTATE-L2P-SWH-Sentinel-3_A_PLRM'],
                ['cw41044','cw46001'],
                dateutil.parser.parse('2018-07-09 00:00:00'),
                dateutil.parser.parse('2018-07-10 12:00:00'),
                {'nb_rows': 4}
        ),
        (
            ['OSTIA-ESACCI-L4-v02.1'],
            ['cci51301', 'cci51302'],
            dateutil.parser.parse('2006-12-30 00:00:00'),
            dateutil.parser.parse('2007-01-01 00:00:00'),
            {'nb_rows': 2}
        ),
        (
                ['OSTIA-ESACCI-L4-v02.1'],
                None,
                dateutil.parser.parse('2006-12-01 00:00:00'),
                dateutil.parser.parse('2007-01-01 00:00:00'),
                {'nb_rows': 33}
        ),
        (
                ['ESACCI-SEASTATE-L2P-SWH-Sentinel-3_A_PLRM',
                 'OSTIA-ESACCI-L4-v02.1'],
                None,
                dateutil.parser.parse('2006-12-30 00:00:00'),
                dateutil.parser.parse('2018-07-09 12:00:00'),
                {'nb_rows': 65}
        ),
    ]
)
def test_get_miniprods_from_manifest(felyx_sys_config, data_dir,
                                     datasets, site_codes,
                                     start_date, end_date, expected_res):

    ds = MiniprodDataSourceFactory.get_miniprod_datasource(
        felyx_sys_config=felyx_sys_config,
        source=Indexing.manifest,
        manifest_root_dir=data_dir / 'manifest')

    res = ds.get_miniprods(
        datasets=datasets,
        sites_codes=site_codes,
        start=start_date,
        stop=end_date,
        filter_on_site_time=True,
        as_dataframe=False)

    print('------------MINIPRODS DICT : {}'.format(datasets))
    print('dict len : {}'.format(len(res)))
    print('dict : {}'.format(res))

    res = ds.get_miniprods(
        datasets=datasets,
        sites_codes=site_codes,
        start=start_date,
        stop=end_date,
        filter_on_site_time=True,
        as_dataframe=True)

    print('------------MINIPRODS DATAFRAME : {}'.format(datasets))
    print('dataframe len : {}'.format(len(res)))
    print('dataframe : {}'.format(res))
    check_dataframe(
        res,
        datasets=datasets,
        site_codes=site_codes,
        start=start_date,
        stop=end_date,
        expected_res=expected_res)
