import shutil
from pathlib import Path

import pandas as pd
import pytest
import xarray as xr

from felyx_processor.main.matchups import assemble_matchups
from felyx_processor.main.miniprod import felyx_miniprod, parse_args

CONFIG_FILE = Path('resources/test_jason_3/felyx_data.yaml')

TEST_EO_FILES = Path('eo/cmems/swh/2018/07')
TEST_DATASET = 'CMEMS-L2P-SWH-Jason-3'


@pytest.fixture(scope='module')
def output_dir(tmpdir_factory):
    return tmpdir_factory.mktemp('output')


@pytest.fixture(scope='module')
def config_file(test_dir):
    return test_dir / CONFIG_FILE


@pytest.fixture
def test_eo_files(data_dir):
    root = data_dir / TEST_EO_FILES

    eo_files = [str(_) for _ in root.glob('*.nc')]
    eo_files.sort()

    return eo_files


@pytest.fixture(scope='module')
def extraction_args(output_dir, config_file):

    Path(output_dir / 'data').mkdir(parents=True, exist_ok=True)
    Path(output_dir / 'manifests').mkdir(parents=True, exist_ok=True)

    return ['-c', str(config_file),
            '--dataset-id', TEST_DATASET,
            '--child-product-dir', str(output_dir / 'data'),
            '--manifest-dir', str(output_dir / 'manifests'),
            '--indexing',  'manifest',
            '--inputs'
            ]

@pytest.mark.dependency()
def test_extraction_defaults(extraction_args, output_dir, test_eo_files):
    # process all files at once
    args = extraction_args.copy()
    del args[4:8]
    args.extend([test_eo_files[0]])

    with pytest.raises(SystemExit) as pytest_wrapped_e:
        felyx_miniprod(args)

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0

    manifest_dir = parse_args(args)[2].options.manifest_dir / TEST_DATASET

    # verify number of manifests
    assert len(list(manifest_dir.glob('*/*/*.manifest'))) == 1


@pytest.mark.dependency()
def test_extraction(extraction_args, output_dir, test_eo_files):
    # process all files at once
    args = extraction_args.copy()
    args.extend(test_eo_files)

    with pytest.raises(SystemExit) as pytest_wrapped_e:
        felyx_miniprod(args)

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0

    manifest_dir = Path(output_dir) / Path('manifests') / TEST_DATASET

    # verify number of manifests
    assert len(list(manifest_dir.glob('*/*/*.manifest'))) == len(test_eo_files)


@pytest.mark.dependency(depends=['test_extraction'])
def test_extraction_sequential(
        extraction_args, output_dir, test_eo_files):

    manifest_dir = Path(output_dir) / Path('manifests') / TEST_DATASET

    # nb of matchups
    count1 = {}
    for m in list(manifest_dir.glob('*/*/*.manifest')):
        count1[str(m)] = sum(1 for _ in open(m))

    # remove previous manifests
    shutil.rmtree(manifest_dir)

    # process each file independently
    for f in test_eo_files:
        args = extraction_args.copy()
        args.append(f)
        with pytest.raises(SystemExit) as pytest_wrapped_e:
            felyx_miniprod(args)

        assert pytest_wrapped_e.type == SystemExit
        assert pytest_wrapped_e.value.code == 0

        manifest = list(manifest_dir.glob(
            f'*/*/{Path(f).name + ".manifest"}'))[0]
        assert manifest.exists()

        # verify same nb of match-ups than in previous step
        count = sum(1 for _ in open(manifest))
        assert count == count1[str(manifest)]

    # verify colocations
    subset_files = (
            Path(output_dir) / Path('data') / TEST_DATASET).glob(
        '*/*/*.nc')
    for f in subset_files:
        dst = xr.open_dataset(f)
        assert dst.attrs['__time_difference'] <= 360
        assert dst.attrs['__distance'] <= 100000.


@pytest.fixture
def assemble_args(output_dir, tmpdir, config_file, request):
    Path(tmpdir / 'mdb').mkdir(parents=True, exist_ok=True)

    args = [
        '--matchup-dataset',
        'CMEMS-L2P-SWH-Jason-3__cmems_wave',
        '--configuration', str(config_file),
        '--child-product-dir', str(Path(output_dir) / Path('data/')),
        '--output-dir', str(tmpdir / 'mdb'),
        '--manifest-dir', str(Path(output_dir) / Path('manifests/')),
        '--start', '2018-07-01',
        '--end', '2018-08-01']
    args.extend(request.param)

    return args


@pytest.mark.dependency(depends=['test_extraction_sequential'])
@pytest.mark.parametrize('config,expected', [(CONFIG_FILE, 1)])
@pytest.mark.parametrize(
    'assemble_args', [
        ['--from-manifests', '--extract-from-source']], indirect=True)
def test_assemble(assemble_args, output_dir, config, expected, tmpdir,
                  test_dir):

    assemble_args[3] = str(test_dir / config)

    with pytest.raises(SystemExit) as pytest_wrapped_e:
        assemble_matchups(assemble_args)

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0

    # verify number of output files
    mdb_files = list((Path(tmpdir) / Path('mdb/2018')).glob('*.nc'))
    assert len(mdb_files) == expected

    # compare nb of matchups from manifests vs nb of matchups from mdb
    manifests = list((Path(output_dir) / Path(f'manifests/{TEST_DATASET}')).glob(
        '*/*/*.manifest'))

    manifest_df = pd.concat(
        [pd.read_json(f, lines=True) for f in manifests])

    # Count the unique matchups (site,time)
    nb_matchups = manifest_df.groupby(['site', 'dynamic_target_time']).ngroups
    # print('Number of matchups from manifests : {}'.format(nb_matchups))

    for f in mdb_files:
        dst = xr.open_dataset(f)
        assert dst.sizes['obs'] == nb_matchups
        dst.close()
