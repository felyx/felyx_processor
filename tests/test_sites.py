import datetime
import json
import time
from datetime import datetime, timedelta
from pathlib import Path
from typing import Union

import numpy as np
import pandas as pd
import pyarrow.parquet as pq
import pytest

from felyx_processor.main.import_parquet_insitu import felyx_import_parquet_insitu
from felyx_processor.sites import insitu_datasource
from felyx_processor.sites.dynamic_site_data import from_parquet
from felyx_processor.sites.insitu_datasource import DEFAULT_FILLVALS
from felyx_processor.sites.insitu_factory import InSituDataSourceFactory
from felyx_processor.sites.parquet_insitu_datasource import ParquetInSituDataSource
from felyx_processor.utils.exceptions import FelyxProcessorError

TEST_INSITU_FILES = (
   'insitu/parquet/cmems_wave/daily/20180701_cmems_wave.parquet',
)
COLLECTION_ES = 'test_cmems_wave'


@pytest.fixture(scope='module')
def process_import_args(data_dir, felyx_processor_config_file):
    return [
        '--inputs', ','.join([str(data_dir / path)
                              for path in TEST_INSITU_FILES]),
        '--collection', COLLECTION_ES,
        '--configuration-file', str(felyx_processor_config_file),
    ]


@pytest.fixture(scope='module', autouse=True)
def import_parquet_insitu(process_import_args,
                          felyx_processor_config, felyx_sys_config):
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        felyx_import_parquet_insitu(process_import_args)
        time.sleep(10)

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0

    yield

    # Remove insitu data from elasticsearch
    ds = InSituDataSourceFactory.get_insitu_datasource(
        felyx_sys_config=felyx_sys_config,
        felyx_processor_config=felyx_processor_config,
        collection_code=COLLECTION_ES,
    )
    ds.delete_raws_index(COLLECTION_ES)


def check_dataframe(dataframe, **kwargs):
    assert len(dataframe) > 0

    # Check the site list
    dataframe = dataframe.reset_index()

    site_list = list(set(dataframe[insitu_datasource.SITE_COLUMN].values))
    if kwargs['site_codes'] is not None:
        assert any(elem in site_list for elem in kwargs['site_codes'])

    # Check the dates
    min_date = dataframe[insitu_datasource.TIME_COLUMN].min()
    assert (min_date >= kwargs['start'])

    max_date = dataframe[insitu_datasource.TIME_COLUMN].max()
    assert (max_date <= kwargs['stop'])

    # Check the expected results
    if kwargs['expected_res'] is not None:
        if kwargs['expected_res']['nb_rows'] is not None:
            assert(len(dataframe) == kwargs['expected_res']['nb_rows'])


@pytest.mark.parametrize(
    'collection_code, site_codes,start_date, end_date, measurements, '
    'expected_res',
    [
        (
                'test_cmems_wave',
                ['cw1300130'],
                datetime(2018, 7, 1),
                datetime(2018, 7, 1, 23, 59, 59),
                ['swh', 'swh_rejection_flag'],
                {'nb_rows': 24}
        ),
        (
                'test4syn',
                ['ts4sy9900019'],
                datetime(2018, 2, 26),
                datetime(2018, 2, 26, 23, 59, 59),
                None,
                {'nb_rows': 1}
        ),
        (
                'cmems_wave_parquet',
                ['cci51301'],
                datetime(2006, 1, 1),
                datetime(2006, 1, 1, 12, 0, 0),
                ['VHM0'],
                {'nb_rows': 24}
        )
    ]
)
def test_sites_insitu_raws(felyx_sys_config, felyx_processor_config,
                           collection_code, site_codes,
                           start_date, end_date, measurements, expected_res):
    ds = InSituDataSourceFactory.get_insitu_datasource(
        felyx_sys_config=felyx_sys_config,
        felyx_processor_config=felyx_processor_config,
        collection_code=collection_code,
    )

    res = ds.get_raws(
        collection_code=collection_code,
        sites_codes=site_codes,
        start=start_date,
        stop=end_date,
        measurements=measurements
    )

    print('------------RAWS : {}'.format(collection_code))
    print('dataframe len : {}'.format(len(res)))
    print('dataframe : {}'.format(res))

    check_dataframe(res, site_codes=site_codes, start=start_date, stop=end_date,
                    expected_res=expected_res)


@pytest.mark.parametrize(
    'collection_code,site_codes,start_date,end_date',
    [
        (
            'test_cmems_wave',
            None,
            datetime(2018, 7, 1),
            datetime(2018, 7, 1, 12, 0, 0)
        ),
        (
            'test4syn',
            ['ts4sy9900019', 'ts4sy9900020'],
            datetime(2018, 2, 26),
            datetime(2018, 2, 26, 23, 59, 59)
        ),
    ]
)
def test_sites_insitu_trajectories(felyx_sys_config, felyx_processor_config,
                            collection_code, site_codes, start_date, end_date):

    ds = InSituDataSourceFactory.get_insitu_datasource(
        felyx_sys_config=felyx_sys_config,
        felyx_processor_config=felyx_processor_config,
        collection_code=collection_code,
    )

    res = ds.get_trajectories(
        collection_code=collection_code,
        sites_codes=site_codes,
        start=start_date,
        stop=end_date
    )

    print('------------TRAJECTORIES : {}'.format(collection_code))
    print('dataframe len : {}'.format(len(res)))
    print('dataframe : {}'.format(res))

    check_dataframe(res, site_codes=site_codes, start=start_date, stop=end_date,
                    expected_res=None)


@pytest.mark.parametrize(
    'collection_code,site_codes,start_date,end_date',
    [
        (
            'test_cmems_wave',
            None,
            datetime(2018, 7, 1),
            datetime(2018, 7, 1, 6, 0, 0)
        ),
        (
            'test4syn',
            ['ts4sy9900019', 'ts4sy9900020'],
            datetime(2018, 2, 26),
            datetime(2018, 2, 26, 23, 59, 59)
        ),
    ]
)
def test_sites_insitu_trajectories_dict(felyx_sys_config,
                                        felyx_processor_config,
                                        collection_code, site_codes,
                                        start_date, end_date):

    ds = InSituDataSourceFactory.get_insitu_datasource(
        felyx_sys_config=felyx_sys_config,
        felyx_processor_config=felyx_processor_config,
        collection_code=collection_code,
    )

    res = ds.get_trajectories(
        collection_code=collection_code,
        sites_codes=site_codes,
        start=start_date,
        stop=end_date,
        as_dataframe=False
    )

    print('------------TRAJECTORIES : {}'.format(collection_code))
    print('dict : {}'.format(res))


@pytest.mark.parametrize(
    'collection_code,expected',
    [
        (
                'test_cmems_wave_fake',
                FelyxProcessorError
        ),
    ]
)
def test_sites_insitu_config_error(felyx_sys_config, felyx_processor_config,
                                   collection_code, expected):
    with pytest.raises(expected):
        InSituDataSourceFactory.get_insitu_datasource(
            felyx_sys_config=felyx_sys_config,
            felyx_processor_config=felyx_processor_config,
            collection_code=collection_code,
        )


def add_metadata_old(df: pd.DataFrame, filepath: Union[str, Path]):
    """Add to the dataframe the metadata from the source parquet file
    Args:
        df: The Pandas data frame to update.
        filepath: The file path to the metadata.
    """
    metadata = {'felyx_globals': {'vars': {}, 'globals': {}}}
    try:
        metadata['felyx_globals'].update(
            json.loads(pq.read_schema(filepath).metadata[b'felyx_globals']))
    except KeyError:
        print('No metadata found in parquet file')

    # fill in metadata
    all_vars = set(metadata['felyx_globals']['vars'].keys()).union(
        df.columns
    )
    for v in all_vars:
        if v not in df:
            continue

        v_attrs = metadata['felyx_globals']['vars'].get(v, {})

        # @TODO Temporary (fix CMEMS format)
        if 'attrs' in v_attrs:
            v_attrs.pop('attrs')

        if 'dtype' not in v_attrs:
            v_attrs['dtype'] = df[v].dtype
        else:
            # from string to dtype
            v_attrs['dtype'] = np.dtype(v_attrs['dtype'])

        if '_FillValue' not in v_attrs:
            try:
                v_attrs['_FillValue'] = DEFAULT_FILLVALS[v_attrs['dtype']]
            except KeyError:
                v_attrs['_FillValue'] = np.ma.default_fill_value(
                    v_attrs['dtype'])
        metadata['felyx_globals']['vars'][v] = v_attrs

    # @TODO Fix : Remove empty attrs {} for id and time which does not belong to df.columns because they are index.
    # @TODO The empty attrs {} comes from some malformed parquet files => Fix this
    for v in metadata['felyx_globals']['vars']:
        v_attrs = metadata['felyx_globals']['vars'].get(v, {})
        if 'attrs' in v_attrs:
            v_attrs.pop('attrs')
        metadata['felyx_globals']['vars'][v] = v_attrs

    df.attrs.update(metadata)


@pytest.mark.parametrize(
    'parquet_file',
    [
        (
            'insitu/parquet/trusted/20210915_trusted.parquet'
        ),
        (
            'insitu/parquet/synthetic/test4syn_20180226.parquet'
        ),
        (
            'insitu/parquet/cmems_wave/cmems_wave_20060101.parquet'
        ),
        (
            'insitu/parquet/cmems_wave/daily/20180701_cmems_wave.parquet'
        ),
    ]
)
def test_parquet_metadata_old_format(felyx_sys_config, felyx_processor_config, parquet_file, data_dir):

    parquet_file_path = str(data_dir / parquet_file)

    # Read the parquet file : single parquet file reading does not need to specify collection and time range
    ds = ParquetInSituDataSource(filename=Path(parquet_file_path))
    df_new = ds.read(collection_code='dummy', start=datetime(2000, 1, 1), stop=datetime(2000, 1, 1))
    print('------------ NEW METADATA ---------------')
    print(df_new.attrs)

    # Read the parquet file with old metadata method
    df_old = pd.read_parquet(parquet_file_path, engine='pyarrow')
    if ['id', 'time'] != df_old.index.names[:2]:
        df_old.sort_values(inplace=True, by=['id', 'time'], ignore_index=True)
        df_old = df_old.set_index(['id', 'time'])
    add_metadata_old(df_old, parquet_file_path)
    print('------------ OLD METADATA ----------------')
    print(df_old.attrs)

    # Compare metadata
    assert df_old.attrs == df_new.attrs


@pytest.mark.parametrize(
    'parquet_file',
    [
        (
            'insitu/parquet/cmems_wave/new_format/cmems_wave_20180701.parquet'
        ),
    ]
)
def test_parquet_metadata_new_format(felyx_sys_config, felyx_processor_config, parquet_file, data_dir):

    parquet_file_path = str(data_dir / parquet_file)

    # Read the parquet file : single parquet file reading does not need to specify collection and time range
    ds = ParquetInSituDataSource(filename=Path(parquet_file_path))
    df_new = ds.read(collection_code='dummy', start=datetime(2000, 1, 1), stop=datetime(2000, 1, 1))
    print('------------ METADATA FROM PARQUET DS ------------')
    print('vars : {}'.format(df_new.attrs['felyx_globals']['vars']))
    print('globals : {}'.format(df_new.attrs['felyx_globals']['globals']))
    print('collection : {}'.format(df_new.attrs['felyx_globals']['collection']))

    # Read from parquet with insitu data
    (dataset_id, odf, metadata_vars, metadata_globals) = from_parquet(Path(parquet_file_path))

    print('------------ METADATA FROM INSITU DATA -----------')
    print('vars : {}'.format(metadata_vars))
    print('globals : {}'.format(metadata_globals))
    print('collection : {}'.format(dataset_id))

    assert dataset_id == df_new.attrs['felyx_globals']['collection']
    assert metadata_globals == df_new.attrs['felyx_globals']['globals']

    # Compare metadata with ignoring some added attributes after reading files
    ignore_keys = ['dtype', '_FillValue', 'attrs']
    d1_filtered = {}
    for k1, v1 in metadata_vars.items():
        d1_filtered[k1] = {k: v for k, v in v1.items() if k not in ignore_keys}
    d2_filtered = {}
    for k1, v1 in df_new.attrs['felyx_globals']['vars'].items():
        d2_filtered[k1] = {k: v for k, v in v1.items() if k not in ignore_keys}

    assert d1_filtered == d2_filtered


@pytest.mark.parametrize(
    'parquet_file',
    [
        'insitu/parquet/trusted/20210915_trusted.parquet',
        #'/data/felyx/insitu/parquet/issue_felyx_processor_41/cmems_wave_20180101000000.parquet'
        #'/data/felyx/insitu/parquet/issue_felyx_processor_41/cmems_wave_20220202000000.parquet'
        #'/home/criou/PycharmProjects/felyx_processor/tests/data/insitu/parquet/cmems_drifter/20210915000000_cmems_drifter.parquet'
    ]
)
def test_sites_insitu_raws_in_window(felyx_sys_config, felyx_processor_config, parquet_file, data_dir):

    # Read the parquet file : single parquet file reading does not need to specify collection and time range
    parquet_file_path = str(data_dir / parquet_file)
    ds = ParquetInSituDataSource(filename=Path(parquet_file_path))

    win = ds.get_raws_in_window(
        'cmems_wave',
        ['1120696', '1133348'],
        #['cw41044', 'cw46114'],
        #['cw6100198'],
        #['6202652', '2101689'],
        np.array([datetime(2021, 9, 15), datetime(2021, 9, 15)]),
        #np.array([datetime(2018, 1, 1), datetime(2018, 1, 1)]),
        #np.array([datetime(2022, 2, 2), datetime(2022, 2, 3)]),
        time_window=timedelta(minutes=360),
        prefix='cmems_',
        as_dataset=True
    )

    print('dimension obs : {}'.format(win.obs))
    print('dimension site_obs : {}'.format(win.site_obs))
    print('dimension cmems_z : {}'.format(win.cmems__z))

    assert(win.obs.size == 2)
    assert(win.site_obs.size == 6)
    assert(win.cmems__z.size == 2)

    print('var cmems__time : {}'.format(win.cmems__time))
    print('var cmems__TEMP : {}'.format(win.cmems__TEMP))

    assert(win.cmems__time.dims == ('obs', 'site_obs'))
    assert(win.cmems__TEMP.dims == ('obs', 'site_obs', 'cmems__z'))

