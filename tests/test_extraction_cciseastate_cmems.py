import os
import shutil
from pathlib import Path

import pytest
import xarray as xr

from felyx_processor.main.matchups import assemble_matchups
from felyx_processor.main.miniprod import felyx_miniprod

CONFIG_FILE = Path('resources/test_cciseastate/felyx_data.yaml')
CONFIG_FILE_SUB = Path('resources/test_cciseastate/felyx_data_subproducts.yaml')

SYSTEM_FILE = Path('resources/test_cciseastate/felyx_system.yaml')

TEST_EO_FILES = Path('eo/cciseastate-s3a-sral/')
TEST_DATASET = 'ESACCI-SEASTATE-L2P-SWH-Sentinel-3_A_PLRM'


@pytest.fixture(scope='module')
def output_dir(tmpdir_factory):
    return tmpdir_factory.mktemp('output')


@pytest.fixture(scope='module')
def config_file(test_dir):
    return test_dir / CONFIG_FILE

@pytest.fixture(scope='module')
def config_sys_file(test_dir):
    return test_dir / SYSTEM_FILE

@pytest.fixture
def test_eo_files(data_dir):
    return data_dir / TEST_EO_FILES


@pytest.fixture(scope='module')
def extraction_args(output_dir, config_file, config_sys_file):
    os.environ['FELYX_SYS_CFG'] = str(config_sys_file)

    Path(output_dir / 'data').mkdir(parents=True, exist_ok=True)
    Path(output_dir / 'manifests').mkdir(parents=True, exist_ok=True)

    return ['-c', str(config_file),
            '--dataset-id', TEST_DATASET,
            '--child-product-dir', str(output_dir / 'data'),
            '--manifest-dir', str(output_dir / 'manifests'),
            '--create-miniprod',
            '--indexing', 'elasticsearch', 'manifest',
            '--inputs'
            ]


@pytest.fixture()
def extraction(extraction_args, test_eo_files):

    eo_files = [str(_) for _ in test_eo_files.glob('*.nc')]
    eo_files.sort()

    # process all files at once
    args = extraction_args.copy()
    args.extend(eo_files)

    with pytest.raises(SystemExit) as pytest_wrapped_e:
        felyx_miniprod(args)

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0


@pytest.mark.dependency(name='test_extraction')
def test_extraction(extraction, extraction_args, output_dir, test_eo_files):
    eo_files = [str(_) for _ in test_eo_files.glob('*.nc')]
    eo_files.sort()

    manifest_dir = Path(output_dir) / Path('manifests') / TEST_DATASET

    # verify number of manifests
    assert len(list(manifest_dir.glob('*/*/*.manifest'))) == len(eo_files)

    # nb of matchups
    count1 = {}
    for m in list(manifest_dir.glob('*/*/*.manifest')):
        count1[str(m)] = sum(1 for _ in open(m))

    # remove previous manifests
    shutil.rmtree(manifest_dir)

    # process each file independently
    for f in eo_files:
        args = extraction_args.copy()
        args.append(f)
        with pytest.raises(SystemExit) as pytest_wrapped_e:
            felyx_miniprod(args)

        assert pytest_wrapped_e.type == SystemExit
        assert pytest_wrapped_e.value.code == 0

        manifest = list(manifest_dir.glob(
            f'*/*/{Path(f).name + ".manifest"}'))[0]
        assert manifest.exists()

        # verify same nb of match-ups than in previous step
        count = sum(1 for _ in open(manifest))
        assert count == count1[str(manifest)]

    # verify colocations
    subset_files = (
            Path(output_dir) / Path('data') / TEST_DATASET).glob(
        '*/*/*.nc')
    for f in subset_files:
        dst = xr.open_dataset(f)
        assert dst.attrs['__time_difference'] <= 360
        assert dst.attrs['__distance'] <= 100000.


@pytest.fixture
def assemble_args(output_dir, tmpdir, config_file, request):
    Path(tmpdir / 'mdb').mkdir(parents=True, exist_ok=True)

    args = [
        '--matchup-dataset',
        'ESACCI-SEASTATE-L2P-SWH-Sentinel-3_A_PLRM__cmems_wave',
        '--configuration', str(config_file),
        '--child-product-dir', str(Path(output_dir) / Path('data/')),
        '--output-dir', str(tmpdir / 'mdb'),
        '--manifest-dir', str(Path(output_dir) / Path('manifests/')),
        '--start', '2018-07-01',
        '--end', '2018-08-01']
    args.extend(request.param)

    return args


@pytest.mark.dependency(depends=['test_extraction'])
@pytest.mark.parametrize('config,expected', [(CONFIG_FILE, 1),
                                             (CONFIG_FILE_SUB, 2)])
@pytest.mark.parametrize(
    'assemble_args', [
        [],
        ['--from-manifests', '--extract-from-source']], indirect=True)
def test_assemble(assemble_args, output_dir, config, expected, tmpdir,
                  test_dir):

    assemble_args[3] = str(test_dir / config)

    with pytest.raises(SystemExit) as pytest_wrapped_e:
        assemble_matchups(assemble_args)

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0

    # verify number of output files
    mdb_files = (Path(tmpdir) / Path('mdb/2018')).glob('*.nc')
    assert len(list(mdb_files)) == expected

    print("TOTO",
          Path(tmpdir), list(mdb_files),
          expected, len(list(mdb_files)) == expected)

    # compare nb of subsets vs nb of matchups
    count_subsets = len(list(
        (Path(output_dir) / Path('data') / TEST_DATASET).glob(
            '*/*/*.nc')
    ))
    for f in mdb_files:
        print(f)
        dst = xr.open_dataset(f)
        assert dst.sizes['obs'] == count_subsets
        dst.close()

        # check excluded variables
        assert 'cciseastate_swh_model' in dst.variables
        assert 'cciseastate_surface_air_temperature' not in dst.variables
