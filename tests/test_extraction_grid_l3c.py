from pathlib import Path
from subprocess import check_output

import numpy as np
import pandas as pd
import pytest
import xarray as xr
import yaml

import cerbere

from felyx_processor.main.matchups import assemble_matchups
from felyx_processor.main.miniprod import felyx_miniprod

CONFIG_FILE = Path('resources/test_l3/felyx_data.yaml')

TEST_EO_FILES = [
        Path('eo/l3/20220607-IFR-L3C_GHRSST-SSTsubskin-ODYSSEA'
             '-GLOB_010_adjusted-v2.0-fv1.0.nc'),
        ]
TEST_DATASET = 'SST_GLO_SST_L3S_NRT_OBSERVATIONS_010_010'


def extract_dummy_sites_all(fname, along=100, across=100):
    """Create dummy in situ measurements over a EO granule"""
    feature = cerbere.open_feature(fname, feature='Grid', reader='GHRSST')

    lons = feature.ds.cb.longitude.broadcast_like(
        feature.ds.cb.time)[::across, ::along].data.flatten()
    lats = feature.ds.cb.latitude.broadcast_like(
        feature.ds.cb.time)[::across, ::along].data.flatten()
    times = feature.ds.time[::across, ::along].data.flatten()

    sid = np.array(
        ['dummy{:06d}'.format(_) for _ in range(len(lats))], dtype=object)

    valid = ~np.isnat(times)

    print(f'COUNT: valid: {valid.sum()}  over: {len(times)}')

    return pd.DataFrame(data={
        'id': sid[valid],
        'lon': lons[valid],
        'lat': lats[valid],
        'time': times[valid],
    })


def extract_dummy_sites(fname, along=100, across=100):
    """Create dummy in situ measurements over a EO granule"""
    df = extract_dummy_sites_all(fname, along, across)
    return df[~np.isnat(df.time)]


@pytest.fixture(scope='module')
def output_dir(tmpdir_factory):
    return tmpdir_factory.mktemp('output')


@pytest.fixture(scope='module')
def output_dir_no_child(tmpdir_factory):
    return tmpdir_factory.mktemp('output_no_child')


@pytest.fixture(params=TEST_EO_FILES)
def eo_file(data_dir, request):
    return data_dir / request.param


@pytest.fixture
def insitu_parquet(output_dir, eo_file):
    parquet_fname = output_dir.join(
        'insitu', 'synthetic_insitu_{}.parquet'.format(eo_file.name))

    if parquet_fname.exists():
        df = pd.read_parquet(parquet_fname)
    else:
        output_dir.mkdir('insitu')
        df = extract_dummy_sites(eo_file)
        df.to_parquet(parquet_fname)

    return parquet_fname, df.count(axis=0)['time']


@pytest.fixture(scope='module')
def insitu_parquet_all_in_one(output_dir, data_dir):
    df = pd.concat([extract_dummy_sites(data_dir / eo_file)
                    for eo_file in TEST_EO_FILES])

    (Path(output_dir) / Path('insitu')).mkdir(parents=True, exist_ok=True)
    parquet_fname = Path(output_dir) / 'insitu/synthetic_insitu.parquet'
    df.to_parquet(parquet_fname)

    return parquet_fname, df.count(axis=0)['time']


def config_file(output_dir, test_dir, insitu):
    fname = output_dir.join('felyx_data.yaml')

    with open(test_dir / CONFIG_FILE, 'r') as ftemplate:
        config = yaml.safe_load(ftemplate)

    config['SiteCollections']['test4syn']['parquet_config'][
        'filepath_pattern'] = str(insitu)

    with open(fname, 'w') as cf:
        yaml.dump(config, cf)

    return fname


def extraction_args(output_dir, eo_file):

    (Path(output_dir) / Path('data')).mkdir(parents=True, exist_ok=True)
    (Path(output_dir) / Path('manifests')).mkdir(parents=True, exist_ok=True)

    return ['-c', str(output_dir.join('felyx_data.yaml')),
            '--dataset-id', TEST_DATASET,
            '--create-miniprod',
            '--child-product-dir', f'{output_dir}/data/',
            '--manifest-dir', f'{output_dir}/manifests/',
            '--inputs', str(eo_file)]


def test_extraction_single_grid_no_child(
        output_dir_no_child,
        test_dir,
        insitu_parquet,
        eo_file
):
    """create manifest only, no netcdf child products"""

    # prepare in situ data
    insitu_file, count = insitu_parquet

    # processing args
    args = extraction_args(output_dir_no_child, eo_file)
    args.extend(args)

    # create configuration file
    config_file(output_dir_no_child, test_dir, insitu_file)

    with pytest.raises(SystemExit) as pytest_wrapped_e:
        felyx_miniprod(args)

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0

    # check manifest was created
    manifests = (
        (Path(output_dir_no_child) / Path(f'manifests/{TEST_DATASET}'))
    ).glob('*/*/*.manifest')
    match = [m for m in list(manifests) if eo_file.name in m.name]
    assert len(match) == 1

    # check the number of matchups is equivalent to the number of virtual
    # dynamic sites
    manifest = match[0]
    assert int(check_output(['wc', '-l', manifest]).split()[0]) == count


@pytest.mark.dependency(name='test_extraction_single_grid')
def test_extraction_single_grid(
        output_dir, test_dir, insitu_parquet, eo_file):
    # prepare in situ data
    insitu_file, count = insitu_parquet

    # processing args - miniprod output
    args = extraction_args(output_dir, eo_file)
    args.extend(['--create-miniprod'])

    # create configuration file
    config_file(output_dir, test_dir, insitu_file)

    with pytest.raises(SystemExit) as pytest_wrapped_e:
        felyx_miniprod(args)

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0

    # check manifest was created
    manifests = (
        (Path(output_dir) / Path(f'manifests/{TEST_DATASET}'))
    ).glob('*/*/*.manifest')
    match = [m for m in list(manifests) if eo_file.name in m.name]
    assert len(match) == 1

    # check the number of matchups is equivalent to the number of virtual
    # dynamic sites
    manifest = match[0]
    assert int(check_output(['wc', '-l', manifest]).split()[0]) == count

    # check size of child products
    miniprods = list((Path(output_dir) / Path(f'data/')).glob(
        f'*/*/{TEST_DATASET}/*/*/*.nc'))
    assert len(miniprods) == 216
    for f in miniprods:
        print(f)
        dst = xr.open_dataset(f)
        # check correct child product size
        assert dst.sizes['lon'] == 21
        assert dst.sizes['lat'] == 21
        # check no padding (circular grid)
        assert dst['lon'].size == dst['lon'].count()
        assert dst['lon'].min() >= -180
        assert dst['lon'].max() <= 180
        assert dst['lat'].size == dst['lat'].count()


@pytest.fixture
def assemble_args(output_dir):
    yield [
        '--matchup-dataset',
        'SST_GLO_SST_L3S_NRT_OBSERVATIONS_010_010_test4dyn',
        '--configuration',  str(output_dir.join('felyx_data.yaml')),
        '--output-dir', str(output_dir / 'mdb'),
        '--child-product-dir', str(Path(output_dir) / Path('data/')),
        '--manifest-dir', str(Path(output_dir) / Path('manifests/')),
        '--from-manifests', '--extract-from-source',
        '--start', '2022-06-06',
        '--end', '2022-06-08'
    ]


@pytest.mark.dependency(depends=['test_extraction_single_grid'])
def test_assemble(
        assemble_args, insitu_parquet_all_in_one, output_dir, test_dir):

    # create configuration file
    insitu_file, count = insitu_parquet_all_in_one
    config_file(output_dir, test_dir, insitu_file)

    # MDB output folder
    (Path(output_dir) / Path('mdb')).mkdir(parents=True)

    with pytest.raises(SystemExit) as pytest_wrapped_e:
        assemble_matchups(assemble_args)

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0

    # compare nb of subsets vs nb of matchups
    count_subsets = len(list(
        (Path(output_dir) / Path('data') / TEST_DATASET).glob('*/*/*.nc')))
    print(count_subsets)

