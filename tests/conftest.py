import logging
import os
from pathlib import Path

import pandas as pd
from _pytest.fixtures import fixture
from pydantic_settings import BaseSettings

from felyx_processor.main.install import felyx_install
from felyx_processor.metrics.es_metrics_datasource import ESMetricsDataSource
from felyx_processor.miniprod.es_miniprod_datasource import ESMiniprodDataSource
from felyx_processor.utils.configuration import FelyxProcessorConfig, FelyxSystemConfig


class FelyxTestConfig(BaseSettings):
    force_reinstall_root_dir: bool = False
    logging_level: str = 'ERROR'

    class Config:  # noqa: D106, WPS431
        env_prefix = 'felyx_processor_tu_'
        validate_default = True
        validate_assignment = True


def setup_logging():
    es_logger = logging.getLogger('elasticsearch')
    es_logger.setLevel(logging.WARNING)

    felyx_logger = logging.getLogger('felyx.processor')
    felyx_logger.setLevel(logging.WARNING)


def setup_pandas():
    pd.set_option('display.max_rows', None)
    pd.set_option('display.max_columns', None)


@fixture(scope='session', autouse=True)
def setup_felyx(test_dir, felyx_sys_config, felyx_test_config):
    setup_logging()
    felyx_install(
        felyx_sys_config,
        force_reinstall=felyx_test_config.force_reinstall_root_dir)
    setup_pandas()

@fixture(scope='session')
def test_dir():
    """Path to resource dir fixture.

    Returns:
        Path to resource dir
    """
    return Path(__file__).parent


@fixture(scope='session')
def data_dir(test_dir) -> Path:
    """Path to data dir fixture.

    Returns:
        Path to data dir
    """
    return test_dir / 'data'


@fixture(scope='session')
def felyx_root_dir(test_dir):
    """Path to resource dir fixture.

    Returns:
        Path to resource dir
    """
    return test_dir / '_build'


@fixture(scope='session')
def felyx_sys_config_file(test_dir) -> Path:
    return test_dir / 'config_sys_felyx.yaml'


@fixture(scope='session')
def felyx_sys_config(felyx_root_dir, felyx_sys_config_file) -> FelyxSystemConfig:
    os.environ['FELYX_PROCESSOR_ROOT_DIR'] = str(felyx_root_dir.absolute())
    os.environ['FELYX_SYS_CFG'] = str(felyx_sys_config_file.absolute())
    return FelyxSystemConfig.from_yaml_file(felyx_sys_config_file)


@fixture(scope='session')
def felyx_test_config() -> FelyxTestConfig:
    return FelyxTestConfig()





@fixture(scope='session')
def resources_dir(test_dir) -> Path:
    """Path to resource dir fixture.

    Returns:
        Path to resource dir
    """
    return test_dir / 'resources'


@fixture()
def es_metrics_datasource(felyx_sys_config):
    ds = ESMetricsDataSource(felyx_sys_config.elasticsearch)
    yield ds
    ds.delete_metrics_index()


@fixture()
def es_miniprod_datasource(felyx_sys_config):
    ds = ESMiniprodDataSource(felyx_sys_config.elasticsearch)
    yield ds
    ds.delete_miniprods_index()


@fixture(scope='session')
def felyx_processor_config_file(test_dir):
    return test_dir / 'config_test_datasources.yaml'


@fixture(scope='session')
def felyx_processor_config(felyx_processor_config_file) -> FelyxProcessorConfig:
    return FelyxProcessorConfig.from_yaml_file(felyx_processor_config_file)
