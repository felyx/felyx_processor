import time
from datetime import datetime

import pytest

from felyx_processor.main.import_parquet_insitu import felyx_import_parquet_insitu
from felyx_processor.sites.insitu_factory import InSituDataSourceFactory

INPUT_DATA = {'test_trusted': (
        'insitu/parquet/trusted/20210915_trusted.parquet',
        'insitu/parquet/trusted/20210916_trusted.parquet'),
    'test_cmems_wave': (
        'insitu/parquet/cmems_wave/daily/20180705_cmems_wave.parquet',)}


@pytest.fixture
def process_args_cmems_wave(data_dir, felyx_processor_config_file):
    return [
        '--inputs',
        ','.join([str(data_dir / path) for path in INPUT_DATA['test_cmems_wave']]),
        '--collection', 'test_cmems_wave',
        '--configuration-file', str(felyx_processor_config_file),
    ]


def test_import_parquet_insitu_cmems_wave(process_args_cmems_wave):
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        felyx_import_parquet_insitu(process_args_cmems_wave)
    time.sleep(10)

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0


@pytest.fixture
def process_args_trusted(data_dir, felyx_processor_config_file):
    return [
        '--inputs',
        ','.join([str(data_dir / path) for path in INPUT_DATA['test_trusted']]),
        '--collection', 'test_trusted',
        '--configuration-file', str(felyx_processor_config_file),
    ]


def test_import_parquet_insitu_trusted(process_args_trusted):
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        felyx_import_parquet_insitu(process_args_trusted)
    time.sleep(10)

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0


@pytest.mark.parametrize(
    'collection_code,site_code,start_date,end_date',
    [
        (
                'test_cmems_wave',
                'cw1300130',
                datetime(2018, 7, 5),
                datetime(2018, 7, 6, 0, 0, 0)
        ),
        (
                'test_trusted',
                '1120662',
                datetime(2021, 9, 15),
                datetime(2021, 9, 16, 0, 0, 0)
        ),
    ]
)
def test_sites_insitu_delete_raws(collection_code, site_code,
                                  start_date, end_date,
                                  felyx_sys_config, felyx_processor_config):
    ds = InSituDataSourceFactory.get_insitu_datasource(
        felyx_sys_config=felyx_sys_config,
        felyx_processor_config=felyx_processor_config,
        collection_code=collection_code,
    )

    ids = ds.get_raws_ids(
        collection_code=collection_code,
        site_code=site_code,
        start=start_date,
        stop=end_date
    )

    nb_deleted_docs = ds.delete_site_raws(collection_code=collection_code,
                                          site_code=site_code,
                                          start_time=start_date,
                                          end_time=end_date)

    print(
        '-------DELETE RAWS : collection : {} site : {} nb docs: {} '
        'nb deleted : {}'
        .format(collection_code, site_code, len(ids), nb_deleted_docs))

    assert (len(ids) == nb_deleted_docs)

    # delete test data
    ds.delete_raws_index(collection_code)
