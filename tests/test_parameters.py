# -*- encoding: utf-8 -*-

"""
@author <>
@date 2021-09-09
"""

import argparse

from felyx_processor.main import (
    get_coverage,
    get_raws,
    get_trajectories,
    import_parquet_insitu,
    matchups,
    metric,
    miniprod,
    register_metrics,
    register_miniprods,
    wipe_miniprods,
)

MAPPING_PARAMS = {
    'GetCoverageParams': get_coverage.parse_args,
    'GetRawsParams': get_raws.parse_args,
    'GetTrajectoriesParams': get_trajectories.parse_args,
    'ImportParquetInSituParams': import_parquet_insitu.parse_args,
    'MatchupsParams': matchups.parse_args,
    'MetricsParams': metric.parse_args,
    'MiniprodsParams': miniprod.parse_args,
    'RegisterMiniprodsParams': register_miniprods.parse_args,
    'RegisterMetricsParams': register_metrics.parse_args,
    'WipeMiniprodsParams': wipe_miniprods.parse_args
}


def parse_args(cli_args):
    """"""

    extraction_parser = argparse.ArgumentParser(description=str('Test parameters'))

    extraction_parser.add_argument(
        '--parameter-class', type=str, default=None,
        help='parameter class to test')

    args_param, args = extraction_parser.parse_known_args(cli_args)

    try:
        if args_param.parameter_class in MAPPING_PARAMS:
            params = MAPPING_PARAMS[args_param.parameter_class](args)
        else:
            print(f'Existing parameter class : {list(MAPPING_PARAMS.keys())}')
            raise Exception('Parameter Class not found')
    except FileNotFoundError:
        raise FileNotFoundError(
            f'{args.configuration_file} configuration file does not exists')

    return params


if __name__ == '__main__':

    print('======================= Felyx arguments ===========================')
    print(parse_args(cli_args=None))
