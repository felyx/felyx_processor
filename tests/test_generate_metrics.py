import json
from pathlib import Path
from subprocess import check_output

import pytest

from felyx_processor.main.metric import felyx_metric
from felyx_processor.main.miniprod import felyx_miniprod

CONFIG_FILE = Path('resources/test_cciseastate/felyx_data_metrics.yaml')

TEST_EO_FILES = [
    Path('eo/cciseastate-s3a-sral/ESACCI-SEASTATE-L2P-SWH-Sentinel-3_A-20180709T075312-fv01.nc'),
    Path('eo/cciseastate-s3a-sral/ESACCI-SEASTATE-L2P-SWH-Sentinel-3_A-20180709T152738-fv01.nc'),
    Path('eo/cciseastate-s3a-sral/ESACCI-SEASTATE-L2P-SWH-Sentinel-3_A-20180709T212105-fv01.nc'),
    Path('eo/cciseastate-s3a-sral/ESACCI-SEASTATE-L2P-SWH-Sentinel-3_A-20180709T235234-fv01.nc'),
]

TEST_COLLECTION = 'cmems_wave'
TEST_DATASET = 'ESACCI-SEASTATE-L2P-SWH-Sentinel-3_A_PLRM'
TEST_MDATASET = 'ESACCI-SEASTATE-L2P-SWH-Sentinel-3_A_PLRM__cmems_wave'

EXTRACT_NB = 10

@pytest.fixture(scope='module')
def config_file(test_dir):
    return test_dir / CONFIG_FILE

@pytest.fixture(scope='module')
def output_dir(tmpdir_factory):
    return tmpdir_factory.mktemp('output')


@pytest.fixture(scope='module')
def extraction_args(output_dir, data_dir, config_file):

    (Path(output_dir) / Path('data')).mkdir(parents=True)
    (Path(output_dir) / Path('manifests')).mkdir(parents=True)

    return ['-c', str(config_file),
            '--create-miniprod',
            '--inputs', ','.join([str(data_dir / path) for path in TEST_EO_FILES]),
            '--dataset-id', TEST_DATASET,
            '--child-product-dir', f'{output_dir}/data/',
            '--manifest-dir', f'{output_dir}/manifests/'
           ]


@pytest.mark.dependency()
def test_extraction_for_metrics(
        output_dir, extraction_args):
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        felyx_miniprod(extraction_args)

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0

    # check manifest was created
    manifests = (Path(output_dir) / Path(f'manifests/{TEST_DATASET}')).glob(
        '*/*/*.manifest'
    )
    filenames = [path.name + '.manifest' for path in TEST_EO_FILES]

    print('filenames : {}'.format(filenames))
    match = [m for m in list(manifests) if m.name in filenames]
    assert len(match) == len(TEST_EO_FILES)

    # check manifest was created
    miniprods = list(
        (Path(output_dir) / Path(f'data/{TEST_COLLECTION}')).glob(
            f'*/{TEST_DATASET}/*/*/*_ESACCI-SEASTATE-L2P-SWH-Sentinel'
            f'-3_A_PLRM.nc'))

    assert len(miniprods) == EXTRACT_NB

    count_extraction = sum(
        [int(check_output(['wc', '-l', _]).split()[0]) for _ in match])
    assert count_extraction == len(miniprods)


@pytest.fixture(scope='module')
def miniprod_list(output_dir):

    miniprod_list = list((Path(output_dir) / Path(
        f'data/{TEST_COLLECTION}')).glob(
        f'*/{TEST_DATASET}/*/*/*_ESACCI-SEASTATE-L2P-SWH-Sentinel-3_A_PLRM.nc')
    )
    return miniprod_list


@pytest.fixture(scope='module')
def metric_miniprod_args(output_dir, miniprod_list, config_file):

    (Path(output_dir) / Path('metrics_miniprod')).mkdir(parents=True)
    miniprods = [str(_) for _ in miniprod_list]

    return ['-c', str(config_file),
            '--mdataset', TEST_MDATASET,
            '--inputs', ','.join(miniprods),
            '--metric-dir', f'{output_dir}/metrics_miniprod/'
           ]


@pytest.mark.dependency(depends=['test_extraction_for_metrics'])
def test_metrics_from_miniprod(
        output_dir, metric_miniprod_args, miniprod_list):

    with pytest.raises(SystemExit) as pytest_wrapped_e:
        felyx_metric(metric_miniprod_args)

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0

    # check metrics was created
    metrics = list((Path(output_dir) / Path(
        f'metrics_miniprod/{TEST_COLLECTION}/{TEST_DATASET}')).glob(
        f'*/*/*/*.json')
    )
    filenames = [_.name + '.json' for _ in miniprod_list]
    match = [m for m in metrics if m.name in filenames]
    assert len(match) == len(filenames)
    assert len(match) == EXTRACT_NB


@pytest.fixture(scope='module')
def manifest_list(output_dir):

    manifests_list = list((
            Path(output_dir) / Path(f'manifests/{TEST_DATASET}')).glob(
        '*/*/*.manifest')
    )
    return manifests_list


@pytest.fixture(scope='module')
def metric_manifest_args(output_dir, manifest_list, config_file):

    (Path(output_dir) / Path('metrics_manifest')).mkdir(parents=True)
    manifests = [str(_) for _ in manifest_list]

    return ['-c', str(config_file),
            '--inputs', ','.join(manifests),
            '--mdataset', TEST_MDATASET,
            '--metric-dir', f'{output_dir}/metrics_manifest/',
            '--from-manifests'
           ]


@pytest.mark.dependency(depends=['test_metrics_from_miniprod'])
def test_metrics_from_manifests(
        output_dir, metric_manifest_args, manifest_list):

    with pytest.raises(SystemExit) as pytest_wrapped_e:
        felyx_metric(metric_manifest_args)

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0

    # check  miniprods source from manifest
    filenames = []
    for manifest in manifest_list:
        with open(manifest) as fd:
            for miniprod in fd.readlines():
                filenames.append(json.loads(miniprod)['id'] + '.json')
    # check metrics was created
    metrics = (Path(output_dir) / Path(
        f'metrics_manifest/{TEST_COLLECTION}/{TEST_DATASET}')).glob(
        '*/*/*/*.json'
    )
    match = [m for m in list(metrics) if m.name in filenames]
    assert len(match) == len(filenames)
    assert len(match) == EXTRACT_NB
