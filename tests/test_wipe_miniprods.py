import os
import time
from datetime import datetime

import pytest
import shapely.wkt

from felyx_processor.main.wipe_miniprods import wipe_miniprods
from felyx_processor.miniprod import ChildMetadata


@pytest.fixture(autouse=True)
def teardown(es_metrics_datasource, es_miniprod_datasource):
    yield
    # Remove metrics and miniprods indices from elasticsearch
    es_metrics_datasource.delete_metrics_index()
    es_miniprod_datasource.delete_miniprods_index()


@pytest.fixture
def register_miniprods(es_miniprod_datasource):
    # create miniprods in ES
    children = [
        ChildMetadata(**{
            'id': 'to_delete_site1_dataset1.nc',
            'site': 'site1',
            'site_collection': 'collection1',
            'source': '20211004000000-file1.nc',
            'shape': shapely.wkt.loads(
                'POLYGON((-84.83 45.76,-84.83 45.88,-84.70 '
                '45.88,-84.70 45.76,-84.83 45.76))'),
            'slices': {'time': slice(1040, 1061, None)},
            'source_center_index': {'time': 1050},
            'coverage': 100.0,
            'date_created': datetime(2021, 10, 4, 0, 0, 0),
            'date_modified': datetime(2021, 10, 4, 0, 0, 0),
            'dataset': 'dataset1_to_delete',
            'time_coverage_start': datetime(2021,10,4),
            'time_coverage_stop': datetime(2021,10,5),
            'source_time_coverage_start': datetime(2021,10,4, 1,9,15),
            'source_time_coverage_stop': datetime(2021, 10,4, 1,59, 43),
            'dynamic_target_longitude': -84.77,
            'dynamic_target_latitude': 45.82,
            'dynamic_target_time': datetime(2021, 10, 4, 12, 0, 0),
            'dynamic_target_time_difference': 12777,
            'dynamic_target_distance': 7585,
        }),
        ChildMetadata(**{
            'id': 'to_delete_site2_dataset1.nc',
            'site': 'site2',
            'site_collection': 'collection1',
            'source': '20211005000000-file1.nc',
            'shape': shapely.wkt.loads(
                'POLYGON((-84.83 45.76,-84.83 45.88,-84.70 '
                '45.88,-84.70 45.76,-84.83 45.76))'),
            'slices': {'time': slice(1040, 1061, None)},
            'source_center_index': {'time': 1050},
            'coverage': 100.0,
            'date_created': datetime(2021, 10, 5, 0, 0, 0),
            'date_modified': datetime(2021, 10, 5, 0, 0, 0),
            'dataset': 'dataset1_to_delete',
            'time_coverage_start': datetime(2021,10,5),
            'time_coverage_stop': datetime(2021,10,6),
            'source_time_coverage_start': datetime(2021,10,5, 1,9,15),
            'source_time_coverage_stop': datetime(2021, 10,5, 1,59, 43),
            'dynamic_target_longitude': -84.77,
            'dynamic_target_latitude': 45.82,
            'dynamic_target_time': datetime(2021, 10, 5, 12, 0, 0),
            'dynamic_target_time_difference': 12777,
            'dynamic_target_distance': 7585,
        })
    ]

    es_miniprod_datasource.register_miniprods(children)

    # Wait the ES refresh between register and delete
    time.sleep(10)

@pytest.fixture
def register_metrics(es_metrics_datasource):
    # create metrics in ES
    docs = [
            {
                'shape': 'POLYGON((-23 11.4,-23 11.5,-22.9 11.5,-22.9 11.4, -23 11.4))',
                'time_coverage': ['2021-10-05 01:00:00',
                                  '2021-10-05 23:00:00'],
                'site': 'site1',
                'dataset': 'dataset1',
                'collection': 'collection1',
                'data': [
                    {
                        'name': 'param1',
                        'value': 298.8
                    }
                ],
                'miniprod': 'to_delete_site1_dataset1.nc'
            },
            {
                'shape': 'POLYGON((-23 11.4,-23 11.5,-22.9 11.5,-22.9 11.4, -23 11.4))',
                'time_coverage': ['2021-10-05 01:00:00',
                                  '2021-10-05 23:00:00'],
                'site': 'site2',
                'dataset': 'dataset1',
                'collection': 'collection1',
                'data': [
                    {
                        'name': 'param1',
                        'value': 298.8
                    }
                ],
                'miniprod': 'to_delete_site2_dataset1.nc'
            }
    ]

    es_metrics_datasource.register_metrics(docs)

    # Wait the ES refresh between register and delete
    time.sleep(10)


@pytest.fixture
def process_args(felyx_processor_config_file, felyx_sys_config_file):
    os.environ['FELYX_SYS_CFG'] = str(felyx_sys_config_file)
    return [
        '--dataset_id', 'dataset1_to_delete',
        '--configuration-file', str(felyx_processor_config_file),
        '--start', '2021-10-01',
        '--end', '2021-10-31',
        '--no-prompt']


def test_wipe_miniprods(register_miniprods, register_metrics, process_args):
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        wipe_miniprods(process_args)

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0
