from pathlib import Path

import pandas as pd
import pytest

from felyx_processor.main.dynamicsites import convert
from felyx_processor.sites.dynamic_site_data import DynamicSiteData, from_csv, from_parquet

DYNAMIC_SITES_CSV = 'insitu/csv/cmems_wave/cmems_wave_20180701.csv'

DYNAMIC_SITES_CSV_NULL = \
    'insitu/csv/cmems_drifter/data/20210915_drifter_global_data.csv'

DYNAMIC_SITES_PARQUET = 'insitu/parquet/argos/part.102.parquet'


@pytest.fixture
def convert_args(tmpdir, data_dir, request):
    dir = data_dir / request.param
    metadata = dir.parent / 'metadata.json'
    output = tmpdir / dir.name.replace('.csv', '.parquet')

    yield [str(dir), 'cmems_wave', '-m', str(metadata), '-p',
           str(output)]


@pytest.fixture
def convert_args_multifile(tmpdir, data_dir, request):
    dir = data_dir / request.param
    metadata = dir.parent / 'metadata.json'
    output = tmpdir / f'%Y%m%d_cmems_wave.parquet'

    yield [str(dir), 'cmems_wave', '-m', str(metadata), '-p',
           str(output), '--duration', 'D']


@pytest.mark.parametrize('convert_args', [DYNAMIC_SITES_CSV],
                         indirect=True)
def test_convert_as_single_file(convert_args):
    output_file = convert_args[-1]
    metadata_file = convert_args[3]

    with pytest.raises(SystemExit) as pytest_wrapped_e:
        convert(convert_args)

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0

    print('Output parquet file: ', Path(output_file))
    assert(Path(output_file).exists())

    # read from CSV
    idf = from_csv(convert_args[0], 'cmems_wave', metadata_file=metadata_file)

    # read from parquet
    (dataset_id, odf, metadata_vars, metadata_globals) = from_parquet(Path(output_file))

    assert(idf.data.size == odf.size)
    assert(dataset_id == 'cmems_wave')


@pytest.mark.parametrize(
    'convert_args_multifile', [DYNAMIC_SITES_CSV], indirect=True)
def test_convert_as_multi_file(convert_args_multifile, tmpdir):
    convert_args_multifile[5]
    metadata_file = convert_args_multifile[3]

    with pytest.raises(SystemExit) as pytest_wrapped_e:
        convert(convert_args_multifile)

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0

    files = list(Path(tmpdir).glob('*.parquet'))
    print('Output parquet file: ', files)
    assert(len(files) == 31)

    # read from CSV
    idf = from_csv(convert_args_multifile[0], 'cmems_wave',
                   metadata_file=metadata_file)

    # read from parquet
    count = 0
    for f in files:
        (dataset_id, odf, metadata_vars, metadata_globals) = from_parquet(f)
        count += odf.size

    assert(idf.data.size == count)
    assert(dataset_id == 'cmems_wave')


def test_convert_parquet_to_parquet(tmpdir, data_dir):
    """Test reading parquet data and converting to felyx format"""
    df = pd.read_parquet(
        data_dir / DYNAMIC_SITES_PARQUET
    ).reset_index().set_index(['id', 'time'])

    dst = DynamicSiteData('argos', df, {}, {})

    assert type(dst.data.index.levels[0][0]) is str
    print(tmpdir)
    dst.save_to_parquet(
        tmpdir / '%Y/%m/%Y%m%d_argos.parquet',
        add_metadata=False,
        frequency='1D'
    )


@pytest.fixture
def convert_args_gen(tmpdir, data_dir, request):
    dir = data_dir / request.param[0]
    metadata = dir.parent / 'metadata.json'
    output = tmpdir / request.param[2]

    yield [str(dir), request.param[1], '-m', str(metadata), '-p',
           str(output), *request.param[3:]]


@pytest.mark.parametrize(
    'convert_args_gen',
    [[DYNAMIC_SITES_CSV_NULL, 'cmems_drifter',
     '%Y%m%d%H%M%S_cmems_drifter.parquet', '-d', '6h']],
    indirect=True)
def test_convert_as_single_file_with_nulls(convert_args_gen):
    output_file = Path(convert_args_gen[5])
    metadata_file = convert_args_gen[3]

    with pytest.raises(SystemExit) as pytest_wrapped_e:
        convert(convert_args_gen)

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0

    mdb_files = list(output_file.parent.glob('*.parquet'))
    print('Output parquet files: ', mdb_files)
    assert len(mdb_files) == 4

    # read from CSV
    idf = from_csv(convert_args_gen[0], 'cmems_drifter',
                   metadata_file=metadata_file)

    # read from parquet
    count = 0
    for f in mdb_files:
        (dataset_id, odf, metadata_vars, metadata_globals) = from_parquet(f)
        assert (dataset_id == 'cmems_drifter')
        assert odf.rejection_flag_3.dtype.name != 'object'
        count += odf.size
    assert(idf.data.size == count)
