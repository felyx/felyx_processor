import json
import time
from pathlib import Path

import pytest

from felyx_processor.main.get_raws import felyx_get_raws
from felyx_processor.main.import_parquet_insitu import felyx_import_parquet_insitu
from felyx_processor.sites.insitu_factory import InSituDataSourceFactory

TEST_INSITU_FILES = (
   'insitu/parquet/cmems_wave/cmems_wave_20060101.parquet',
)
COLLECTION_ES = 'test_cmems_wave'
COLLECTION_PARQUET = 'cmems_wave_parquet'


@pytest.fixture
def process_import_args(tmpdir, data_dir, felyx_processor_config_file):
    return [
        '--inputs', ','.join([str(data_dir / path) for path in TEST_INSITU_FILES]),
        '--collection', COLLECTION_ES,
        '--configuration-file', str(felyx_processor_config_file),
    ]


@pytest.fixture()
def import_parquet_insitu(felyx_sys_config_file, process_import_args):
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        felyx_import_parquet_insitu(process_import_args)
        time.sleep(10)

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0


@pytest.fixture
def process_args_es(tmpdir, felyx_processor_config_file):
    return [
        '--collection', COLLECTION_ES,
        '--sites', 'cci51301',
        '--start', '2006-01-01T00:00:00',
        '--end', '2006-02-01T00:00:00',
        '--fields', 'VHM0,VTPK',
        '--wkt', '-78,35.4 -78,30.5 -76,30.5 -76,35.4 -78,35.4',
        '--configuration-file', str(felyx_processor_config_file),
        '--output_path', str(tmpdir / 'raws_es.json')
    ]


@pytest.fixture
def process_args_parquet(tmpdir, felyx_processor_config_file):
    return [
        '--collection', COLLECTION_PARQUET,
        '--sites', 'cci51301',
        '--start', '2006-01-01T00:00:00',
        '--end', '2006-02-01T00:00:00',
        '--fields', 'VHM0,VTPK',
        '--configuration-file', str(felyx_processor_config_file),
        '--output_path', str(tmpdir / 'raws_parquet.json')
    ]


def assert_raws(json_file, collection, sites):

    assert json_file.exists()

    with open(json_file.resolve()) as file:
        data = json.load(file)
    assert len(data) > 0

    # Check collection and sites
    collections = [k for (k, v) in data.items() if k == collection]
    assert len(collections) == 1
    assert collections[0] == collection

    sites_list = [k for (k, v) in data[collections[0]].items()]
    sites_args = sites.split(',')
    assert sorted(sites_list) == sorted(sites_args)


def test_get_raws_es(felyx_sys_config, felyx_processor_config, import_parquet_insitu, process_args_es):
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        time.sleep(10)
        felyx_get_raws(process_args_es)

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0

    # check raws
    assert_raws(
        json_file=Path(process_args_es[15]),
        collection=process_args_es[1],
        sites=process_args_es[3],
    )

    # delete test raws
    collection = process_args_es[1]
    ds = InSituDataSourceFactory.get_insitu_datasource(
        felyx_sys_config=felyx_sys_config,
        felyx_processor_config=felyx_processor_config,
        collection_code=collection,
    )
    ds.delete_raws_index(collection)


def test_get_raws_parquet(process_args_parquet):
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        felyx_get_raws(process_args_parquet)

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0

    # check raws
    assert_raws(
        json_file=Path(process_args_parquet[13]),
        collection=process_args_parquet[1],
        sites=process_args_parquet[3],
    )
