# EODatasets: dictionary of the datasets for which extractions can be defined.
# The keys of this dictionary are the identifiers of each dataset, which
# should be a single string with no spaces or special characters.
EODatasets:
  SST_GLO_SST_L3S_NRT_OBSERVATIONS_010_010:
    # dataset_class: the reader class name in cerbere library to read this
    # dataset. Check cerbere at:
    # https://cerbere.gitlab-pages.ifremer.fr/cerbere/
    dataset_class: GHRSST

    # feature_class: the feature class name in cerbere library for the
    # observation pattern of this dataset. Possible values are: Swath, Grid,
    # Trajectory
    # Check cerbere at: https://cerbere.gitlab-pages.ifremer.fr/cerbere/
    feature_class: Grid

    # pattern of the full path to the EO data files wrt their time coverage
    # start. The organization of the data files into year, month, day etc...
    # subfolders must therefore be based on the time coverage start of the
    # data files. Time dependent formatting in the pattern uses python style for
    # date format. Only used when assembling MMDB files from source EO files
    # instead of pre-extracted child products.
    filenaming: tests/data/eo/l3/%Y%m%d-IFR-L3C_GHRSST-SSTsubskin-ODYSSEA-GLOB_010_adjusted-v2.0-fv1.0.nc
    # description: a summary of the dataset. Optional. Not used for processing.
    description: ODYSSEA L3S
    # level: the processing level of the dataset. Optional. Not used for
    # processing.
    level: L3S
    # name: the full name of the dataset. Optional.
    name: ODYSSEA L3S
    # supplier: the producer of the dataset. Optional. Not used for
    # processing.
    supplier: Ifremer

# SiteCollections: dictionary of the site collections for which extractions can
# be defined.
# The keys of this dictionary are the identifiers of each site collection, which
# should be a single string with no spaces or special characters.
SiteCollections:
  test4syn:
    # description: a summary of the site collection. Optional. Not used for
    # processing.
    description: 'Synthetic SST drifters'
    # name: the full name of the site collection. Optional.
    name: test4syn
    # static: true if the type of sites included in this collection is static
    # sites (fixed locations), false if it is trajectories (dynamic sites).
    static: false
    # if the sites are dynamic (static = false), the source for the site time
    # and locations (and associated data) must be defined.
    #
    # datasource_access: indicates the storage source for the sites, can be
    # either `parquet` (site data are in parquet file) or `elasticsearch7`
    # (site data are registered into Elasticsearch search engine).
    datasource_access: parquet
    # parquet_config: access information to the parquet files containing the
    # dynamic site data of the collection.
    # In parquet, the site data are split in a series of parquet files, each
    # one covering a constant and periodic time coverage.
    parquet_config:
      # filepath_pattern: path and naming of the parquet file tree archive. Use
      # python convention for date related fields. The reference date for
      # each parquet file is the start date of the temporal frame covered by
      # the file.
      filepath_pattern: tests/data/insitu/parquet/test4syn_%Y%m%d.parquet
      # frequency: the periodicity of the parquet files. Use python pandas style
      # for frequencies in DateTimeIndex.
      frequency: '1D'

# MMDatasets: dictionary of the extractions that can be called in
# felyx-extraction.
# The keys of this dictionary are the identifiers of each extraction, which
# should be a single string with no spaces or special characters.
MMDatasets:
  SST_GLO_SST_L3S_NRT_OBSERVATIONS_010_010_test4dyn:
    # site_collection: the site collection for which EO data are extracted in
    # this extraction. Must be one of the keys defined in `SiteCollections`
    # dictionary.
    site_collection_id: test4syn

    # eo_datasets: the dictionary of EO datasets which can be extracted for
    # above `site_collection`, where keys are the identifier of the datasets,
    # and values the extraction parameters for the pair (EO dataset, site
    #  collection)
    eo_datasets:
      # identifier of the EO dataset matched with the site collection. Must be
      # one of the keys defined in `EODatasets` dictionary.
      SST_GLO_SST_L3S_NRT_OBSERVATIONS_010_010:
        # subset_size: the size, in number of consecutive measurements, of the
        # child product to be extracted, centered on the matched site time and
        # location. Must be an odd number. Valid for dynamic site collections
        # only.
        subset_size: 21
        # matching_criteria: the search criteria for EO observation / dynamic
        # site match-ups
        matching_criteria:
          # time_window: the maximum time difference between the EO
          # observation and the nearest site data, in minutes.
          time_window: 120
          # search_radius: the maximum spatial distance between the EO
          # observation and the nearest site data, in km.
          search_radius: 5.

        non_empty_field:
          - adjusted_sea_surface_temperature


    # output: this block describes how to assemble the match-ups into multi
    # match-up datasets. It is used only by felyx-assemble command.
    output:
      # the temporal extent of each created match-up file
      frequency: 6h


      # history length of the dynamic site data to include with the matched
      # measurement, as a duration in minutes before and after the matching time
      history: 360

      # default time units default time units in CF convention style (default is
      # 'seconds since 1950-01-01')
      default_time_units: seconds since 1980-01-01

      # The output products that will be written to disk, where keys are the
      # identifier of each product and the values their definition.
      # In most case, there would be only one output product with all the
      # selected dataset variables and attributes. However, one can define
      # multiple products, each one having a particular selection of EO
      # datasets, variables and attributes.
      products:
        SST_GLO_SST_L3S_NRT_OBSERVATIONS_010_010_test4dyn:
          # file pattern for the output product
          filenaming: '%Y/%Y%m%d%H%M%S_SLSTRA-MAR-L2P-v1.0__test4syn.nc'
          # tailor the content of the assembled files for the output product
          content:
            SST_GLO_SST_L3S_NRT_OBSERVATIONS_010_010:
              # [Optional] list of variables to include in the assembled files
              # (all of them by default). Python regexp can be used to select
              # several variables at once.
              variables: [.*]

              # [Optional] variables NOT TO include in extracted child products
              # (none of them by default)
              #except_variables:

              # [Optional] list of global attributes to include in the assembled
              # files (all of them by default)
              #attributes:

              # [Optional] list of global attributes NOT TO include in the assembled
              # files (none of them by default)
              except_attributes: [.*]

              # [Optional] list of global attributes from the child products
              # to stack as new variables into the assembled files.
              attributes_as_variables:
                - date_created

              # [Optional] complementary hard-coded (constant) global attributes
              # to include in all output files
              custom_attributes:
                version: v1.0

              # prefix by which to rename all variables and global attributes
              # coming from this dataset (by default the dataset id is used)
              prefix: s3a
