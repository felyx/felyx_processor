import datetime
import time
from datetime import datetime

import dateutil
import numpy as np
import pytest

from felyx_processor.metrics import metrics_datasource
from felyx_processor.utils.exceptions import FelyxProcessorError


@pytest.fixture(autouse=True)
def teardown(es_metrics_datasource):
    yield
    # Remove metrics index from elasticsearch
    es_metrics_datasource.delete_metrics_index()

@pytest.fixture
def register_metrics(es_metrics_datasource):
    docs = [
            {
                'shape': 'POLYGON((-23 11.4,-23 11.5,-22.9 11.5,-22.9 11.4, -23 11.4))',
                'time_coverage': ['2021-10-05 01:00:00',
                                  '2021-10-05 23:00:00'],
                'site': 'site1',
                'dataset': 'dataset1',
                'collection': 'collection1',
                'data': [
                    {
                        'name': 'param1',
                        'value': 298.8
                    }
                ],
                'miniprod': '20211004000000_site1_dataset1.nc'
            },
            {
                'shape': 'POLYGON((-23 11.4,-23 11.5,-22.9 11.5,-22.9 11.4, -23 11.4))',
                'time_coverage': ['2021-10-05 01:00:00',
                                  '2021-10-05 23:00:00'],
                'site': 'site2',
                'dataset': 'dataset1',
                'collection': 'collection1',
                'data': [
                    {
                        'name': 'param1',
                        'value': 298.8
                    }
                ],
                'miniprod': '20211004000000_site2_dataset1.nc'
            },
            {
                'shape': 'POLYGON((-23 11.4,-23 11.5,-22.9 11.5,-22.9 11.4, -23 11.4))',
                'time_coverage': ['2021-10-15 01:00:00',
                                  '2021-10-15 23:00:00'],
                'site': 'site2',
                'dataset': 'dataset2',
                'collection': 'collection2',
                'data': [
                    {
                        'name': 'param1',
                        'value': 1111
                    }
                ],
                'miniprod': 'miniprod2'
            },
            {
                'shape': 'POLYGON((-23 11.4,-23 11.5,-22.9 11.5,-22.9 11.4, -23 11.4))',
                'time_coverage': ['2021-10-15 02:00:00',
                                  '2021-10-15 23:00:00'],
                'site': 'site2',
                'dataset': 'dataset2',
                'collection': 'collection2',
                'data': [
                    {
                        'name': 'param3',
                        'value': 3333
                    }
                ],
                'miniprod': 'miniprod2'
            },
    ]

    es_metrics_datasource.register_metrics(docs)

    # Wait the ES refresh between register and delete
    time.sleep(10)


def check_dataframe(dataframe, **kwargs):

    if len(dataframe) > 0:
        # Check the site list
        site_list = list(set(dataframe[metrics_datasource.SITE_COLUMN].values))
        if kwargs['site_codes'] is not None:
            print('SITE LIST : {}'.format(site_list))
            assert any(elem in site_list for elem in kwargs['site_codes'])

        # Check the dates
        min_date = min(dataframe[metrics_datasource.TIME_COLUMN].values.astype(
            np.int64))
        min_date = datetime.utcfromtimestamp(min_date // 10 ** 9)
        assert (min_date >= kwargs['start'])

        max_date = max(dataframe[metrics_datasource.TIME_COLUMN].values.astype(
            np.int64))
        max_date = datetime.utcfromtimestamp(max_date // 10 ** 9)
        assert (max_date <= kwargs['stop'])

        print('TIME COVERAGE : {} -> {}'.format(min_date, max_date))

    # Check the expected results
    if kwargs['expected_res'] is not None:
        if kwargs['expected_res']['nb_rows'] is not None:
            assert(len(dataframe) == kwargs['expected_res']['nb_rows'])
        else:
            assert len(dataframe) > 0


@pytest.mark.parametrize(
    'docs,expected_res',
    [
        (
            [
                {
                    'shape': 'POLYGON((-23 11.4,-23 11.5,-22.9 11.5,-22.9 11.4, -23 11.4))',
                    'time_coverage': ['2021-10-05 01:00:00',
                                      '2021-10-05 23:00:00'],
                    'site': 'site1',
                    'dataset': 'dataset3',
                    'collection': 'collection1',
                    'data': [
                        {
                            'name': 'param1',
                            'value': 298.8
                        }
                    ],
                    'miniprod': 'miniprod3'
                },
                {
                    'shape': 'POLYGON((-23 11.4,-23 11.5,-22.9 11.5,-22.9 11.4, -23 11.4))',
                    'time_coverage': ['2021-10-15 01:00:00',
                                      '2021-10-15 23:00:00'],
                    'site': 'site2',
                    'dataset': 'dataset3',
                    'collection': 'collection2',
                    'data': [
                        {
                            'name': 'param1',
                            'value': 1111
                        }
                    ],
                    'miniprod': 'miniprod3'
                },
                {
                    'shape': 'POLYGON((-23 11.4,-23 11.5,-22.9 11.5,-22.9 11.4, -23 11.4))',
                    'time_coverage': ['2021-10-15 02:00:00',
                                      '2021-10-15 23:00:00'],
                    'site': 'site2',
                    'dataset': 'dataset3',
                    'collection': 'collection2',
                    'data': [
                        {
                            'name': 'param3',
                            'value': 3333
                        }
                    ],
                    'miniprod': 'miniprod3'
                },
            ],
            {'nb_docs': 3}
        ),
    ]
)
def test_register_metrics(es_metrics_datasource, docs, expected_res):
    es_metrics_datasource.register_metrics(docs)

@pytest.mark.parametrize(
    'datasets,site_codes,start_date,end_date,metrics,expected_res',
    [
        (
            ['dataset1','dataset2'],
            None,
            dateutil.parser.parse('2021-10-01 12:00:00'),
            dateutil.parser.parse('2021-10-20 12:00:00'),
            ['param1'],
            {'nb_rows': 4}
        ),
        (
            ['dataset1'],
            ['site1'],
            dateutil.parser.parse('2006-12-21 12:00:00'),
            dateutil.parser.parse('2007-01-01 12:00:00'),
            ['param1'],
            {'nb_rows': 0}
        ),
    ]
)
def test_get_metrics(register_metrics, es_metrics_datasource,
                     datasets, site_codes,
                     start_date, end_date, metrics, expected_res):

    res = es_metrics_datasource.get_metrics(
        datasets=datasets,
        sites_codes=site_codes,
        start=start_date,
        stop=end_date,
        wkt_coords=None,
        constraints_list=None,
        metrics=metrics,
        as_dataframe=True)

    print('------------METRICS : {}'.format(datasets))
    print('dataframe len : {}'.format(len(res)))
    print('dataframe : {}'.format(res))

    check_dataframe(
        res,
        datasets=datasets,
        site_codes=site_codes,
        start=start_date,
        stop=end_date,
        expected_res=expected_res)


@pytest.mark.parametrize(
    'docs,expected',
    [
        (
            [
                (
                    {
                        'shape': 'POLYGON((-23 11.4,-23 11.5,-22.9 11.5,-22.9 11.4, -23 11.4))',
                        'time_coverage': ['2021-10-15 01:00:00',
                                          '2021-10-15 23:00:00'],
                        'site': 'site3',
                        'dataset': 'dataset3',
                        'collection': 'collection3',
                        'data': [
                            {
                                'name': 'param3',
                                'value': 333.3
                            }
                        ],
                        'miniprod': ''
                    },
                )
            ],
            FelyxProcessorError
        )
    ]
)
def test_register_metrics_error(es_metrics_datasource, docs, expected):
    with pytest.raises(expected):
        es_metrics_datasource.register_metrics(docs)

@pytest.mark.parametrize(
    'miniprod_ids,expected_res',
    [
        (
            ['20211004000000_site1_dataset1.nc',
             '20211004000000_site2_dataset1.nc'],
            {'nb_docs': 2}
        ),
    ]
)
def test_delete_metrics(register_metrics, es_metrics_datasource,
                        miniprod_ids, expected_res):
    nb_metrics = es_metrics_datasource.delete_metrics(miniprod_ids)

    print('------------METRICS DELETED : {}'.format(nb_metrics))

    # Check the expected results
    if expected_res is not None:
        if expected_res['nb_docs'] is not None:
            assert(nb_metrics == expected_res['nb_docs'])
        else:
            assert nb_metrics > 0
