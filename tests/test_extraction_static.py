from pathlib import Path
from subprocess import check_output

import pytest

from felyx_processor.main.miniprod import felyx_miniprod

CONFIG_FILE = Path('resources/test_slstr_a/felyx_data_static.yaml')

TEST_EO_FILES = [
        Path('eo/slstr-a/20180226001644-MR1-L2P_GHRSST-SSTskin-SLSTRA'
             '-20180929065321-v02.0-fv01.0.nc'),
        ]
TEST_DATASET = 'SLSTRA-MAR-L2P-v1.0'

EXTRACT_NB = 2


@pytest.fixture(scope='module')
def output_dir(tmpdir_factory):
    return tmpdir_factory.mktemp('output')


@pytest.fixture(params=TEST_EO_FILES)
def eo_file(data_dir, request):
    return data_dir / request.param


@pytest.fixture
def extraction_args(output_dir, eo_file, test_dir):

    (Path(output_dir) / Path('data')).mkdir(parents=True)
    (Path(output_dir) / Path('manifests')).mkdir(parents=True)

    yield ['-c', str(test_dir / CONFIG_FILE),
            '--dataset-id', TEST_DATASET,
            '--create-miniprod',
            '--child-product-dir', f'{output_dir}/data/',
            '--manifest-dir', f'{output_dir}/manifests/',
            '--inputs', str(eo_file)]


def test_extraction_single_swath(
        output_dir, extraction_args, eo_file):

    with pytest.raises(SystemExit) as pytest_wrapped_e:
        felyx_miniprod(extraction_args)

        assert pytest_wrapped_e.type == SystemExit
        assert pytest_wrapped_e.value.code == 0

    # check manifest was created
    manifests = (Path(output_dir) / Path(f'manifests/{TEST_DATASET}')).glob(
        '*/*/*.manifest'
    )
    match = [m for m in list(manifests) if eo_file.name in m.name]
    assert len(match) == 1

    manifest = match[0]
    assert int(check_output(['wc', '-l', manifest]).split()[0]) >= EXTRACT_NB
