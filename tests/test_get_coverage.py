import time
from datetime import datetime
from pathlib import Path

import pandas as pd
import shapely.wkt
import pytest

from felyx_processor.main.get_coverage import felyx_get_coverage
from felyx_processor.miniprod import ChildMetadata


@pytest.fixture
def register_miniprods(es_miniprod_datasource):
    es_miniprod_datasource.delete_miniprods_index()
    es_miniprod_datasource.create_miniprods_index()

    # create miniprods in ES
    children = [
        ChildMetadata(**{
            'id': 'test_coverage_ds1_1.nc',
            'site': 'test_coverage_site1',
            'site_collection': 'test_coverage_collection1',
            'source': '20211004000000-file1.nc',
            'shape': shapely.wkt.loads(
                'POLYGON((-84.83 45.76,-84.83 45.88,-84.70 '
                '45.88,-84.70 45.76,-84.83 45.76))'),
            'slices': {'time': slice(1040, 1061, None)},
            'source_center_index': {'time': 1050},
            'coverage': 100.0,
            'date_created': datetime(2021, 10, 4, 0, 0, 0),
            'date_modified': datetime(2021, 10, 4, 0, 0, 0),
            'dataset': 'test_coverage_ds1',
            'time_coverage_start': datetime(2021,10,4),
            'time_coverage_stop': datetime(2021,10,5),
            'source_time_coverage_start': datetime(2021,10,4, 1,9,15),
            'source_time_coverage_stop': datetime(2021, 10,4, 1,59, 43),
            'dynamic_target_longitude': -84.77,
            'dynamic_target_latitude': 45.82,
            'dynamic_target_time': datetime(2021, 10, 4, 12, 0, 0),
            'dynamic_target_time_difference': 12777,
            'dynamic_target_distance': 7585,
        }),
        ChildMetadata(**{
            'id': 'test_coverage_ds2_1.nc',
            'site': 'test_coverage_site1',
            'site_collection': 'test_coverage_collection1',
            'source': '20211005000000-file1.nc',
            'shape': shapely.wkt.loads(
                'POLYGON((-84.83 45.76,-84.83 45.88,-84.70 '
                '45.88,-84.70 45.76,-84.83 45.76))'),
            'slices': {'time': slice(1040, 1061, None)},
            'source_center_index': {'time': 1050},
            'coverage': 100.0,
            'date_created': datetime(2021, 10, 4, 0, 0, 0),
            'date_modified': datetime(2021, 10, 4, 0, 0, 0),
            'dataset': 'test_coverage_ds1',
            'time_coverage_start': datetime(2021,10,5),
            'time_coverage_stop': datetime(2021,10,6),
            'source_time_coverage_start': datetime(2021,10,5, 1,9,15),
            'source_time_coverage_stop': datetime(2021, 10,6, 1,59, 43),
            'dynamic_target_longitude': -84.77,
            'dynamic_target_latitude': 45.82,
            'dynamic_target_time': datetime(2021, 10, 4, 12, 0, 0),
            'dynamic_target_time_difference': 12777,
            'dynamic_target_distance': 7585,
        }),
        ChildMetadata(**{
            'id': 'test_coverage_ds2.nc',
            'site': 'test_coverage_site1',
            'site_collection': 'test_coverage_collection1',
            'source': '20220101000000-file1.nc',
            'shape': shapely.wkt.loads(
                'POLYGON((-84.83 45.76,-84.83 45.88,-84.70 '
                '45.88,-84.70 45.76,-84.83 45.76))'),
            'slices': {'time': slice(1040, 1061, None)},
            'source_center_index': {'time': 1050},
            'coverage': 100.0,
            'date_created': datetime(2022, 1, 4, 0, 0, 0),
            'date_modified': datetime(2022, 1, 5, 0, 0, 0),
            'dataset': 'test_coverage_ds2',
            'time_coverage_start': datetime(2021,10,4),
            'time_coverage_stop': datetime(2021,10,5),
            'source_time_coverage_start': datetime(2021,10,4, 1,9,15),
            'source_time_coverage_stop': datetime(2021, 10,4, 1,59, 43),
            'dynamic_target_longitude': -84.77,
            'dynamic_target_latitude': 45.82,
            'dynamic_target_time': datetime(2021, 10, 4, 12, 0, 0),
            'dynamic_target_time_difference': 12777,
            'dynamic_target_distance': 7585,
        })
    ]

    es_miniprod_datasource.register_miniprods(children)

    # Wait the ES refresh between register and get
    time.sleep(10)


@pytest.fixture
def process_args(tmpdir, felyx_processor_config_file):
    return [
        '--configuration-file', str(felyx_processor_config_file),
        '--output_path', str(tmpdir / 'coverages.json'),
    ]


@pytest.fixture
def expected_res():
    return {
        'nb_rows': 2,
        'datasets': ['test_coverage_ds1', 'test_coverage_ds2'],
        'miniprod_ids': ['test_coverage_ds1_1.nc', 'test_coverage_ds1_2.nc', 'test_coverage_ds2.nc'],
    }


def assert_coverage(json_file, **kwargs):
    assert json_file.exists()

    dataframe = pd.read_json(json_file, orient='index')
    assert len(dataframe) > 0

    ds_list = list(dataframe.index.values)
    assert any(elem in ds_list for elem in kwargs['expected_res']['datasets'])

    # Check the expected results
    result = dataframe.loc[kwargs['expected_res']['datasets']]
    assert (len(result) == kwargs['expected_res']['nb_rows'])


def test_get_coverage(register_miniprods, process_args, expected_res):
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        felyx_get_coverage(process_args)

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0

    assert_coverage(Path(process_args[3]), expected_res=expected_res)
