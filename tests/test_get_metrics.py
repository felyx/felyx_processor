import json
import time
from pathlib import Path

import pytest

from felyx_processor.main.get_metrics import felyx_get_metrics

METRICS_DATA = [
    {
        'shape': 'POLYGON((-23 11.4,-23 11.5,-22.9 11.5,-22.9 11.4, -23 11.4))',
        'time_coverage': ['2022-01-01 01:00:00',
                          '2022-01-10 23:00:00'],
        'site': 'metrics_site1',
        'dataset': 'metrics_dataset1',
        'collection': 'metrics_collection1',
        'data': [
            {
                'name': 'param1',
                'value': 111.1
            },
            {
                'name': 'param2',
                'value': 1
            }
        ],
        'miniprod': 'metrics_miniprod1'
    },
    {
        'shape': 'POLYGON((-22 11.4,-22 11.5,-22.9 11.5,-22.9 11.4, -22 11.4))',
        'time_coverage': ['2022-01-05 01:00:00',
                          '2022-01-05 23:00:00'],
        'site': 'metrics_site2',
        'dataset': 'metrics_dataset1',
        'collection': 'metrics_collection1',
        'data': [
            {
                'name': 'param1',
                'value': 222.2
            },
            {
                'name': 'param2',
                'value': 2
            }
        ],
        'miniprod': 'metrics_miniprod2'
    },
    {
        'shape': 'POLYGON((-23 12.4,-23 11.5,-22.9 11.5,-22.9 12.4, -23 12.4))',
        'time_coverage': ['2022-01-06 01:00:00',
                          '2022-01-06 23:00:00'],
        'site': 'metrics_site1',
        'dataset': 'metrics_dataset2',
        'collection': 'metrics_collection2',
        'data': [
            {
                'name': 'param1',
                'value': 333.3
            },
            {
                'name': 'param2',
                'value': 3
            }
        ],
        'miniprod': 'metrics_miniprod3'
    },
    {
        'shape': 'POLYGON((-21 11.4,-21 11.5,-22.9 11.5,-22.9 11.4, -21 11.4))',
        'time_coverage': ['2022-01-06 01:00:00',
                          '2022-01-06 23:00:00'],
        'site': 'metrics_site2',
        'dataset': 'metrics_dataset2',
        'collection': 'metrics_collection2',
        'data': [
            {
                'name': 'param1',
                'value': 444.4
            },
            {
                'name': 'param2',
                'value': 4
            }
        ],
        'miniprod': 'metrics_miniprod4'
    }
]


@pytest.fixture
def register_metrics(es_metrics_datasource):
    es_metrics_datasource.register_metrics(METRICS_DATA)
    # Wait the ES refresh between register and get
    time.sleep(10)


@pytest.fixture
def process_args(tmpdir, felyx_processor_config_file):
    return [
        '--datasets', 'metrics_dataset1,metrics_dataset2',
        '--sites', 'metrics_site1',
        '--start', '2022-01-01T00:00:00',
        '--end', '2022-02-01T00:00:00',
        '--metrics', 'param1',
        '--wkt', '-24,15.4 -24,10.5 -22.5,10.5 -22.5,15.4 -24,15.4',
        '--configuration-file', str(felyx_processor_config_file),
        '--output_path', str(tmpdir / 'metrics.json')
    ]


@pytest.fixture
def expected_res():
    return {'nb_metrics': 2}


def assert_metrics(json_file, **kwargs):
    assert json_file.exists()

    with open(json_file.resolve()) as file:
        data = json.load(file)
    assert len(data) > 0

    # Check datasets
    datasets = [x['dataset'] for x in METRICS_DATA]
    assert any(elem in data.keys() for elem in datasets)

    # Check the expected results
    nb_metrics = 0
    for dataset in data:
        nb_metrics = nb_metrics + len(data[dataset].keys())
    assert (nb_metrics == kwargs['expected_res']['nb_metrics'])


def test_get_metrics(felyx_sys_config, register_metrics, process_args, expected_res):
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        felyx_get_metrics(cli_args=process_args)

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0

    # check metrics
    print(process_args)
    assert_metrics(Path(process_args[15]), expected_res=expected_res)
