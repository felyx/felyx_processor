from pathlib import Path
from subprocess import check_output

import cerbere
import pandas as pd
import pytest
import yaml

from felyx_processor.main.matchups import assemble_matchups
from felyx_processor.main.miniprod import felyx_miniprod

CONFIG_FILE = Path('resources/test_slstr_a/felyx_data.yaml')

TEST_EO_FILES = [
        Path('eo/slstr-a/20180226001644-MR1-L2P_GHRSST-SSTskin'
             '-SLSTRA-20180929065321-v02.0-fv01.0.nc'),
        ]
TEST_DATASET = 'SLSTRA-MAR-L2P-v1.0'


def extract_dummy_sites(fname, along=300, across=750):
    """Create dummy in situ measurements over a EO granule"""
    feature = cerbere.open_feature(fname, feature='Swath', reader='GHRSST')

    lons = feature.ds.cb.longitude[::across, ::along]
    lats = feature.ds.cb.latitude[::across, ::along]
    times = feature.ds.cb.time[::across, ::along]
    sid = ['dummy{:06d}'.format(_) for _ in range(lats.size)]

    print('COUNT ', times.count())

    return pd.DataFrame(data={
        'id': sid,
        'lon': lons.to_masked_array().flatten(),
        'lat': lats.to_masked_array().flatten(),
        'time': times.to_masked_array().flatten(),
    })


@pytest.fixture(scope='module')
def output_dir(tmpdir_factory):
    return tmpdir_factory.mktemp('output')


@pytest.fixture(params=TEST_EO_FILES)
def eo_file(data_dir, request):
    return data_dir / request.param


@pytest.fixture
def insitu_parquet(output_dir, eo_file):
    parquet_fname = output_dir.join(
        'insitu', 'synthetic_insitu_{}.parquet'.format(eo_file.name))

    if parquet_fname.exists():
        df = pd.read_parquet(parquet_fname)
    else:
        output_dir.mkdir('insitu')
        df = extract_dummy_sites(eo_file)
        df.to_parquet(parquet_fname)

    return parquet_fname, df.count(axis=0)['time']


@pytest.fixture(scope='module')
def insitu_parquet_all_in_one(output_dir, data_dir):
    df = pd.concat([extract_dummy_sites(data_dir / eo_file)
                    for eo_file in TEST_EO_FILES])

    (Path(output_dir) / Path('insitu')).mkdir(parents=True, exist_ok=True)
    parquet_fname = Path(output_dir) / 'insitu/synthetic_insitu.parquet'
    df.to_parquet(parquet_fname)

    return parquet_fname, df.count(axis=0)['time']


def config_file(output_dir, test_dir, insitu):
    fname = output_dir.join('felyx_data.yaml')

    with open(test_dir / CONFIG_FILE, 'r') as ftemplate:
        config = yaml.safe_load(ftemplate)

    config['SiteCollections']['test4syn']['parquet_config'][
        'filepath_pattern'] = str(insitu)

    with open(fname, 'w') as cf:
        yaml.dump(config, cf)

    return fname


@pytest.fixture
def extraction_args(output_dir, eo_file):

    (Path(output_dir) / Path('data')).mkdir(parents=True, exist_ok=True)
    (Path(output_dir) / Path('manifests')).mkdir(parents=True, exist_ok=True)

    yield ['-c', str(output_dir.join('felyx_data.yaml')),
            '--dataset-id', TEST_DATASET,
            '--child-product-dir', f'{output_dir}/data/',
            '--manifest-dir', f'{output_dir}/manifests/',
            '--inputs', str(eo_file)]


@pytest.mark.parametrize(
    'area,area_args,expected', [
        (('-90', '-180', '90', '180'), 'cf', None),
        (('87', '-180', '90', '-177',), 'cf', 0),
        (('87', '-180', '90', '-177',), None, 0)])
def test_extraction_single_swath_w_area(
        output_dir, test_dir, extraction_args, insitu_parquet, eo_file, area,
        area_args, expected):
    """extraction of matchups with area filter"""

    # prepare in situ data
    insitu_file, count = insitu_parquet

    # create configuration file
    config_file(output_dir, test_dir, insitu_file)

    # add area restriction
    extraction_args.extend(['--area', *area])
    if area_args is not None:
        extraction_args.extend(['--area-attribute', area_args])
    print(extraction_args)

    with pytest.raises(SystemExit) as pytest_wrapped_e:
        felyx_miniprod(extraction_args)

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0

    # check manifest was created
    manifests = (Path(output_dir) / Path(f'manifests/{TEST_DATASET}')).glob(
        '*/*/*.manifest'
    )
    match = [m for m in list(manifests) if eo_file.name in m.name]
    assert len(match) == 1

    # check no matchup found (out of area)
    manifest = match[0]
    if expected is None:
        expected = count
    assert int(check_output(['wc', '-l', manifest]).split()[0]) == expected


@pytest.mark.dependency(name='test_extraction_single_swath')
def test_extraction_single_swath(
        output_dir, test_dir, extraction_args, insitu_parquet, eo_file):

    # prepare in situ data
    insitu_file, count = insitu_parquet

    # create configuration file
    config_file(output_dir, test_dir, insitu_file)

    with pytest.raises(SystemExit) as pytest_wrapped_e:
        felyx_miniprod(extraction_args)

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0

    # check manifest was created
    manifests = (Path(output_dir) / Path(f'manifests/{TEST_DATASET}')).glob(
        '*/*/*.manifest'
    )
    match = [m for m in list(manifests) if eo_file.name in m.name]
    assert len(match) == 1

    # check the number of matchups is equivalent to the number of virtual
    # dynamic sites
    manifest = match[0]
    assert int(check_output(['wc', '-l', manifest]).split()[0]) == count


@pytest.fixture
def assemble_args(output_dir):
    yield [
        '--matchup-dataset',
        'SLSTRA-MAR-L2P-v1.0_test4dyn',
        '--configuration',  str(output_dir.join('felyx_data.yaml')),
        '--output-dir', str(output_dir / 'mdb'),
        '--child-product-dir', str(Path(output_dir) / Path('data/')),
        '--manifest-dir', str(Path(output_dir) / Path('manifests/')),
        '--from-manifests', '--extract-from-source',
        '--start', '2018-02-26',
        '--end', '2018-02-27'
    ]


@pytest.mark.dependency(depends=['test_extraction_single_swath'])
def test_assemble(assemble_args, insitu_parquet_all_in_one, output_dir,
                  test_dir):

    # create configuration file
    insitu_file, count = insitu_parquet_all_in_one
    config_file(output_dir, test_dir, insitu_file)

    # MDB output folder
    (Path(output_dir) / Path('mdb')).mkdir(parents=True)

    with pytest.raises(SystemExit) as pytest_wrapped_e:
        assemble_matchups(assemble_args)

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0

    # compare nb of subsets vs nb of matchups
    count_subsets = len(list(
        (Path(output_dir) / Path('data') / TEST_DATASET).glob('*/*/*.nc')))
    print(count_subsets)
