from pathlib import Path

import numpy as np
import pytest
import xarray as xr
import yaml

from felyx_processor.extraction import get_slices
from felyx_processor.extraction.source import SourceFile
from felyx_processor.felyx_data import get_felyx_data
from felyx_processor.utils.configuration import load_felyx_processor_config

TEST_EO_FILE = Path(
    'eo/jason-2/ESACCI-SEASTATE-L2P-SWH-Jason-2-20130916T004358-fv01.nc')


DYNAMIC_SITES_INDICES = {f'site{_}': _ for _ in range(3277)}


CONFIG = """
EODatasets:
  ESACCI-SEASTATE-L2P-SWH-Jason-2:
    feature_class: Trajectory
    description: test along-track trajectory (Jason-2 pass)
    level: L2P
    name: ESACCI-SEASTATE-L2P-SWH-Jason-2
    supplier: ESA CCI Sea State

SiteCollections:
  dummy:
    description: dummy dynamic sites
    name: dummy
    static: false
    datasource_access: parquet
    parquet_config:
      filepath_pattern:

MMDatasets:
  # Multi-Match-up Datasets to be configured
  ESACCI-SEASTATE-L2P-SWH-Jason-2__dummy:
    # identifier of the in situ collection to match with
    site_collection_id: dummy
    eo_datasets:
      # EO dataset(s) to match with SiteCollection
      ESACCI-SEASTATE-L2P-SWH-Jason-2:

        # size of the extracted subset array, in number of elements for the
        # spatial axis
        subset_size: 101
        matching_criteria:
          # colocation time window, in minutes
          time_window: 10
          # colocation search radius in km
          search_radius: 100.

"""


@pytest.fixture
def config(tmpdir, insitu):
    config = yaml.safe_load(CONFIG)
    config['SiteCollections']['dummy']['parquet_config']['filepath_pattern'] = \
        str(insitu)

    output = tmpdir / 'config.yaml'
    with open(output, 'w') as cfg:
        yaml.dump(config, cfg)

    return output


@pytest.fixture
def eo_file(data_dir):
    return data_dir / TEST_EO_FILE


@pytest.fixture
def insitu(tmpdir, eo_file):
    # create in situ data in parquet
    trajectory = xr.open_dataset(eo_file)[['time', 'lat', 'lon']] \
        .to_dask_dataframe().compute()

    # create dummy colocation points along the track
    colocation_points = trajectory.iloc[
        list(DYNAMIC_SITES_INDICES.values())].reset_index(drop=True)
    colocation_points['id'] = list(DYNAMIC_SITES_INDICES.keys())
    colocation_points['time'] = colocation_points['time'] \
        + np.timedelta64(9, 'm')
    print(colocation_points)

    fname = tmpdir / f'{eo_file.name}_trajectory.parquet'
    colocation_points.to_parquet(fname, allow_truncated_timestamps=True,
                                 use_deprecated_int96_timestamps=True)

    return fname


def test_extraction_single_track(insitu, config, felyx_sys_config,
                                 monkeypatch, eo_file):

    felyx_processor_config = load_felyx_processor_config(config)
    dataset_id = 'ESACCI-SEASTATE-L2P-SWH-Jason-2'

    with SourceFile(
            felyx_processor_config,
            eo_file,
            dataset_id).load() as source_file:

        server_data = get_felyx_data(felyx_processor_config)
        site_collections = server_data.related_site_collections(
            source_file.dataset_id)

        extractions = get_slices(
            felyx_sys_config=felyx_sys_config,
            felyx_processor_config=felyx_processor_config,
            source_file=source_file,
            site_collections=site_collections,
            allowed_sites=None)

    items = list(extractions['dummy'])
    assert (len(items) == len(DYNAMIC_SITES_INDICES))

    for i, child in enumerate(items):
        assert len(child.slices) == 1

        # verify colocation point as expectd
        center = (
            child.slices['time'].start
            + (child.slices['time'].stop - child.slices['time'].start) // 2)

        assert center == DYNAMIC_SITES_INDICES[child.site]
